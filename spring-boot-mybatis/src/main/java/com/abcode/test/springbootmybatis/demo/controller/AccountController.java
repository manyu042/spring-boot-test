package com.abcode.test.springbootmybatis.demo.controller;

import com.abcode.test.springbootmybatis.demo.entity.Account;
import com.abcode.test.springbootmybatis.demo.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
@RequestMapping("/account")
@RestController
public class AccountController {
    @Autowired
    private AccountService accountService;

    @RequestMapping("/getOne/{accountId}")
    public Account getAccount(@PathVariable("accountId") Integer accountId){
        return accountService.getAccountById(accountId);
    }
}
