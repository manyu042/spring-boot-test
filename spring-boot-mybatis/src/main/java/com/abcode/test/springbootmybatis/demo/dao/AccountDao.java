package com.abcode.test.springbootmybatis.demo.dao;

import com.abcode.test.springbootmybatis.demo.entity.Account;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AccountDao {
    int deleteByPrimaryKey(Integer accountId);

    int insert(Account record);

    int insertSelective(Account record);

    Account selectByPrimaryKey(Integer accountId);

    int updateByPrimaryKeySelective(Account record);

    int updateByPrimaryKey(Account record);
}