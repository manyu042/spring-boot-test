package com.abcode.test.springbootmybatis.demo.entity;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * tb_account
 * @author 
 */
@Data
public class Account implements Serializable {
    private Integer accountId;

    /**
     * 账号
     */
    private String account;

    /**
     * 昵称
     */
    private String accountName;

    /**
     * 代理商ID
     */
    private Integer agentId;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 状态：正常，正常；删除，删除；
     */
    private String tbStatus;

    private static final long serialVersionUID = 1L;
}