package com.abcode.test.springbootmybatis.demo.service;

import com.abcode.test.springbootmybatis.demo.dao.AccountDao;
import com.abcode.test.springbootmybatis.demo.entity.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class AccountService {

    @Autowired
    private AccountDao accountDao;

    public Account getAccountById(Integer accountId) {

        //
        /*Account account = new Account();
        account.setAccount("ddddd555");
        account.setAccountName("sdfdsfsd34342");
        account.setAgentId(6);
        account.setTbStatus("正常");

        accountDao.insert(account);*/

        return accountDao.selectByPrimaryKey(accountId);
    }
}
