package com.abcode.tes.springbootdynamicdatasource.demo.service;

import com.abcode.tes.springbootdynamicdatasource.demo.entity.GameKind;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 游戏类型(GameKind)表服务接口
 *
 * @author abcode
 * @since 2021-01-26 15:56:38
 */
public interface GameKindService extends IService<GameKind> {

}