package com.abcode.tes.springbootdynamicdatasource.demo.service.impl;

import com.abcode.tes.springbootdynamicdatasource.datasource.CurDataSource;
import com.abcode.tes.springbootdynamicdatasource.demo.dao.AccountDao;
import com.abcode.tes.springbootdynamicdatasource.demo.entity.Account;
import com.abcode.tes.springbootdynamicdatasource.demo.service.AccountService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 用户表(Account)表服务实现类
 *
 * @author abcode
 * @since 2021-01-25 16:39:18
 */
@Service("accountService")
public class AccountServiceImpl extends ServiceImpl<AccountDao, Account> implements AccountService {

    @CurDataSource(name = "db1")
    @Override
    public Account getAccountById(Integer id) {
        return this.getById(id);
    }

    @CurDataSource(name = "db1")
    @Override
    public boolean insertBatch(List<Account> list) {
        return  this.saveBatch(list);
    }
}