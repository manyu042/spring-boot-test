package com.abcode.tes.springbootdynamicdatasource.demo.service.impl;

import com.abcode.tes.springbootdynamicdatasource.demo.dao.GameKindDao;
import com.abcode.tes.springbootdynamicdatasource.demo.entity.GameKind;
import com.abcode.tes.springbootdynamicdatasource.demo.service.GameKindService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 游戏类型(GameKind)表服务实现类
 *
 * @author abcode
 * @since 2021-01-26 15:56:38
 */
@Service("gameKindService")
public class GameKindServiceImpl extends ServiceImpl<GameKindDao, GameKind> implements GameKindService {

}