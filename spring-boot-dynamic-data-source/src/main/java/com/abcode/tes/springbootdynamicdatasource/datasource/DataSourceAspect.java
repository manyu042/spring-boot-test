package com.abcode.tes.springbootdynamicdatasource.datasource;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

/**
 * 多数据源，切面处理类
 *
 * @author xiaohe
 * @version V1.0.0
 */
@Slf4j
@Aspect
@Component
public class DataSourceAspect implements Ordered {

    @Pointcut("@annotation(com.abcode.tes.springbootdynamicdatasource.datasource.CurDataSource)")
    public void dataSourcePointCut() {

    }

    @Around("dataSourcePointCut()")
    public Object around(ProceedingJoinPoint point) throws Throwable {
        MethodSignature signature = (MethodSignature) point.getSignature();
        Method method = signature.getMethod();

        CurDataSource ds = method.getAnnotation(CurDataSource.class);
        if (ds == null) {
            DataSourceContextHolder.setDbType(DBTypeEnum.db2.getValue());
            log.info("设数据源为： " + DBTypeEnum.db2.getValue());
        } else {
            DataSourceContextHolder.setDbType(ds.name());
            log.info("设数据源为：  " + ds.name());
        }

        try {
            return point.proceed();
        } finally {
            log.info("清空数据源: " + DataSourceContextHolder.getDbType());
            DataSourceContextHolder.clearDbType();

        }
    }

    @Override
    public int getOrder() {
        return 1;
    }
}

