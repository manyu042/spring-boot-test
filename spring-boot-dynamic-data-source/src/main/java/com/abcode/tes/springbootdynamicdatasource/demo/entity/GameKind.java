package com.abcode.tes.springbootdynamicdatasource.demo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 游戏类型(GameKind)表实体类
 *
 * @author abcode
 * @since 2021-01-26 15:56:35
 */
@Data
@SuppressWarnings("serial")
@TableName("tb_game_kind")
public class GameKind extends Model<GameKind> {


    /**
     * 类型ID
     */
    @TableId(value = "game_kind_id", type = IdType.AUTO)
    private Integer gameKindId;

    /**
     * 类型
     */

    @TableField("kind_name")
    private String kindName;

    /**
     * 排序
     */

    @TableField("sort_id")
    private Integer sortId;

    /**
     * 是否可用
     */

    @TableField("enable")
    private Integer enable;

    /**
     * 备注
     */

    @TableField("remark")
    private String remark;

    /**
     * 创建时间
     */

    @TableField("create_time")
    private Date createTime;

    /**
     * 更新时间
     */

    @TableField("update_time")
    private Date updateTime;


    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.gameKindId;
    }
}