package com.abcode.tes.springbootdynamicdatasource.demo.dto;


import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 游戏类型(GameKind)表实体类
 *
 * @author abcode
 * @since 2021-01-26 15:56:40
 */
@Data
@SuppressWarnings("serial")
public class GameKindDTO implements Serializable {

    /**
     * 类型ID
     */
    private Integer gameKindId;
    /**
     * 类型
     */
    private String kindName;
    /**
     * 排序
     */
    private Integer sortId;
    /**
     * 是否可用
     */
    private Integer enable;
    /**
     * 备注
     */
    private String remark;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 更新时间
     */
    private Date updateTime;


}