package com.abcode.tes.springbootdynamicdatasource.demo.service;

import com.abcode.tes.springbootdynamicdatasource.demo.entity.Account;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 用户表(Account)表服务接口
 *
 * @author abcode
 * @since 2021-01-25 16:39:17
 */
public interface AccountService extends IService<Account> {

    Account getAccountById(Integer id);

    boolean insertBatch(List<Account> list);
}