package com.abcode.tes.springbootdynamicdatasource.demo.controller;


import com.abcode.tes.springbootdynamicdatasource.demo.entity.GameKind;
import com.abcode.tes.springbootdynamicdatasource.demo.service.GameKindService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.api.ApiController;
import com.baomidou.mybatisplus.extension.api.R;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 游戏类型(GameKind)表控制层
 *
 * @author abcode
 * @since 2021-01-26 15:56:39
 */
@RestController
@RequestMapping("/gameKind")
public class GameKindController extends ApiController {
    /**
     * 服务对象
     */
    @Resource
    private GameKindService gameKindService;

    /**
     * 分页查询所有数据
     *
     * @param page     分页对象
     * @param gameKind 查询实体
     * @return 所有数据
     */
    @GetMapping("/selectAll")
    public R selectAll(Page<GameKind> page, GameKind gameKind) {
        return success(this.gameKindService.page(page, new QueryWrapper<>(gameKind)));
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("/selectOne/{id}")
    public R selectOne(@PathVariable Serializable id) {
        return success(this.gameKindService.getById(id));
    }

    /**
     * 新增数据
     *
     * @param gameKind 实体对象
     * @return 新增结果
     */
    @PostMapping("/insert")
    public R insert(@RequestBody GameKind gameKind) {
        List<GameKind> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            GameKind kind = new GameKind();
            kind.setKindName("测试——2"+ i);
            kind.setSortId(i);
            kind.setEnable(0);

            list.add(kind);
        }
        boolean rst = this.gameKindService.saveBatch(list);

        return success(rst);
    }

    /**
     * 修改数据
     *
     * @param gameKind 实体对象
     * @return 修改结果
     */
    @PutMapping("/update")
    public R update(@RequestBody GameKind gameKind) {
        return success(this.gameKindService.updateById(gameKind));
    }

    /**
     * 删除数据
     *
     * @param idList 主键结合
     * @return 删除结果
     */
    @DeleteMapping("/delete")
    public R delete(@RequestParam("idList") List<Long> idList) {
        return success(this.gameKindService.removeByIds(idList));
    }
}