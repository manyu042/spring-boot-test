package com.abcode.tes.springbootdynamicdatasource.demo.dao;

import com.abcode.tes.springbootdynamicdatasource.demo.entity.GameKind;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 游戏类型(GameKind)表数据库访问层
 *
 * @author abcode
 * @since 2021-01-26 15:56:37
 */
public interface GameKindDao extends BaseMapper<GameKind> {

}