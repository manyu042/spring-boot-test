package com.abcode.tes.springbootdynamicdatasource.demo.dao;

import com.abcode.tes.springbootdynamicdatasource.demo.entity.Account;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 用户表(Account)表数据库访问层
 *
 * @author abcode
 * @since 2021-01-25 16:39:17
 */

public interface AccountDao extends BaseMapper<Account> {

}