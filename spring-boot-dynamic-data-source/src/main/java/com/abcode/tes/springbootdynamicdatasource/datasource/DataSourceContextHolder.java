package com.abcode.tes.springbootdynamicdatasource.datasource;

public class DataSourceContextHolder {
    //实际上就是开启多个线程，每个线程进行初始化一个数据源
    private static final ThreadLocal contextHolder = new ThreadLocal();

    /**
     *  设置当前线程操作的数据源
     */
    public static void setDbType(String dbType){
        contextHolder.set(dbType);
    }

    /**
     * 获取当前线程操作的数据源
     * @return
     */
    public static String getDbType(){
        return (String) contextHolder.get();
    }

    /**
     * 清空线程的数据源
     */
    public static void clearDbType(){
        contextHolder.remove();
    }
}
