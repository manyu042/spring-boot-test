package com.abcode.tes.springbootdynamicdatasource.demo.dto;


import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户表(Account)表实体类
 *
 * @author abcode
 * @since 2021-01-25 16:39:20
 */
@Data
@SuppressWarnings("serial")
public class AccountDTO implements Serializable {


    private Integer accountId;
    /**
     * 账号
     */
    private String account;
    /**
     * 昵称
     */
    private String accountName;
    /**
     * 代理商ID
     */
    private Integer agentId;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 更新时间
     */
    private Date updateTime;
    /**
     * 状态：正常，正常；删除，删除；
     */
    private String tbStatus;


}