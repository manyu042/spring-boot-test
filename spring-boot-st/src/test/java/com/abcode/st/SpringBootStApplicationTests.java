package com.abcode.st;

import com.abcode.st.domain.entity.SysTableHeader;
import com.abcode.st.domain.entity.SysTableQuery;
import com.abcode.st.domain.vo.DocVO;
import com.abcode.st.domain.vo.FlightTrendsVO;
import com.abcode.st.common.utils.DocUtil;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;
import java.lang.reflect.Field;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@SpringBootTest
class SpringBootStApplicationTests {

    @Test
    void contextLoads() {
    }

    @Test
    void buildTableHeaderSql(){
        String beanFilePath = "D:\\ab\\gitlab-pro\\temp\\spring-boot-test\\spring-boot-st\\src\\main\\java\\com\\abcode\\st\\domain\\vo\\FlightTrendsVO.java";
        DocVO docVO = DocUtil.execute(beanFilePath);
        List<DocVO.FieldVO> fieldVOTempList =  docVO.getFieldVOList();
        //过滤一些不需要的字段
        List<DocVO.FieldVO> fieldVOList =  fieldVOTempList.stream().filter(item-> (!"serialVersionUID".equals(item.getFieldName()))
                && (!"arrFlightId".equals(item.getFieldName()))
                && (!"depFlightId".equals(item.getFieldName())) ).collect(Collectors.toList());
        for (int i = 0; i < fieldVOList.size(); i++) {
            DocVO.FieldVO fieldVO = fieldVOList.get(i);
            SysTableHeader header = new SysTableHeader();
            header.setId((long) (i+1));
            header.setTableCode("flightInfo");
            header.setFieldCode(fieldVO.getFieldName());
            header.setFieldName(fieldVO.getDescribe());
            header.setFieldOrder((i+1)*100);
            header.setFieldStatus("Y");
            header.setDelFlag(false);
            header.setCreateBy("auto");
            header.setCreateTime(LocalDateTime.now());
            header.setRemark("初始化");

            String insertSql = DocUtil.insert_sql(header, "sys_table_header");
            insertSql = insertSql.replace("'false'", "0");
            insertSql += ";";
            System.out.println(insertSql);
        }

    }

    @Test
    void buildTableQuerySql(){
        String beanFilePath = "D:\\ab\\gitlab-pro\\temp\\spring-boot-test\\spring-boot-st\\src\\main\\java\\com\\abcode\\st\\domain\\vo\\FlightTrendsVO.java";
        DocVO docVO = DocUtil.execute(beanFilePath);
        List<DocVO.FieldVO> fieldVOTempList =  docVO.getFieldVOList();
        //过滤一些不需要的字段
        List<DocVO.FieldVO> fieldVOList =  fieldVOTempList.stream().filter(item-> (!"serialVersionUID".equals(item.getFieldName()))
                && (!"arrFlightId".equals(item.getFieldName()))
                && (!"depFlightId".equals(item.getFieldName())) ).collect(Collectors.toList());
        for (int i = 0; i < fieldVOList.size(); i++) {
            DocVO.FieldVO fieldVO = fieldVOList.get(i);
            SysTableQuery query = new SysTableQuery();
            query.setId((long) (i+1));
            query.setTableCode("flightInfo");
            query.setFieldCode(fieldVO.getFieldName());
            query.setFieldName(fieldVO.getDescribe());
            query.setFieldOrder((i+1)*100);
            query.setFieldStatus("Y");
            query.setDelFlag(false);
            query.setCreateBy("auto");
            query.setCreateTime(LocalDateTime.now());
            query.setRemark("初始化");
            query.setFieldType(fieldVO.getFieldType());

            String insertSql = DocUtil.insert_sql(query, "sys_table_query");
            insertSql = insertSql.replace("'false'", "0");
            insertSql += ";";
            System.out.println(insertSql);
        }

    }

    @Test
    void getFieldDoc(){
        //String beanFilePath = "D:\\ab\\gitlab-pro\\temp\\spring-boot-test\\spring-boot-st\\src\\test\\java\\com\\abcode\\st\\pojo\\FlightTrendsVO";
        String beanFilePath = "D:\\ab\\gitlab-pro\\temp\\spring-boot-test\\spring-boot-st\\src\\main\\java\\com\\abcode\\st\\domain\\vo\\FlightTrendsVO.java";
        //String beanFilePath = "\\spring-boot-st\\src\\test\\java\\com\\abcode\\st\\pojo\\FlightTrendsVO";
        DocVO docVO = DocUtil.execute(beanFilePath);
        List<DocVO.FieldVO> fieldVOList =  docVO.getFieldVOList();
        List<DocVO.FieldVO> resultList =  fieldVOList.stream().filter(item-> (!"serialVersionUID".equals(item.getFieldName()))
                && (!"arrFlightId".equals(item.getFieldName()))
                && (!"depFlightId".equals(item.getFieldName())) ).collect(Collectors.toList());
        resultList.forEach(System.out::println);
    }

    @Test
    void getClassFilePath(){
        Class class1 = FlightTrendsVO.class;
        String javaPath=new File("").getAbsolutePath()+"\\src\\"+class1.getPackage().getName().replace(".", "\\");
        System.out.println("该Java文件所在文件夹路径为："+javaPath);
        String className=new File("").getAbsolutePath()+"\\src\\"+class1.getName().replace(".", "\\")+".java";
        System.out.println("该Java文件路径为："+className);
    }
}
