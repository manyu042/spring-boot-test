package com.abcode.st.controller;

import com.abcode.st.domain.param.DemoUserParam;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@ContextConfiguration(classes = {DemoUserController.class})
@ExtendWith(SpringExtension.class)
class DemoUserControllerTest {
    @Autowired
    private DemoUserController demoUserController;

    /**
     * Method under test: {@link DemoUserController#addUser(DemoUserParam)}
     */
    @Test
    @Disabled("TODO: Complete this test")
    void testAddUser() throws Exception {
        // TODO: Complete this test.
        //   Reason: R013 No inputs found that don't throw a trivial exception.
        //   Diffblue Cover tried to run the arrange/act section, but the method under
        //   test threw
        //   com.fasterxml.jackson.databind.exc.InvalidDefinitionException: Java 8 date/time type `java.time.LocalDateTime` not supported by default: add Module "com.fasterxml.jackson.datatype:jackson-datatype-jsr310" to enable handling (through reference chain: com.abcode.st.domain.param.DemoUserParam["birthday"])
        //       at com.fasterxml.jackson.databind.exc.InvalidDefinitionException.from(InvalidDefinitionException.java:77)
        //       at com.fasterxml.jackson.databind.SerializerProvider.reportBadDefinition(SerializerProvider.java:1300)
        //       at com.fasterxml.jackson.databind.ser.impl.UnsupportedTypeSerializer.serialize(UnsupportedTypeSerializer.java:35)
        //       at com.fasterxml.jackson.databind.ser.BeanPropertyWriter.serializeAsField(BeanPropertyWriter.java:728)
        //       at com.fasterxml.jackson.databind.ser.std.BeanSerializerBase.serializeFields(BeanSerializerBase.java:774)
        //       at com.fasterxml.jackson.databind.ser.BeanSerializer.serialize(BeanSerializer.java:178)
        //       at com.fasterxml.jackson.databind.ser.DefaultSerializerProvider._serialize(DefaultSerializerProvider.java:480)
        //       at com.fasterxml.jackson.databind.ser.DefaultSerializerProvider.serializeValue(DefaultSerializerProvider.java:319)
        //       at com.fasterxml.jackson.databind.ObjectMapper._writeValueAndClose(ObjectMapper.java:4568)
        //       at com.fasterxml.jackson.databind.ObjectMapper.writeValueAsString(ObjectMapper.java:3821)
        //   See https://diff.blue/R013 to resolve this issue.

        MockHttpServletRequestBuilder contentTypeResult = MockMvcRequestBuilders.post("/user/addUser")
                .contentType(MediaType.APPLICATION_JSON);

        DemoUserParam demoUserParam = new DemoUserParam();
        demoUserParam.setAccount("3");
        demoUserParam.setAge(1);
        demoUserParam.setBalance(BigDecimal.valueOf(1L));
        //demoUserParam.setBirthday(LocalDateTime.of(1, 1, 1, 1, 1));
        demoUserParam.setEmail("jane.doe@example.org");
        demoUserParam.setGender("Gender");
        demoUserParam.setHeight(10.01f);
        demoUserParam.setIdentityCard("Identity Card");
        demoUserParam.setLastLoginIp("Last Login Ip");
        demoUserParam.setName("Name");
        demoUserParam.setPhone("6625550144");
        demoUserParam.setPwd("Pwd");
        demoUserParam.setSno(1L);
        demoUserParam.setUserId(1);
        demoUserParam.setWeight(10.0f);
        MockHttpServletRequestBuilder requestBuilder = contentTypeResult
                .content((new ObjectMapper()).writeValueAsString(demoUserParam));
        MockMvcBuilders.standaloneSetup(demoUserController).build().perform(requestBuilder);
    }

    /**
     * Method under test: {@link DemoUserController#addUser(DemoUserParam)}
     */
    @Test
    void testAddUser2() throws Exception {
        DemoUserParam demoUserParam = new DemoUserParam();
        demoUserParam.setAccount("3");
        demoUserParam.setAge(1);
        demoUserParam.setBalance(BigDecimal.valueOf(1L));
        demoUserParam.setBirthday(null);
        demoUserParam.setEmail("jane.doe@example.org");
        demoUserParam.setGender("Gender");
        demoUserParam.setHeight(10.0f);
        demoUserParam.setIdentityCard("Identity Card");
        demoUserParam.setLastLoginIp("Last Login Ip");
        demoUserParam.setName("Name");
        demoUserParam.setPhone("6625550144");
        demoUserParam.setPwd("Pwd");
        demoUserParam.setSno(1L);
        demoUserParam.setUserId(1);
        demoUserParam.setWeight(10.0f);
        String content = (new ObjectMapper()).writeValueAsString(demoUserParam);
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/user/addUser")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content);
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(demoUserController)
                .build()
                .perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().is(400));
    }

    /**
     * Method under test: {@link DemoUserController#addUser(DemoUserParam)}
     */
    @Test
    void testAddUser3() throws Exception {
        DemoUserParam demoUserParam = new DemoUserParam();
        demoUserParam.setAccount("3");
        demoUserParam.setAge(1);
        demoUserParam.setBalance(BigDecimal.valueOf(1L));
        demoUserParam.setBirthday(null);
        demoUserParam.setEmail("jane.doe@example.org");
        demoUserParam.setGender("Gender");
        demoUserParam.setHeight(10.0f);
        demoUserParam.setIdentityCard("Identity Card");
        demoUserParam.setLastLoginIp("Last Login Ip");
        demoUserParam.setName("Name");
        demoUserParam.setPhone("6625550144");
        demoUserParam.setPwd("Pwd");
        demoUserParam.setSno(1L);
        demoUserParam.setUserId(1);
        demoUserParam.setWeight(10.0f);
        String content = (new ObjectMapper()).writeValueAsString(demoUserParam);
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/user/addUser", "Uri Vars")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content);
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(demoUserController)
                .build()
                .perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().is(400));
    }

    /**
     * Method under test: {@link DemoUserController#addUser(DemoUserParam)}
     */
    @Test
    @Disabled("TODO: Complete this test")
    void testAddUser4() throws Exception {
        // TODO: Complete this test.
        //   Reason: R013 No inputs found that don't throw a trivial exception.
        //   Diffblue Cover tried to run the arrange/act section, but the method under
        //   test threw
        //   com.fasterxml.jackson.databind.exc.InvalidDefinitionException: Java 8 date/time type `java.time.LocalDateTime` not supported by default: add Module "com.fasterxml.jackson.datatype:jackson-datatype-jsr310" to enable handling (through reference chain: com.abcode.st.domain.param.DemoUserParam["birthday"])
        //       at com.fasterxml.jackson.databind.exc.InvalidDefinitionException.from(InvalidDefinitionException.java:77)
        //       at com.fasterxml.jackson.databind.SerializerProvider.reportBadDefinition(SerializerProvider.java:1300)
        //       at com.fasterxml.jackson.databind.ser.impl.UnsupportedTypeSerializer.serialize(UnsupportedTypeSerializer.java:35)
        //       at com.fasterxml.jackson.databind.ser.BeanPropertyWriter.serializeAsField(BeanPropertyWriter.java:728)
        //       at com.fasterxml.jackson.databind.ser.std.BeanSerializerBase.serializeFields(BeanSerializerBase.java:774)
        //       at com.fasterxml.jackson.databind.ser.BeanSerializer.serialize(BeanSerializer.java:178)
        //       at com.fasterxml.jackson.databind.ser.DefaultSerializerProvider._serialize(DefaultSerializerProvider.java:480)
        //       at com.fasterxml.jackson.databind.ser.DefaultSerializerProvider.serializeValue(DefaultSerializerProvider.java:319)
        //       at com.fasterxml.jackson.databind.ObjectMapper._writeValueAndClose(ObjectMapper.java:4568)
        //       at com.fasterxml.jackson.databind.ObjectMapper.writeValueAsString(ObjectMapper.java:3821)
        //   See https://diff.blue/R013 to resolve this issue.

        MockHttpServletRequestBuilder contentTypeResult = MockMvcRequestBuilders.post("/user/addUser")
                .contentType(MediaType.APPLICATION_JSON);

        DemoUserParam demoUserParam = new DemoUserParam();
        demoUserParam.setAccount("3");
        demoUserParam.setAge(1);
        demoUserParam.setBalance(BigDecimal.valueOf(1L));
        demoUserParam.setBirthday(LocalDateTime.of(1, 1, 1, 1, 1));
        demoUserParam.setEmail("jane.doe@example.org");
        demoUserParam.setGender("Gender");
        demoUserParam.setHeight(10.0f);
        demoUserParam.setIdentityCard("Identity Card");
        demoUserParam.setLastLoginIp("Last Login Ip");
        demoUserParam.setName("Name");
        demoUserParam.setPhone("6625550144");
        demoUserParam.setPwd("Pwd");
        demoUserParam.setSno(1L);
        demoUserParam.setUserId(1);
        demoUserParam.setWeight(10.0f);
        MockHttpServletRequestBuilder requestBuilder = contentTypeResult
                .content((new ObjectMapper()).writeValueAsString(demoUserParam));
        MockMvcBuilders.standaloneSetup(demoUserController).build().perform(requestBuilder);
    }

    /**
     * Method under test: {@link DemoUserController#getUser(String, String, LocalDateTime)}
     */
    @Test
    void testGetUser() throws Exception {
        MockHttpServletRequestBuilder paramResult = MockMvcRequestBuilders.get("/user/getUser").param("account", "foo");
        MockHttpServletRequestBuilder requestBuilder = paramResult.param("birthday", String.valueOf((Object) null))
                .param("name", "foo");
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(demoUserController)
                .build()
                .perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().is(400));
    }

    /**
     * Method under test: {@link DemoUserController#getUser(String, String, LocalDateTime)}
     */
    @Test
    void testGetUser2() throws Exception {
        MockHttpServletRequestBuilder paramResult = MockMvcRequestBuilders.get("/user/getUser").param("account", "foo");
        MockHttpServletRequestBuilder requestBuilder = paramResult.param("birthday", String.valueOf(""))
                .param("name", "foo");
        MockMvcBuilders.standaloneSetup(demoUserController)
                .build()
                .perform(requestBuilder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("text/plain;charset=ISO-8859-1"))
                .andExpect(MockMvcResultMatchers.content().string("hello foo-foo birthday: null"));
    }

    /**
     * Method under test: {@link DemoUserController#queryUser(DemoUserParam)}
     */
    @Test
    @Disabled("TODO: Complete this test")
    void testQueryUser() throws Exception {
        // TODO: Complete this test.
        //   Reason: R013 No inputs found that don't throw a trivial exception.
        //   Diffblue Cover tried to run the arrange/act section, but the method under
        //   test threw
        //   com.fasterxml.jackson.databind.exc.InvalidDefinitionException: Java 8 date/time type `java.time.LocalDateTime` not supported by default: add Module "com.fasterxml.jackson.datatype:jackson-datatype-jsr310" to enable handling (through reference chain: com.abcode.st.domain.param.DemoUserParam["birthday"])
        //       at com.fasterxml.jackson.databind.exc.InvalidDefinitionException.from(InvalidDefinitionException.java:77)
        //       at com.fasterxml.jackson.databind.SerializerProvider.reportBadDefinition(SerializerProvider.java:1300)
        //       at com.fasterxml.jackson.databind.ser.impl.UnsupportedTypeSerializer.serialize(UnsupportedTypeSerializer.java:35)
        //       at com.fasterxml.jackson.databind.ser.BeanPropertyWriter.serializeAsField(BeanPropertyWriter.java:728)
        //       at com.fasterxml.jackson.databind.ser.std.BeanSerializerBase.serializeFields(BeanSerializerBase.java:774)
        //       at com.fasterxml.jackson.databind.ser.BeanSerializer.serialize(BeanSerializer.java:178)
        //       at com.fasterxml.jackson.databind.ser.DefaultSerializerProvider._serialize(DefaultSerializerProvider.java:480)
        //       at com.fasterxml.jackson.databind.ser.DefaultSerializerProvider.serializeValue(DefaultSerializerProvider.java:319)
        //       at com.fasterxml.jackson.databind.ObjectMapper._writeValueAndClose(ObjectMapper.java:4568)
        //       at com.fasterxml.jackson.databind.ObjectMapper.writeValueAsString(ObjectMapper.java:3821)
        //   See https://diff.blue/R013 to resolve this issue.

        MockHttpServletRequestBuilder contentTypeResult = MockMvcRequestBuilders.post("/user/queryUser")
                .contentType(MediaType.APPLICATION_JSON);

        DemoUserParam demoUserParam = new DemoUserParam();
        demoUserParam.setAccount("3");
        demoUserParam.setAge(1);
        demoUserParam.setBalance(BigDecimal.valueOf(1L));
        demoUserParam.setBirthday(LocalDateTime.of(1, 1, 1, 1, 1));
        demoUserParam.setEmail("jane.doe@example.org");
        demoUserParam.setGender("Gender");
        demoUserParam.setHeight(10.0f);
        demoUserParam.setIdentityCard("Identity Card");
        demoUserParam.setLastLoginIp("Last Login Ip");
        demoUserParam.setName("Name");
        demoUserParam.setPhone("6625550144");
        demoUserParam.setPwd("Pwd");
        demoUserParam.setSno(1L);
        demoUserParam.setUserId(1);
        demoUserParam.setWeight(10.0f);
        MockHttpServletRequestBuilder requestBuilder = contentTypeResult
                .content((new ObjectMapper()).writeValueAsString(demoUserParam));
        MockMvcBuilders.standaloneSetup(demoUserController).build().perform(requestBuilder);
    }

    /**
     * Method under test: {@link DemoUserController#queryUser(DemoUserParam)}
     */
    @Test
    void testQueryUser2() throws Exception {
        DemoUserParam demoUserParam = new DemoUserParam();
        demoUserParam.setAccount("3");
        demoUserParam.setAge(1);
        demoUserParam.setBalance(BigDecimal.valueOf(1L));
        demoUserParam.setBirthday(null);
        demoUserParam.setEmail("jane.doe@example.org");
        demoUserParam.setGender("Gender");
        demoUserParam.setHeight(10.0f);
        demoUserParam.setIdentityCard("Identity Card");
        demoUserParam.setLastLoginIp("Last Login Ip");
        demoUserParam.setName("Name");
        demoUserParam.setPhone("6625550144");
        demoUserParam.setPwd("Pwd");
        demoUserParam.setSno(1L);
        demoUserParam.setUserId(1);
        demoUserParam.setWeight(10.0f);
        String content = (new ObjectMapper()).writeValueAsString(demoUserParam);
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/user/queryUser")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content);
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(demoUserController)
                .build()
                .perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().is(400));
    }

    /**
     * Method under test: {@link DemoUserController#updateUser(DemoUserParam)}
     */
    @Test
    @Disabled("TODO: Complete this test")
    void testUpdateUser() throws Exception {
        // TODO: Complete this test.
        //   Reason: R013 No inputs found that don't throw a trivial exception.
        //   Diffblue Cover tried to run the arrange/act section, but the method under
        //   test threw
        //   com.fasterxml.jackson.databind.exc.InvalidDefinitionException: Java 8 date/time type `java.time.LocalDateTime` not supported by default: add Module "com.fasterxml.jackson.datatype:jackson-datatype-jsr310" to enable handling (through reference chain: com.abcode.st.domain.param.DemoUserParam["birthday"])
        //       at com.fasterxml.jackson.databind.exc.InvalidDefinitionException.from(InvalidDefinitionException.java:77)
        //       at com.fasterxml.jackson.databind.SerializerProvider.reportBadDefinition(SerializerProvider.java:1300)
        //       at com.fasterxml.jackson.databind.ser.impl.UnsupportedTypeSerializer.serialize(UnsupportedTypeSerializer.java:35)
        //       at com.fasterxml.jackson.databind.ser.BeanPropertyWriter.serializeAsField(BeanPropertyWriter.java:728)
        //       at com.fasterxml.jackson.databind.ser.std.BeanSerializerBase.serializeFields(BeanSerializerBase.java:774)
        //       at com.fasterxml.jackson.databind.ser.BeanSerializer.serialize(BeanSerializer.java:178)
        //       at com.fasterxml.jackson.databind.ser.DefaultSerializerProvider._serialize(DefaultSerializerProvider.java:480)
        //       at com.fasterxml.jackson.databind.ser.DefaultSerializerProvider.serializeValue(DefaultSerializerProvider.java:319)
        //       at com.fasterxml.jackson.databind.ObjectMapper._writeValueAndClose(ObjectMapper.java:4568)
        //       at com.fasterxml.jackson.databind.ObjectMapper.writeValueAsString(ObjectMapper.java:3821)
        //   See https://diff.blue/R013 to resolve this issue.

        MockHttpServletRequestBuilder contentTypeResult = MockMvcRequestBuilders.post("/user/updateUser")
                .contentType(MediaType.APPLICATION_JSON);

        DemoUserParam demoUserParam = new DemoUserParam();
        demoUserParam.setAccount("3");
        demoUserParam.setAge(1);
        demoUserParam.setBalance(BigDecimal.valueOf(1L));
        demoUserParam.setBirthday(LocalDateTime.of(1, 1, 1, 1, 1));
        demoUserParam.setEmail("jane.doe@example.org");
        demoUserParam.setGender("Gender");
        demoUserParam.setHeight(10.0f);
        demoUserParam.setIdentityCard("Identity Card");
        demoUserParam.setLastLoginIp("Last Login Ip");
        demoUserParam.setName("Name");
        demoUserParam.setPhone("6625550144");
        demoUserParam.setPwd("Pwd");
        demoUserParam.setSno(1L);
        demoUserParam.setUserId(1);
        demoUserParam.setWeight(10.0f);
        MockHttpServletRequestBuilder requestBuilder = contentTypeResult
                .content((new ObjectMapper()).writeValueAsString(demoUserParam));
        MockMvcBuilders.standaloneSetup(demoUserController).build().perform(requestBuilder);
    }

    /**
     * Method under test: {@link DemoUserController#updateUser(DemoUserParam)}
     */
    @Test
    void testUpdateUser2() throws Exception {
        DemoUserParam demoUserParam = new DemoUserParam();
        demoUserParam.setAccount("3");
        demoUserParam.setAge(1);
        demoUserParam.setBalance(BigDecimal.valueOf(1L));
        demoUserParam.setBirthday(null);
        demoUserParam.setEmail("jane.doe@example.org");
        demoUserParam.setGender("Gender");
        demoUserParam.setHeight(10.0f);
        demoUserParam.setIdentityCard("Identity Card");
        demoUserParam.setLastLoginIp("Last Login Ip");
        demoUserParam.setName("Name");
        demoUserParam.setPhone("6625550144");
        demoUserParam.setPwd("Pwd");
        demoUserParam.setSno(1L);
        demoUserParam.setUserId(1);
        demoUserParam.setWeight(10.0f);
        String content = (new ObjectMapper()).writeValueAsString(demoUserParam);
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/user/updateUser")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content);
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(demoUserController)
                .build()
                .perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().is(400));
    }
}

