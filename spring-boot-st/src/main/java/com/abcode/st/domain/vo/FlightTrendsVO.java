package com.abcode.st.domain.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;


/**
 * <p>航班动态</p>
 * @author huangkaisheng
 * @date 2023-02-27 15:48
 */

@ApiModel("航班动态")
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class FlightTrendsVO {

    private static final long serialVersionUID = 1L;

    /** 进港航班id */
    private String arrFlightId;
    /** 出港航班id */
    private String depFlightId;
    /** 航空公司二字码 */
    private String airlinesIata;
    /** 进港航班号 */
    private String arrFlightNo;
    /** 出港航班号 */
    private String depFlightNo;
    /** 进出标志 */
    private String offInFlag;
    /** VIP标志 */
    private String vipFlag;
    /** 航站楼 */
    private String terminal;
    /** 任务 */
    private String task;
    /** 属性 */
    private String attribute;
    /** 机号 */
    private String reg;
    /** 代理 */
    private String agency;
    /** 机位 */
    private String stand;
    /** 机型 */
    private String craftType;
    /** 航线 */
    private String airlineFull;
    /** 前起 */
    private String previousTakeoffTime;
    /** 到下站 */
    private String nextTerminalTime;
    /** 计划落地 */
    private String sta;
    /** 预计落地 */
    private String eta;
    /** 实际落地 */
    private String ata;
    /** 计划起飞 */
    private String std;
    /** 预计起飞 */
    private String etd;
    /** 实际起飞 */
    private String atd;
    /** 起飞时间 */
    private String depTime;
    /** 落地时间 */
    private String arrTime;
    /** 跑道 */
    private String runway;
    /** 进港状态（含异常） */
    private String arrStatusContainAbnl;
    /** 进港状态 */
    private String arrStatus;
    /** 进港异常状态 */
    private String arrAbnormalStatus;
    /** 进港异常原因 */
    private String arrAbnormalReason;
    /** 出港状态（含异常） */
    private String depStatusContainAbnl;
    /** 出港状态 */
    private String depStatus;
    /** 出港异常状态 */
    private String depAbnormalStatus;
    /** 出港异常原因 */
    private String depAbnormalReason;
    /** 是否虚拟 */
    private String virtualFlag;
    /** 值机柜台 */
    private String checkinCounterArea;
    /** 登机口 */
    private String boardingGate;
    /** 装卸转盘 */
    private String loadAndUnloadTurntable;
    /** 行李转盘 */
    private String luggageCarousel;
    /** 到达出口 */
    private String arrivalPort;
    /** 进港备降 */
    private String arrDivertStationCode;
    /** 出港备降 */
    private String depDivertStationCode;
    /** 进港执行日期 */
    private String arrExecDate;
    /** 出港执行日期 */
    private String depExecDate;
    /** 允许登机 */
    private String allowBoardingTime;
    /** 登机 */
    private String boardingTime;
    /** 过站登机 */
    private String transitBoardingTime;
    /** 催促登机 */
    private String urgeBoardingTime;
    /** 登机结束 */
    private String endBoardingTime;
    /** 国际段登机 */
    private String intlBoardingTime;
    /** 国际段过站登机 */
    private String intlTransitBoardingTime;
    /** 国际段催促登机 */
    private String intlUrgeBoardingTime;
    /** 国际段登机结束 */
    private String intlEndBoardingTime;
    /** 计划开始值机 */
    private String scheduledCheckinStart;
    /** 计划值机截止 */
    private String scheduledCheckinEnd;
    /** 实际开始值机 */
    private String actualCheckinStart;
    /** 实际值机截止 */
    private String actualCheckinEnd;
    /** 进港共享航班 */
    private String arrShareFlightNo;
    /** 出港共享航班 */
    private String depShareFlightNo;









}