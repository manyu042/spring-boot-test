package com.abcode.st.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * <p>系统默认表的表头信息</p>
 * @author qinzhitao
 * @date 2023-02-23 13:38
 */

@ApiModel("系统默认表的表头信息")
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class SysTableHeader {

private static final long serialVersionUID = 1L;
    /**
     * 主键ID
     * 
     */
    @ApiModelProperty("主键ID")
    private Long id;
    /**
     * 表代码
     * 
     */
    @ApiModelProperty("表代码")
    private String tableCode;
    /**
     * 字段代码
     * 
     */
    @ApiModelProperty("字段代码")
    private String fieldCode;
    /**
     * 字段名称
     * 
     */
    @ApiModelProperty("字段名称")
    private String fieldName;
    /**
     * 字段排序
     * 
     */
    @ApiModelProperty("字段排序")
    private Integer fieldOrder;
    /**
     * 字段状态
     * 
     */
    @ApiModelProperty("字段状态")
    private String fieldStatus;
    /**
     * 是否删除
     * 
     */
    @ApiModelProperty("是否删除")
    private Boolean delFlag;
    /**
     * 创建人
     * 
     */
    @ApiModelProperty("创建人")
    private String createBy;
    /**
     * 创建时间
     * 
     */
    @ApiModelProperty("创建时间")
    private LocalDateTime createTime;
    /**
     * 更新人
     * 
     */
    @ApiModelProperty("更新人")
    private String updateBy;
    /**
     * 更新时间
     * 
     */
    @ApiModelProperty("更新时间")
    private LocalDateTime updateTime;
    /**
     * 备注;业务场景
     * 
     */
    @ApiModelProperty("备注;业务场景")
    private String remark;
}