package com.abcode.st.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * <p>高级查询表字段信息表</p>
 * @author qinzhitao
 * @date 2023-03-01 16:27
 */

@ApiModel("高级查询表字段信息表")
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class SysTableQuery {

private static final long serialVersionUID = 1L;
    /**
     * 主键ID
     * 
     */
    @ApiModelProperty("主键ID")
    private Long id;
    /**
     * 创建人
     * 
     */
    @ApiModelProperty("创建人")
    private String createBy;
    /**
     * 创建时间
     * 
     */
    @ApiModelProperty("创建时间")
    private LocalDateTime createTime;
    /**
     * 是否删除
     * 
     */
    @ApiModelProperty("是否删除")
    private Boolean delFlag;
    /**
     * 字段代码
     * 
     */
    @ApiModelProperty("字段代码")
    private String fieldCode;
    /**
     * 字段名称
     * 
     */
    @ApiModelProperty("字段名称")
    private String fieldName;
    /**
     * 字段排序
     * 
     */
    @ApiModelProperty("字段排序")
    private Integer fieldOrder;
    /**
     * 字段状态
     * 
     */
    @ApiModelProperty("字段状态")
    private String fieldStatus;
    /**
     * 字段数据类型
     * 
     */
    @ApiModelProperty("字段数据类型")
    private String fieldType;
    /**
     * 备注;业务场景
     * 
     */
    @ApiModelProperty("备注;业务场景")
    private String remark;
    /**
     * 表代码
     * 
     */
    @ApiModelProperty("表代码")
    private String tableCode;
    /**
     * 更新人
     * 
     */
    @ApiModelProperty("更新人")
    private String updateBy;
    /**
     * 更新时间
     * 
     */
    @ApiModelProperty("更新时间")
    private LocalDateTime updateTime;
}