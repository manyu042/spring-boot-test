package com.abcode.st.domain.vo;

import com.abcode.st.common.validate.GroupValidator;
import lombok.Data;
import org.hibernate.validator.constraints.Range;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.*;
import javax.validation.groups.Default;
import java.util.Date;

@Data
public class UserVo {

	// 默认分组是 Default.class。
	
	/**
	 * 用户新增校验分组（用于新增用户时校验的分组）
	 */
	public interface AddUser extends Default{
		
	}
	
	/**
	 * 用户更新校验分组（用于更新用户时校验的分组）
	 */
	public interface UpdateUser extends Default{
		
	}

	@NotNull(message = "用户id不能为空",  groups = {GroupValidator.Add.class, GroupValidator.Update.class})
	private Long id;
	
	@NotBlank(message = "用户姓名不能为空", groups = {GroupValidator.Update.class})
	private String name;

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Future(message = "只能是将来的日期")
	private Date date;
	
	@DecimalMax(value = "10000.0")
	@DecimalMin(value = "1.0")
	private Double doubleValue = null;
	
	@Max(value = 100, message = "最大值")
	@Min(value = 1, message = "最小值")
	private Integer integer;
	
	@Range(min = 1, max = 100, message = "范围")
	private Long range;
	
	@Email(message = "邮箱格式错误")
	private String email;
	
	@Size(min = 2, max = 10, message = "字符串长度在2~10")
	private String size;
	
}

