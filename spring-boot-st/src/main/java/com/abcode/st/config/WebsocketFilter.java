package com.abcode.st.config;

//import com.abcode.test.springbootmybatisplus.common.api.CommonAPI;
//import com.abcode.test.springbootmybatisplus.common.util.RedisUtil;
//import com.abcode.test.springbootmybatisplus.common.util.SpringContextUtils;
//import com.abcode.test.springbootmybatisplus.common.util.TokenUtils;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * websocket 前端将token放到子协议里传入 与后端建立连接时需要用到http协议，此处用于校验token的有效性
 * @Author taoYan
 * @Date 2022/4/21 17:01
 **/
@Slf4j
public class WebsocketFilter implements Filter {

    private static final String TOKEN_KEY = "Sec-WebSocket-Protocol";

   // private static CommonAPI commonApi;

  //  private static RedisUtil redisUtil;

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
//        if (commonApi == null) {
//            commonApi = SpringContextUtils.getBean(CommonAPI.class);
//        }
//        if (redisUtil == null) {
//            redisUtil = SpringContextUtils.getBean(RedisUtil.class);
//        }
        HttpServletRequest request = (HttpServletRequest)servletRequest;
        String token = request.getHeader(TOKEN_KEY);

        log.info("websocket连接 Token安全校验，Path = {}，token:{}", request.getRequestURI(), token);

        try {
            System.out.println("check token");
          //  TokenUtils.verifyToken(token, commonApi, redisUtil);
        } catch (Exception exception) {
            log.error("websocket连接校验失败，{}，token:{}", exception.getMessage(), token);
            return;
        }
        HttpServletResponse response = (HttpServletResponse)servletResponse;
        response.setHeader(TOKEN_KEY, token);
        filterChain.doFilter(servletRequest, servletResponse);
    }

}
