package com.abcode.st.common.utils;

import com.abcode.st.domain.vo.DocVO;
/*import com.sun.javadoc.ClassDoc;
import com.sun.javadoc.FieldDoc;
import com.sun.javadoc.RootDoc;
import com.sun.tools.javadoc.Main;
import org.springframework.format.annotation.DateTimeFormat;*/

import java.lang.reflect.Field;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Description:
 *
 * @author jack
 * @date 2021/7/13 5:10 下午
 */
public class DocUtil {

    /**
     * 会自动注入
     */
   // private static RootDoc rootDoc;

    /**
     * 会自动调用这个方法
     *
     * @param root root
     * @return true
     */
   /* public static boolean start(RootDoc root) {
        rootDoc = root;
        return true;
    }*/

    /**
     * 生成文档
     *
     * @param beanFilePath 注意这里是.java文件绝对路径
     * @return 文档注释
     */
    public static DocVO execute(String beanFilePath) {
        /*Main.execute(new String[]{"-doclet", DocUtil.class.getName(), "-docletpath",
                DocUtil.class.getResource("/").getPath(), "-encoding", "utf-8", beanFilePath});

        ClassDoc[] classes = rootDoc.classes();

        if (classes == null || classes.length == 0) {
            return null;
        }
        ClassDoc classDoc = classes[0];
        // 获取属性名称和注释
        FieldDoc[] fields = classDoc.fields(false);

        List<DocVO.FieldVO> fieldVOList = new ArrayList<>(fields.length);

        for (FieldDoc field : fields) {
            fieldVOList.add(new DocVO.FieldVO(field.name(), field.type().typeName(), field.commentText()));
        }
        return new DocVO(fieldVOList);*/
        return null;
    }

    private static final Pattern TPATTERN = Pattern.compile("[A-Z0-9]");

    private static String buildFieldStr(String str) {
        Matcher matcher = TPATTERN.matcher(str);
        StringBuffer sb = new StringBuffer();
        while (matcher.find()) {
            matcher.appendReplacement(sb, "_" + matcher.group(0).toLowerCase());
        }
        matcher.appendTail(sb);
        return sb.toString();
    }


    /**
     * @param object     实体类对象
     * @param table_name 表名
     * @return 返回insert    sql
     */
    public static String insert_sql(Object object, String table_name) {

        //利用反射 把对象装换成 insert 语句
        String sql = "  insert into " + table_name + " (";
        try {
            Field[] declaredFieldArr = object.getClass().getDeclaredFields();
            List<Field> declaredFields = Arrays.stream(declaredFieldArr).filter(item-> !"serialVersionUID".equals(item.getName())).collect(Collectors.toList());
            for (int i = 0; i < declaredFields.size(); i++) {
                Field field = declaredFields.get(i);
                if (!"dao".equals(field.getName())) {
                    //设置是否允许访问，不是修改原来的访问权限修饰词。
                    field.setAccessible(true);
                    //为空的不加入
                    if (field.get(object) != null && !"".equals(field.get(object))) {
                        sql += buildFieldStr(field.getName()) + ",";
                    }
                }
            }
            sql += "* values (";
            for (int i = 0; i < declaredFields.size(); i++) {
                Field field = declaredFields.get(i);
                if (!"dao".equals(field.getName())) {
                    //设置是否允许访问，不是修改原来的访问权限修饰词。
                    field.setAccessible(true);
                    if (field.get(object) != null && !"".equals(field.get(object))) {
                        if ("class java.util.Date".equals(field.getType().toString())) {
                            DateFormat df1 = DateFormat.getInstance();
                            String format = df1.format(field.get(object));
                            sql += "to_date('" + format + "', 'dd-mm-yyyy'),";
                        } else {
                            sql += "'" + field.get(object) + "',";
                        }
                    }
                }
            }
            sql += "*";

            sql = sql.replace(",*", ")");
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        sql = sql.replace("'#pk#'", "pk.nextval");

        return sql;
    }

}