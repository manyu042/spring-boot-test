package com.abcode.st.common.enums;

import com.abcode.st.common.validate.EnumValidator;

public enum GenderTypeEnum implements EnumValidator {
    MALE("male", "男"),
    FEMALE("female", "女");
    private final String code;
    private final String label;

    GenderTypeEnum(String code, String label) {
        this.code = code;
        this.label = label;
    }

    public String getCode() {
        return code;
    }

    public String getLabel() {
        return label;
    }

    @Override // 需要实现getValue方法
    public Object getValue() {
        return code;
    }
}