package com.abcode.st.common.validate;

import javax.validation.groups.Default;

/**
 * @author qinzhitao
 * @Description 通过分组类
 * @date 2023/2/24 16:13
 */
public class GroupValidator {

    public interface Add extends Default {

    }

    public interface Query  extends Default{

    }

    public interface Update extends Default {

    }

}
