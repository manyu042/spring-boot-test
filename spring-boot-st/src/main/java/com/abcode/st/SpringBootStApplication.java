package com.abcode.st;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

//@EnableAspectJAutoProxy(proxyTargetClass = true)
@SpringBootApplication
public class SpringBootStApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootStApplication.class, args);
    }

}
