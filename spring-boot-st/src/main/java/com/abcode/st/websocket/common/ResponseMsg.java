package com.abcode.st.websocket.common;

import com.alibaba.fastjson.JSON;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

/**
 * 服务端推送消息封装对象
 *
 * @author qinzhitao
 * @date 2023/3/21 11:05
 */
@Data
@AllArgsConstructor
public class ResponseMsg implements Serializable {

    private static final String CODE_SUCCESS =  "0";
    private static final String CODE_FAIL =  "1";

    private static final String MSG_SUCCESS =  "操作成功";
    private static final String MSG_FAIL =  "操作失败";

    /** 请求结果 ：0-成功；1-错误；*/
    @ApiModelProperty("请求结果")
    String code;

    /** 提示信息 */
    @ApiModelProperty("提示信息")
    String message;

    /** 消息类型 */
    @ApiModelProperty("消息类型")
    String type;

    /** 推送数据 */
    @ApiModelProperty("推送数据")
    Object data;


    public static ResponseMsg ok(String type, Object data){
        return new ResponseMsg(CODE_SUCCESS, MSG_SUCCESS, type, data);
    }

    public static ResponseMsg error(String msg, String type, Object data){
        return new ResponseMsg(CODE_FAIL, msg, type, data);
    }

    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }
}
