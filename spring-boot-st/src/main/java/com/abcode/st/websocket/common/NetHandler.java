package com.abcode.st.websocket.common;

import javax.websocket.Session;

/**
 * websocket事件处理接口
 * @author qinzhitao
 * @date 2023-02-22
 */
public interface NetHandler{

    /**
     * 断开连接
     * @param session
     * @throws Exception
     */
    void onDisconnect(Session session) throws Exception;

    /**
     * 发生异常
     * @param session
     * @param cause
     * @throws Exception
     */
    void onException(Session session, Throwable cause) throws Exception;

    /**
     * 创建连接
     * @param session
     * @throws Exception
     */
    void createSession(Session session) throws Exception;

    /**
     * 收到消息
     * @param msg
     * @param session
     * @throws Exception
     */
    void onMessage(String msg, Session session) throws Exception;
}
