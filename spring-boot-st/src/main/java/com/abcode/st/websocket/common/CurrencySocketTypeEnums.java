package com.abcode.st.websocket.common;

import java.util.HashMap;
import java.util.Map;

/**
 * @description 通用消息数据源名称枚举对象
 * @Author: huangkaisheng
 * @Date:   2023-03-10
 */
public enum CurrencySocketTypeEnums {
    /** 心跳 */
    PING("ping", "心跳"),

    /** 审计信息 */
    AUDIT("sysMsg", "审计信息"),

    /** 审计信息列表 */
    LIST_AUDIT("listAudit", "审计信息列表"),

    /** 用户登陆     */
    LOGIN("login", "用户登陆"),

    /** 用户登出     */
    LOGIN_OUT("login", "用户登陆")

    ;
    /**
     * 数据源名称
     */
    private String value;


    /**
     * 描述
     */
    private String desc;

    CurrencySocketTypeEnums(String value, String desc) {
        this.value = value;
        this.desc = desc;
    }


    public String getValue() {
        return value;
    }

    public String getDesc() {
        return desc;
    }

    public static Map<String, CurrencySocketTypeEnums> enumMap = new HashMap();

    static {
        for (CurrencySocketTypeEnums data : CurrencySocketTypeEnums.values()) {
            enumMap.put(data.getValue(), data);
        }
    }
    /**
     * 根据值取枚举
     * @param type
     * @return
     * @author qinzhitao
     * @date 2023/3/23
     */
    public static CurrencySocketTypeEnums getTypeEnums(String type) {
        return enumMap.get(type);
    }
}
