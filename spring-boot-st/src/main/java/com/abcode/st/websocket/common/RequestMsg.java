package com.abcode.st.websocket.common;

import com.alibaba.fastjson.JSON;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

/**
 * 客户端请求消息封装对象
 *
 * @author qinzhitao
 * @date 2023/3/21 11:05
 */
@Data
@AllArgsConstructor
public class RequestMsg implements Serializable {

    /** 请求类型 */
    @ApiModelProperty("请求类型")
    String type;

    /** 请求参数 */
    @ApiModelProperty("请求参数")
    Object data;

    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }
}
