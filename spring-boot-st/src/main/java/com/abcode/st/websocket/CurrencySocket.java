package com.abcode.st.websocket;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;

/**
 * 用于一些通用功能
 * @author huangkaisheng
 *
 */
@Slf4j
//@Controller
//@ServerEndpoint("/currencySocket")
public class CurrencySocket {

    private static CurrencySocketHandler handler;
    @Autowired
    public void setAuditService(CurrencySocketHandler socketHandler) {
        CurrencySocket.handler = socketHandler;
    }

    /**
     * 连接建立成功调用的方法
     */
    @OnOpen
    public void onOpen(Session session) {
        handler.createSession(session);
    }

    /**
     * 连接关闭调用的方法
     */
    @OnClose
    public void onClose(Session session) {
        handler.onDisconnect(session);
    }

    /**
     * 收到客户端消息后调用的方法
     * @param message 客户端发送过来的消息
     */
    @OnMessage
    public void onMessage(String message, Session session) {
        handler.onMessage(message, session);
    }

    @OnError
    public void onError(Session session, Throwable error) {
        log.error("|onError|发生错误|sessionId:{}:error:{}",session.getId(), error);
    }

}
