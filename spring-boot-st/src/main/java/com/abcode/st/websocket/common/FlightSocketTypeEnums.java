package com.abcode.st.websocket.common;

import java.util.HashMap;
import java.util.Map;

/**
 * 航班动态socket消息类型枚举类
 *
 * @author hks
 */
public enum FlightSocketTypeEnums {
    /**
     * 心跳
     */
    PING("ping", "心跳"),

    /**
     * 初始化
     */
    INIT("initFlight", "初始化"),

    /**
     * 新增
     */
    ADD("addFlight", "新增"),

    /**
     * 更新
     */
    UPDATE("updateFlight", "更新"),

    /**
     * 删除
     */
    REMOVE("removeFlight", "删除"),

    /**
     * 列表
     */
    LIST("listFlight", "列表"),

    /**
     * 覆盖
     */
    OVERRIDE("overrideFlight", "覆盖"),

    /**
     * 自定义查询
     */
    DEFINE_QUERY("defineQueryFlight", "自定义查询");


    private final String value;
    private final String desc;
    /** 值和枚举对应集合 */
    public static Map<String, FlightSocketTypeEnums> enumMap = new HashMap();

    static {
        for (FlightSocketTypeEnums data : FlightSocketTypeEnums.values()) {
            enumMap.put(data.getValue(), data);
        }
    }

    FlightSocketTypeEnums(String value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public String getValue() {
        return value;
    }

    public String getDesc() {
        return desc;
    }

    /**
     * 根据值取枚举
     * @param type
     * @return
     * @author qinzhitao
     * @date 2023/3/23
     */
    public static FlightSocketTypeEnums getTypeEnums(String type) {
        return enumMap.get(type);
    }
}
