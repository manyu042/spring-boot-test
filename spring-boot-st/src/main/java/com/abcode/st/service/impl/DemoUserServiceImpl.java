package com.abcode.st.service.impl;

import com.abcode.st.common.api.vo.Result;
import com.abcode.st.domain.param.DemoUserParam;
import com.abcode.st.service.IDemoUserService;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

/**
 * TODO
 *
 * @author qinzhitao
 * @date 2023/3/31 10:51
 */
@Service
public class DemoUserServiceImpl implements IDemoUserService {
    @Override
    public Result addUser(DemoUserParam userParam) {
        Result result = Result.ok(userParam);
        result.setMessage("addUser");
        return result;
    }

    @Override
    public Result updateUser(DemoUserParam userParam) {
        Result result = Result.ok(userParam);
        result.setMessage("updateUser");
        return result;
    }

    @Override
    public Result queryUser(DemoUserParam userParam) {
        Result result = Result.ok(userParam);
        result.setMessage("queryUser");
        return result;
    }

    @Override
    public Result getUser(String account, String name, LocalDateTime birthday) {
        DemoUserParam userParam = new DemoUserParam();
        userParam.setAccount(account);
        userParam.setName(name);
        userParam.setBirthday(birthday);
        Result result = Result.ok(userParam);
        result.setMessage("getUser");
        return result;
    }
}
