package com.abcode.st.service;

import com.abcode.st.common.api.vo.Result;
import com.abcode.st.domain.param.DemoUserParam;

import java.time.LocalDateTime;

/**
 * TODO
 *
 * @author qinzhitao
 * @date 2023/3/31 10:46
 */
public interface IDemoUserService {

    Result addUser(DemoUserParam userParam);

    Result updateUser(DemoUserParam userParam);

    Result queryUser(DemoUserParam userParam);

    Result getUser(String account, String name, LocalDateTime birthday);
}
