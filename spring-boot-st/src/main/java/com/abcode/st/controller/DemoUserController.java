package com.abcode.st.controller;

import com.abcode.st.common.api.vo.Result;
import com.abcode.st.common.validate.GroupValidator;
import com.abcode.st.domain.param.DemoUserParam;
import com.abcode.st.service.IDemoUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Past;
import java.time.LocalDateTime;


/**
 * 用户管理
 */
@Slf4j
@RestController
@RequestMapping("/user")
@Validated
public class DemoUserController {

    @Autowired
    IDemoUserService demoUserService;

    @PostMapping("/addUser")
    public Result addUser(@RequestBody @Validated DemoUserParam userDto){
        log.info("addUser user info: {}", userDto);
        Result result = demoUserService.addUser(userDto);
        return result;
    }

    @PostMapping("/updateUser")
    public Result updateUser(@RequestBody @Validated(GroupValidator.Update.class) DemoUserParam userDto){
        log.info("updateUser user info: {}", userDto);
        Result result = demoUserService.updateUser(userDto);
        return result;
    }

    @PostMapping("queryUser")
    public Result queryUser(@RequestBody @Validated({GroupValidator.Query.class,}) DemoUserParam userDto){

        log.info("queryUser user info: {}", userDto);
        Result result = demoUserService.queryUser(userDto);
        return result;
    }

    @RequestMapping("getUser")
    public Result getUser(@NotBlank(message = "账号不能为空") String account,
                          @RequestParam("name") @NotBlank(message = "姓名不能为空") String name,
                          @Past(message = "请输入合理的时间") LocalDateTime birthday
                          ){
        log.info("getUser user account: {}", account);
        Result result = demoUserService.getUser(account, name, birthday);
        return result;
    }
}
