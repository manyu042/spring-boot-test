package com.abcode.xxljob.service;

import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.annotation.XxlJob;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * 任务测试类
 *
 * @author qinzhitao
 * @date 2023/5/6 16:19
 */
@Component
public class TestHandler {

    @XxlJob("testHandler")
    public void testHandler() throws Exception{
        XxlJobHelper.log("|testHandler|开始|");
        TimeUnit.SECONDS.sleep(1);
        XxlJobHelper.log("|testHandler|结束|");
    }
}
