package com.abcode.xxljob.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 测试控制类
 *
 * @author qinzhitao
 * @date 2023/5/6 15:34
 */
@RequestMapping("xxjob")
@RestController
public class TestController {

    @RequestMapping("test")
    public String test(){
        return "xxjob test";
    }
}
