package com.abcode.design;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootDesignApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootDesignApplication.class, args);
    }

}
