package com.abcode.design.behaviour.observer.demo;

/**
 * 测试
 *
 * @author qinzhitao
 * @date 2023/4/10 10:03
 */
public class DemoTest {

    public static void main(String[] args) {
        String filePath = "src/main/resources/text.text";
        String logPath = "src/main/resources/log.text";
        Editor editor = new Editor();
        editor.eventManager.subscribe("open", new LogOpenListener(logPath));
        editor.eventManager.subscribe("save", new EmailNotificationListener("33666@qq.com"));

        try {
            editor.openFile(filePath);
            editor.saveFile();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
