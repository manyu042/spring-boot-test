package com.abcode.design.behaviour.observer.demo;

import java.io.File;

/**
 * 通用观察者接口
 *
 * @author qinzhitao
 * @date 2023/4/10 9:29
 */
public interface EventListener {

    void  update(String eventType, File file);
}
