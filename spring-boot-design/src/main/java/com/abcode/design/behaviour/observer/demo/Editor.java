package com.abcode.design.behaviour.observer.demo;

import java.io.File;

/**
 * 具体发布者，由其他对象追踪
 *
 * @author qinzhitao
 * @date 2023/4/10 9:50
 */
public class Editor {
    public EventManager eventManager;
    public File file;

    public Editor(){
        this.eventManager = new EventManager("open", "save");
    }

    public  void  openFile(String filePath){
        this.file = new File(filePath);
        eventManager.notify("open", file);
    }

    public void saveFile() throws Exception{
        if(this.file !=null){
            eventManager.notify("save", file);
        }else{
            throw new Exception("please open a file first.");
        }
    }
}
