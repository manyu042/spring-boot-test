package com.abcode.design.behaviour.observer.demo;

import java.io.File;

/**
 * 收到通知后发送邮件
 *
 * @author qinzhitao
 * @date 2023/4/10 9:56
 */
public class EmailNotificationListener implements EventListener{
    private String email;

    public EmailNotificationListener(String email) {
        this.email = email;
    }

    @Override
    public void update(String eventType, File file) {
        System.out.println("Email to " + email + ": Someone has performed " + eventType + " operation with the following file: " + file.getName());
    }
}
