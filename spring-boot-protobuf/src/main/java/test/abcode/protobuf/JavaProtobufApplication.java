package test.abcode.protobuf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaProtobufApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavaProtobufApplication.class, args);
	}

}
