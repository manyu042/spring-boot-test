package test.abcode.protobuf.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ChatController {

    @GetMapping("/chatroom")
    public String chatroom() {
        return "chatroom"; // 返回对应的 Thymeleaf 模板名
    }

    @GetMapping("/websocket")
    public String websocket() {
        return "websocket"; // 返回对应的 Thymeleaf 模板名
    }
}
