let socket;
let root;
let MessageType; // 存储枚举类型
let isConnected = false;  // 标记是否已经连接
let UserInfo;
let heartbeatInterval; // 用于保存心跳定时器

protobuf.load("/chat.proto", function (err, loadedRoot) {
    if (err) {
        console.error("Failed to load .proto file:", err);
        return;
    }
    root = loadedRoot;
    MessageType = root.lookupEnum("MessageType").values; // 存储枚举
    UserInfo = root.lookupType("UserInfo");
});

// 随机生成中文昵称的函数
function getRandomChineseNickname() {
    const firstNames = ['张', '李', '王', '赵', '刘', '陈', '杨', '黄', '周', '吴'];
    const lastNames = ['伟', '芳', '娜', '敏', '静', '秀英', '丽', '强', '磊', '军'];
    const firstName = firstNames[Math.floor(Math.random() * firstNames.length)];
    const lastName = lastNames[Math.floor(Math.random() * lastNames.length)];
    return firstName + lastName;
}
// 页面加载时自动初始化随机昵称
// 使用 DOMContentLoaded 事件确保脚本在 DOM 加载完后执行
document.addEventListener('DOMContentLoaded', function() {
    console.log("DOMContentLoaded executed"); // 调试日志
    var nicknameInput = document.getElementById("nickname");
    if (!nicknameInput.value) {
        nicknameInput.value = getRandomChineseNickname();
    }
});

// 自动填充昵称的逻辑
function ensureNickname() {
    var nicknameInput = document.getElementById("nickname");
    if (!nicknameInput.value) {
        // 如果没有输入昵称，随机生成一个
        nicknameInput.value = getRandomChineseNickname();
    }
}

function connect() {
    ensureNickname();  // 在连接前确保已经有昵称
    var nickname = document.getElementById("nickname").value;
    var role = document.querySelector('input[name="role"]:checked').value;

    if (!nickname || !role) {
        alert("请输入昵称并选择角色！");
        return;
    }
    // 获取当前浏览器访问的 IP 和端口
    socket = new WebSocket("ws://" + window.location.host + "/ws");
    socket.binaryType = 'arraybuffer';

    socket.onopen = function () {
        isConnected = true;
        // 启用消息输入框
        toggleMessageInput(true);

        // userInfo
        let userInfo = UserInfo.create({
            userId: getUUID(),
            nickname: nickname,
            isHost: true
        });
        // joinRoom
        let JoinRoom = root.lookupType("JoinRoom");
        let joinRoom = JoinRoom.create({
            user: userInfo
        });
        // 编码joinRoom为二进制
        let joinRoomBuffer = JoinRoom.encode(joinRoom).finish();

        sendToServer(MessageType.JOIN_ROOM, joinRoomBuffer);
        // 开始心跳
        startHeartbeat();
    };
    // 接收服务器推送的消息
    socket.onmessage = function (event) {
        const GameMessage = root.lookupType("GameMessage");
        const gameMessage = GameMessage.decode(new Uint8Array(event.data));
        handleGameMessage(gameMessage);
    };

    socket.onclose = function () {
        isConnected = false;
        toggleMessageInput(false); // 禁用消息输入框
        console.log("连接关闭");
    };

    // 开始心跳
    function startHeartbeat() {
        // 发送心跳消息
        heartbeatInterval = setInterval(() => {
            sendToServer(MessageType.HEARTBEAT, null);
        }, 10000); // 每10秒发送一次心跳
    }
}

function getUUID() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        const r = Math.random() * 16 | 0;
        const v = c === 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

// 发送消息到server
function sendToServer(msgType, encodeBuff) {
    const GameMessage = root.lookupType("GameMessage");
    const gameMessage = GameMessage.create({
        reqType: msgType,
        content: encodeBuff
    });
    const errMsg = GameMessage.verify(gameMessage);
    if (errMsg) throw Error(errMsg);
    const encodedMessage = GameMessage.encode(GameMessage.create(gameMessage)).finish();
    socket.send(encodedMessage);
}

// 处理接收到的游戏消息
function handleGameMessage(gameMessage) {
    switch (gameMessage.resType) {
        case MessageType.HEARTBEAT_RES: // 心跳回复
            console.log('Heartbeat response received');
            break;
        case MessageType.JOIN_ROOM_NOTICE: // 加入房间通知
            console.log('Join room notice:');
            const JoinNotice = root.lookupType("JoinRoomNotice");
            var notice = JoinNotice.decode(new Uint8Array(gameMessage.content));
            updateUserList(notice, true)
            break;
        case MessageType.SEND_MESSAGE_NOTICE: // 发送消息通知
            const SendMsgNotice = root.lookupType("SendMsgNotice");
            var notice = SendMsgNotice.decode(new Uint8Array(gameMessage.content));
            displayMessage(notice);
            console.log('Message received:', gameMessage.content);
            break;
        case MessageType.LEAVE_ROOM_NOTICE: // 离开房间通知
            console.log('Leave room notice:', gameMessage.content);
            break;
        default:
            console.log('Unknown message type:', gameMessage.type);
    }
}

function displayMessage(notice) {
    var chatArea = document.getElementById("chatArea");
    var newMessage = document.createElement("div");
    newMessage.classList.add("message");

    var avatar = document.createElement("span");
    avatar.classList.add("avatar");
    avatar.textContent = notice.sender.nickname.charAt(0); // 取昵称首字母

    var messageContent = document.createElement("span");
    messageContent.textContent = notice.content;

    // 判断是自己发的消息还是别人发的
    if (document.getElementById("nickname").value === notice.sender.nickname) {
        newMessage.classList.add("mine"); // 自己的消息
        messageContent.classList.add("mine-content"); // 应用自己的消息样式
    } else {
        newMessage.classList.add("other"); // 其他人的消息
        messageContent.classList.add("other-content"); // 应用其他人的消息样式
    }


    newMessage.appendChild(avatar); // 始终把昵称作为头像放在左边
    newMessage.appendChild(messageContent);
    chatArea.appendChild(newMessage);
    chatArea.scrollTop = chatArea.scrollHeight; // 保持滚动到底部
}

function sendMessage() {
    var input = document.getElementById("messageInput").value;
    var nickname = document.getElementById("nickname").value;
    var role = document.querySelector('input[name="role"]:checked').value;

    if (!input.trim()) {
        alert("消息不能为空！");
        return;
    }

    if (input.length > 300) {
        alert("消息不能超过300字！");
        return;
    }

    let SendMsg = root.lookupType("SendMsg");
    let sengMsg = SendMsg.create({
        content: input
    });
    let SendMsgBuffer = SendMsg.encode(sengMsg).finish();
    sendToServer(MessageType.SEND_MESSAGE, SendMsgBuffer)
    document.getElementById("messageInput").value = '';
}

function updateUserList(joinRoomNotice, isJoinFlag) {
    var userList = document.getElementById("userList");
    var newUser = document.createElement("div");

    if (document.getElementById("nickname").value === joinRoomNotice.joinUser.nickname) {
        newUser.classList.add("user", "mine"); // 自己
    } else {
        newUser.classList.add("user", "other"); // 其他用户
    }

    newUser.textContent = joinRoomNotice.joinUser.nickname + " " + (isJoinFlag ? "加入" : "离开");
    userList.appendChild(newUser);
}

// 启用/禁用消息输入框
function toggleMessageInput(enable) {
    document.getElementById("messageInput").disabled = !enable;
    document.getElementById("sendButton").disabled = !enable;
}

// 页面加载时禁用消息输入框
window.onload = function () {
    toggleMessageInput(false);
};
