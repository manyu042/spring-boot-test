package com.abcode.test.mybatisoracle.gencode.service.impl;

import com.abcode.test.mybatisoracle.gencode.entity.User;
import com.abcode.test.mybatisoracle.gencode.dao.UserDao;
import com.abcode.test.mybatisoracle.gencode.service.UserService;
import org.springframework.stereotype.Service;


import javax.annotation.Resource;
import java.util.List;

/**
 * 用户表(User)表服务实现类
 *
 * @author makejava
 * @since 2021-10-12 01:42:00
 */
@Service("userService")
public class UserServiceImpl implements UserService {
    @Resource
    private UserDao userDao;

    /**
     * 通过ID查询单条数据
     *
     * @param userId 主键
     * @return 实例对象
     */
    @Override
    public User queryById(String userId) {
        return null;
        //return this.userDao.queryById(userId);
    }

    /**
     * 新增数据
     *
     * @param user 实例对象
     * @return 实例对象
     */
    @Override
    public User insert(User user) {
        this.userDao.insert(user);
        return user;
    }

    @Override
    public void batchInsert(List<User> list) {
        userDao.insertBatch(list);
    }

    /**
     * 修改数据
     *
     * @param user 实例对象
     * @return 实例对象
     */
    @Override
    public User update(User user) {
        //this.userDao.update(user);
        return this.queryById(user.getUserId());
    }

    /**
     * 通过主键删除数据
     *
     * @param userId 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Long userId) {
        return false;
        //return this.userDao.deleteById(userId) > 0;
    }

    @Override
    public Integer userCnt() {
        return userDao.count(null);
    }
}
