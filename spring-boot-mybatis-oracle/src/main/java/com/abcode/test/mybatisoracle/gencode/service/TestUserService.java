package com.abcode.test.mybatisoracle.gencode.service;

import com.abcode.test.mybatisoracle.gencode.entity.TestUser;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.List;

/**
 * 用户表(TestUser)表服务接口
 *
 * @author makejava
 * @since 2022-03-21 11:52:18
 */
public interface TestUserService {

    /**
     * 通过ID查询单条数据
     *
     * @param userId 主键
     * @return 实例对象
     */
    TestUser queryById(Long userId);

    /**
     * 分页查询
     *
     * @param testUser 筛选条件
     * @param pageRequest      分页对象
     * @return 查询结果
     */
    Page<TestUser> queryByPage(TestUser testUser, PageRequest pageRequest);

    /**
     * 新增数据
     *
     * @param testUser 实例对象
     * @return 实例对象
     */
    TestUser insert(TestUser testUser);

    /**
     * 修改数据
     *
     * @param testUser 实例对象
     * @return 实例对象
     */
    TestUser update(TestUser testUser);

    /**
     * 通过主键删除数据
     *
     * @param userId 主键
     * @return 是否成功
     */
    boolean deleteById(Long userId);

    void insertBatch(List<TestUser> userList);
}
