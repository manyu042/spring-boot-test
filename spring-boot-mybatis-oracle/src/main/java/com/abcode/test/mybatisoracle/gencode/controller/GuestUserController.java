package com.abcode.test.mybatisoracle.gencode.controller;

import com.abcode.test.mybatisoracle.gencode.entity.GuestUser;
import com.abcode.test.mybatisoracle.gencode.entity.User;
import com.abcode.test.mybatisoracle.gencode.service.GuestUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.RandomUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 用户表(GuestUser)表控制层
 *
 * @author makejava
 * @since 2022-03-18 11:14:45
 */
@Api(tags = "游客管理")
@RestController
@RequestMapping("/guestUser")
public class GuestUserController {

    @Qualifier("testExecutor")
    @Autowired
    private ThreadPoolTaskExecutor taskExecutor;

    /**
     * 服务对象
     */
    @Resource
    private GuestUserService guestUserService;

    /**
     * 分页查询
     *
     * @param guestUser   筛选条件
     * @param pageRequest 分页对象
     * @return 查询结果
     */
    @GetMapping
    public ResponseEntity<Page<GuestUser>> queryByPage(GuestUser guestUser, PageRequest pageRequest) {
        return ResponseEntity.ok(this.guestUserService.queryByPage(guestUser, pageRequest));
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("{id}")
    public ResponseEntity<GuestUser> queryById(@PathVariable("id") String id) {
        return ResponseEntity.ok(this.guestUserService.queryById(id));
    }

    /**
     * 新增数据
     *
     * @param
     * @return 新增结果
     */
    @ApiOperation(value = "添加游客")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "account", value = "账号", required = true, dataType = "String"),
            @ApiImplicitParam(name = "nickName", value = "昵称", required = true, dataType = "String")
    })
    @PostMapping("/add")
    public ResponseEntity<GuestUser> add(String account, String nickName) {
        GuestUser user = new GuestUser();
        user.setUserId(RandomUtils.nextInt(1,100000000)+"");
        user.setAccount(account+ "_"+ RandomUtils.nextInt(1,100000000)+"");
        user.setNickName(nickName);
        user.setAge(RandomUtils.nextLong(1, 100));
        user.setSex(RandomUtils.nextInt(1,2)+"");
        user.setRemark(RandomUtils.nextBytes(10).toString());
        user.setCreateTime(LocalDateTime.now());
        user.setUpdateTime(LocalDateTime.now());
        GuestUser userRst = guestUserService.insert(user);
        return ResponseEntity.ok(userRst);
    }

    /**
     * 批量增加
     * @param count
     * @return
     */
    @ApiOperation(value = "批量添加游客账号")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "count", value = "数量", required = true, dataType = "Integer")
    })
    @PostMapping("/batchAdd")
    public ResponseEntity<Object> batchAdd(int count) {

        int size = 1;
        int j = count/size;
        if(j<=0){
            j=1;
        }

        AtomicInteger atomicInteger = new AtomicInteger(1);

        for (int i = 0; i < j; i++) {

            int finalI = i;
            taskExecutor.execute(()->{
                List<GuestUser> userList = new ArrayList<>();
                try {
                    GuestUser user  = null;
                    for (int a = 0; a < size; a++) {
                        user = new GuestUser();
                        user.setAccount("account_"+ UUID.randomUUID().toString().replace("-",""));
                        user.setNickName(user.getAccount());
                        user.setAge(RandomUtils.nextLong(1, 100));
                        user.setSex(RandomUtils.nextInt(1,3)+"");
                        user.setRemark(RandomUtils.nextBytes(10).toString());
                        user.setCreateTime(LocalDateTime.now());
                        user.setUpdateTime(LocalDateTime.now());

                        userList.add(user);
                    }

                    guestUserService.insertBatch(userList);

                    int insertCnt = atomicInteger.getAndAdd(size);
                    System.out.println("insertCnt==================="+ insertCnt );
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
        }

        return ResponseEntity.ok(null);
    }

    /**
     * 编辑数据
     *
     * @param guestUser 实体
     * @return 编辑结果
     */
    @PutMapping
    public ResponseEntity<GuestUser> edit(GuestUser guestUser) {
        return ResponseEntity.ok(this.guestUserService.update(guestUser));
    }

    /**
     * 删除数据
     *
     * @param id 主键
     * @return 删除是否成功
     */
    @DeleteMapping
    public ResponseEntity<Boolean> deleteById(String id) {
        return ResponseEntity.ok(this.guestUserService.deleteById(id));
    }

}

