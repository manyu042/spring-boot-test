package com.abcode.test.mybatisoracle.gencode.service;

import com.abcode.test.mybatisoracle.gencode.entity.WoJcStpXmlPartition;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.List;

/**
 * 结构化工作指令步骤表(WoJcStpXmlPartition)表服务接口
 *
 * @author makejava
 * @since 2022-03-22 09:18:50
 */
public interface WoJcStpXmlPartitionService {

    /**
     * 通过ID查询单条数据
     *
     * @param rid 主键
     * @return 实例对象
     */
    WoJcStpXmlPartition queryById(Long rid);

    /**
     * 分页查询
     *
     * @param woJcStpXmlPartition 筛选条件
     * @param pageRequest      分页对象
     * @return 查询结果
     */
    Page<WoJcStpXmlPartition> queryByPage(WoJcStpXmlPartition woJcStpXmlPartition, PageRequest pageRequest);

    /**
     * 新增数据
     *
     * @param woJcStpXmlPartition 实例对象
     * @return 实例对象
     */
    WoJcStpXmlPartition insert(WoJcStpXmlPartition woJcStpXmlPartition);

    /**
     * 修改数据
     *
     * @param woJcStpXmlPartition 实例对象
     * @return 实例对象
     */
    WoJcStpXmlPartition update(WoJcStpXmlPartition woJcStpXmlPartition);

    /**
     * 通过主键删除数据
     *
     * @param rid 主键
     * @return 是否成功
     */
    boolean deleteById(Long rid);

    void insertBatch(List<WoJcStpXmlPartition> list);
}
