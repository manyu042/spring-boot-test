package com.abcode.test.mybatisoracle.gencode.entity;

import java.time.LocalDateTime;
import java.io.Serializable;

/**
 * 结构化工作指令步骤表(WoJcStpXmlPartition)实体类
 *
 * @author makejava
 * @since 2022-03-22 09:18:50
 */
public class WoJcStpXmlPartition implements Serializable {
    private static final long serialVersionUID = 789695138413896391L;
    /**
     * 主键
     */
    private Long rid;
    /**
     * 工卡模板ID
     */
    private Long jcRid;
    /**
     * 工卡指令ID
     */
    private Long woJcRid;
    /**
     * 工卡编号
     */
    private String jobcard;
    /**
     * 上级长步骤
     */
    private String pStepLong;
    /**
     * 步骤号
     */
    private String step;
    /**
     * 长步骤号
     */
    private String stepLong;
    /**
     * 步骤ID
     */
    private Long stepid;
    /**
     *  Para,NOTE,CAUTION,WARNING等等
     */
    private String paraType;
    /**
     * 步骤描述(英文) Step Description (EN)
     */
    private String stepDesc;
    /**
     * 步骤描述(中文) Step Description (CN)
     */
    private String stepDescLc;
    /**
     * 步骤状态O-Open,C-Closed
     */
    private String stepStatus;
    /**
     * 关闭人
     */
    private String closedBy;
    /**
     * 关闭时间
     */
    private LocalDateTime closedDate;
    /**
     * 机械员签署  Y 全部签署   PY 是部分签署   N没签署    A不需要签署
     */
    private String mechSignStatus;
    /**
     * 检验员签署  Y 全部签署   PY 是部分签署   N没签署    A不需要签署
     */
    private String inspSignStatus;
    /**
     * 重做标志   Y为重做状态   N不是
     */
    private String reworkFlag;
    /**
     * 重做理由
     */
    private String reworkNote;
    /**
     * NA理由
     */
    private String naReason;
    /**
     * 创建时间
     */
    private LocalDateTime createTime;
    /**
     * 修改时间
     */
    private LocalDateTime updateTime;
    /**
     * 工卡创建时间
     */
    private LocalDateTime orderCreateTime;


    public Long getRid() {
        return rid;
    }

    public void setRid(Long rid) {
        this.rid = rid;
    }

    public Long getJcRid() {
        return jcRid;
    }

    public void setJcRid(Long jcRid) {
        this.jcRid = jcRid;
    }

    public Long getWoJcRid() {
        return woJcRid;
    }

    public void setWoJcRid(Long woJcRid) {
        this.woJcRid = woJcRid;
    }

    public String getJobcard() {
        return jobcard;
    }

    public void setJobcard(String jobcard) {
        this.jobcard = jobcard;
    }

    public String getPStepLong() {
        return pStepLong;
    }

    public void setPStepLong(String pStepLong) {
        this.pStepLong = pStepLong;
    }

    public String getStep() {
        return step;
    }

    public void setStep(String step) {
        this.step = step;
    }

    public String getStepLong() {
        return stepLong;
    }

    public void setStepLong(String stepLong) {
        this.stepLong = stepLong;
    }

    public Long getStepid() {
        return stepid;
    }

    public void setStepid(Long stepid) {
        this.stepid = stepid;
    }

    public String getParaType() {
        return paraType;
    }

    public void setParaType(String paraType) {
        this.paraType = paraType;
    }

    public String getStepDesc() {
        return stepDesc;
    }

    public void setStepDesc(String stepDesc) {
        this.stepDesc = stepDesc;
    }

    public String getStepDescLc() {
        return stepDescLc;
    }

    public void setStepDescLc(String stepDescLc) {
        this.stepDescLc = stepDescLc;
    }

    public String getStepStatus() {
        return stepStatus;
    }

    public void setStepStatus(String stepStatus) {
        this.stepStatus = stepStatus;
    }

    public String getClosedBy() {
        return closedBy;
    }

    public void setClosedBy(String closedBy) {
        this.closedBy = closedBy;
    }

    public LocalDateTime getClosedDate() {
        return closedDate;
    }

    public void setClosedDate(LocalDateTime closedDate) {
        this.closedDate = closedDate;
    }

    public String getMechSignStatus() {
        return mechSignStatus;
    }

    public void setMechSignStatus(String mechSignStatus) {
        this.mechSignStatus = mechSignStatus;
    }

    public String getInspSignStatus() {
        return inspSignStatus;
    }

    public void setInspSignStatus(String inspSignStatus) {
        this.inspSignStatus = inspSignStatus;
    }

    public String getReworkFlag() {
        return reworkFlag;
    }

    public void setReworkFlag(String reworkFlag) {
        this.reworkFlag = reworkFlag;
    }

    public String getReworkNote() {
        return reworkNote;
    }

    public void setReworkNote(String reworkNote) {
        this.reworkNote = reworkNote;
    }

    public String getNaReason() {
        return naReason;
    }

    public void setNaReason(String naReason) {
        this.naReason = naReason;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public LocalDateTime getOrderCreateTime() {
        return orderCreateTime;
    }

    public void setOrderCreateTime(LocalDateTime orderCreateTime) {
        this.orderCreateTime = orderCreateTime;
    }

}

