package com.abcode.test.mybatisoracle.gencode.controller;

import com.abcode.test.mybatisoracle.gencode.entity.User;
import com.abcode.test.mybatisoracle.gencode.service.UserService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.RandomUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 用户表(User)表控制层
 *
 * @author makejava
 * @since 2021-10-12 01:41:59
 */
@Api(tags = "用户管理")
@RestController
@RequestMapping("/user")
public class UserController {

    @Qualifier("testExecutor")
    @Autowired
    private ThreadPoolTaskExecutor taskExecutor;

    /**
     * 服务对象
     */
    @Resource
    private UserService userService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @ApiOperation(value = "查询单个用户", notes = "通过ID查询")
    @GetMapping("/getUser/{id}")
    public ResponseEntity<User> queryById(@PathVariable("id") String id) {

        User user = userService.queryById(id);

        return ResponseEntity.ok(user);
    }

    @ApiOperation(value = "查询用数量", notes = "查询用数量")
    @GetMapping("/getUserCnt")
    public ResponseEntity getUserCnt() {

        int count = userService.userCnt();

        return ResponseEntity.ok(count);
    }

/**
     * 新增数据
     *
     * @param account 账号
     * @param
     * @return 新增结果
     */
    @ApiOperation(value = "添加用户")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "account", value = "账号", required = true, dataType = "String"),
            @ApiImplicitParam(name = "nickName", value = "昵称", required = true, dataType = "String")
    })
    @PostMapping("/add")
    public ResponseEntity<User> add(String account, String nickName) {
        User user = new User();
        user.setUserId(RandomUtils.nextInt(1,100000000)+"");
        user.setAccount(account+ "_"+ RandomUtils.nextInt(1,100000000)+"");
        user.setNickName(nickName);
        user.setAge(RandomUtils.nextInt(1, 100));
        user.setSex(RandomUtils.nextInt(1,2)+"");
        user.setRemark(RandomUtils.nextBytes(10).toString());
        user.setCreateTime(LocalDateTime.now());
        user.setUpdateTime(LocalDateTime.now());
        User userRst = userService.insert(user);
        return ResponseEntity.ok(userRst);
    }

    @ApiOperation(value = "批量添加用户")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "count", value = "数量", required = true, dataType = "Integer")
    })
    @PostMapping("/batchAdd")
    public ResponseEntity<Object> batchAdd(int count) {

        int size = 1000;
        int j = count/size + 1;

        AtomicInteger atomicInteger = new AtomicInteger(1);

        for (int i = 0; i < j; i++) {

            int finalI = i;
            taskExecutor.execute(()->{
                    String uuid = getUUId();
                    int b = finalI;
                    List<User> userList = new ArrayList<>();
                try {
                    User user  = null;
                    for (int a = 0; a < size; a++) {
                        String uid = uuid + RandomUtils.nextBytes(10).toString() + (finalI*1000+a);
                        user = new User();
                        user.setUserId(uid);
                        user.setAccount("account_"+ RandomUtils.nextInt(1,100000000)+"");
                        user.setNickName(user.getAccount());
                        user.setAge(RandomUtils.nextInt(1, 100));
                        user.setSex(RandomUtils.nextInt(1,3)+"");
                        user.setRemark(RandomUtils.nextBytes(10).toString());
                        user.setCreateTime(LocalDateTime.now());
                        user.setUpdateTime(LocalDateTime.now());

                        userList.add(user);
                    }

                    userService.batchInsert(userList);
                    int insertCnt = atomicInteger.getAndAdd(size);
                    System.out.println("insertCnt==================="+ insertCnt );
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
        }

        return ResponseEntity.ok("完成");
    }

    private synchronized String getUUId(){
        return UUID.randomUUID().toString().replace("-", "");
    }

/**
     * 编辑数据
     *
     * @param user 实体
     * @return 编辑结果
     *//*

    @PutMapping
    public ResponseEntity<User> edit(User user) {
        return ResponseEntity.ok(this.userService.update(user));
    }

    */
/**
     * 删除数据
     *
     * @param id 主键
     * @return 删除是否成功
     *//*

    @DeleteMapping
    public ResponseEntity<Boolean> deleteById(Long id) {
        return ResponseEntity.ok(this.userService.deleteById(id));
    }
*/

}

