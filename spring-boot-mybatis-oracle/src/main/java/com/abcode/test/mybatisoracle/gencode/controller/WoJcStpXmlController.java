package com.abcode.test.mybatisoracle.gencode.controller;

import com.abcode.test.mybatisoracle.gencode.entity.WoJcStpXml;
import com.abcode.test.mybatisoracle.gencode.entity.WoJcStpXmlPartition;
import com.abcode.test.mybatisoracle.gencode.service.WoJcStpXmlPartitionService;
import com.abcode.test.mybatisoracle.gencode.service.WoJcStpXmlService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.RandomUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

/**
 * 结构化工作指令步骤表(WoJcStpXml)表控制层
 *
 * @author makejava
 * @since 2022-03-22 09:18:50
 */
@Api(tags = "步骤")
@RestController
@RequestMapping("woJcStpXml")
public class WoJcStpXmlController {

    /**
     * 服务对象
     */
    @Resource
    private WoJcStpXmlPartitionService woJcStpXmlPartitionService;
    /**
     * 服务对象
     */
    @Resource
    private WoJcStpXmlService woJcStpXmlService;

    @Qualifier("testExecutor")
    @Autowired
    private ThreadPoolTaskExecutor taskExecutor;

    /**
     * 分页查询
     *
     * @param woJcStpXml 筛选条件
     * @param pageRequest      分页对象
     * @return 查询结果
     */
    @GetMapping
    public ResponseEntity<Page<WoJcStpXml>> queryByPage(WoJcStpXml woJcStpXml, PageRequest pageRequest) {
        return ResponseEntity.ok(this.woJcStpXmlService.queryByPage(woJcStpXml, pageRequest));
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("{id}")
    public ResponseEntity<WoJcStpXml> queryById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(this.woJcStpXmlService.queryById(id));
    }

    /**
     * 新增数据
     *
     * @param woJcStpXml 实体
     * @return 新增结果
     */
    @PostMapping
    public ResponseEntity<WoJcStpXml> add(WoJcStpXml woJcStpXml) {
        return ResponseEntity.ok(this.woJcStpXmlService.insert(woJcStpXml));
    }

    /**
     * 编辑数据
     *
     * @param woJcStpXml 实体
     * @return 编辑结果
     */
    @PutMapping
    public ResponseEntity<WoJcStpXml> edit(WoJcStpXml woJcStpXml) {
        return ResponseEntity.ok(this.woJcStpXmlService.update(woJcStpXml));
    }

    /**
     * 删除数据
     *
     * @param id 主键
     * @return 删除是否成功
     */
    @DeleteMapping
    public ResponseEntity<Boolean> deleteById(Long id) {
        return ResponseEntity.ok(this.woJcStpXmlService.deleteById(id));
    }


    /**
     * 新增数据
     *
     * @param
     * @return 新增结果
     */
    @ApiOperation(value = "添加一条数据")
    @PostMapping("/add")
    public ResponseEntity<WoJcStpXml> add() {
        LocalDate localDate = LocalDate.of(2021, 8,1);
        LocalTime localTime = LocalTime.of(0,0,0);
        int dayCnt = RandomUtils.nextInt(1,1000);
        localDate = localDate.plusDays(dayCnt);
        int secondsCnt = RandomUtils.nextInt(1,86400);
        localTime = localTime.plusSeconds(secondsCnt);

        LocalDateTime orderCreateTime = LocalDateTime.of(localDate, localTime);


        WoJcStpXml woJcStpXml = new WoJcStpXml();
        //woJcStpXml.setRid(0L);
        Long jcRid = RandomUtils.nextLong(10000, 90000000000l);
        Long woJcRid = RandomUtils.nextLong(800000, 80000000000000000l);
        String jobCard = RandomUtils.nextBytes(8).toString();

        woJcStpXml.setJcRid(jcRid);
        woJcStpXml.setWoJcRid(woJcRid);
        woJcStpXml.setJobcard(jobCard);
        woJcStpXml.setPStepLong("1.2");
        woJcStpXml.setStep("1");
        woJcStpXml.setStepLong("1.2.1");
        woJcStpXml.setStepid(0L);
        woJcStpXml.setParaType("TITLE");
        woJcStpXml.setStepDesc("this is a test jobCard");
        woJcStpXml.setStepDescLc("测试工卡");
        woJcStpXml.setStepStatus("C");
        woJcStpXml.setClosedBy("0558555");
        woJcStpXml.setClosedDate(LocalDateTime.now());
        woJcStpXml.setMechSignStatus("Y");
        woJcStpXml.setInspSignStatus("Y");
        woJcStpXml.setReworkFlag("");
        woJcStpXml.setReworkNote("");
        woJcStpXml.setNaReason("");
        woJcStpXml.setCreateTime(LocalDateTime.now());
        woJcStpXml.setUpdateTime(LocalDateTime.now());
        woJcStpXml.setOrderCreateTime(orderCreateTime);

        this.woJcStpXmlService.insert(woJcStpXml);

        return ResponseEntity.ok(woJcStpXml);
    }


    /**
     * 批量增加
     * @param count
     * @return
     */
    @ApiOperation(value = "批量添加步骤")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "count", value = "数量", required = true, dataType = "Integer")
    })
    @PostMapping("/batchAdd")
    public ResponseEntity<Object> batchAdd(int count) {

        int size = 2000;
        int j = count/size;
        if(j<=0){
            j=1;
        }

        for (int i = 0; i < j; i++) {

            taskExecutor.execute(()->{
                LocalDate localDate = LocalDate.of(2021, 8,1);
                LocalTime localTime = LocalTime.of(0,0,0);
                List<WoJcStpXml> list = new ArrayList<>();
                Long jcRid = RandomUtils.nextLong(10000, 90000000000l);
                Long woJcRid = RandomUtils.nextLong(800000, 80000000000000000l);
                String jobCard = RandomUtils.nextBytes(8).toString();

                int dayCnt = RandomUtils.nextInt(1,1000);
                localDate = localDate.plusDays(dayCnt);
                int secondsCnt = RandomUtils.nextInt(1,86400);
                localTime = localTime.plusSeconds(secondsCnt);

                LocalDateTime orderCreateTime = LocalDateTime.of(localDate, localTime);

                try {
                    WoJcStpXml woJcStpXml  = null;
                    for (int a = 0; a < size; a++) {
                        woJcStpXml = new WoJcStpXml();
                        //woJcStpXml.setRid(0L);
                        woJcStpXml.setJcRid(jcRid);
                        woJcStpXml.setWoJcRid(woJcRid);
                        woJcStpXml.setJobcard(jobCard);
                        woJcStpXml.setPStepLong("1.2");
                        woJcStpXml.setStep("1");
                        woJcStpXml.setStepLong("1.2.1");
                        woJcStpXml.setStepid(0L);
                        woJcStpXml.setParaType("TITLE");
                        woJcStpXml.setStepDesc("this is a test jobCard");
                        woJcStpXml.setStepDescLc("测试工卡");
                        woJcStpXml.setStepStatus("C");
                        woJcStpXml.setClosedBy("0558555");
                        woJcStpXml.setClosedDate(LocalDateTime.now());
                        woJcStpXml.setMechSignStatus("Y");
                        woJcStpXml.setInspSignStatus("Y");
                        woJcStpXml.setReworkFlag("");
                        woJcStpXml.setReworkNote("");
                        woJcStpXml.setNaReason("");
                        woJcStpXml.setCreateTime(LocalDateTime.now());
                        woJcStpXml.setUpdateTime(LocalDateTime.now());

                        woJcStpXml.setOrderCreateTime(orderCreateTime);

                        list.add(woJcStpXml);
                    }
                    woJcStpXmlService.insertBatch(list);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
        }
        return ResponseEntity.ok(null);
    }

}

