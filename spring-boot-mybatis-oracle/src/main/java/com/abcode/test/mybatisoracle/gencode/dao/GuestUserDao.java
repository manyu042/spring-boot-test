package com.abcode.test.mybatisoracle.gencode.dao;

import com.abcode.test.mybatisoracle.gencode.entity.GuestUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * 用户表(GuestUser)表数据库访问层
 *
 * @author makejava
 * @since 2022-03-18 11:14:47
 */
@Mapper
public interface GuestUserDao {

    /**
     * 通过ID查询单条数据
     *
     * @param userId 主键
     * @return 实例对象
     */
    GuestUser queryById(String userId);

    /**
     * 查询指定行数据
     *
     * @param guestUser 查询条件
     * @param pageable  分页对象
     * @return 对象列表
     */
    List<GuestUser> queryAllByLimit(GuestUser guestUser, @Param("pageable") Pageable pageable);

    /**
     * 统计总行数
     *
     * @param guestUser 查询条件
     * @return 总行数
     */
    long count(GuestUser guestUser);

    /**
     * 新增数据
     *
     * @param guestUser 实例对象
     * @return 影响行数
     */
    int insert(GuestUser guestUser);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<GuestUser> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(List<GuestUser> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<GuestUser> 实例对象列表
     * @return 影响行数
     * @throws org.springframework.jdbc.BadSqlGrammarException 入参是空List的时候会抛SQL语句错误的异常，请自行校验入参
     */
    int insertOrUpdateBatch(@Param("entities") List<GuestUser> entities);

    /**
     * 修改数据
     *
     * @param guestUser 实例对象
     * @return 影响行数
     */
    int update(GuestUser guestUser);

    /**
     * 通过主键删除数据
     *
     * @param userId 主键
     * @return 影响行数
     */
    int deleteById(String userId);

}

