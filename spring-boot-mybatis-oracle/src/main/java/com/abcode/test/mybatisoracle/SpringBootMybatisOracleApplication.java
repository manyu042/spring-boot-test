package com.abcode.test.mybatisoracle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootMybatisOracleApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootMybatisOracleApplication.class, args);
    }

}
