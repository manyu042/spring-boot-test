package com.abcode.test.mybatisoracle.gencode.dao;

import com.abcode.test.mybatisoracle.gencode.entity.WoJcStpXmlPartition;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;
import java.util.List;

/**
 * 结构化工作指令步骤表(WoJcStpXmlPartition)表数据库访问层
 *
 * @author makejava
 * @since 2022-03-22 09:18:50
 */
@Mapper
public interface WoJcStpXmlPartitionDao {

    /**
     * 通过ID查询单条数据
     *
     * @param rid 主键
     * @return 实例对象
     */
    WoJcStpXmlPartition queryById(Long rid);

    /**
     * 查询指定行数据
     *
     * @param woJcStpXmlPartition 查询条件
     * @param pageable         分页对象
     * @return 对象列表
     */
    List<WoJcStpXmlPartition> queryAllByLimit(WoJcStpXmlPartition woJcStpXmlPartition, @Param("pageable") Pageable pageable);

    /**
     * 统计总行数
     *
     * @param woJcStpXmlPartition 查询条件
     * @return 总行数
     */
    long count(WoJcStpXmlPartition woJcStpXmlPartition);

    /**
     * 新增数据
     *
     * @param woJcStpXmlPartition 实例对象
     * @return 影响行数
     */
    int insert(WoJcStpXmlPartition woJcStpXmlPartition);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param list List<WoJcStpXmlPartition> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(List<WoJcStpXmlPartition> list);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<WoJcStpXmlPartition> 实例对象列表
     * @return 影响行数
     * @throws org.springframework.jdbc.BadSqlGrammarException 入参是空List的时候会抛SQL语句错误的异常，请自行校验入参
     */
    int insertOrUpdateBatch(@Param("entities") List<WoJcStpXmlPartition> entities);

    /**
     * 修改数据
     *
     * @param woJcStpXmlPartition 实例对象
     * @return 影响行数
     */
    int update(WoJcStpXmlPartition woJcStpXmlPartition);

    /**
     * 通过主键删除数据
     *
     * @param rid 主键
     * @return 影响行数
     */
    int deleteById(Long rid);

}

