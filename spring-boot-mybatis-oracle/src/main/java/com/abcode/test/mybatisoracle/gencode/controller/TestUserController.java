package com.abcode.test.mybatisoracle.gencode.controller;

import com.abcode.test.mybatisoracle.gencode.entity.GuestUser;
import com.abcode.test.mybatisoracle.gencode.entity.TestUser;
import com.abcode.test.mybatisoracle.gencode.service.TestUserService;
import com.alibaba.druid.support.json.JSONUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.RandomUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 用户表(TestUser)表控制层
 *
 * @author makejava
 * @since 2022-03-21 11:52:18
 */
@Api(tags = "测试管理")
@RestController
@RequestMapping("testUser")
public class TestUserController {
    /**
     * 服务对象
     */
    @Resource
    private TestUserService testUserService;

    /**
     * 分页查询
     *
     * @param testUser 筛选条件
     * @param pageRequest      分页对象
     * @return 查询结果
     */
    @GetMapping
    public ResponseEntity<Page<TestUser>> queryByPage(TestUser testUser, PageRequest pageRequest) {
        return ResponseEntity.ok(this.testUserService.queryByPage(testUser, pageRequest));
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("{id}")
    public ResponseEntity<TestUser> queryById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(this.testUserService.queryById(id));
    }

    /**
     * 新增数据
     *
     * @param testUser 实体
     * @return 新增结果
     */
    @PostMapping
    public ResponseEntity<TestUser> add(TestUser testUser) {
        return ResponseEntity.ok(this.testUserService.insert(testUser));
    }

    /**
     * 编辑数据
     *
     * @param testUser 实体
     * @return 编辑结果
     */
    @PutMapping
    public ResponseEntity<TestUser> edit(TestUser testUser) {
        return ResponseEntity.ok(this.testUserService.update(testUser));
    }

    /**
     * 删除数据
     *
     * @param id 主键
     * @return 删除是否成功
     */
    @DeleteMapping
    public ResponseEntity<Boolean> deleteById(Long id) {
        return ResponseEntity.ok(this.testUserService.deleteById(id));
    }



    /**
     * 批量增加
     * @param count
     * @return
     */
    @ApiOperation(value = "批量添加测试账号")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "count", value = "数量", required = true, dataType = "Integer")
    })
    @PostMapping("/batchAdd")
    public ResponseEntity<Object> batchAdd(int count) {

        int size = 1;
        int j = count;
        if(j<=0){
            j=1;
        }else if(j > 1000){
            j=1000;
        }
        List<TestUser> userList = new ArrayList<>();
        for (int i = 0; i < j; i++) {

            int finalI = i;

            try {
                TestUser user  = null;
                for (int a = 0; a < size; a++) {
                    user = new TestUser();
                    user.setAccount("account_"+ UUID.randomUUID().toString().replace("-",""));
                    user.setNickName(user.getAccount());
                    user.setAge(RandomUtils.nextLong(1, 100));
                    user.setSex(RandomUtils.nextInt(1,3)+"");
                    user.setRemark(RandomUtils.nextBytes(10).toString());
                    user.setCreateTime(LocalDateTime.now());
                    user.setUpdateTime(LocalDateTime.now());

                    userList.add(user);
                }
                testUserService.insertBatch(userList);
                System.out.println(userList);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return ResponseEntity.ok(userList);
    }

}

