package com.abcode.test.mybatisoracle.gencode.controller;

import com.abcode.test.mybatisoracle.gencode.entity.TestUser;
import com.abcode.test.mybatisoracle.gencode.entity.WoJcStpXmlPartition;
import com.abcode.test.mybatisoracle.gencode.service.WoJcStpXmlPartitionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.RandomUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * 结构化工作指令步骤表(WoJcStpXmlPartition)表控制层
 *
 * @author makejava
 * @since 2022-03-22 09:18:50
 */
@Api(tags = "步骤分区")
@RestController
@RequestMapping("woJcStpXmlPartition")
public class WoJcStpXmlPartitionController {

    @Qualifier("testExecutor")
    @Autowired
    private ThreadPoolTaskExecutor taskExecutor;
    /**
     * 服务对象
     */
    @Resource
    private WoJcStpXmlPartitionService woJcStpXmlPartitionService;

    /**
     * 分页查询
     *
     * @param woJcStpXmlPartition 筛选条件
     * @param pageRequest      分页对象
     * @return 查询结果
     */
    @GetMapping
    public ResponseEntity<Page<WoJcStpXmlPartition>> queryByPage(WoJcStpXmlPartition woJcStpXmlPartition, PageRequest pageRequest) {
        return ResponseEntity.ok(this.woJcStpXmlPartitionService.queryByPage(woJcStpXmlPartition, pageRequest));
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("{id}")
    public ResponseEntity<WoJcStpXmlPartition> queryById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(this.woJcStpXmlPartitionService.queryById(id));
    }


    /**
     * 编辑数据
     *
     * @param woJcStpXmlPartition 实体
     * @return 编辑结果
     */
    @PutMapping
    public ResponseEntity<WoJcStpXmlPartition> edit(WoJcStpXmlPartition woJcStpXmlPartition) {
        return ResponseEntity.ok(this.woJcStpXmlPartitionService.update(woJcStpXmlPartition));
    }

    /**
     * 删除数据
     *
     * @param id 主键
     * @return 删除是否成功
     */
    @DeleteMapping
    public ResponseEntity<Boolean> deleteById(Long id) {
        return ResponseEntity.ok(this.woJcStpXmlPartitionService.deleteById(id));
    }



    /**
     * 新增数据
     *
     * @param
     * @return 新增结果
     */
    @ApiOperation(value = "添加一条数据")
    @PostMapping("/add")
    public ResponseEntity<WoJcStpXmlPartition> add() {
        LocalDate localDate = LocalDate.of(2021, 8,1);
        LocalTime localTime = LocalTime.of(0,0,0);
        int dayCnt = RandomUtils.nextInt(1,1000);
        localDate = localDate.plusDays(dayCnt);
        int secondsCnt = RandomUtils.nextInt(1,86400);
        localTime = localTime.plusSeconds(secondsCnt);

        LocalDateTime orderCreateTime = LocalDateTime.of(localDate, localTime);


        WoJcStpXmlPartition woJcStpXml = new WoJcStpXmlPartition();
        //woJcStpXml.setRid(0L);
        Long jcRid = RandomUtils.nextLong(10000, 90000000000l);
        Long woJcRid = RandomUtils.nextLong(800000, 80000000000000000l);
        String jobCard = RandomUtils.nextBytes(8).toString();

        woJcStpXml.setJcRid(jcRid);
        woJcStpXml.setWoJcRid(woJcRid);
        woJcStpXml.setJobcard(jobCard);
        woJcStpXml.setPStepLong("1.2");
        woJcStpXml.setStep("1");
        woJcStpXml.setStepLong("1.2.1");
        woJcStpXml.setStepid(0L);
        woJcStpXml.setParaType("TITLE");
        woJcStpXml.setStepDesc("this is a test jobCard");
        woJcStpXml.setStepDescLc("测试工卡");
        woJcStpXml.setStepStatus("C");
        woJcStpXml.setClosedBy("0558555");
        woJcStpXml.setClosedDate(LocalDateTime.now());
        woJcStpXml.setMechSignStatus("Y");
        woJcStpXml.setInspSignStatus("Y");
        woJcStpXml.setReworkFlag("");
        woJcStpXml.setReworkNote("");
        woJcStpXml.setNaReason("");
        woJcStpXml.setCreateTime(LocalDateTime.now());
        woJcStpXml.setUpdateTime(LocalDateTime.now());
        woJcStpXml.setOrderCreateTime(orderCreateTime);

        this.woJcStpXmlPartitionService.insert(woJcStpXml);


        return ResponseEntity.ok(woJcStpXml);
    }


    /**
     * 批量增加
     * @param count
     * @return
     */
    @ApiOperation(value = "批量添加步骤")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "count", value = "数量", required = true, dataType = "Integer")
    })
    @PostMapping("/batchAdd")
    public ResponseEntity<Object> batchAdd(int count) {

        int size = 2000;
        int j = count/size;
        if(j<=0){
            j=1;
        }

        for (int i = 0; i < j; i++) {

            taskExecutor.execute(()->{
                LocalDate localDate = LocalDate.of(2021, 8,1);
                LocalTime localTime = LocalTime.of(0,0,0);
                List<WoJcStpXmlPartition> list = new ArrayList<>();
                Long jcRid = RandomUtils.nextLong(10000, 90000000000l);
                Long woJcRid = RandomUtils.nextLong(800000, 80000000000000000l);
                String jobCard = RandomUtils.nextBytes(8).toString();

                int dayCnt = RandomUtils.nextInt(1,1000);
                localDate = localDate.plusDays(dayCnt);
                int secondsCnt = RandomUtils.nextInt(1,86400);
                localTime = localTime.plusSeconds(secondsCnt);

                LocalDateTime orderCreateTime = LocalDateTime.of(localDate, localTime);

                try {
                    WoJcStpXmlPartition woJcStpXml  = null;
                    for (int a = 0; a < size; a++) {
                        woJcStpXml = new WoJcStpXmlPartition();
                        //woJcStpXml.setRid(0L);
                        woJcStpXml.setJcRid(jcRid);
                        woJcStpXml.setWoJcRid(woJcRid);
                        woJcStpXml.setJobcard(jobCard);
                        woJcStpXml.setPStepLong("1.2");
                        woJcStpXml.setStep("1");
                        woJcStpXml.setStepLong("1.2.1");
                        woJcStpXml.setStepid(0L);
                        woJcStpXml.setParaType("TITLE");
                        woJcStpXml.setStepDesc("this is a test jobCard");
                        woJcStpXml.setStepDescLc("测试工卡");
                        woJcStpXml.setStepStatus("C");
                        woJcStpXml.setClosedBy("0558555");
                        woJcStpXml.setClosedDate(LocalDateTime.now());
                        woJcStpXml.setMechSignStatus("Y");
                        woJcStpXml.setInspSignStatus("Y");
                        woJcStpXml.setReworkFlag("");
                        woJcStpXml.setReworkNote("");
                        woJcStpXml.setNaReason("");
                        woJcStpXml.setCreateTime(LocalDateTime.now());
                        woJcStpXml.setUpdateTime(LocalDateTime.now());

                        woJcStpXml.setOrderCreateTime(orderCreateTime);

                        list.add(woJcStpXml);
                    }
                    woJcStpXmlPartitionService.insertBatch(list);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
        }
        return ResponseEntity.ok(null);
    }
}

