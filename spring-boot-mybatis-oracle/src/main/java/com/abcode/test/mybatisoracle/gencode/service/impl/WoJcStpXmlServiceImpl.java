package com.abcode.test.mybatisoracle.gencode.service.impl;

import com.abcode.test.mybatisoracle.gencode.entity.WoJcStpXml;
import com.abcode.test.mybatisoracle.gencode.dao.WoJcStpXmlDao;
import com.abcode.test.mybatisoracle.gencode.service.WoJcStpXmlService;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import javax.annotation.Resource;
import java.util.List;

/**
 * 结构化工作指令步骤表(WoJcStpXml)表服务实现类
 *
 * @author makejava
 * @since 2022-03-22 09:18:50
 */
@Service("woJcStpXmlService")
public class WoJcStpXmlServiceImpl implements WoJcStpXmlService {
    @Resource
    private WoJcStpXmlDao woJcStpXmlDao;

    /**
     * 通过ID查询单条数据
     *
     * @param rid 主键
     * @return 实例对象
     */
    @Override
    public WoJcStpXml queryById(Long rid) {
        return this.woJcStpXmlDao.queryById(rid);
    }

    /**
     * 分页查询
     *
     * @param woJcStpXml 筛选条件
     * @param pageRequest      分页对象
     * @return 查询结果
     */
    @Override
    public Page<WoJcStpXml> queryByPage(WoJcStpXml woJcStpXml, PageRequest pageRequest) {
        long total = this.woJcStpXmlDao.count(woJcStpXml);
        return new PageImpl<>(this.woJcStpXmlDao.queryAllByLimit(woJcStpXml, pageRequest), pageRequest, total);
    }

    /**
     * 新增数据
     *
     * @param woJcStpXml 实例对象
     * @return 实例对象
     */
    @Override
    public WoJcStpXml insert(WoJcStpXml woJcStpXml) {
        this.woJcStpXmlDao.insert(woJcStpXml);
        return woJcStpXml;
    }

    /**
     * 修改数据
     *
     * @param woJcStpXml 实例对象
     * @return 实例对象
     */
    @Override
    public WoJcStpXml update(WoJcStpXml woJcStpXml) {
        this.woJcStpXmlDao.update(woJcStpXml);
        return this.queryById(woJcStpXml.getRid());
    }

    /**
     * 通过主键删除数据
     *
     * @param rid 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Long rid) {
        return this.woJcStpXmlDao.deleteById(rid) > 0;
    }

    @Override
    public void insertBatch(List<WoJcStpXml> list) {
        woJcStpXmlDao.insertBatch(list);
    }
}
