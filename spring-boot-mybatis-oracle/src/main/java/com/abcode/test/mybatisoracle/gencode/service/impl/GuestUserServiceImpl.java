package com.abcode.test.mybatisoracle.gencode.service.impl;

import com.abcode.test.mybatisoracle.gencode.entity.GuestUser;
import com.abcode.test.mybatisoracle.gencode.dao.GuestUserDao;
import com.abcode.test.mybatisoracle.gencode.service.GuestUserService;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import javax.annotation.Resource;
import java.util.List;

/**
 * 用户表(GuestUser)表服务实现类
 *
 * @author makejava
 * @since 2022-03-18 11:14:52
 */
@Service("guestUserService")
public class GuestUserServiceImpl implements GuestUserService {
    @Resource
    private GuestUserDao guestUserDao;

    /**
     * 通过ID查询单条数据
     *
     * @param userId 主键
     * @return 实例对象
     */
    @Override
    public GuestUser queryById(String userId) {
        return this.guestUserDao.queryById(userId);
    }

    /**
     * 分页查询
     *
     * @param guestUser   筛选条件
     * @param pageRequest 分页对象
     * @return 查询结果
     */
    @Override
    public Page<GuestUser> queryByPage(GuestUser guestUser, PageRequest pageRequest) {
        long total = this.guestUserDao.count(guestUser);
        return new PageImpl<>(this.guestUserDao.queryAllByLimit(guestUser, pageRequest), pageRequest, total);
    }

    /**
     * 新增数据
     *
     * @param guestUser 实例对象
     * @return 实例对象
     */
    @Override
    public GuestUser insert(GuestUser guestUser) {
        this.guestUserDao.insert(guestUser);
        return guestUser;
    }

    /**
     * 修改数据
     *
     * @param guestUser 实例对象
     * @return 实例对象
     */
    @Override
    public GuestUser update(GuestUser guestUser) {
        this.guestUserDao.update(guestUser);
        return this.queryById(guestUser.getUserId());
    }

    /**
     * 通过主键删除数据
     *
     * @param userId 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(String userId) {
        return this.guestUserDao.deleteById(userId) > 0;
    }

    @Override
    public int insertBatch(List<GuestUser> entities) {
        return guestUserDao.insertBatch(entities);
    }
}
