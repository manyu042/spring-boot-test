package com.abcode.test.mybatisoracle.gencode.service;

import com.abcode.test.mybatisoracle.gencode.entity.GuestUser;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.List;

/**
 * 用户表(GuestUser)表服务接口
 *
 * @author makejava
 * @since 2022-03-18 11:14:51
 */
public interface GuestUserService {

    /**
     * 通过ID查询单条数据
     *
     * @param userId 主键
     * @return 实例对象
     */
    GuestUser queryById(String userId);

    /**
     * 分页查询
     *
     * @param guestUser   筛选条件
     * @param pageRequest 分页对象
     * @return 查询结果
     */
    Page<GuestUser> queryByPage(GuestUser guestUser, PageRequest pageRequest);

    /**
     * 新增数据
     *
     * @param guestUser 实例对象
     * @return 实例对象
     */
    GuestUser insert(GuestUser guestUser);

    /**
     * 修改数据
     *
     * @param guestUser 实例对象
     * @return 实例对象
     */
    GuestUser update(GuestUser guestUser);

    /**
     * 通过主键删除数据
     *
     * @param userId 主键
     * @return 是否成功
     */
    boolean deleteById(String userId);


    int insertBatch(List<GuestUser> entities);

}
