package com.abcode.test.mybatisoracle.gencode.service.impl;

import com.abcode.test.mybatisoracle.gencode.entity.WoJcStpXmlPartition;
import com.abcode.test.mybatisoracle.gencode.dao.WoJcStpXmlPartitionDao;
import com.abcode.test.mybatisoracle.gencode.service.WoJcStpXmlPartitionService;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import javax.annotation.Resource;
import java.util.List;

/**
 * 结构化工作指令步骤表(WoJcStpXmlPartition)表服务实现类
 *
 * @author makejava
 * @since 2022-03-22 09:18:50
 */
@Service("woJcStpXmlPartitionService")
public class WoJcStpXmlPartitionServiceImpl implements WoJcStpXmlPartitionService {
    @Resource
    private WoJcStpXmlPartitionDao woJcStpXmlPartitionDao;

    /**
     * 通过ID查询单条数据
     *
     * @param rid 主键
     * @return 实例对象
     */
    @Override
    public WoJcStpXmlPartition queryById(Long rid) {
        return this.woJcStpXmlPartitionDao.queryById(rid);
    }

    /**
     * 分页查询
     *
     * @param woJcStpXmlPartition 筛选条件
     * @param pageRequest      分页对象
     * @return 查询结果
     */
    @Override
    public Page<WoJcStpXmlPartition> queryByPage(WoJcStpXmlPartition woJcStpXmlPartition, PageRequest pageRequest) {
        long total = this.woJcStpXmlPartitionDao.count(woJcStpXmlPartition);
        return new PageImpl<>(this.woJcStpXmlPartitionDao.queryAllByLimit(woJcStpXmlPartition, pageRequest), pageRequest, total);
    }

    /**
     * 新增数据
     *
     * @param woJcStpXmlPartition 实例对象
     * @return 实例对象
     */
    @Override
    public WoJcStpXmlPartition insert(WoJcStpXmlPartition woJcStpXmlPartition) {
        this.woJcStpXmlPartitionDao.insert(woJcStpXmlPartition);
        return woJcStpXmlPartition;
    }

    /**
     * 修改数据
     *
     * @param woJcStpXmlPartition 实例对象
     * @return 实例对象
     */
    @Override
    public WoJcStpXmlPartition update(WoJcStpXmlPartition woJcStpXmlPartition) {
        this.woJcStpXmlPartitionDao.update(woJcStpXmlPartition);
        return this.queryById(woJcStpXmlPartition.getRid());
    }

    /**
     * 通过主键删除数据
     *
     * @param rid 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Long rid) {
        return this.woJcStpXmlPartitionDao.deleteById(rid) > 0;
    }

    @Override
    public void insertBatch(List<WoJcStpXmlPartition> list) {
        woJcStpXmlPartitionDao.insertBatch(list);
    }
}
