package com.abcode.test.mybatisoracle.gencode.service.impl;

import com.abcode.test.mybatisoracle.gencode.entity.TestUser;
import com.abcode.test.mybatisoracle.gencode.dao.TestUserDao;
import com.abcode.test.mybatisoracle.gencode.service.TestUserService;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import javax.annotation.Resource;
import java.util.List;

/**
 * 用户表(TestUser)表服务实现类
 *
 * @author makejava
 * @since 2022-03-21 11:52:18
 */
@Service("testUserService")
public class TestUserServiceImpl implements TestUserService {
    @Resource
    private TestUserDao testUserDao;

    /**
     * 通过ID查询单条数据
     *
     * @param userId 主键
     * @return 实例对象
     */
    @Override
    public TestUser queryById(Long userId) {
        return this.testUserDao.queryById(userId);
    }

    /**
     * 分页查询
     *
     * @param testUser 筛选条件
     * @param pageRequest      分页对象
     * @return 查询结果
     */
    @Override
    public Page<TestUser> queryByPage(TestUser testUser, PageRequest pageRequest) {
        long total = this.testUserDao.count(testUser);
        return new PageImpl<>(this.testUserDao.queryAllByLimit(testUser, pageRequest), pageRequest, total);
    }

    /**
     * 新增数据
     *
     * @param testUser 实例对象
     * @return 实例对象
     */
    @Override
    public TestUser insert(TestUser testUser) {
        this.testUserDao.insert(testUser);
        return testUser;
    }

    /**
     * 修改数据
     *
     * @param testUser 实例对象
     * @return 实例对象
     */
    @Override
    public TestUser update(TestUser testUser) {
        this.testUserDao.update(testUser);
        return this.queryById(testUser.getUserId());
    }

    /**
     * 通过主键删除数据
     *
     * @param userId 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Long userId) {
        return this.testUserDao.deleteById(userId) > 0;
    }

    @Override
    public void insertBatch(List<TestUser> userList) {
        testUserDao.insertBatch(userList);
    }
}
