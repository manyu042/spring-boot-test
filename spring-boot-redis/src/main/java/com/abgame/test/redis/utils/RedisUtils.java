package com.abgame.test.redis.utils;

import com.alibaba.fastjson.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.SetOperations;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

@SuppressWarnings({ "rawtypes", "unchecked" })
@Component
public class RedisUtils {

	@Autowired
	private RedisTemplate redisTemplate;

	public Object getJavaBean(String key) {
		return redisTemplate.opsForValue().get(key);
	}

	/**
	 * 保存java类型数据
	 * 
	 * @param key
	 * @param obj
	 */
	public void setJavaBean(String key, Object obj) {
		redisTemplate.opsForValue().set(key, obj);
	}

	/**
	 * 实现命令：DEL key，删除一个key
	 *
	 * @param key
	 */
	public void del(String key) {
		redisTemplate.delete(key);
	}

	/**
	 * set(K key, V value) 新增一个字符串类型的值,key是键，value是值。
	 */

	public void set(Object key, Object value) {
		redisTemplate.opsForValue().set(key, value);
	}

	/**
	 * get(Object key) 获取key键对应的值。
	 */

	public Object get(Object key) {
		return redisTemplate.opsForValue().get(key);
	}

	/**
	 * get(K key, long start, long end) 截取key键对应值得字符串，从开始下标位置开始到结束下标的位置(包含结束下标)的字符串。
	 */

	public String get(Object key, long start, long end) {
		return redisTemplate.opsForValue().get(key, start, end);
	}

	/**
	 * size(K key) 获取指定字符串的长度。
	 */
	public Long size(Object key) {
		ValueOperations opsForValue = redisTemplate.opsForValue();
		return opsForValue.size(key);
	}

	/**
	 * increment(K key, double delta) 以增量的方式将double值存储在变量中。
	 * 
	 */
	public double increment(Object key, double delta) {
		ValueOperations opsForValue = redisTemplate.opsForValue();
		return opsForValue.increment(key, delta);
	}

	public Long decrement(Object key, long delta) {
		ValueOperations opsForValue = redisTemplate.opsForValue();
		return opsForValue.decrement(key, delta);
	}

	/**
	 * increment(K key, long delta) 以增量的方式将long值存储在变量中。
	 * 
	 */
	public Long increment(Object key, long delta) {
		ValueOperations opsForValue = redisTemplate.opsForValue();
		return opsForValue.increment(key, delta);
	}

	/**
	 * set(K key, V value, long timeout, TimeUnit unit) 设置变量值的过期时间。。
	 * 
	 */
	public void set(Object key, Object value, long timeout, TimeUnit unit) {
		ValueOperations opsForValue = redisTemplate.opsForValue();
		opsForValue.set(key, value, timeout, unit);
	}

	/**
	 * 保存复杂类型数据到缓存（并设置失效时间）
	 * 
	 * @param key
	 * @param Object
	 * @param seconds
	 * @return
	 */
	public void setBean(String key, Object obj, int seconds) {
		redisTemplate.opsForValue().set(key, JSON.toJSONString(obj), seconds, TimeUnit.SECONDS);
	}

	/**
	 * 保存复杂类型数据到缓存
	 *
	 * @param key
	 * @param obj
	 * @return
	 */
	public void setBean(String key, Object obj) {
		redisTemplate.opsForValue().set(key, JSON.toJSONString(obj));
	}

	/**
	 * 取得复杂类型数据
	 *
	 * @param key
	 * @param obj
	 * @param clazz
	 * @return
	 */
	public <T> T getBean(String key, Class<T> clazz) {
		String value = (String) redisTemplate.opsForValue().get(key);
		if (value == null) {
			return null;
		}
		return JSON.parseObject(value, clazz);
	}

	/**
	 * @Title: delete
	 * @Description: 删除key
	 * @param @param key
	 * @param @return 参数
	 * @return boolean 返回类型
	 */
	public boolean delete(String key) {
		return redisTemplate.delete(key);
	}

	/**
	 * set(K key, V value, long offset) 覆盖从指定位置开始的值。
	 * 
	 */
	public void set(Object key, Object value, long offset) {
		ValueOperations opsForValue = redisTemplate.opsForValue();
		opsForValue.set(key, value, offset);
	}

	/**
	 * multiSet(Map<? extends K,? extends V> map) 设置map集合到redis。
	 * 
	 */
	public void multiSet(Map<Object, Object> map) {
		ValueOperations opsForValue = redisTemplate.opsForValue();
		opsForValue.multiSet(map);
	}

	/**
	 * multiGet(Collection<K> keys) 根据集合取出对应的value值。
	 * 
	 */
	public List<String> multiGet(Collection<Object> keys) {
		ValueOperations opsForValue = redisTemplate.opsForValue();
		return opsForValue.multiGet(keys);
	}

	/**
	 * multiSetIfAbsent(Map<? extends K,? extends V> map)
	 * 如果对应的map集合名称不存在，则添加，如果存在则不做修改。
	 * 
	 */
	public void multiSetIfAbsent(Map<Object, Object> map) {
		ValueOperations opsForValue = redisTemplate.opsForValue();
		opsForValue.multiSetIfAbsent(map);
	}

	/**
	 * @Title: addSet
	 * @Description: 向变量中批量添加值
	 * @param key
	 * @param values
	 * @return long 返回影响条数
	 */
	public long addSet(String key, Object... values) {
		SetOperations opsForSet = redisTemplate.opsForSet();
		return opsForSet.add(key, values);
	}

	/**
	 * @Title: members
	 * @Description: 获取变量中的值 void
	 */
	public Set members(String key) {
		return redisTemplate.opsForSet().members(key);
	}

	/**   
	 * @Title: removeSet   
	 * @Description: 删除set里面的元素
	 * @param string
	 * @param orderid
	 * void
	 */
	public long removeSet(String key, Object... values) {
		return redisTemplate.opsForSet().remove(key, values);
	}

	/**
	 * 返回当前key所对应的剩余过期时间
	 * @param key
	 * @param unit
	 * @return
	 */
	public Long getExpire(String key, TimeUnit unit) {
		return redisTemplate.getExpire(key, unit);
	}


}
