package com.abcode.test.oracletest.common.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 字段校验注解对象(String类型的参数可以不加上此注解)
 *
 * @author qinzhitao
 * @date 2023/3/7 14:01
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface FieldInfo {
    /**
     * 字段代码
     *
     * @return
     */
    String fieldCode() default "";

    /**
     * 字段名称
     *
     * @return
     */
    String fieldText() default "";

    /**
     * 虚拟字段（表里没有对应的字段）
     * @return
     */
    boolean virtualField() default false;
}
