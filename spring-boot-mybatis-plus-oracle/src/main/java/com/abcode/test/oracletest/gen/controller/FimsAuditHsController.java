package com.abcode.test.oracletest.gen.controller;



import com.abcode.test.oracletest.Result;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.io.Serializable;
import java.util.List;

import com.abcode.test.oracletest.gen.entity.FimsAuditHs;
import com.abcode.test.oracletest.gen.service.FimsAuditHsService;


/**
 * 审计信息(FimsAuditHs)表控制层
 *
 * @author qinzhitao
 * @since 2023-05-05 10:59:09
 */
@RestController
@RequestMapping("fimsAuditHs")
public class FimsAuditHsController {
    /**
     * 服务对象
     */
    @Resource
    private FimsAuditHsService fimsAuditHsService;

    /**
     * 分页查询所有数据
     * @param page 分页对象
     * @param fimsAuditHs 查询实体
     * @return 所有数据
     */
    @GetMapping("/selectAll")
    public Result selectAll(Page<FimsAuditHs> page, FimsAuditHs fimsAuditHs) {
        Page<FimsAuditHs> rst = this.fimsAuditHsService.page(page, new QueryWrapper<>(fimsAuditHs));
        return Result.ok(rst);
    }

    /**
     * 通过主键查询单条数据
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("/selectOne/{id}")
    public Result selectOne(@PathVariable Serializable id) {
        FimsAuditHs fimsAuditHs = this.fimsAuditHsService.getById(id);
        return Result.ok(fimsAuditHs);
    }

    /**
     * 新增数据
     * @param fimsAuditHs 实体对象
     * @return 新增结果
     */
    @PostMapping("/insert")
    public Result insert(@RequestBody FimsAuditHs fimsAuditHs) {
        for (int i = 0; i < 100; i++) {
            int temp = 100000 + i;
            fimsAuditHs.setFlightId((long) temp);
            fimsAuditHs.setId(null);
            Boolean rst = this.fimsAuditHsService.save(fimsAuditHs);

        }

        return Result.OK(true);
    }

    /**
     * 修改数据
     * @param fimsAuditHs 实体对象
     * @return 修改结果
     */
    @PutMapping("/update")
    public Result update(@RequestBody FimsAuditHs fimsAuditHs) {
        Boolean rst = this.fimsAuditHsService.updateById(fimsAuditHs);
        return Result.OK(rst);
    }

    /**
     * 删除数据
     *
     * @param idList 主键结合
     * @return 删除结果
     */
    @DeleteMapping("/delete")
    public Result delete(@RequestParam("idList") List<Long> idList) {
        boolean rst = this.fimsAuditHsService.removeByIds(idList);
        return Result.ok(rst);
    }
}

