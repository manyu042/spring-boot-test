package com.abcode.test.oracletest.gen.service.impl;


import com.abcode.test.oracletest.Result;
import com.abcode.test.oracletest.common.utils.BatchOperateDbUtil;
import com.abcode.test.oracletest.gen.entity.FimsFlightShare;
import com.abcode.test.oracletest.gen.mapper.FimsFlightShareMapper;
import com.abcode.test.oracletest.gen.param.FimsFlightShareParam;
import com.abcode.test.oracletest.gen.service.FimsFlightShareService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


/**
 * 动态共享航班表(FimsFlightShare)表服务实现类
 *
 * @author qinzhitao
 * @since 2023-08-19 22:02:35
 */
@Service
public class FimsFlightShareServiceImpl extends ServiceImpl<FimsFlightShareMapper, FimsFlightShare> implements FimsFlightShareService {
    @Resource
    FimsFlightShareMapper fimsFlightShareMapper;

    @Override
    public IPage pageQuery(FimsFlightShareParam param) {
        FimsFlightShare fimsFlightShare = param.toEntity();
        Page page = param.getPage();
        LambdaQueryWrapper queryWrapper = new LambdaQueryWrapper<>(fimsFlightShare);
        IPage selectPage = this.page(page, queryWrapper);
        return selectPage;
    }

    @Override
    public Result add(FimsFlightShareParam param) {
        FimsFlightShare fimsFlightShare = param.toEntity();
        boolean resultFlag = this.save(fimsFlightShare);
        if (resultFlag) {
            return Result.OK();
        } else {
            return Result.error(Result.ERROR_MESSAGE);
        }
    }

    @Override
    public Result edit(FimsFlightShareParam param) {
        FimsFlightShare fimsFlightShare = param.toEntity();
        boolean resultFlag = this.updateById(fimsFlightShare);
        if (resultFlag) {
            return Result.OK(true);
        } else {
            return Result.error(Result.ERROR_MESSAGE, false);
        }
    }

    @Override
    public List<FimsFlightShare> listQuery(FimsFlightShareParam param) {
        FimsFlightShare fimsFlightShare = param.toEntity();
        LambdaQueryWrapper queryWrapper = new LambdaQueryWrapper(fimsFlightShare);
        return this.list(queryWrapper);
    }

    @Override
    public Result batchAdd() {
        List<FimsFlightShare> list = new ArrayList<>();
        long fltId = System.currentTimeMillis();
        int count = 4000;
        for (int i = 0; i < count; i++) {
            FimsFlightShare entity = new FimsFlightShare();
            //entity.setId(IdWorker.getId());
            entity.setFlightId(fltId);
            entity.setAirlinesIata("iata_");
            entity.setFlightNo("no_");
            entity.setOrderCode(1);
            entity.setDelFlag(0);
            entity.setCreateBy("test");
            entity.setCreateTime(LocalDateTime.now());
            entity.setUpdateBy("test");
            entity.setUpdateTime(LocalDateTime.now());

            list.add(entity);
        }
        //BatchOperateDbUtil.batchExecute(this::addList, list);
       // BatchOperateDbUtil.batchExecute(this::saveBatch, list);
        addList(list);

        return Result.ok();
    }

    private void addList(List<FimsFlightShare> list){
        fimsFlightShareMapper.batchAddEntity(list);
    }
}

