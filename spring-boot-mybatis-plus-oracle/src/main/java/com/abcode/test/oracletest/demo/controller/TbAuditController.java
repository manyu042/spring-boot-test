package com.abcode.test.oracletest.demo.controller;


import com.abcode.test.oracletest.Result;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.abcode.test.oracletest.demo.entity.TbAudit;
import com.abcode.test.oracletest.demo.service.TbAuditService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 审计信息(TbAudit)表控制层
 *
 * @author qinzhitao
 * @since 2023-03-14 14:46:55
 */
@RestController
@RequestMapping("tbAudit")
public class TbAuditController{
    /**
     * 服务对象
     */
    @Resource
    private TbAuditService tbAuditService;

    /**
     * 分页查询所有数据
     *
     * @param page    分页对象
     * @param tbAudit 查询实体
     * @return 所有数据
     */
    @GetMapping
    public Result selectAll(Page<TbAudit> page, TbAudit tbAudit) {
        return Result.ok(this.tbAuditService.page(page, new QueryWrapper<>(tbAudit)));
    }

    /**
     * 新增数据
     *
     * @param username 实体对象
     * @return 新增结果
     */
    @GetMapping("add")
    public Result insert(@RequestParam String username) {
        TbAudit tbAudit = new TbAudit();
        tbAudit.setJobNo("sdfdsf");
        tbAudit.setUsername(username);
        tbAudit.setRoleName("rolename");
        tbAudit.setActionTime(new Date());
        tbAudit.setActionTableName("sdfdsf");
        tbAudit.setActionType("sdfdsf");
        tbAudit.setFlightId(6666L);
        tbAudit.setFlightNo("CZsfdsf");
        tbAudit.setOffInFlag("s");
        tbAudit.setDescription("sdfdsf");
        tbAudit.setMsgType("sdfds");
        tbAudit.setAirlinesIata("sdfdsf");
        tbAudit.setSubtableType("sdfsd");
        tbAudit.setDelFlag(true);
        tbAudit.setCreateBy("auto");
        tbAudit.setCreateTime(new Date());
        tbAudit.setUpdateBy("sdfds");
        tbAudit.setUpdateTime(LocalDateTime.now());

        List<TbAudit> tbAuditList = new ArrayList<>();
        tbAuditList.add(tbAudit);
        TbAudit tbAudit2 = new TbAudit();
        tbAudit2.setJobNo("sdfdsf");
        tbAudit2.setUsername(username);
        tbAudit2.setRoleName("rolename22");
        tbAudit2.setActionTime(new Date());
        tbAudit2.setActionTableName("sdfdsf");
        tbAudit2.setActionType("sdfdsf");
        tbAudit2.setFlightId(6666L);
        tbAudit2.setFlightNo("CZsfdsf");
        tbAudit2.setOffInFlag("s");
        tbAudit2.setDescription("sdfdsf");
        tbAudit2.setMsgType("sdfds");
        tbAudit2.setAirlinesIata("sdfdsf");
        tbAudit2.setSubtableType("sdfsd");
        tbAudit2.setDelFlag(true);
        tbAudit2.setCreateBy("auto");
        tbAudit2.setCreateTime(new Date());
        tbAudit2.setUpdateBy("sdfds");
        tbAudit2.setUpdateTime(LocalDateTime.now());
        tbAuditList.add(tbAudit2);
        //tbAudit.setId((long) new Random().nextInt(1000000));
        boolean rst = this.tbAuditService.saveBatch(tbAuditList);
        return Result.ok(rst);
       // return Result.ok(this.tbAuditService.save(tbAudit));
    }

}

