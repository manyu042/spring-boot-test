package com.abcode.test.oracletest.gen.param;

import com.abcode.test.oracletest.common.validate.StrLength;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.abcode.test.oracletest.common.annotation.FieldInfo;

import javax.validation.constraints.NotNull;
import javax.validation.groups.Default;

import lombok.Data;
import com.abcode.test.oracletest.gen.entity.FimsFlightShare;
import com.abcode.test.oracletest.common.PageParam;

/**
 * 动态共享航班表(FimsFlightShare)参数类
 *
 * @author qinzhitao
 * @since 2023-08-19 22:02:35
 */
@ApiModel("动态共享航班表参数实体")
@Data
@SuppressWarnings("serial")
public class FimsFlightShareParam extends PageParam {

    public interface QueryGroup {
    }

    public interface EditGroup extends Default {
    }

    public interface AddGroup extends Default {
    }

    /**
     * ID
     */
    @NotNull(message = "【ID】不能为NUll", groups = EditGroup.class)
    @ApiModelProperty("ID")
    @FieldInfo(fieldCode = "id", fieldText = "ID")
    private Long id;

    /**
     * 航班ID
     */
    @ApiModelProperty("航班ID")
    @FieldInfo(fieldCode = "flightId", fieldText = "航班ID")
    private Long flightId;

    /**
     * 航空公司二字码
     */
    @StrLength(min = 1, max = 8, message = "【航空公司二字码】取值的长度在{min}至{max}个字符之间")
    @ApiModelProperty("航空公司二字码")
    @FieldInfo(fieldCode = "airlinesIata", fieldText = "航空公司二字码")
    private String airlinesIata;

    /**
     * 航班号
     */
    @StrLength(min = 1, max = 16, message = "【航班号】取值的长度在{min}至{max}个字符之间")
    @ApiModelProperty("航班号")
    @FieldInfo(fieldCode = "flightNo", fieldText = "航班号")
    private String flightNo;

    /**
     * 排序编码
     */
    @ApiModelProperty("排序编码")
    @FieldInfo(fieldCode = "orderCode", fieldText = "排序编码")
    private Integer orderCode;


    /**
     * 构建数据库实体类
     *
     * @return
     */
    public FimsFlightShare toEntity() {
        FimsFlightShare entity = new FimsFlightShare();

        entity.setId(this.id);
        entity.setFlightId(this.flightId);
        entity.setAirlinesIata(this.airlinesIata);
        entity.setFlightNo(this.flightNo);
        entity.setOrderCode(this.orderCode);

        return entity;
    }

}
