package com.abcode.test.oracletest.gen.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.abcode.test.oracletest.gen.entity.FimsFlightShareHs;

/**
 * 动态共享航班表(FimsFlightShareHs)表服务接口
 *
 * @author qinzhitao
 * @since 2023-05-05 11:40:05
 */
public interface FimsFlightShareHsService extends IService<FimsFlightShareHs> {

}

