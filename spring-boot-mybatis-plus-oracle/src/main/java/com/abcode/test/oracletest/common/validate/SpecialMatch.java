package com.abcode.test.oracletest.common.validate;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * @author qzt
 * @Date 2023-07-06
 * @Description 校验特殊字符注解
 */
@Target({FIELD, PARAMETER})
@Retention(RUNTIME)
@Documented
@Constraint(validatedBy = {SpecialCharacterValidator.class})
public @interface SpecialMatch {
    String message() default "不能包含除'-''_'外的特殊字符"; // 匹配失败提示消息内容
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
