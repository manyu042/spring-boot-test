package com.abcode.test.oracletest.gen.controller;


import cn.hutool.core.util.ObjectUtil;
import com.abcode.test.oracletest.PageUtil;
import com.abcode.test.oracletest.Result;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;

import javax.annotation.Resource;

import com.abcode.test.oracletest.gen.entity.FimsFlightShare;
import com.abcode.test.oracletest.gen.param.FimsFlightShareParam;
import com.abcode.test.oracletest.gen.vo.FimsFlightShareVO;
import com.abcode.test.oracletest.gen.service.FimsFlightShareService;


import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.util.CollectionUtils;

/**
 * 动态共享航班表(FimsFlightShare)表控制层
 *
 * @author qinzhitao
 * @since 2023-08-19 22:02:34
 */
@Slf4j
@Api(tags = "动态共享航班表")
@RestController
@RequestMapping("fimsFlightShare")
public class FimsFlightShareController {

    @Resource
    private FimsFlightShareService fimsFlightShareService;

    /**
     * 分页查询动态共享航班表
     *
     * @param param 参数对象
     * @return
     */
    @ApiOperation(value = "分页查询动态共享航班表", notes = "分页查询动态共享航班表")
    @PostMapping("/pageQuery")
    public Result<IPage<FimsFlightShareVO>> pageQuery(@RequestBody @Validated(FimsFlightShareParam.QueryGroup.class) FimsFlightShareParam param) {
        //分页查询
        IPage selectPage = this.fimsFlightShareService.pageQuery(param);
        //转化
        IPage resultPage = PageUtil.convert(FimsFlightShareVO.class, selectPage);
        return Result.ok(resultPage);
    }

    /**
     * 条件查询动态共享航班表
     *
     * @param param 参数对象
     * @return
     */
    @ApiOperation(value = "条件查询动态共享航班表", notes = "条件查询动态共享航班表")
    @PostMapping("/listQuery")
    public Result<List<FimsFlightShareVO>> listQuery(@RequestBody @Validated(FimsFlightShareParam.QueryGroup.class) FimsFlightShareParam param) {
        List<FimsFlightShare> selectList = this.fimsFlightShareService.listQuery(param);
        List<FimsFlightShareVO> list = selectList.stream().map(FimsFlightShareVO::new).collect(Collectors.toList());
        return Result.ok(list);
    }

    /**
     * 通过主键查询单条数据
     *
     * @param param 主键
     * @return 单条数据
     */
    @ApiOperation(value = "通过主键查询单条数据", notes = "通过主键查询单条数据")
    @PostMapping("/queryById")
    public Result<FimsFlightShareVO> selectOne(@RequestBody @Validated(FimsFlightShareParam.QueryGroup.class) FimsFlightShareParam param) {
        FimsFlightShare fimsFlightShare = this.fimsFlightShareService.getById(param.getId());
        FimsFlightShareVO fimsFlightShareVO = null;
        if (ObjectUtil.isNotEmpty(fimsFlightShare)) {
            fimsFlightShareVO = new FimsFlightShareVO(fimsFlightShare);
        }
        return Result.ok(fimsFlightShareVO);
    }

    /**
     * 新增动态共享航班表
     *
     * @param param 参数对象
     * @return
     */
    @ApiOperation(value = "新增动态共享航班表", notes = "新增动态共享航班表")
    @PostMapping("/add")
    public Result<String> insert(@RequestBody @Validated(FimsFlightShareParam.AddGroup.class) FimsFlightShareParam param) {
        Result rst = this.fimsFlightShareService.add(param);
        return rst;
    }

    /**
     * 修改动态共享航班表
     *
     * @param param 参数对象
     * @return 修改结果
     */
    @ApiOperation(value = "修改动态共享航班表", notes = "修改动态共享航班表")
    @PostMapping("/edit")
    public Result update(@RequestBody @Validated(FimsFlightShareParam.EditGroup.class) FimsFlightShareParam param) {
        Result rst = this.fimsFlightShareService.edit(param);
        return rst;
    }

    /**
     * 批量删除数据
     *
     * @param idListMap 参数对象
     * @return 删除结果
     */
    @ApiOperation(value = "批量删除数据", notes = "批量删除数据")
    @PostMapping("/delete")
    public Result<Boolean> delete(@RequestBody Map<String, Object> idListMap) {
        String idKey = "ids";
        if (CollectionUtils.isEmpty(idListMap) || ObjectUtils.isEmpty(idListMap.get(idKey))) {
            return Result.error("参数不能为空");
        }
        List<String> idList = (List<String>) idListMap.get(idKey);
        boolean resultFlag = this.fimsFlightShareService.removeBatchByIds(idList);
        if (resultFlag) {
            return Result.OK(true);
        } else {
            return Result.error(Result.ERROR_MESSAGE, false);
        }
    }
    @PostMapping("/batchAdd")
    public Result batchAdd(){
        LocalDateTime startTime = LocalDateTime.now();
        Result result = this.fimsFlightShareService.batchAdd();
        log.info("|batchAdd|用时：{} 秒", Duration.between(startTime, LocalDateTime.now()).getSeconds());
        return  result;
    }
}

