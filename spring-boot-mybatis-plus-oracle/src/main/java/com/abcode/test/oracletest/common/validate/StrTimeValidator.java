package com.abcode.test.oracletest.common.validate;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Pattern;

/**
 * @author qzt
 * @Date 2023-07-31
 * @Description 匹配规则
 */
public class StrTimeValidator implements ConstraintValidator<StrTime, String> {
    static String dateTimeRegex = "^((([0-9]{3}[1-9]|[0-9]{2}[1-9][0-9]{1}|[0-9]{1}[1-9][0-9]{2}|[1-9][0-9]{3})-(((0[13578]|1[02])-(0[1-9]|[12][0-9]|3[01]))|((0[469]|11)-(0[1-9]|[12][0-9]|30))|(02-(0[1-9]|[1][0-9]|2[0-8]))))|((([0-9]{2})(0[48]|[2468][048]|[13579][26])|((0[48]|[2468][048]|[3579][26])00))-02-29))\\s+([0-1]?[0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])$";
    static String timeRegex = "^([0-1]?[0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])$";
    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (value != null) {
            return Pattern.matches(dateTimeRegex, value) || Pattern.matches(timeRegex, value) ;
        }
        return true;
    }

    /**
     * 判断字符串是否是时间格式：HH:mm:ss
     * @param timeStr
     * @return
     */
    public static boolean isTimeStr(String timeStr){
        return Pattern.matches(timeRegex, timeStr);
    }

    /**
     * 判断字符串是否是时间格式：HH:mm:ss
     * @param timeStr
     * @return
     */
    public static boolean isDateTimeStr(String timeStr){
        return Pattern.matches(dateTimeRegex, timeStr);
    }
}
