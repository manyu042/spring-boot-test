package com.abcode.test.oracletest.common.validate;

/**
 * 定义EnumValidator接口，让需要校验的枚举类实现其接口的getValue()方法
 * @author qzt
 */
public interface EnumValidator {
    Object getValue();
}
