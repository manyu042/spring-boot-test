package com.abcode.test.oracletest.gen.mapper;

import com.abcode.test.oracletest.Result;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.abcode.test.oracletest.gen.entity.FimsFlightShare;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 动态共享航班表(FimsFlightShare)表数据库访问层
 *
 * @author qinzhitao
 * @since 2023-08-19 22:02:35
 */
public interface FimsFlightShareMapper extends BaseMapper<FimsFlightShare> {
    void batchAddEntity(@Param("list") List<FimsFlightShare> list);
}

