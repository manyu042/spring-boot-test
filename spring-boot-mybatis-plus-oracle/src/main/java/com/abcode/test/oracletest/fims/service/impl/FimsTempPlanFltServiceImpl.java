package com.abcode.test.oracletest.fims.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.abcode.test.oracletest.fims.mapper.FimsTempPlanFltMapper;
import com.abcode.test.oracletest.fims.entity.FimsTempPlanFlt;
import com.abcode.test.oracletest.fims.service.FimsTempPlanFltService;
import org.springframework.stereotype.Service;

/**
 * 临时计划航班表(FimsTempPlanFlt)表服务实现类
 *
 * @author qinzhitao
 * @since 2023-06-29 16:39:47
 */
@Service
public class FimsTempPlanFltServiceImpl extends ServiceImpl<FimsTempPlanFltMapper, FimsTempPlanFlt> implements FimsTempPlanFltService {

}

