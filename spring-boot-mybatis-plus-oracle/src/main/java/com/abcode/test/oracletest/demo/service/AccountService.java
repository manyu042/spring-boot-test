package com.abcode.test.oracletest.demo.service;

import com.abcode.test.oracletest.demo.entity.Account;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 用户表(Account)表服务接口
 *
 * @author abcode
 * @since 2021-01-22 20:37:45
 */
public interface AccountService extends IService<Account> {

}