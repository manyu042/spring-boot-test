package com.abcode.test.oracletest.gen.param;

import java.time.LocalDateTime;

import com.abcode.test.oracletest.common.validate.StrLength;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.abcode.test.oracletest.common.annotation.FieldInfo;

import javax.validation.constraints.NotNull;
import javax.validation.groups.Default;

import lombok.Data;
import com.abcode.test.oracletest.gen.entity.FimsFlightAirline;
import com.abcode.test.oracletest.common.PageParam;

/**
 * 动态经停站（航线）表(FimsFlightAirline)参数类
 *
 * @author qinzhitao
 * @since 2023-08-19 22:02:20
 */
@ApiModel("动态经停站（航线）表参数实体")
@Data
@SuppressWarnings("serial")
public class FimsFlightAirlineParam extends PageParam {

    public interface QueryGroup {
    }

    public interface EditGroup extends Default {
    }

    public interface AddGroup extends Default {
    }

    /**
     * ID
     */
    @NotNull(message = "【ID】不能为NUll", groups = EditGroup.class)
    @ApiModelProperty("ID")
    @FieldInfo(fieldCode = "id", fieldText = "ID")
    private Long id;

    /**
     * 航班ID
     */
    @ApiModelProperty("航班ID")
    @FieldInfo(fieldCode = "flightId", fieldText = "航班ID")
    private Long flightId;

    /**
     * 排序编码
     */
    @ApiModelProperty("排序编码")
    @FieldInfo(fieldCode = "orderCode", fieldText = "排序编码")
    private Integer orderCode;

    /**
     * 经停站（机场）三字码
     */
    @StrLength(min = 1, max = 8, message = "【经停站（机场）三字码】取值的长度在{min}至{max}个字符之间")
    @ApiModelProperty("经停站（机场）三字码")
    @FieldInfo(fieldCode = "airportIata", fieldText = "经停站（机场）三字码")
    private String airportIata;

    /**
     * 计划起飞时间（如果航班为出港航班，第一航站（始发站）计划起飞时间必须有值）
     */
    @ApiModelProperty("计划起飞时间（如果航班为出港航班，第一航站（始发站）计划起飞时间必须有值）")
    @FieldInfo(fieldCode = "std", fieldText = "计划起飞时间（如果航班为出港航班，第一航站（始发站）计划起飞时间必须有值）")
    private LocalDateTime std;

    /**
     * 预计起飞时间
     */
    @ApiModelProperty("预计起飞时间")
    @FieldInfo(fieldCode = "etd", fieldText = "预计起飞时间")
    private LocalDateTime etd;

    /**
     * 实际起飞时间
     */
    @ApiModelProperty("实际起飞时间")
    @FieldInfo(fieldCode = "atd", fieldText = "实际起飞时间")
    private LocalDateTime atd;

    /**
     * 计划降落时间（如果航班为进港航班，最后一个航站（目的站）计划降落时间必须有值）
     */
    @ApiModelProperty("计划降落时间（如果航班为进港航班，最后一个航站（目的站）计划降落时间必须有值）")
    @FieldInfo(fieldCode = "sta", fieldText = "计划降落时间（如果航班为进港航班，最后一个航站（目的站）计划降落时间必须有值）")
    private LocalDateTime sta;

    /**
     * 预计降落时间
     */
    @ApiModelProperty("预计降落时间")
    @FieldInfo(fieldCode = "eta", fieldText = "预计降落时间")
    private LocalDateTime eta;

    /**
     * 实际降落时间
     */
    @ApiModelProperty("实际降落时间")
    @FieldInfo(fieldCode = "ata", fieldText = "实际降落时间")
    private LocalDateTime ata;

    /**
     * 属性 2403-国内、2401-国际、2404-混合、2402-地区
     */
    @StrLength(min = 1, max = 8, message = "【属性 2403-国内、2401-国际、2404-混合、2402-地区】取值的长度在{min}至{max}个字符之间")
    @ApiModelProperty("属性 2403-国内、2401-国际、2404-混合、2402-地区")
    @FieldInfo(fieldCode = "attribute", fieldText = "属性 2403-国内、2401-国际、2404-混合、2402-地区")
    private String attribute;

    /**
     * 备注
     */
    @StrLength(min = 1, max = 64, message = "【备注】取值的长度在{min}至{max}个字符之间")
    @ApiModelProperty("备注")
    @FieldInfo(fieldCode = "remark", fieldText = "备注")
    private String remark;


    /**
     * 构建数据库实体类
     *
     * @return
     */
    public FimsFlightAirline toEntity() {
        FimsFlightAirline entity = new FimsFlightAirline();

        entity.setId(this.id);
        entity.setFlightId(this.flightId);
        entity.setOrderCode(this.orderCode);
        entity.setAirportIata(this.airportIata);
        entity.setStd(this.std);
        entity.setEtd(this.etd);
        entity.setAtd(this.atd);
        entity.setSta(this.sta);
        entity.setEta(this.eta);
        entity.setAta(this.ata);
        entity.setAttribute(this.attribute);

        return entity;
    }

}
