package com.abcode.test.oracletest.gen.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.abcode.test.oracletest.gen.entity.FimsFlightInfoHs;

/**
 * 航班动态表(FimsFlightInfoHs)表服务接口
 *
 * @author qinzhitao
 * @since 2023-05-04 17:01:59
 */
public interface FimsFlightInfoHsService extends IService<FimsFlightInfoHs> {

}

