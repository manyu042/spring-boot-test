package com.abcode.test.oracletest.gen.mapper;

import com.abcode.test.oracletest.common.MyBaseMapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.abcode.test.oracletest.gen.entity.FimsFlightAirline;

/**
 * 动态经停站（航线）表(FimsFlightAirline)表数据库访问层
 *
 * @author qinzhitao
 * @since 2023-08-19 22:02:20
 */
public interface FimsFlightAirlineMapper extends MyBaseMapper<FimsFlightAirline> {

}

