package com.abcode.test.oracletest.gen.controller;


import cn.hutool.core.util.ObjectUtil;
import com.abcode.test.oracletest.PageUtil;
import com.abcode.test.oracletest.Result;
import com.alibaba.druid.sql.visitor.functions.Locate;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;

import javax.annotation.Resource;

import com.abcode.test.oracletest.gen.entity.FimsFlightAirline;
import com.abcode.test.oracletest.gen.param.FimsFlightAirlineParam;
import com.abcode.test.oracletest.gen.vo.FimsFlightAirlineVO;
import com.abcode.test.oracletest.gen.service.FimsFlightAirlineService;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.util.CollectionUtils;

/**
 * 动态经停站（航线）表(FimsFlightAirline)表控制层
 *
 * @author qinzhitao
 * @since 2023-08-19 22:02:18
 */
@Slf4j
@Api(tags = "动态经停站（航线）表")
@RestController
@RequestMapping("fimsFlightAirline")
public class FimsFlightAirlineController {

    @Resource
    private FimsFlightAirlineService fimsFlightAirlineService;

    /**
     * 分页查询动态经停站（航线）表
     *
     * @param param 参数对象
     * @return
     */
    @ApiOperation(value = "分页查询动态经停站（航线）表", notes = "分页查询动态经停站（航线）表")
    @PostMapping("/pageQuery")
    public Result<IPage<FimsFlightAirlineVO>> pageQuery(@RequestBody @Validated(FimsFlightAirlineParam.QueryGroup.class) FimsFlightAirlineParam param) {
        //分页查询
        IPage selectPage = this.fimsFlightAirlineService.pageQuery(param);
        //转化
        IPage resultPage = PageUtil.convert(FimsFlightAirlineVO.class, selectPage);
        return Result.ok(resultPage);
    }

    /**
     * 条件查询动态经停站（航线）表
     *
     * @param param 参数对象
     * @return
     */
    @ApiOperation(value = "条件查询动态经停站（航线）表", notes = "条件查询动态经停站（航线）表")
    @PostMapping("/listQuery")
    public Result<List<FimsFlightAirlineVO>> listQuery(@RequestBody @Validated(FimsFlightAirlineParam.QueryGroup.class) FimsFlightAirlineParam param) {
        List<FimsFlightAirline> selectList = this.fimsFlightAirlineService.listQuery(param);
        List<FimsFlightAirlineVO> list = selectList.stream().map(FimsFlightAirlineVO::new).collect(Collectors.toList());
        return Result.ok(list);
    }

    /**
     * 通过主键查询单条数据
     *
     * @param param 主键
     * @return 单条数据
     */
    @ApiOperation(value = "通过主键查询单条数据", notes = "通过主键查询单条数据")
    @PostMapping("/queryById")
    public Result<FimsFlightAirlineVO> selectOne(@RequestBody @Validated(FimsFlightAirlineParam.QueryGroup.class) FimsFlightAirlineParam param) {
        FimsFlightAirline fimsFlightAirline = this.fimsFlightAirlineService.getById(param.getId());
        FimsFlightAirlineVO fimsFlightAirlineVO = null;
        if (ObjectUtil.isNotEmpty(fimsFlightAirline)) {
            fimsFlightAirlineVO = new FimsFlightAirlineVO(fimsFlightAirline);
        }
        return Result.ok(fimsFlightAirlineVO);
    }

    /**
     * 新增动态经停站（航线）表
     *
     * @param param 参数对象
     * @return
     */
    @ApiOperation(value = "新增动态经停站（航线）表", notes = "新增动态经停站（航线）表")
    @PostMapping("/add")
    public Result<String> insert(@RequestBody @Validated(FimsFlightAirlineParam.AddGroup.class) FimsFlightAirlineParam param) {
        Result rst = this.fimsFlightAirlineService.add(param);
        return rst;
    }

    /**
     * 修改动态经停站（航线）表
     *
     * @param param 参数对象
     * @return 修改结果
     */
    @ApiOperation(value = "修改动态经停站（航线）表", notes = "修改动态经停站（航线）表")
    @PostMapping("/edit")
    public Result<Boolean> update(@RequestBody @Validated(FimsFlightAirlineParam.EditGroup.class) FimsFlightAirlineParam param) {
        Result rst = this.fimsFlightAirlineService.edit(param);
        return rst;
    }

    /**
     * 批量删除数据
     *
     * @param idListMap 参数对象
     * @return 删除结果
     */
    @ApiOperation(value = "批量删除数据", notes = "批量删除数据")
    @PostMapping("/delete")
    public Result<Boolean> delete(@RequestBody Map<String, Object> idListMap) {
        String idKey = "ids";
        if (CollectionUtils.isEmpty(idListMap) || ObjectUtils.isEmpty(idListMap.get(idKey))) {
            return Result.error("参数不能为空");
        }
        List<String> idList = (List<String>) idListMap.get(idKey);
        boolean resultFlag = this.fimsFlightAirlineService.removeBatchByIds(idList);
        if (resultFlag) {
            return Result.OK(true);
        } else {
            return Result.error(Result.ERROR_MESSAGE, false);
        }
    }

    @PostMapping("/batchAdd")
    public Result<Boolean> batchAdd() {
        LocalDateTime startTime = LocalDateTime.now();
        Result rst = this.fimsFlightAirlineService.batchAdd();
        log.info("|batchAdd|用时：{} 秒", Duration.between(startTime, LocalDateTime.now()).getSeconds());
        return rst;
    }
}

