package com.abcode.test.oracletest.gen.controller;


import cn.hutool.core.util.ObjectUtil;
import com.abcode.test.oracletest.PageUtil;
import com.abcode.test.oracletest.Result;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;

import javax.annotation.Resource;

import com.abcode.test.oracletest.gen.entity.FimsFlightInfo;
import com.abcode.test.oracletest.gen.param.FimsFlightInfoParam;
import com.abcode.test.oracletest.gen.vo.FimsFlightInfoVO;
import com.abcode.test.oracletest.gen.service.FimsFlightInfoService;


import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.util.CollectionUtils;

/**
 * 航班动态表(FimsFlightInfo)表控制层
 *
 * @author qinzhitao
 * @since 2023-08-19 22:02:22
 */
@Slf4j
@Api(tags = "航班动态表")
@RestController
@RequestMapping("fimsFlightInfo")
public class FimsFlightInfoController {

    @Resource
    private FimsFlightInfoService fimsFlightInfoService;

    /**
     * 分页查询航班动态表
     *
     * @param param 参数对象
     * @return
     */
    @ApiOperation(value = "分页查询航班动态表", notes = "分页查询航班动态表")
    @PostMapping("/pageQuery")
    public Result<IPage<FimsFlightInfoVO>> pageQuery(@RequestBody @Validated(FimsFlightInfoParam.QueryGroup.class) FimsFlightInfoParam param) {
        //分页查询
        IPage selectPage = this.fimsFlightInfoService.pageQuery(param);
        //转化
        IPage resultPage = PageUtil.convert(FimsFlightInfoVO.class, selectPage);
        return Result.ok(resultPage);
    }

    /**
     * 条件查询航班动态表
     *
     * @param param 参数对象
     * @return
     */
    @ApiOperation(value = "条件查询航班动态表", notes = "条件查询航班动态表")
    @PostMapping("/listQuery")
    public Result<List<FimsFlightInfoVO>> listQuery(@RequestBody @Validated(FimsFlightInfoParam.QueryGroup.class) FimsFlightInfoParam param) {
        List<FimsFlightInfo> selectList = this.fimsFlightInfoService.listQuery(param);
        List<FimsFlightInfoVO> list = selectList.stream().map(FimsFlightInfoVO::new).collect(Collectors.toList());
        return Result.ok(list);
    }

    /**
     * 通过主键查询单条数据
     *
     * @param param 主键
     * @return 单条数据
     */
    @ApiOperation(value = "通过主键查询单条数据", notes = "通过主键查询单条数据")
    @PostMapping("/queryById")
    public Result<FimsFlightInfoVO> selectOne(@RequestBody @Validated(FimsFlightInfoParam.QueryGroup.class) FimsFlightInfoParam param) {
        FimsFlightInfo fimsFlightInfo = this.fimsFlightInfoService.getById(param.getFlightId());
        FimsFlightInfoVO fimsFlightInfoVO = null;
        if (ObjectUtil.isNotEmpty(fimsFlightInfo)) {
            fimsFlightInfoVO = new FimsFlightInfoVO(fimsFlightInfo);
        }
        return Result.ok(fimsFlightInfoVO);
    }

    /**
     * 新增航班动态表
     *
     * @param param 参数对象
     * @return
     */
    @ApiOperation(value = "新增航班动态表", notes = "新增航班动态表")
    @PostMapping("/add")
    public Result<String> insert(@RequestBody @Validated(FimsFlightInfoParam.AddGroup.class) FimsFlightInfoParam param) {
        Result rst = this.fimsFlightInfoService.add(param);
        return rst;
    }

    /**
     * 修改航班动态表
     *
     * @param param 参数对象
     * @return 修改结果
     */
    @ApiOperation(value = "修改航班动态表", notes = "修改航班动态表")
    @PostMapping("/edit")
    public Result<Boolean> update(@RequestBody @Validated(FimsFlightInfoParam.EditGroup.class) FimsFlightInfoParam param) {
        Result rst = this.fimsFlightInfoService.edit(param);
        return rst;
    }

    /**
     * 批量删除数据
     *
     * @param idListMap 参数对象
     * @return 删除结果
     */
    @ApiOperation(value = "批量删除数据", notes = "批量删除数据")
    @PostMapping("/delete")
    public Result<Boolean> delete(@RequestBody Map<String, Object> idListMap) {
        String idKey = "ids";
        if (CollectionUtils.isEmpty(idListMap) || ObjectUtils.isEmpty(idListMap.get(idKey))) {
            return Result.error("参数不能为空");
        }
        List<String> idList = (List<String>) idListMap.get(idKey);
        boolean resultFlag = this.fimsFlightInfoService.removeBatchByIds(idList);
        if (resultFlag) {
            return Result.OK(true);
        } else {
            return Result.error(Result.ERROR_MESSAGE, false);
        }
    }
    @PostMapping("batchAd")
    public Result batchAdd(){
        LocalDateTime startTime = LocalDateTime.now();
        Result result =  this.fimsFlightInfoService.batchAdd();
        log.info("|batchAdd|用时：{} 秒", Duration.between(startTime, LocalDateTime.now()).getSeconds());
        return result;
    }
}

