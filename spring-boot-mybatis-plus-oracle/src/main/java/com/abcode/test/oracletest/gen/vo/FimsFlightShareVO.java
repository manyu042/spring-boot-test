package com.abcode.test.oracletest.gen.vo;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import com.abcode.test.oracletest.gen.entity.FimsFlightShare;

/**
 * 动态共享航班表(FimsFlightShare)前端显示类
 *
 * @author qinzhitao
 * @since 2023-08-19 22:02:35
 */
@ApiModel("动态共享航班表返回实体")
@NoArgsConstructor
@Data
@SuppressWarnings("serial")
public class FimsFlightShareVO {

    /**
     * ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty("ID")
    private Long id;

    /**
     * 航班ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty("航班ID")
    private Long flightId;

    /**
     * 航空公司二字码
     */
    @ApiModelProperty("航空公司二字码")
    private String airlinesIata;

    /**
     * 航班号
     */
    @ApiModelProperty("航班号")
    private String flightNo;

    /**
     * 排序编码
     */
    @ApiModelProperty("排序编码")
    private Integer orderCode;

    /**
     * 是否删除
     */
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @ApiModelProperty("是否删除")
    private Integer delFlag;

    /**
     * 创建人
     */
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @ApiModelProperty("创建人")
    private String createBy;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @ApiModelProperty("创建时间")
    private LocalDateTime createTime;

    /**
     * 更新人
     */
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @ApiModelProperty("更新人")
    private String updateBy;

    /**
     * 更新时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @ApiModelProperty("更新时间")
    private LocalDateTime updateTime;

    /**
     * 有参构造函数
     *
     * @param entity
     */
    public FimsFlightShareVO(FimsFlightShare entity) {

        this.id = entity.getId();
        this.flightId = entity.getFlightId();
        this.airlinesIata = entity.getAirlinesIata();
        this.flightNo = entity.getFlightNo();
        this.orderCode = entity.getOrderCode();
        this.delFlag = entity.getDelFlag();
        this.createBy = entity.getCreateBy();
        this.createTime = entity.getCreateTime();
        this.updateBy = entity.getUpdateBy();
        this.updateTime = entity.getUpdateTime();
    }


}
