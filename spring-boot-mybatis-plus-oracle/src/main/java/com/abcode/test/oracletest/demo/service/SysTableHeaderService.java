package com.abcode.test.oracletest.demo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.abcode.test.oracletest.demo.entity.SysTableHeader;

/**
 * 系统默认表的表头信息(SysTableHeader)表服务接口
 *
 * @author qinzhitao
 * @since 2023-03-14 13:53:54
 */
public interface SysTableHeaderService extends IService<SysTableHeader> {

}

