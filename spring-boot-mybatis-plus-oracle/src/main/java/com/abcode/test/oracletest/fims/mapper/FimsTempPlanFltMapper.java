package com.abcode.test.oracletest.fims.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.abcode.test.oracletest.fims.entity.FimsTempPlanFlt;

/**
 * 临时计划航班表(FimsTempPlanFlt)表数据库访问层
 *
 * @author qinzhitao
 * @since 2023-06-29 16:39:47
 */
public interface FimsTempPlanFltMapper extends BaseMapper<FimsTempPlanFlt> {

}

