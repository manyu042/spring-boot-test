package com.abcode.test.oracletest.gen.service;


import com.abcode.test.oracletest.Result;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.abcode.test.oracletest.gen.entity.FimsFlightInfo;
import com.abcode.test.oracletest.gen.param.FimsFlightInfoParam;

import java.util.List;

/**
 * 航班动态表(FimsFlightInfo)表服务接口
 *
 * @author qinzhitao
 * @since 2023-08-19 22:02:23
 */
public interface FimsFlightInfoService extends IService<FimsFlightInfo> {
    /**
     * 分页查找航班动态表
     *
     * @param param
     * @return
     */
    IPage pageQuery(FimsFlightInfoParam param);

    /**
     * 新增航班动态表
     *
     * @param param
     * @return
     */
    Result add(FimsFlightInfoParam param);

    /**
     * 修改航班动态表
     *
     * @param param
     * @return
     */
    Result edit(FimsFlightInfoParam param);

    /**
     * 条件查询航班动态表
     *
     * @param param
     * @return
     */
    List<FimsFlightInfo> listQuery(FimsFlightInfoParam param);

    Result batchAdd();
}

