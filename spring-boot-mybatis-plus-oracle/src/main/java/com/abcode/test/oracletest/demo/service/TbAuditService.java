package com.abcode.test.oracletest.demo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.abcode.test.oracletest.demo.entity.TbAudit;

/**
 * 审计信息(TbAudit)表服务接口
 *
 * @author qinzhitao
 * @since 2023-03-14 14:46:56
 */
public interface TbAuditService extends IService<TbAudit> {

    boolean insertOne(TbAudit tbAudit);
}

