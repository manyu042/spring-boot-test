package com.abcode.test.oracletest.gen.entity;

import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 动态经停站（航线）表(FimsFlightAirline)表实体类
 *
 * @author qinzhitao
 * @since 2023-08-19 22:02:19
 */
@Data
@SuppressWarnings("serial")
@TableName("FIMS_FLIGHT_AIRLINE")
public class FimsFlightAirline {

    /**
     * ID
     */
    @TableId(value = "ID")
    private Long id;

    /**
     * 航班ID
     */
    @TableField("FLIGHT_ID")
    private Long flightId;

    /**
     * 排序编码
     */
    @TableField("ORDER_CODE")
    private Integer orderCode;

    /**
     * 经停站（机场）三字码
     */
    @TableField("AIRPORT_IATA")
    private String airportIata;

    /**
     * 计划起飞时间（如果航班为出港航班，第一航站（始发站）计划起飞时间必须有值）
     */
    @TableField("STD")
    private LocalDateTime std;

    /**
     * 预计起飞时间
     */
    @TableField("ETD")
    private LocalDateTime etd;

    /**
     * 实际起飞时间
     */
    @TableField("ATD")
    private LocalDateTime atd;

    /**
     * 计划降落时间（如果航班为进港航班，最后一个航站（目的站）计划降落时间必须有值）
     */
    @TableField("STA")
    private LocalDateTime sta;

    /**
     * 预计降落时间
     */
    @TableField("ETA")
    private LocalDateTime eta;

    /**
     * 实际降落时间
     */
    @TableField("ATA")
    private LocalDateTime ata;

    /**
     * 属性 2403-国内、2401-国际、2404-混合、2402-地区
     */
    @TableField("ATTRIBUTE")
    private String attribute;

    /**
     * 备注
     */
    @TableField("REMARK")
    private String remark;

    /**
     * 是否删除
     */
    @TableField("DEL_FLAG")
    private Integer delFlag;

    /**
     * 创建人
     */
    @TableField("CREATE_BY")
    private String createBy;

    /**
     * 创建时间
     */
    @TableField("CREATE_TIME")
    private LocalDateTime createTime;

    /**
     * 更新人
     */
    @TableField("UPDATE_BY")
    private String updateBy;

    /**
     * 更新时间
     */
    @TableField("UPDATE_TIME")
    private LocalDateTime updateTime;

//<editor-fold desc="字段名常量">
    /**
     * ID
     */
    public static final String ID = "ID";
    /**
     * 航班ID
     */
    public static final String FLIGHT_ID = "FLIGHT_ID";
    /**
     * 排序编码
     */
    public static final String ORDER_CODE = "ORDER_CODE";
    /**
     * 经停站（机场）三字码
     */
    public static final String AIRPORT_IATA = "AIRPORT_IATA";
    /**
     * 计划起飞时间（如果航班为出港航班，第一航站（始发站）计划起飞时间必须有值）
     */
    public static final String STD = "STD";
    /**
     * 预计起飞时间
     */
    public static final String ETD = "ETD";
    /**
     * 实际起飞时间
     */
    public static final String ATD = "ATD";
    /**
     * 计划降落时间（如果航班为进港航班，最后一个航站（目的站）计划降落时间必须有值）
     */
    public static final String STA = "STA";
    /**
     * 预计降落时间
     */
    public static final String ETA = "ETA";
    /**
     * 实际降落时间
     */
    public static final String ATA = "ATA";
    /**
     * 属性 2403-国内、2401-国际、2404-混合、2402-地区
     */
    public static final String ATTRIBUTE = "ATTRIBUTE";
    /**
     * 备注
     */
    public static final String REMARK = "REMARK";
    /**
     * 是否删除
     */
    public static final String DEL_FLAG = "DEL_FLAG";
    /**
     * 创建人
     */
    public static final String CREATE_BY = "CREATE_BY";
    /**
     * 创建时间
     */
    public static final String CREATE_TIME = "CREATE_TIME";
    /**
     * 更新人
     */
    public static final String UPDATE_BY = "UPDATE_BY";
    /**
     * 更新时间
     */
    public static final String UPDATE_TIME = "UPDATE_TIME";
//</editor-fold>

}
