package com.abcode.test.oracletest.demo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

import java.util.Date;

/**
 * 用户表(Account)表实体类
 *
 * @author abcode
 * @since 2021-01-22 20:37:44
 */
@Data
@SuppressWarnings("serial")
@TableName("tb_account")
public class Account extends Model<Account> {


    @TableId(value = "account_id", type = IdType.AUTO)
    private Integer accountId;

    /**
     * 账号
     */

    @TableField("account")
    private String account;

    /**
     * 昵称
     */

    @TableField("account_name")
    private String accountName;

    /**
     * 代理商ID
     */

    @TableField("agent_id")
    private Integer agentId;

    /**
     * 创建时间
     */

    @TableField("create_time")
    private Date createTime;

    /**
     * 更新时间
     */

    @TableField("update_time")
    private Date updateTime;

    /**
     * 状态：正常，正常；删除，删除；
     */

    @TableField("tb_status")
    private String tbStatus;


}