package com.abcode.test.oracletest.common;

import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 分页参数对象
 *
 * @author qinzhitao
 * @date 2023/6/25 14:46
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class PageParam<T>  implements Serializable {

    /**
     * 页码
     */
    @ApiModelProperty("当前页")
    protected Long pageNum=1L;
    /**
     * 每页数量
     */
    @ApiModelProperty("每页数量")
    protected Long pageSize=10L;
    /**
     * 排序字段
     */
    @ApiModelProperty("排序字段")
    protected String orderField = "create_time";
    /**
     * 是否是增序排（默认为降序）
     */
    @ApiModelProperty("是否增序排")
    protected Boolean ascFlag = false;

    /**
     * 获取分页对象
     * @return
     */
    public Page<T> getPage() {
        Page<T> page = new Page<>();
        page.setSize(this.pageSize);
        page.setCurrent(this.pageNum);
        OrderItem orderItem = new OrderItem();
        orderItem.setColumn(orderField);
        orderItem.setAsc(ascFlag);
        List<OrderItem> orderItemList = new ArrayList<>();
        orderItemList.add(orderItem);
        page.setOrders(orderItemList);
        return page;
    }
}
