package com.abcode.test.oracletest.demo.entity;


import java.time.LocalDateTime;

/**
 * 系统默认表的表头信息(SysTableHeader)表实体类
 *
 * @author qinzhitao
 * @since 2023-03-14 13:53:54
 */
@SuppressWarnings("serial")
public class SysTableHeader {
    //主键ID
    private Long id;
    //表代码
    private String tableCode;
    //字段代码
    private String fieldCode;
    //字段名称
    private String fieldName;
    //字段排序
    private Integer fieldOrder;
    //字段状态
    private String fieldStatus;
    //列宽
    private Integer columnWidth;
    //是否删除
    private Integer delFlag;
    //创建人
    private String createBy;
    //创建时间
    private LocalDateTime createTime;
    //更新人
    private String updateBy;
    //更新时间
    private LocalDateTime updateTime;
    //备注;业务场景
    private String remark;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTableCode() {
        return tableCode;
    }

    public void setTableCode(String tableCode) {
        this.tableCode = tableCode;
    }

    public String getFieldCode() {
        return fieldCode;
    }

    public void setFieldCode(String fieldCode) {
        this.fieldCode = fieldCode;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public Integer getFieldOrder() {
        return fieldOrder;
    }

    public void setFieldOrder(Integer fieldOrder) {
        this.fieldOrder = fieldOrder;
    }

    public String getFieldStatus() {
        return fieldStatus;
    }

    public void setFieldStatus(String fieldStatus) {
        this.fieldStatus = fieldStatus;
    }

    public Integer getColumnWidth() {
        return columnWidth;
    }

    public void setColumnWidth(Integer columnWidth) {
        this.columnWidth = columnWidth;
    }

    public Integer getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(Integer delFlag) {
        this.delFlag = delFlag;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

}

