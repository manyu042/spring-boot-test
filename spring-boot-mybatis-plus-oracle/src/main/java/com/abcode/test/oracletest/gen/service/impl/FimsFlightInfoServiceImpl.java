package com.abcode.test.oracletest.gen.service.impl;


import com.abcode.test.oracletest.Result;
import com.abcode.test.oracletest.common.utils.BatchOperateDbUtil;
import com.abcode.test.oracletest.gen.entity.FimsFlightInfo;
import com.abcode.test.oracletest.gen.mapper.FimsFlightInfoMapper;
import com.abcode.test.oracletest.gen.param.FimsFlightInfoParam;
import com.abcode.test.oracletest.gen.service.FimsFlightInfoService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import com.google.common.collect.Lists;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * 航班动态表(FimsFlightInfo)表服务实现类
 *
 * @author qinzhitao
 * @since 2023-08-19 22:02:23
 */
@Service
public class FimsFlightInfoServiceImpl extends ServiceImpl<FimsFlightInfoMapper, FimsFlightInfo> implements FimsFlightInfoService {

    @Resource
    FimsFlightInfoMapper fimsFlightInfoMapper;

    @Override
    public IPage pageQuery(FimsFlightInfoParam param) {
        FimsFlightInfo fimsFlightInfo = param.toEntity();
        Page page = param.getPage();
        LambdaQueryWrapper queryWrapper = new LambdaQueryWrapper<>(fimsFlightInfo);
        IPage selectPage = this.page(page, queryWrapper);
        return selectPage;
    }

    @Override
    public Result add(FimsFlightInfoParam param) {
        FimsFlightInfo fimsFlightInfo = param.toEntity();
        boolean resultFlag = this.save(fimsFlightInfo);
        if (resultFlag) {
            return Result.OK();
        } else {
            return Result.error(Result.ERROR_MESSAGE);
        }
    }

    @Override
    public Result edit(FimsFlightInfoParam param) {
        FimsFlightInfo fimsFlightInfo = param.toEntity();
        boolean resultFlag = this.updateById(fimsFlightInfo);
        if (resultFlag) {
            return Result.OK(true);
        } else {
            return Result.error(Result.ERROR_MESSAGE, false);
        }
    }

    @Override
    public List<FimsFlightInfo> listQuery(FimsFlightInfoParam param) {
        FimsFlightInfo fimsFlightInfo = param.toEntity();
        LambdaQueryWrapper queryWrapper = new LambdaQueryWrapper(fimsFlightInfo);
        return this.list(queryWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public Result batchAdd() {
        List<FimsFlightInfo> list = new ArrayList<>();
        int count = 2500;
        for (int i = 0; i < count; i++) {
            FimsFlightInfo entity = new FimsFlightInfo();
            //entity.setFlightId(0L);
            entity.setAssociatedFlightId(0L);
            entity.setMajorFlightId(0L);
            entity.setVirtualOrder("test");
            entity.setExecDate(LocalDateTime.now());
            entity.setTask("test");
            entity.setAirlinesIata("test");
            entity.setSubAirlinesId(0L);
            entity.setFlightNo("test");
            entity.setOffInFlag("A");
            entity.setAgency("222");
            entity.setTerminal("322");
            entity.setIntlTerminal("222");
            entity.setAttribute("22");
            entity.setReg("2ss");
            entity.setCraftType("3es");
            entity.setRunway("23");
            entity.setStand("234");
            entity.setTakeoffLandingCount(0L);
            entity.setPreviousTakeoffTime(LocalDateTime.now());
            entity.setNextTerminalTime(LocalDateTime.now());
            entity.setVipFlag("0");
            entity.setVirtualFlag("0");
            entity.setEndedFlag("1");
            entity.setExpressFlag("2");
            entity.setFocusOnFlag("3");
            entity.setRemark("4");
            entity.setInnerProvidingStatus("5");
            entity.setInnerAbnormalStatus("5");
            entity.setInnerAbnormalReason("5");
            entity.setProvidingStatus("5");
            entity.setAbnormalStatus("5");
            entity.setAbnormalReason("5");
            entity.setIntlInnerProvidingStatus("5");
            entity.setIntlInnerAbnormalStatus("5");
            entity.setIntlInnerAbnormalReason("5");
            entity.setIntlProvidingStatus("5");
            entity.setIntlAbnormalStatus("5");
            entity.setIntlAbnormalReason("5");
            entity.setScheduledCheckinStart(LocalDateTime.now());
            entity.setScheduledCheckinEnd(LocalDateTime.now());
            entity.setActualCheckinStart(LocalDateTime.now());
            entity.setActualCheckinEnd(LocalDateTime.now());
            entity.setIntlScheduledCheckinStart(LocalDateTime.now());
            entity.setIntlScheduledCheckinEnd(LocalDateTime.now());
            entity.setIntlActualCheckinStart(LocalDateTime.now());
            entity.setIntlActualCheckinEnd(LocalDateTime.now());
            entity.setAllowBoardingTime(LocalDateTime.now());
            entity.setBoardingTime(LocalDateTime.now());
            entity.setUrgeBoardingTime(LocalDateTime.now());
            entity.setTransitBoardingTime(LocalDateTime.now());
            entity.setEndBoardingTime(LocalDateTime.now());
            entity.setIntlBoardingTime(LocalDateTime.now());
            entity.setIntlUrgeBoardingTime(LocalDateTime.now());
            entity.setIntlTransitBoardingTime(LocalDateTime.now());
            entity.setIntlEndBoardingTime(LocalDateTime.now());
            entity.setDivertStationCode("");
            entity.setDivertReason("");
            entity.setDivertStd(LocalDateTime.now());
            entity.setDivertEtd(LocalDateTime.now());
            entity.setDivertAtd(LocalDateTime.now());
            entity.setDivertSta(LocalDateTime.now());
            entity.setDivertEta(LocalDateTime.now());
            entity.setDivertAta(LocalDateTime.now());
            entity.setUpdateCode("");
            entity.setDelFlag(0);
            entity.setCreateBy("");
            entity.setCreateTime(LocalDateTime.now());
            entity.setUpdateBy("");
            entity.setUpdateTime(LocalDateTime.now());
            entity.setVersion(0L);

            list.add(entity);
        }
//        List<List<FimsFlightInfo>> arrList = Lists.partition(list, 500);
//        arrList.stream().forEach(itemList->{
//            fimsFlightInfoMapper.batchInsertEntity(itemList);
//        });
        //fimsFlightInfoMapper.batchInsertEntity(list);
       BatchOperateDbUtil.batchExecute(this::saveBatch, list, 1000);
       // BatchOperateDbUtil.batchExecute(fimsFlightInfoMapper::batchInsertEntity, list, 400);
      //  this.saveBatch(list);
        return Result.ok();
    }
}

