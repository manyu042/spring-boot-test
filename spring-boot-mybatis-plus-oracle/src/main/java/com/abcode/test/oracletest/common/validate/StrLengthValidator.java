package com.abcode.test.oracletest.common.validate;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;


/**
 * @author qzt
 * @Date 2023-07-06
 * @Description 字符串有效长度规则效验类
 */
public class StrLengthValidator implements ConstraintValidator<StrLength, String> {

    private int min;
    private int max;

    @Override
    public void initialize(StrLength constraintAnnotation) {
        this.min = constraintAnnotation.min();
        this.max = constraintAnnotation.max();
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {

        if (value == null) {
            // 允许为null
           return true;
        }
        int length = value.trim().length();
        return length >= min && length <= max;
    }
}
