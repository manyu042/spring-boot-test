package com.abcode.test.oracletest.gen.param;

import java.time.LocalDateTime;

import com.abcode.test.oracletest.common.annotation.FieldInfo;
import com.abcode.test.oracletest.common.validate.StrLength;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.groups.Default;

import lombok.Data;
import com.abcode.test.oracletest.gen.entity.FimsFlightInfo;
import com.abcode.test.oracletest.common.PageParam;

/**
 * 航班动态表(FimsFlightInfo)参数类
 *
 * @author qinzhitao
 * @since 2023-08-19 22:02:23
 */
@ApiModel("航班动态表参数实体")
@Data
@SuppressWarnings("serial")
public class FimsFlightInfoParam extends PageParam {

    public interface QueryGroup {
    }

    public interface EditGroup extends Default {
    }

    public interface AddGroup extends Default {
    }

    /**
     * 航班ID
     */
    @NotNull(message = "【航班ID】不能为NUll", groups = EditGroup.class)
    @ApiModelProperty("航班ID")
    @FieldInfo(fieldCode = "flightId", fieldText = "航班ID")
    private Long flightId;

    /**
     * 关联航班ID
     */
    @ApiModelProperty("关联航班ID")
    @FieldInfo(fieldCode = "associatedFlightId", fieldText = "关联航班ID")
    private Long associatedFlightId;

    /**
     * 主航班ID（代表此虚拟航班所挂靠的执行航班）
     */
    @ApiModelProperty("主航班ID（代表此虚拟航班所挂靠的执行航班）")
    @FieldInfo(fieldCode = "majorFlightId", fieldText = "主航班ID（代表此虚拟航班所挂靠的执行航班）")
    private Long majorFlightId;

    /**
     * 虚拟航班序号
     */
    @StrLength(min = 1, max = 16, message = "【虚拟航班序号】取值的长度在{min}至{max}个字符之间")
    @ApiModelProperty("虚拟航班序号")
    @FieldInfo(fieldCode = "virtualOrder", fieldText = "虚拟航班序号")
    private String virtualOrder;

    /**
     * 执行日期——年月日（进港航班的计划降落时间里面的日期，出港航班的计划起飞时间里面的日期）
     */
    @NotNull(message = "【执行日期——年月日（进港航班的计划降落时间里面的日期，出港航班的计划起飞时间里面的日期）】不能为NUll", groups = AddGroup.class)
    @ApiModelProperty("执行日期——年月日（进港航班的计划降落时间里面的日期，出港航班的计划起飞时间里面的日期）")
    @FieldInfo(fieldCode = "execDate", fieldText = "执行日期——年月日（进港航班的计划降落时间里面的日期，出港航班的计划起飞时间里面的日期）")
    private LocalDateTime execDate;

    /**
     * 任务
     */
    @StrLength(min = 1, max = 16, message = "【任务】取值的长度在{min}至{max}个字符之间")
    @ApiModelProperty("任务")
    @FieldInfo(fieldCode = "task", fieldText = "任务")
    private String task;

    /**
     * 航空公司二字码
     */
    @StrLength(min = 1, max = 8, message = "【航空公司二字码】取值的长度在{min}至{max}个字符之间")
    @ApiModelProperty("航空公司二字码")
    @FieldInfo(fieldCode = "airlinesIata", fieldText = "航空公司二字码")
    private String airlinesIata;

    /**
     * 航空分公司ID
     */
    @ApiModelProperty("航空分公司ID")
    @FieldInfo(fieldCode = "subAirlinesId", fieldText = "航空分公司ID")
    private Long subAirlinesId;

    /**
     * 航班号
     */
    @StrLength(min = 1, max = 16, message = "【航班号】取值的长度在{min}至{max}个字符之间")
    @ApiModelProperty("航班号")
    @FieldInfo(fieldCode = "flightNo", fieldText = "航班号")
    private String flightNo;

    /**
     * 进出标志
     */
    @StrLength(min = 1, max = 8, message = "【进出标志】取值的长度在{min}至{max}个字符之间")
    @ApiModelProperty("进出标志")
    @FieldInfo(fieldCode = "offInFlag", fieldText = "进出标志")
    private String offInFlag;

    /**
     * 代理
     */
    @StrLength(min = 1, max = 16, message = "【代理】取值的长度在{min}至{max}个字符之间")
    @ApiModelProperty("代理")
    @FieldInfo(fieldCode = "agency", fieldText = "代理")
    private String agency;

    /**
     * 航站楼
     */
    @StrLength(min = 1, max = 8, message = "【航站楼】取值的长度在{min}至{max}个字符之间")
    @ApiModelProperty("航站楼")
    @FieldInfo(fieldCode = "terminal", fieldText = "航站楼")
    private String terminal;

    /**
     * 国际航站楼
     */
    @StrLength(min = 1, max = 8, message = "【国际航站楼】取值的长度在{min}至{max}个字符之间")
    @ApiModelProperty("国际航站楼")
    @FieldInfo(fieldCode = "intlTerminal", fieldText = "国际航站楼")
    private String intlTerminal;

    /**
     * 属性 2403-国内、2401-国际、2404-混合、2402-地区;2403-国内、2401-国际、2404-混合、2402-地区
     */
    @StrLength(min = 1, max = 8, message = "【属性 2403-国内、2401-国际、2404-混合、2402-地区;2403-国内、2401-国际、2404-混合、2402-地区】取值的长度在{min}至{max}个字符之间")
    @ApiModelProperty("属性 2403-国内、2401-国际、2404-混合、2402-地区;2403-国内、2401-国际、2404-混合、2402-地区")
    @FieldInfo(fieldCode = "attribute", fieldText = "属性 2403-国内、2401-国际、2404-混合、2402-地区;2403-国内、2401-国际、2404-混合、2402-地区")
    private String attribute;

    /**
     * 机号
     */
    @StrLength(min = 1, max = 16, message = "【机号】取值的长度在{min}至{max}个字符之间")
    @ApiModelProperty("机号")
    @FieldInfo(fieldCode = "reg", fieldText = "机号")
    private String reg;

    /**
     * 机型（国际民用航空组织指定代码ICAO）
     */
    @StrLength(min = 1, max = 16, message = "【机型（国际民用航空组织指定代码ICAO）】取值的长度在{min}至{max}个字符之间")
    @ApiModelProperty("机型（国际民用航空组织指定代码ICAO）")
    @FieldInfo(fieldCode = "craftType", fieldText = "机型（国际民用航空组织指定代码ICAO）")
    private String craftType;

    /**
     * 跑道
     */
    @StrLength(min = 1, max = 16, message = "【跑道】取值的长度在{min}至{max}个字符之间")
    @ApiModelProperty("跑道")
    @FieldInfo(fieldCode = "runway", fieldText = "跑道")
    private String runway;

    /**
     * 机位（变更情况下，多个之间加上逗号隔开，这里只做本系统记录显示，实际使用记录在动态机位表中）
     */
    @StrLength(min = 1, max = 64, message = "【机位（变更情况下，多个之间加上逗号隔开，这里只做本系统记录显示，实际使用记录在动态机位表中）】取值的长度在{min}至{max}个字符之间")
    @ApiModelProperty("机位（变更情况下，多个之间加上逗号隔开，这里只做本系统记录显示，实际使用记录在动态机位表中）")
    @FieldInfo(fieldCode = "stand", fieldText = "机位（变更情况下，多个之间加上逗号隔开，这里只做本系统记录显示，实际使用记录在动态机位表中）")
    private String stand;

    /**
     * 起降架次（计数字段，航线与广州有关的都设置为1，其他的为0）
     */
    @ApiModelProperty("起降架次（计数字段，航线与广州有关的都设置为1，其他的为0）")
    @FieldInfo(fieldCode = "takeoffLandingCount", fieldText = "起降架次（计数字段，航线与广州有关的都设置为1，其他的为0）")
    private Long takeoffLandingCount;

    /**
     * 前站起飞时间
     */
    @ApiModelProperty("前站起飞时间")
    @FieldInfo(fieldCode = "previousTakeoffTime", fieldText = "前站起飞时间")
    private LocalDateTime previousTakeoffTime;

    /**
     * 到达下站时间
     */
    @ApiModelProperty("到达下站时间")
    @FieldInfo(fieldCode = "nextTerminalTime", fieldText = "到达下站时间")
    private LocalDateTime nextTerminalTime;

    /**
     * 是否要客（VIP标识）
     */
    @NotBlank(message = "【是否要客（VIP标识）】不能为空", groups = AddGroup.class)
    @StrLength(min = 1, max = 4, message = "【是否要客（VIP标识）】取值的长度在{min}至{max}个字符之间")
    @ApiModelProperty("是否要客（VIP标识）")
    @FieldInfo(fieldCode = "vipFlag", fieldText = "是否要客（VIP标识）")
    private String vipFlag;

    /**
     * 是否虚拟航班
     */
    @NotBlank(message = "【是否虚拟航班】不能为空", groups = AddGroup.class)
    @StrLength(min = 1, max = 4, message = "【是否虚拟航班】取值的长度在{min}至{max}个字符之间")
    @ApiModelProperty("是否虚拟航班")
    @FieldInfo(fieldCode = "virtualFlag", fieldText = "是否虚拟航班")
    private String virtualFlag;

    /**
     * 是否执行完毕
     */
    @NotBlank(message = "【是否执行完毕】不能为空", groups = AddGroup.class)
    @StrLength(min = 1, max = 4, message = "【是否执行完毕】取值的长度在{min}至{max}个字符之间")
    @ApiModelProperty("是否执行完毕")
    @FieldInfo(fieldCode = "endedFlag", fieldText = "是否执行完毕")
    private String endedFlag;

    /**
     * 是否快线
     */
    @NotBlank(message = "【是否快线】不能为空", groups = AddGroup.class)
    @StrLength(min = 1, max = 4, message = "【是否快线】取值的长度在{min}至{max}个字符之间")
    @ApiModelProperty("是否快线")
    @FieldInfo(fieldCode = "expressFlag", fieldText = "是否快线")
    private String expressFlag;

    /**
     * 是否重点关注
     */
    @NotBlank(message = "【是否重点关注】不能为空", groups = AddGroup.class)
    @StrLength(min = 1, max = 4, message = "【是否重点关注】取值的长度在{min}至{max}个字符之间")
    @ApiModelProperty("是否重点关注")
    @FieldInfo(fieldCode = "focusOnFlag", fieldText = "是否重点关注")
    private String focusOnFlag;

    /**
     * 备注信息
     */
    @StrLength(min = 1, max = 256, message = "【备注信息】取值的长度在{min}至{max}个字符之间")
    @ApiModelProperty("备注信息")
    @FieldInfo(fieldCode = "remark", fieldText = "备注信息")
    private String remark;

    /**
     * 内部进展状态（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内部分状态）
     */
    @StrLength(min = 1, max = 8, message = "【内部进展状态（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内部分状态）】取值的长度在{min}至{max}个字符之间")
    @ApiModelProperty("内部进展状态（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内部分状态）")
    @FieldInfo(fieldCode = "innerProvidingStatus", fieldText = "内部进展状态（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内部分状态）")
    private String innerProvidingStatus;

    /**
     * 内部异常状态（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内部分状态）
     */
    @StrLength(min = 1, max = 8, message = "【内部异常状态（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内部分状态）】取值的长度在{min}至{max}个字符之间")
    @ApiModelProperty("内部异常状态（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内部分状态）")
    @FieldInfo(fieldCode = "innerAbnormalStatus", fieldText = "内部异常状态（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内部分状态）")
    private String innerAbnormalStatus;

    /**
     * 内部异常原因（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内部分不正常原因）
     */
    @StrLength(min = 1, max = 16, message = "【内部异常原因（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内部分不正常原因）】取值的长度在{min}至{max}个字符之间")
    @ApiModelProperty("内部异常原因（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内部分不正常原因）")
    @FieldInfo(fieldCode = "innerAbnormalReason", fieldText = "内部异常原因（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内部分不正常原因）")
    private String innerAbnormalReason;

    /**
     * 对外进展状态（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内部分状态）
     */
    @StrLength(min = 1, max = 8, message = "【对外进展状态（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内部分状态）】取值的长度在{min}至{max}个字符之间")
    @ApiModelProperty("对外进展状态（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内部分状态）")
    @FieldInfo(fieldCode = "providingStatus", fieldText = "对外进展状态（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内部分状态）")
    private String providingStatus;

    /**
     * 对外异常状态（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内部分状态）
     */
    @StrLength(min = 1, max = 8, message = "【对外异常状态（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内部分状态）】取值的长度在{min}至{max}个字符之间")
    @ApiModelProperty("对外异常状态（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内部分状态）")
    @FieldInfo(fieldCode = "abnormalStatus", fieldText = "对外异常状态（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内部分状态）")
    private String abnormalStatus;

    /**
     * 对外异常原因（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内部分不正常原因）
     */
    @StrLength(min = 1, max = 16, message = "【对外异常原因（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内部分不正常原因）】取值的长度在{min}至{max}个字符之间")
    @ApiModelProperty("对外异常原因（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内部分不正常原因）")
    @FieldInfo(fieldCode = "abnormalReason", fieldText = "对外异常原因（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内部分不正常原因）")
    private String abnormalReason;

    /**
     * 国际内部进展状态（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际部分状态）
     */
    @StrLength(min = 1, max = 8, message = "【国际内部进展状态（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际部分状态）】取值的长度在{min}至{max}个字符之间")
    @ApiModelProperty("国际内部进展状态（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际部分状态）")
    @FieldInfo(fieldCode = "intlInnerProvidingStatus", fieldText = "国际内部进展状态（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际部分状态）")
    private String intlInnerProvidingStatus;

    /**
     * 国际内部异常状态（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际部分状态）
     */
    @StrLength(min = 1, max = 8, message = "【国际内部异常状态（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际部分状态）】取值的长度在{min}至{max}个字符之间")
    @ApiModelProperty("国际内部异常状态（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际部分状态）")
    @FieldInfo(fieldCode = "intlInnerAbnormalStatus", fieldText = "国际内部异常状态（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际部分状态）")
    private String intlInnerAbnormalStatus;

    /**
     * 国际内部异常原因（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际部分不正常原因）
     */
    @StrLength(min = 1, max = 16, message = "【国际内部异常原因（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际部分不正常原因）】取值的长度在{min}至{max}个字符之间")
    @ApiModelProperty("国际内部异常原因（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际部分不正常原因）")
    @FieldInfo(fieldCode = "intlInnerAbnormalReason", fieldText = "国际内部异常原因（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际部分不正常原因）")
    private String intlInnerAbnormalReason;

    /**
     * 国际进展状态（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际部分状态）
     */
    @StrLength(min = 1, max = 8, message = "【国际进展状态（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际部分状态）】取值的长度在{min}至{max}个字符之间")
    @ApiModelProperty("国际进展状态（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际部分状态）")
    @FieldInfo(fieldCode = "intlProvidingStatus", fieldText = "国际进展状态（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际部分状态）")
    private String intlProvidingStatus;

    /**
     * 国际异常状态（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际部分状态）
     */
    @StrLength(min = 1, max = 8, message = "【国际异常状态（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际部分状态）】取值的长度在{min}至{max}个字符之间")
    @ApiModelProperty("国际异常状态（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际部分状态）")
    @FieldInfo(fieldCode = "intlAbnormalStatus", fieldText = "国际异常状态（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际部分状态）")
    private String intlAbnormalStatus;

    /**
     * 国际异常原因（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际部分不正常原因）
     */
    @StrLength(min = 1, max = 16, message = "【国际异常原因（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际部分不正常原因）】取值的长度在{min}至{max}个字符之间")
    @ApiModelProperty("国际异常原因（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际部分不正常原因）")
    @FieldInfo(fieldCode = "intlAbnormalReason", fieldText = "国际异常原因（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际部分不正常原因）")
    private String intlAbnormalReason;

    /**
     * 计划开始值机（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内旅客值机时间）
     */
    @ApiModelProperty("计划开始值机（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内旅客值机时间）")
    @FieldInfo(fieldCode = "scheduledCheckinStart", fieldText = "计划开始值机（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内旅客值机时间）")
    private LocalDateTime scheduledCheckinStart;

    /**
     * 计划值机截止（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内旅客值机时间）
     */
    @ApiModelProperty("计划值机截止（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内旅客值机时间）")
    @FieldInfo(fieldCode = "scheduledCheckinEnd", fieldText = "计划值机截止（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内旅客值机时间）")
    private LocalDateTime scheduledCheckinEnd;

    /**
     * 实际开始值机（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内旅客值机时间）
     */
    @ApiModelProperty("实际开始值机（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内旅客值机时间）")
    @FieldInfo(fieldCode = "actualCheckinStart", fieldText = "实际开始值机（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内旅客值机时间）")
    private LocalDateTime actualCheckinStart;

    /**
     * 实际值机截止（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内旅客值机时间）
     */
    @ApiModelProperty("实际值机截止（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内旅客值机时间）")
    @FieldInfo(fieldCode = "actualCheckinEnd", fieldText = "实际值机截止（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内旅客值机时间）")
    private LocalDateTime actualCheckinEnd;

    /**
     * 国际计划开始值机（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际旅客值机时间）
     */
    @ApiModelProperty("国际计划开始值机（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际旅客值机时间）")
    @FieldInfo(fieldCode = "intlScheduledCheckinStart", fieldText = "国际计划开始值机（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际旅客值机时间）")
    private LocalDateTime intlScheduledCheckinStart;

    /**
     * 国际计划值机截止（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际旅客值机时间）
     */
    @ApiModelProperty("国际计划值机截止（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际旅客值机时间）")
    @FieldInfo(fieldCode = "intlScheduledCheckinEnd", fieldText = "国际计划值机截止（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际旅客值机时间）")
    private LocalDateTime intlScheduledCheckinEnd;

    /**
     * 国际实际开始值机（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际旅客值机时间）
     */
    @ApiModelProperty("国际实际开始值机（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际旅客值机时间）")
    @FieldInfo(fieldCode = "intlActualCheckinStart", fieldText = "国际实际开始值机（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际旅客值机时间）")
    private LocalDateTime intlActualCheckinStart;

    /**
     * 国际实际值机截止（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际旅客值机时间）
     */
    @ApiModelProperty("国际实际值机截止（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际旅客值机时间）")
    @FieldInfo(fieldCode = "intlActualCheckinEnd", fieldText = "国际实际值机截止（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际旅客值机时间）")
    private LocalDateTime intlActualCheckinEnd;

    /**
     * 允许登机时间
     */
    @ApiModelProperty("允许登机时间")
    @FieldInfo(fieldCode = "allowBoardingTime", fieldText = "允许登机时间")
    private LocalDateTime allowBoardingTime;

    /**
     * 开始登机时间（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内开始登机时间）
     */
    @ApiModelProperty("开始登机时间（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内开始登机时间）")
    @FieldInfo(fieldCode = "boardingTime", fieldText = "开始登机时间（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内开始登机时间）")
    private LocalDateTime boardingTime;

    /**
     * 催促登机时间（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内催促登机时间）
     */
    @ApiModelProperty("催促登机时间（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内催促登机时间）")
    @FieldInfo(fieldCode = "urgeBoardingTime", fieldText = "催促登机时间（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内催促登机时间）")
    private LocalDateTime urgeBoardingTime;

    /**
     * 过站登机时间（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内过站登机时间）
     */
    @ApiModelProperty("过站登机时间（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内过站登机时间）")
    @FieldInfo(fieldCode = "transitBoardingTime", fieldText = "过站登机时间（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内过站登机时间）")
    private LocalDateTime transitBoardingTime;

    /**
     * 结束登机时间（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内结束登机时间）
     */
    @ApiModelProperty("结束登机时间（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内结束登机时间）")
    @FieldInfo(fieldCode = "endBoardingTime", fieldText = "结束登机时间（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内结束登机时间）")
    private LocalDateTime endBoardingTime;

    /**
     * 国际段开始登机时间（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际开始登机时间）
     */
    @ApiModelProperty("国际段开始登机时间（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际开始登机时间）")
    @FieldInfo(fieldCode = "intlBoardingTime", fieldText = "国际段开始登机时间（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际开始登机时间）")
    private LocalDateTime intlBoardingTime;

    /**
     * 国际段催促登机时间（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际催促登机时间）
     */
    @ApiModelProperty("国际段催促登机时间（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际催促登机时间）")
    @FieldInfo(fieldCode = "intlUrgeBoardingTime", fieldText = "国际段催促登机时间（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际催促登机时间）")
    private LocalDateTime intlUrgeBoardingTime;

    /**
     * 国际段过站登机时间（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际过站登机时间）
     */
    @ApiModelProperty("国际段过站登机时间（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际过站登机时间）")
    @FieldInfo(fieldCode = "intlTransitBoardingTime", fieldText = "国际段过站登机时间（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际过站登机时间）")
    private LocalDateTime intlTransitBoardingTime;

    /**
     * 国际段登机结束时间（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际结束登机时间）
     */
    @ApiModelProperty("国际段登机结束时间（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际结束登机时间）")
    @FieldInfo(fieldCode = "intlEndBoardingTime", fieldText = "国际段登机结束时间（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际结束登机时间）")
    private LocalDateTime intlEndBoardingTime;

    /**
     * 备降站
     */
    @StrLength(min = 1, max = 16, message = "【备降站】取值的长度在{min}至{max}个字符之间")
    @ApiModelProperty("备降站")
    @FieldInfo(fieldCode = "divertStationCode", fieldText = "备降站")
    private String divertStationCode;

    /**
     * 备降原因
     */
    @StrLength(min = 1, max = 8, message = "【备降原因】取值的长度在{min}至{max}个字符之间")
    @ApiModelProperty("备降原因")
    @FieldInfo(fieldCode = "divertReason", fieldText = "备降原因")
    private String divertReason;

    /**
     * 备降计划起飞时间
     */
    @ApiModelProperty("备降计划起飞时间")
    @FieldInfo(fieldCode = "divertStd", fieldText = "备降计划起飞时间")
    private LocalDateTime divertStd;

    /**
     * 备降预计起飞时间
     */
    @ApiModelProperty("备降预计起飞时间")
    @FieldInfo(fieldCode = "divertEtd", fieldText = "备降预计起飞时间")
    private LocalDateTime divertEtd;

    /**
     * 备降实际起飞时间
     */
    @ApiModelProperty("备降实际起飞时间")
    @FieldInfo(fieldCode = "divertAtd", fieldText = "备降实际起飞时间")
    private LocalDateTime divertAtd;

    /**
     * 备降计划降落时间
     */
    @ApiModelProperty("备降计划降落时间")
    @FieldInfo(fieldCode = "divertSta", fieldText = "备降计划降落时间")
    private LocalDateTime divertSta;

    /**
     * 备降预计降落时间
     */
    @ApiModelProperty("备降预计降落时间")
    @FieldInfo(fieldCode = "divertEta", fieldText = "备降预计降落时间")
    private LocalDateTime divertEta;

    /**
     * 备降实际降落时间
     */
    @ApiModelProperty("备降实际降落时间")
    @FieldInfo(fieldCode = "divertAta", fieldText = "备降实际降落时间")
    private LocalDateTime divertAta;

    /**
     * 修改代码
     */
    @StrLength(min = 1, max = 1024, message = "【修改代码】取值的长度在{min}至{max}个字符之间")
    @ApiModelProperty("修改代码")
    @FieldInfo(fieldCode = "updateCode", fieldText = "修改代码")
    private String updateCode;


    /**
     * 版本号
     */
    @NotNull(message = "【版本号】不能为NUll", groups = AddGroup.class)
    @ApiModelProperty("版本号")
    @FieldInfo(fieldCode = "version", fieldText = "版本号")
    private Long version;

    /**
     * 构建数据库实体类
     *
     * @return
     */
    public FimsFlightInfo toEntity() {
        FimsFlightInfo entity = new FimsFlightInfo();

        entity.setFlightId(this.flightId);
        entity.setAssociatedFlightId(this.associatedFlightId);
        entity.setMajorFlightId(this.majorFlightId);
        entity.setVirtualOrder(this.virtualOrder);
        entity.setExecDate(this.execDate);
        entity.setTask(this.task);
        entity.setAirlinesIata(this.airlinesIata);
        entity.setSubAirlinesId(this.subAirlinesId);
        entity.setFlightNo(this.flightNo);
        entity.setOffInFlag(this.offInFlag);
        entity.setAgency(this.agency);
        entity.setTerminal(this.terminal);
        entity.setIntlTerminal(this.intlTerminal);
        entity.setAttribute(this.attribute);
        entity.setReg(this.reg);
        entity.setCraftType(this.craftType);
        entity.setRunway(this.runway);
        entity.setStand(this.stand);
        entity.setTakeoffLandingCount(this.takeoffLandingCount);
        entity.setPreviousTakeoffTime(this.previousTakeoffTime);
        entity.setNextTerminalTime(this.nextTerminalTime);
        entity.setVipFlag(this.vipFlag);
        entity.setVirtualFlag(this.virtualFlag);
        entity.setEndedFlag(this.endedFlag);
        entity.setExpressFlag(this.expressFlag);
        entity.setFocusOnFlag(this.focusOnFlag);
        entity.setInnerProvidingStatus(this.innerProvidingStatus);
        entity.setInnerAbnormalStatus(this.innerAbnormalStatus);
        entity.setInnerAbnormalReason(this.innerAbnormalReason);
        entity.setProvidingStatus(this.providingStatus);
        entity.setAbnormalStatus(this.abnormalStatus);
        entity.setAbnormalReason(this.abnormalReason);
        entity.setIntlInnerProvidingStatus(this.intlInnerProvidingStatus);
        entity.setIntlInnerAbnormalStatus(this.intlInnerAbnormalStatus);
        entity.setIntlInnerAbnormalReason(this.intlInnerAbnormalReason);
        entity.setIntlProvidingStatus(this.intlProvidingStatus);
        entity.setIntlAbnormalStatus(this.intlAbnormalStatus);
        entity.setIntlAbnormalReason(this.intlAbnormalReason);
        entity.setScheduledCheckinStart(this.scheduledCheckinStart);
        entity.setScheduledCheckinEnd(this.scheduledCheckinEnd);
        entity.setActualCheckinStart(this.actualCheckinStart);
        entity.setActualCheckinEnd(this.actualCheckinEnd);
        entity.setIntlScheduledCheckinStart(this.intlScheduledCheckinStart);
        entity.setIntlScheduledCheckinEnd(this.intlScheduledCheckinEnd);
        entity.setIntlActualCheckinStart(this.intlActualCheckinStart);
        entity.setIntlActualCheckinEnd(this.intlActualCheckinEnd);
        entity.setAllowBoardingTime(this.allowBoardingTime);
        entity.setBoardingTime(this.boardingTime);
        entity.setUrgeBoardingTime(this.urgeBoardingTime);
        entity.setTransitBoardingTime(this.transitBoardingTime);
        entity.setEndBoardingTime(this.endBoardingTime);
        entity.setIntlBoardingTime(this.intlBoardingTime);
        entity.setIntlUrgeBoardingTime(this.intlUrgeBoardingTime);
        entity.setIntlTransitBoardingTime(this.intlTransitBoardingTime);
        entity.setIntlEndBoardingTime(this.intlEndBoardingTime);
        entity.setDivertStationCode(this.divertStationCode);
        entity.setDivertReason(this.divertReason);
        entity.setDivertStd(this.divertStd);
        entity.setDivertEtd(this.divertEtd);
        entity.setDivertAtd(this.divertAtd);
        entity.setDivertSta(this.divertSta);
        entity.setDivertEta(this.divertEta);
        entity.setDivertAta(this.divertAta);
        entity.setUpdateCode(this.updateCode);
        entity.setVersion(this.version);

        return entity;
    }

}
