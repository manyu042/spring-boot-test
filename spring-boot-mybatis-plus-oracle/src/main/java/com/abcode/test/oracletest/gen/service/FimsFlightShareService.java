package com.abcode.test.oracletest.gen.service;

import com.abcode.test.oracletest.Result;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.abcode.test.oracletest.gen.entity.FimsFlightShare;
import com.abcode.test.oracletest.gen.param.FimsFlightShareParam;

import java.util.List;

/**
 * 动态共享航班表(FimsFlightShare)表服务接口
 *
 * @author qinzhitao
 * @since 2023-08-19 22:02:35
 */
public interface FimsFlightShareService extends IService<FimsFlightShare> {
    /**
     * 分页查找动态共享航班表
     *
     * @param param
     * @return
     */
    IPage pageQuery(FimsFlightShareParam param);

    /**
     * 新增动态共享航班表
     *
     * @param param
     * @return
     */
    Result add(FimsFlightShareParam param);

    /**
     * 修改动态共享航班表
     *
     * @param param
     * @return
     */
    Result edit(FimsFlightShareParam param);

    /**
     * 条件查询动态共享航班表
     *
     * @param param
     * @return
     */
    List<FimsFlightShare> listQuery(FimsFlightShareParam param);

    Result batchAdd();
}

