package com.abcode.test.oracletest.gen.entity;

import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
/**
 * 航班动态表(FimsFlightInfoHs)表实体类
 *
 * @author qinzhitao
 * @since 2023-05-05 09:45:56
 */
@Data
@SuppressWarnings("serial")
@TableName("FIMS_FLIGHT_INFO_HS")
public class FimsFlightInfoHs {
            
    /** 航班ID */                                  
    @TableId(value= "FLIGHT_ID")
    private Long flightId;
                        
    /** 关联航班ID */                                
    @TableField("ASSOCIATED_FLIGHT_ID")
    private Long associatedFlightId;
                        
    /** 主航班ID（代表此虚拟航班所挂靠的执行航班） */                                
    @TableField("MAJOR_FLIGHT_ID")
    private Long majorFlightId;
                        
    /** 虚拟航班序号 */                                
    @TableField("VIRTUAL_ORDER")
    private String virtualOrder;
                        
    /** 执行日期——年月日（进港航班的计划降落时间里面的日期，出港航班的计划起飞时间里面的日期） */                                
    @TableField("EXEC_DATE")
    private LocalDateTime execDate;
                        
    /** 任务 */                                
    @TableField("TASK")
    private String task;
                        
    /** 航空公司二字码 */                                
    @TableField("AIRLINES_IATA")
    private String airlinesIata;
                        
    /** 航空分公司ID */                                
    @TableField("SUB_AIRLINES_ID")
    private Long subAirlinesId;
                        
    /** 航班号 */                                
    @TableField("FLIGHT_NO")
    private String flightNo;
                        
    /** 进出标志 */                                
    @TableField("OFF_IN_FLAG")
    private String offInFlag;
                        
    /** 代理 */                                
    @TableField("AGENCY")
    private String agency;
                        
    /** 航站楼 */                                
    @TableField("TERMINAL")
    private String terminal;
                        
    /** 国际航站楼 */                                
    @TableField("INTL_TERMINAL")
    private String intlTerminal;
                        
    /** 属性 2403-国内、2401-国际、2404-混合、2402-地区;2403-国内、2401-国际、2404-混合、2402-地区 */                                
    @TableField("ATTRIBUTE")
    private String attribute;
                        
    /** 机号 */                                
    @TableField("REG")
    private String reg;
                        
    /** 机型（国际民用航空组织指定代码ICAO） */                                
    @TableField("CRAFT_TYPE")
    private String craftType;
                        
    /** 跑道 */                                
    @TableField("RUNWAY")
    private String runway;
                        
    /** 机位（变更情况下，多个之间加上逗号隔开，这里只做本系统记录显示，实际使用记录在动态机位表中） */                                
    @TableField("STAND")
    private String stand;
                        
    /** 起降架次（计数字段，航线与广州有关的都设置为1，其他的为0） */                                
    @TableField("TAKEOFF_LANDING_COUNT")
    private Long takeoffLandingCount;
                        
    /** 前站起飞时间 */                                
    @TableField("PREVIOUS_TAKEOFF_TIME")
    private LocalDateTime previousTakeoffTime;
                        
    /** 到达下站时间 */                                
    @TableField("NEXT_TERMINAL_TIME")
    private LocalDateTime nextTerminalTime;
                        
    /** 是否要客（VIP标识） */                                
    @TableField("VIP_FLAG")
    private String vipFlag;
                        
    /** 是否虚拟航班 */                                
    @TableField("VIRTUAL_FLAG")
    private String virtualFlag;
                        
    /** 是否执行完毕 */                                
    @TableField("ENDED_FLAG")
    private String endedFlag;
                        
    /** 是否快线 */                                
    @TableField("EXPRESS_FLAG")
    private String expressFlag;
                        
    /** 是否重点关注 */                                
    @TableField("FOCUS_ON_FLAG")
    private String focusOnFlag;
                        
    /** 备注信息 */                                
    @TableField("REMARK")
    private String remark;
                        
    /** 内部进展状态（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内部分状态） */                                
    @TableField("INNER_PROVIDING_STATUS")
    private String innerProvidingStatus;
                        
    /** 内部异常状态（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内部分状态） */                                
    @TableField("INNER_ABNORMAL_STATUS")
    private String innerAbnormalStatus;
                        
    /** 内部异常原因（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内部分不正常原因） */                                
    @TableField("INNER_ABNORMAL_REASON")
    private String innerAbnormalReason;
                        
    /** 对外进展状态（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内部分状态） */                                
    @TableField("PROVIDING_STATUS")
    private String providingStatus;
                        
    /** 对外异常状态（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内部分状态） */                                
    @TableField("ABNORMAL_STATUS")
    private String abnormalStatus;
                        
    /** 对外异常原因（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内部分不正常原因） */                                
    @TableField("ABNORMAL_REASON")
    private String abnormalReason;
                        
    /** 国际内部进展状态（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际部分状态） */                                
    @TableField("INTL_INNER_PROVIDING_STATUS")
    private String intlInnerProvidingStatus;
                        
    /** 国际内部异常状态（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际部分状态） */                                
    @TableField("INTL_INNER_ABNORMAL_STATUS")
    private String intlInnerAbnormalStatus;
                        
    /** 国际内部异常原因（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际部分不正常原因） */                                
    @TableField("INTL_INNER_ABNORMAL_REASON")
    private String intlInnerAbnormalReason;
                        
    /** 国际进展状态（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际部分状态） */                                
    @TableField("INTL_PROVIDING_STATUS")
    private String intlProvidingStatus;
                        
    /** 国际异常状态（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际部分状态） */                                
    @TableField("INTL_ABNORMAL_STATUS")
    private String intlAbnormalStatus;
                        
    /** 国际异常原因（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际部分不正常原因） */                                
    @TableField("INTL_ABNORMAL_REASON")
    private String intlAbnormalReason;
                        
    /** 计划开始值机（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内旅客值机时间） */                                
    @TableField("SCHEDULED_CHECKIN_START")
    private LocalDateTime scheduledCheckinStart;
                        
    /** 计划值机截止（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内旅客值机时间） */                                
    @TableField("SCHEDULED_CHECKIN_END")
    private LocalDateTime scheduledCheckinEnd;
                        
    /** 实际开始值机（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内旅客值机时间） */                                
    @TableField("ACTUAL_CHECKIN_START")
    private LocalDateTime actualCheckinStart;
                        
    /** 实际值机截止（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内旅客值机时间） */                                
    @TableField("ACTUAL_CHECKIN_END")
    private LocalDateTime actualCheckinEnd;
                        
    /** 国际计划开始值机（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际旅客值机时间） */                                
    @TableField("INTL_SCHEDULED_CHECKIN_START")
    private LocalDateTime intlScheduledCheckinStart;
                        
    /** 国际计划值机截止（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际旅客值机时间） */                                
    @TableField("INTL_SCHEDULED_CHECKIN_END")
    private LocalDateTime intlScheduledCheckinEnd;
                        
    /** 国际实际开始值机（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际旅客值机时间） */                                
    @TableField("INTL_ACTUAL_CHECKIN_START")
    private LocalDateTime intlActualCheckinStart;
                        
    /** 国际实际值机截止（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际旅客值机时间） */                                
    @TableField("INTL_ACTUAL_CHECKIN_END")
    private LocalDateTime intlActualCheckinEnd;
                        
    /** 允许登机时间 */                                
    @TableField("ALLOW_BOARDING_TIME")
    private LocalDateTime allowBoardingTime;
                        
    /** 开始登机时间（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内开始登机时间） */                                
    @TableField("BOARDING_TIME")
    private LocalDateTime boardingTime;
                        
    /** 催促登机时间（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内催促登机时间） */                                
    @TableField("URGE_BOARDING_TIME")
    private LocalDateTime urgeBoardingTime;
                        
    /** 过站登机时间（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内过站登机时间） */                                
    @TableField("TRANSIT_BOARDING_TIME")
    private LocalDateTime transitBoardingTime;
                        
    /** 结束登机时间（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内结束登机时间） */                                
    @TableField("END_BOARDING_TIME")
    private LocalDateTime endBoardingTime;
                        
    /** 国际段开始登机时间（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际开始登机时间） */                                
    @TableField("INTL_BOARDING_TIME")
    private LocalDateTime intlBoardingTime;
                        
    /** 国际段催促登机时间（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际催促登机时间） */                                
    @TableField("INTL_URGE_BOARDING_TIME")
    private LocalDateTime intlUrgeBoardingTime;
                        
    /** 国际段过站登机时间（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际过站登机时间） */                                
    @TableField("INTL_TRANSIT_BOARDING_TIME")
    private LocalDateTime intlTransitBoardingTime;
                        
    /** 国际段登机结束时间（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际结束登机时间） */                                
    @TableField("INTL_END_BOARDING_TIME")
    private LocalDateTime intlEndBoardingTime;
                        
    /** 备降站 */                                
    @TableField("DIVERT_STATION_CODE")
    private String divertStationCode;
                        
    /** 备降原因 */                                
    @TableField("DIVERT_REASON")
    private String divertReason;
                        
    /** 备降计划起飞时间 */                                
    @TableField("DIVERT_STD")
    private LocalDateTime divertStd;
                        
    /** 备降预计起飞时间 */                                
    @TableField("DIVERT_ETD")
    private LocalDateTime divertEtd;
                        
    /** 备降实际起飞时间 */                                
    @TableField("DIVERT_ATD")
    private LocalDateTime divertAtd;
                        
    /** 备降计划降落时间 */                                
    @TableField("DIVERT_STA")
    private LocalDateTime divertSta;
                        
    /** 备降预计降落时间 */                                
    @TableField("DIVERT_ETA")
    private LocalDateTime divertEta;
                        
    /** 备降实际降落时间 */                                
    @TableField("DIVERT_ATA")
    private LocalDateTime divertAta;
                        
    /** 修改代码 */                                
    @TableField("UPDATE_CODE")
    private String updateCode;
                        
    /** 是否删除 */                                
    @TableField("DEL_FLAG")
    private Integer delFlag;
                        
    /** 创建人 */                                
    @TableField("CREATE_BY")
    private String createBy;
                        
    /** 创建时间 */                                
    @TableField("CREATE_TIME")
    private LocalDateTime createTime;
                        
    /** 更新人 */                                
    @TableField("UPDATE_BY")
    private String updateBy;
                        
    /** 更新时间 */                                
    @TableField("UPDATE_TIME")
    private LocalDateTime updateTime;
            
}
