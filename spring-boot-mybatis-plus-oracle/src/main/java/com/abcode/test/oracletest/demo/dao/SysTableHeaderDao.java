package com.abcode.test.oracletest.demo.dao;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import com.abcode.test.oracletest.demo.entity.SysTableHeader;

/**
 * 系统默认表的表头信息(SysTableHeader)表数据库访问层
 *
 * @author qinzhitao
 * @since 2023-03-14 13:53:54
 */
public interface SysTableHeaderDao extends BaseMapper<SysTableHeader> {

/**
* 批量新增数据（MyBatis原生foreach方法）
*
* @param entities List<SysTableHeader> 实例对象列表
* @return 影响行数
*/
int insertBatch(@Param("entities") List<SysTableHeader> entities);

/**
* 批量新增或按主键更新数据（MyBatis原生foreach方法）
*
* @param entities List<SysTableHeader> 实例对象列表
* @return 影响行数
* @throws org.springframework.jdbc.BadSqlGrammarException 入参是空List的时候会抛SQL语句错误的异常，请自行校验入参
*/
int insertOrUpdateBatch(@Param("entities") List<SysTableHeader> entities);

}

