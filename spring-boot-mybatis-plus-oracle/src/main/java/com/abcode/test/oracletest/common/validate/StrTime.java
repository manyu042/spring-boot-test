package com.abcode.test.oracletest.common.validate;


import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * @author qzt
 * @Date 2023-07-31
 * @Description 检测计划时间格式是否正确：yyyy-MM-dd HH:mm:ss 或 HH:mm:ss
 */
@Target({FIELD, PARAMETER})
@Retention(RUNTIME)
@Documented
@Constraint(validatedBy = {StrTimeValidator.class})
public @interface StrTime {
    String message() default "时间格式只能是：yyyy-MM-dd HH:mm:ss 或 HH:mm:ss";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};

}
