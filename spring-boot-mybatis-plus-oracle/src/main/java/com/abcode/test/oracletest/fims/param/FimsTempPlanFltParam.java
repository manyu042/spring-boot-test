package com.abcode.test.oracletest.fims.param;

import java.time.LocalDateTime;
import lombok.Data;
/**
 * 临时计划航班表(FimsTempPlanFlt)参数类
 *
 * @author qinzhitao
 * @since 2023-06-29 16:39:47
 */
@Data
@SuppressWarnings("serial")
public class FimsTempPlanFltParam {
            
    /** 主键 */        
    private Long id;
            
    /** 航空公司ID */        
    private Long airlinesId;
            
    /** 航班号 */        
    private String flightNo;
            
    /** 进出港标识：A-进；D-出； */        
    private String offInFlag;
            
    /** 任务 */        
    private String task;
            
    /** 属性 */        
    private String attribute;
            
    /** 机型ID */        
    private Long craftTypeId;
            
    /** 始发站ID */        
    private Long startStationId;
            
    /** 计划起飞时间 */        
    private LocalDateTime std;
            
    /** 目的站ID */        
    private Long terminalStationId;
            
    /** 计划降落时间 */        
    private LocalDateTime sta;
            
    /** 航站楼 */        
    private String terminal;
            
    /** 是否是虚拟航班：Y-是；N-否； */        
    private String virtualFlag;
            
    /** 主航班ID */        
    private Long majorFltId;
            
    /** 虚拟航班排序 */        
    private Integer virtualOrder;
            
    /** 关联航班ID */        
    private Long associatedFltId;
            
    /** 班期开始时间 */        
    private LocalDateTime startTime;
            
    /** 班期结束时间 */        
    private LocalDateTime endTime;
            
    /** 星期一 */        
    private String execMon;
            
    /** 星期二 */        
    private String execTue;
            
    /** 星期三 */        
    private String execWed;
            
    /** 星期四 */        
    private String execThu;
            
    /** 星期五 */        
    private String execFri;
            
    /** 星期六 */        
    private String execSat;
            
    /** 星期天 */        
    private String execSun;
            
    /** 是否取消：Y-是；N-否； */        
    private String cancelFlag;
            
    /** 是否删除：0-否；1-是； */        
    private Integer delFlag;
            
    /** 创建人 */        
    private String createBy;
            
    /** 创建时间 */        
    private LocalDateTime createTime;
            
    /** 更新人 */        
    private String updateBy;
            
    /** 更新时间 */        
    private LocalDateTime updateTime;
            
    /** 备注 */        
    private String remark;

}
