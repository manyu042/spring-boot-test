package com.abcode.test.oracletest.gen.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.abcode.test.oracletest.gen.entity.FimsFlightInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 航班动态表(FimsFlightInfo)表数据库访问层
 *
 * @author qinzhitao
 * @since 2023-08-19 22:02:23
 */
public interface FimsFlightInfoMapper extends BaseMapper<FimsFlightInfo> {

    void batchInsertEntity(@Param("list") List<FimsFlightInfo> list);
}

