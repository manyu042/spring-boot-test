package com.abcode.test.oracletest.demo.controller;

import com.abcode.test.oracletest.Result;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.abcode.test.oracletest.demo.entity.SysTableHeader;
import com.abcode.test.oracletest.demo.service.SysTableHeaderService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 系统默认表的表头信息(SysTableHeader)表控制层
 *
 * @author qinzhitao
 * @since 2023-03-14 13:53:54
 */
@RestController
@RequestMapping("sysTableHeader")
public class SysTableHeaderController{
    /**
     * 服务对象
     */
    @Resource
    private SysTableHeaderService sysTableHeaderService;

    /**
     * 分页查询所有数据
     *
     * @param page 分页对象
     * @param sysTableHeader 查询实体
     * @return 所有数据
     */
    @GetMapping
    public Result selectAll(Page<SysTableHeader> page, SysTableHeader sysTableHeader) {
        return Result.ok(this.sysTableHeaderService.page(page, new QueryWrapper<>(sysTableHeader)));
    }



    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("{id}")
    public Result selectOne(@PathVariable Serializable id) {
        return Result.ok(this.sysTableHeaderService.getById(id));
    }

    /**
     * 新增数据
     *
     * @param fieldCode 实体对象
     * @return 新增结果
     */
    @GetMapping("/add")
    public Result insert(@RequestParam(name = "fieldCode") String fieldCode) {
        String tabldeCode = "testTable11";
        SysTableHeader sysTableHeader = new SysTableHeader();
        sysTableHeader.setTableCode(tabldeCode);
        sysTableHeader.setFieldCode(fieldCode);
        sysTableHeader.setFieldName(fieldCode);
        sysTableHeader.setFieldOrder(1);
        sysTableHeader.setFieldStatus("Y");
        sysTableHeader.setColumnWidth(120);
        sysTableHeader.setDelFlag(0);
        sysTableHeader.setCreateBy("auto");
        sysTableHeader.setCreateTime(LocalDateTime.now());
        //sysTableHeader.setUpdateBy("");
        //sysTableHeader.setUpdateTime();
        sysTableHeader.setRemark("test");

        return Result.ok(this.sysTableHeaderService.save(sysTableHeader));
    }
/*
    *//**
     * 修改数据
     *
     * @param sysTableHeader 实体对象
     * @return 修改结果
     *//*
    @PutMapping
    public R update(@RequestBody SysTableHeader sysTableHeader) {
        return success(this.sysTableHeaderService.updateById(sysTableHeader));
    }

    *//**
     * 删除数据
     *
     * @param idList 主键结合
     * @return 删除结果
     *//*
    @DeleteMapping
    public R delete(@RequestParam("idList") List<Long> idList) {
        return success(this.sysTableHeaderService.removeByIds(idList));
    }*/
}

