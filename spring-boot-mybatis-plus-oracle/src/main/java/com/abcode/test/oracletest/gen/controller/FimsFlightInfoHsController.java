package com.abcode.test.oracletest.gen.controller;



import com.abcode.test.oracletest.Result;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.abcode.test.oracletest.gen.entity.FimsFlightInfoHs;
import com.abcode.test.oracletest.gen.service.FimsFlightInfoHsService;

/**
 * 航班动态表(FimsFlightInfoHs)表控制层
 *
 * @author qinzhitao
 * @since 2023-05-04 16:56:56
 */
@RestController
@RequestMapping("fimsFlightInfoHs")
public class FimsFlightInfoHsController {
    /**
     * 服务对象
     */
    @Resource
    private FimsFlightInfoHsService fimsFlightInfoHsService;

    /**
     * 分页查询所有数据
     * @param page 分页对象
     * @param fimsFlightInfoHs 查询实体
     * @return 所有数据
     */
    @GetMapping("/selectAll")
    public Result selectAll(Page<FimsFlightInfoHs> page, FimsFlightInfoHs fimsFlightInfoHs) {
        Page<FimsFlightInfoHs> rst = this.fimsFlightInfoHsService.page(page, new QueryWrapper<>(fimsFlightInfoHs));
        return Result.ok(rst);
    }

    /**
     * 通过主键查询单条数据
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("/selectOne/{id}")
    public Result selectOne(@PathVariable Serializable id) {
        FimsFlightInfoHs fimsFlightInfoHs = this.fimsFlightInfoHsService.getById(id);
        return Result.ok(fimsFlightInfoHs);
    }

    /**
     * 新增数据
     * @param fimsFlightInfoHs 实体对象
     * @return 新增结果
     */
    @PostMapping("/insert")
    public Result insert(@RequestBody FimsFlightInfoHs fimsFlightInfoHs) {
        List<FimsFlightInfoHs> list = new ArrayList<>();
        LocalDateTime localDateTime = LocalDate.now().atTime(0,0,0);
        for (int i = 0; i < 1000; i++) {
            FimsFlightInfoHs item = new FimsFlightInfoHs();
            item.setExecDate(localDateTime.plusYears(new Random().nextInt(10)));
            item.setCreateTime(LocalDateTime.now());

            list.add(item);
        }

        Boolean rst = this.fimsFlightInfoHsService.saveBatch(list);
        return Result.OK(rst);
    }

    /**
     * 修改数据
     * @param fimsFlightInfoHs 实体对象
     * @return 修改结果
     */
    @PutMapping("/update")
    public Result update(@RequestBody FimsFlightInfoHs fimsFlightInfoHs) {
        Boolean rst = this.fimsFlightInfoHsService.updateById(fimsFlightInfoHs);
        return Result.OK(rst);
    }

    /**
     * 删除数据
     *
     * @param idList 主键结合
     * @return 删除结果
     */
    @DeleteMapping("/delete")
    public Result delete(@RequestParam("idList") List<Long> idList) {
        boolean rst = this.fimsFlightInfoHsService.removeByIds(idList);
        return Result.ok(rst);
    }
}

