package com.abcode.test.oracletest.common.validate;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author qzt
 * @Date 2023-07-06
 * @Description 匹配规则
 */
public class SpecialCharacterValidator implements ConstraintValidator<SpecialMatch, String> {
    private static final Pattern PATTERN = Pattern.compile("[^`!~@#$￥%^&*()+=\\[\\]\\\\|'\"“”‘’；;:《》，<>,?/？{}]*$");
    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (value != null) {
            Matcher matcher = PATTERN.matcher(value);
            return matcher.matches();
        }
        return true;
    }
}
