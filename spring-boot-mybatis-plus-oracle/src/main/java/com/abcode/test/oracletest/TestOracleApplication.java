package com.abcode.test.oracletest;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@MapperScan({"com.abcode.test.oracletest.demo.dao", "com.abcode.test.oracletest.gen.mapper", "com.abcode.test.oracletest.fims.mapper"})
@SpringBootApplication
public class TestOracleApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestOracleApplication.class, args);
    }

}