package com.abcode.test.oracletest.gen.vo;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import com.abcode.test.oracletest.gen.entity.FimsFlightAirline;

/**
 * 动态经停站（航线）表(FimsFlightAirline)前端显示类
 *
 * @author qinzhitao
 * @since 2023-08-19 22:02:20
 */
@ApiModel("动态经停站（航线）表返回实体")
@NoArgsConstructor
@Data
@SuppressWarnings("serial")
public class FimsFlightAirlineVO {

    /**
     * ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty("ID")
    private Long id;

    /**
     * 航班ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty("航班ID")
    private Long flightId;

    /**
     * 排序编码
     */
    @ApiModelProperty("排序编码")
    private Integer orderCode;

    /**
     * 经停站（机场）三字码
     */
    @ApiModelProperty("经停站（机场）三字码")
    private String airportIata;

    /**
     * 计划起飞时间（如果航班为出港航班，第一航站（始发站）计划起飞时间必须有值）
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty("计划起飞时间（如果航班为出港航班，第一航站（始发站）计划起飞时间必须有值）")
    private LocalDateTime std;

    /**
     * 预计起飞时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty("预计起飞时间")
    private LocalDateTime etd;

    /**
     * 实际起飞时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty("实际起飞时间")
    private LocalDateTime atd;

    /**
     * 计划降落时间（如果航班为进港航班，最后一个航站（目的站）计划降落时间必须有值）
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty("计划降落时间（如果航班为进港航班，最后一个航站（目的站）计划降落时间必须有值）")
    private LocalDateTime sta;

    /**
     * 预计降落时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty("预计降落时间")
    private LocalDateTime eta;

    /**
     * 实际降落时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty("实际降落时间")
    private LocalDateTime ata;

    /**
     * 属性 2403-国内、2401-国际、2404-混合、2402-地区
     */
    @ApiModelProperty("属性 2403-国内、2401-国际、2404-混合、2402-地区")
    private String attribute;

    /**
     * 备注
     */
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @ApiModelProperty("备注")
    private String remark;

    /**
     * 是否删除
     */
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @ApiModelProperty("是否删除")
    private Integer delFlag;

    /**
     * 创建人
     */
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @ApiModelProperty("创建人")
    private String createBy;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @ApiModelProperty("创建时间")
    private LocalDateTime createTime;

    /**
     * 更新人
     */
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @ApiModelProperty("更新人")
    private String updateBy;

    /**
     * 更新时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @ApiModelProperty("更新时间")
    private LocalDateTime updateTime;

    /**
     * 有参构造函数
     *
     * @param entity
     */
    public FimsFlightAirlineVO(FimsFlightAirline entity) {

        this.id = entity.getId();
        this.flightId = entity.getFlightId();
        this.orderCode = entity.getOrderCode();
        this.airportIata = entity.getAirportIata();
        this.std = entity.getStd();
        this.etd = entity.getEtd();
        this.atd = entity.getAtd();
        this.sta = entity.getSta();
        this.eta = entity.getEta();
        this.ata = entity.getAta();
        this.attribute = entity.getAttribute();
        this.remark = entity.getRemark();
        this.delFlag = entity.getDelFlag();
        this.createBy = entity.getCreateBy();
        this.createTime = entity.getCreateTime();
        this.updateBy = entity.getUpdateBy();
        this.updateTime = entity.getUpdateTime();
    }


}
