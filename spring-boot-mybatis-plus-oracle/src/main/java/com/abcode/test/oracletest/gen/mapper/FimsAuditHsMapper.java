package com.abcode.test.oracletest.gen.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.abcode.test.oracletest.gen.entity.FimsAuditHs;

/**
 * 审计信息(FimsAuditHs)表数据库访问层
 *
 * @author qinzhitao
 * @since 2023-05-05 10:59:09
 */
public interface FimsAuditHsMapper extends BaseMapper<FimsAuditHs> {

}

