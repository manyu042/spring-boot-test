package com.abcode.test.oracletest.gen.vo;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import com.abcode.test.oracletest.gen.entity.FimsFlightInfo;

/**
 * 航班动态表(FimsFlightInfo)前端显示类
 *
 * @author qinzhitao
 * @since 2023-08-19 22:02:25
 */
@ApiModel("航班动态表返回实体")
@NoArgsConstructor
@Data
@SuppressWarnings("serial")
public class FimsFlightInfoVO {

    /**
     * 航班ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty("航班ID")
    private Long flightId;

    /**
     * 关联航班ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty("关联航班ID")
    private Long associatedFlightId;

    /**
     * 主航班ID（代表此虚拟航班所挂靠的执行航班）
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty("主航班ID（代表此虚拟航班所挂靠的执行航班）")
    private Long majorFlightId;

    /**
     * 虚拟航班序号
     */
    @ApiModelProperty("虚拟航班序号")
    private String virtualOrder;

    /**
     * 执行日期——年月日（进港航班的计划降落时间里面的日期，出港航班的计划起飞时间里面的日期）
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty("执行日期——年月日（进港航班的计划降落时间里面的日期，出港航班的计划起飞时间里面的日期）")
    private LocalDateTime execDate;

    /**
     * 任务
     */
    @ApiModelProperty("任务")
    private String task;

    /**
     * 航空公司二字码
     */
    @ApiModelProperty("航空公司二字码")
    private String airlinesIata;

    /**
     * 航空分公司ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty("航空分公司ID")
    private Long subAirlinesId;

    /**
     * 航班号
     */
    @ApiModelProperty("航班号")
    private String flightNo;

    /**
     * 进出标志
     */
    @ApiModelProperty("进出标志")
    private String offInFlag;

    /**
     * 代理
     */
    @ApiModelProperty("代理")
    private String agency;

    /**
     * 航站楼
     */
    @ApiModelProperty("航站楼")
    private String terminal;

    /**
     * 国际航站楼
     */
    @ApiModelProperty("国际航站楼")
    private String intlTerminal;

    /**
     * 属性 2403-国内、2401-国际、2404-混合、2402-地区;2403-国内、2401-国际、2404-混合、2402-地区
     */
    @ApiModelProperty("属性 2403-国内、2401-国际、2404-混合、2402-地区;2403-国内、2401-国际、2404-混合、2402-地区")
    private String attribute;

    /**
     * 机号
     */
    @ApiModelProperty("机号")
    private String reg;

    /**
     * 机型（国际民用航空组织指定代码ICAO）
     */
    @ApiModelProperty("机型（国际民用航空组织指定代码ICAO）")
    private String craftType;

    /**
     * 跑道
     */
    @ApiModelProperty("跑道")
    private String runway;

    /**
     * 机位（变更情况下，多个之间加上逗号隔开，这里只做本系统记录显示，实际使用记录在动态机位表中）
     */
    @ApiModelProperty("机位（变更情况下，多个之间加上逗号隔开，这里只做本系统记录显示，实际使用记录在动态机位表中）")
    private String stand;

    /**
     * 起降架次（计数字段，航线与广州有关的都设置为1，其他的为0）
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty("起降架次（计数字段，航线与广州有关的都设置为1，其他的为0）")
    private Long takeoffLandingCount;

    /**
     * 前站起飞时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty("前站起飞时间")
    private LocalDateTime previousTakeoffTime;

    /**
     * 到达下站时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty("到达下站时间")
    private LocalDateTime nextTerminalTime;

    /**
     * 是否要客（VIP标识）
     */
    @ApiModelProperty("是否要客（VIP标识）")
    private String vipFlag;

    /**
     * 是否虚拟航班
     */
    @ApiModelProperty("是否虚拟航班")
    private String virtualFlag;

    /**
     * 是否执行完毕
     */
    @ApiModelProperty("是否执行完毕")
    private String endedFlag;

    /**
     * 是否快线
     */
    @ApiModelProperty("是否快线")
    private String expressFlag;

    /**
     * 是否重点关注
     */
    @ApiModelProperty("是否重点关注")
    private String focusOnFlag;

    /**
     * 备注信息
     */
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @ApiModelProperty("备注信息")
    private String remark;

    /**
     * 内部进展状态（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内部分状态）
     */
    @ApiModelProperty("内部进展状态（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内部分状态）")
    private String innerProvidingStatus;

    /**
     * 内部异常状态（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内部分状态）
     */
    @ApiModelProperty("内部异常状态（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内部分状态）")
    private String innerAbnormalStatus;

    /**
     * 内部异常原因（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内部分不正常原因）
     */
    @ApiModelProperty("内部异常原因（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内部分不正常原因）")
    private String innerAbnormalReason;

    /**
     * 对外进展状态（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内部分状态）
     */
    @ApiModelProperty("对外进展状态（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内部分状态）")
    private String providingStatus;

    /**
     * 对外异常状态（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内部分状态）
     */
    @ApiModelProperty("对外异常状态（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内部分状态）")
    private String abnormalStatus;

    /**
     * 对外异常原因（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内部分不正常原因）
     */
    @ApiModelProperty("对外异常原因（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内部分不正常原因）")
    private String abnormalReason;

    /**
     * 国际内部进展状态（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际部分状态）
     */
    @ApiModelProperty("国际内部进展状态（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际部分状态）")
    private String intlInnerProvidingStatus;

    /**
     * 国际内部异常状态（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际部分状态）
     */
    @ApiModelProperty("国际内部异常状态（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际部分状态）")
    private String intlInnerAbnormalStatus;

    /**
     * 国际内部异常原因（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际部分不正常原因）
     */
    @ApiModelProperty("国际内部异常原因（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际部分不正常原因）")
    private String intlInnerAbnormalReason;

    /**
     * 国际进展状态（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际部分状态）
     */
    @ApiModelProperty("国际进展状态（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际部分状态）")
    private String intlProvidingStatus;

    /**
     * 国际异常状态（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际部分状态）
     */
    @ApiModelProperty("国际异常状态（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际部分状态）")
    private String intlAbnormalStatus;

    /**
     * 国际异常原因（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际部分不正常原因）
     */
    @ApiModelProperty("国际异常原因（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际部分不正常原因）")
    private String intlAbnormalReason;

    /**
     * 计划开始值机（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内旅客值机时间）
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty("计划开始值机（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内旅客值机时间）")
    private LocalDateTime scheduledCheckinStart;

    /**
     * 计划值机截止（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内旅客值机时间）
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty("计划值机截止（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内旅客值机时间）")
    private LocalDateTime scheduledCheckinEnd;

    /**
     * 实际开始值机（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内旅客值机时间）
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty("实际开始值机（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内旅客值机时间）")
    private LocalDateTime actualCheckinStart;

    /**
     * 实际值机截止（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内旅客值机时间）
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty("实际值机截止（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内旅客值机时间）")
    private LocalDateTime actualCheckinEnd;

    /**
     * 国际计划开始值机（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际旅客值机时间）
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty("国际计划开始值机（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际旅客值机时间）")
    private LocalDateTime intlScheduledCheckinStart;

    /**
     * 国际计划值机截止（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际旅客值机时间）
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty("国际计划值机截止（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际旅客值机时间）")
    private LocalDateTime intlScheduledCheckinEnd;

    /**
     * 国际实际开始值机（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际旅客值机时间）
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty("国际实际开始值机（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际旅客值机时间）")
    private LocalDateTime intlActualCheckinStart;

    /**
     * 国际实际值机截止（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际旅客值机时间）
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty("国际实际值机截止（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际旅客值机时间）")
    private LocalDateTime intlActualCheckinEnd;

    /**
     * 允许登机时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty("允许登机时间")
    private LocalDateTime allowBoardingTime;

    /**
     * 开始登机时间（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内开始登机时间）
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty("开始登机时间（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内开始登机时间）")
    private LocalDateTime boardingTime;

    /**
     * 催促登机时间（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内催促登机时间）
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty("催促登机时间（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内催促登机时间）")
    private LocalDateTime urgeBoardingTime;

    /**
     * 过站登机时间（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内过站登机时间）
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty("过站登机时间（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内过站登机时间）")
    private LocalDateTime transitBoardingTime;

    /**
     * 结束登机时间（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内结束登机时间）
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty("结束登机时间（当航班属性为国内、国际、地区时使用，当航班属性为混合时特指国内结束登机时间）")
    private LocalDateTime endBoardingTime;

    /**
     * 国际段开始登机时间（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际开始登机时间）
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty("国际段开始登机时间（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际开始登机时间）")
    private LocalDateTime intlBoardingTime;

    /**
     * 国际段催促登机时间（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际催促登机时间）
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty("国际段催促登机时间（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际催促登机时间）")
    private LocalDateTime intlUrgeBoardingTime;

    /**
     * 国际段过站登机时间（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际过站登机时间）
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty("国际段过站登机时间（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际过站登机时间）")
    private LocalDateTime intlTransitBoardingTime;

    /**
     * 国际段登机结束时间（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际结束登机时间）
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty("国际段登机结束时间（国内、国际、地区航班不使用该内容，当航班属性为混合时特指国际结束登机时间）")
    private LocalDateTime intlEndBoardingTime;

    /**
     * 备降站
     */
    @ApiModelProperty("备降站")
    private String divertStationCode;

    /**
     * 备降原因
     */
    @ApiModelProperty("备降原因")
    private String divertReason;

    /**
     * 备降计划起飞时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty("备降计划起飞时间")
    private LocalDateTime divertStd;

    /**
     * 备降预计起飞时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty("备降预计起飞时间")
    private LocalDateTime divertEtd;

    /**
     * 备降实际起飞时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty("备降实际起飞时间")
    private LocalDateTime divertAtd;

    /**
     * 备降计划降落时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty("备降计划降落时间")
    private LocalDateTime divertSta;

    /**
     * 备降预计降落时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty("备降预计降落时间")
    private LocalDateTime divertEta;

    /**
     * 备降实际降落时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty("备降实际降落时间")
    private LocalDateTime divertAta;

    /**
     * 修改代码
     */
    @ApiModelProperty("修改代码")
    private String updateCode;

    /**
     * 是否删除
     */
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @ApiModelProperty("是否删除")
    private Integer delFlag;

    /**
     * 创建人
     */
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @ApiModelProperty("创建人")
    private String createBy;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @ApiModelProperty("创建时间")
    private LocalDateTime createTime;

    /**
     * 更新人
     */
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @ApiModelProperty("更新人")
    private String updateBy;

    /**
     * 更新时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @ApiModelProperty("更新时间")
    private LocalDateTime updateTime;

    /**
     * 版本号
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty("版本号")
    private Long version;

    /**
     * 有参构造函数
     *
     * @param entity
     */
    public FimsFlightInfoVO(FimsFlightInfo entity) {

        this.flightId = entity.getFlightId();
        this.associatedFlightId = entity.getAssociatedFlightId();
        this.majorFlightId = entity.getMajorFlightId();
        this.virtualOrder = entity.getVirtualOrder();
        this.execDate = entity.getExecDate();
        this.task = entity.getTask();
        this.airlinesIata = entity.getAirlinesIata();
        this.subAirlinesId = entity.getSubAirlinesId();
        this.flightNo = entity.getFlightNo();
        this.offInFlag = entity.getOffInFlag();
        this.agency = entity.getAgency();
        this.terminal = entity.getTerminal();
        this.intlTerminal = entity.getIntlTerminal();
        this.attribute = entity.getAttribute();
        this.reg = entity.getReg();
        this.craftType = entity.getCraftType();
        this.runway = entity.getRunway();
        this.stand = entity.getStand();
        this.takeoffLandingCount = entity.getTakeoffLandingCount();
        this.previousTakeoffTime = entity.getPreviousTakeoffTime();
        this.nextTerminalTime = entity.getNextTerminalTime();
        this.vipFlag = entity.getVipFlag();
        this.virtualFlag = entity.getVirtualFlag();
        this.endedFlag = entity.getEndedFlag();
        this.expressFlag = entity.getExpressFlag();
        this.focusOnFlag = entity.getFocusOnFlag();
        this.remark = entity.getRemark();
        this.innerProvidingStatus = entity.getInnerProvidingStatus();
        this.innerAbnormalStatus = entity.getInnerAbnormalStatus();
        this.innerAbnormalReason = entity.getInnerAbnormalReason();
        this.providingStatus = entity.getProvidingStatus();
        this.abnormalStatus = entity.getAbnormalStatus();
        this.abnormalReason = entity.getAbnormalReason();
        this.intlInnerProvidingStatus = entity.getIntlInnerProvidingStatus();
        this.intlInnerAbnormalStatus = entity.getIntlInnerAbnormalStatus();
        this.intlInnerAbnormalReason = entity.getIntlInnerAbnormalReason();
        this.intlProvidingStatus = entity.getIntlProvidingStatus();
        this.intlAbnormalStatus = entity.getIntlAbnormalStatus();
        this.intlAbnormalReason = entity.getIntlAbnormalReason();
        this.scheduledCheckinStart = entity.getScheduledCheckinStart();
        this.scheduledCheckinEnd = entity.getScheduledCheckinEnd();
        this.actualCheckinStart = entity.getActualCheckinStart();
        this.actualCheckinEnd = entity.getActualCheckinEnd();
        this.intlScheduledCheckinStart = entity.getIntlScheduledCheckinStart();
        this.intlScheduledCheckinEnd = entity.getIntlScheduledCheckinEnd();
        this.intlActualCheckinStart = entity.getIntlActualCheckinStart();
        this.intlActualCheckinEnd = entity.getIntlActualCheckinEnd();
        this.allowBoardingTime = entity.getAllowBoardingTime();
        this.boardingTime = entity.getBoardingTime();
        this.urgeBoardingTime = entity.getUrgeBoardingTime();
        this.transitBoardingTime = entity.getTransitBoardingTime();
        this.endBoardingTime = entity.getEndBoardingTime();
        this.intlBoardingTime = entity.getIntlBoardingTime();
        this.intlUrgeBoardingTime = entity.getIntlUrgeBoardingTime();
        this.intlTransitBoardingTime = entity.getIntlTransitBoardingTime();
        this.intlEndBoardingTime = entity.getIntlEndBoardingTime();
        this.divertStationCode = entity.getDivertStationCode();
        this.divertReason = entity.getDivertReason();
        this.divertStd = entity.getDivertStd();
        this.divertEtd = entity.getDivertEtd();
        this.divertAtd = entity.getDivertAtd();
        this.divertSta = entity.getDivertSta();
        this.divertEta = entity.getDivertEta();
        this.divertAta = entity.getDivertAta();
        this.updateCode = entity.getUpdateCode();
        this.delFlag = entity.getDelFlag();
        this.createBy = entity.getCreateBy();
        this.createTime = entity.getCreateTime();
        this.updateBy = entity.getUpdateBy();
        this.updateTime = entity.getUpdateTime();
        this.version = entity.getVersion();
    }


}
