package com.abcode.test.oracletest.gen.service.impl;


import com.abcode.test.oracletest.Result;
import com.abcode.test.oracletest.gen.entity.FimsFlightAirline;
import com.abcode.test.oracletest.gen.mapper.FimsFlightAirlineMapper;
import com.abcode.test.oracletest.gen.param.FimsFlightAirlineParam;
import com.abcode.test.oracletest.gen.service.FimsFlightAirlineService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


/**
 * 动态经停站（航线）表(FimsFlightAirline)表服务实现类
 *
 * @author qinzhitao
 * @since 2023-08-19 22:02:20
 */
@Service
public class FimsFlightAirlineServiceImpl extends ServiceImpl<FimsFlightAirlineMapper, FimsFlightAirline> implements FimsFlightAirlineService {

    @Resource
    FimsFlightAirlineMapper fimsFlightAirlineMapper;

    @Override
    public IPage pageQuery(FimsFlightAirlineParam param) {
        FimsFlightAirline fimsFlightAirline = param.toEntity();
        Page page = param.getPage();
        LambdaQueryWrapper queryWrapper = new LambdaQueryWrapper<>(fimsFlightAirline);
        IPage selectPage = this.page(page, queryWrapper);
        return selectPage;
    }

    @Override
    public Result add(FimsFlightAirlineParam param) {
        FimsFlightAirline fimsFlightAirline = param.toEntity();
        boolean resultFlag = this.save(fimsFlightAirline);
        if (resultFlag) {
            return Result.OK();
        } else {
            return Result.error(Result.ERROR_MESSAGE);
        }
    }

    @Override
    public Result edit(FimsFlightAirlineParam param) {
        FimsFlightAirline fimsFlightAirline = param.toEntity();
        boolean resultFlag = this.updateById(fimsFlightAirline);
        if (resultFlag) {
            return Result.OK(true);
        } else {
            return Result.error(Result.ERROR_MESSAGE, false);
        }
    }

    @Override
    public List<FimsFlightAirline> listQuery(FimsFlightAirlineParam param) {
        FimsFlightAirline fimsFlightAirline = param.toEntity();
        LambdaQueryWrapper queryWrapper = new LambdaQueryWrapper(fimsFlightAirline);
        return this.list(queryWrapper);
    }

    @Override
    public Result batchAdd() {
        List<FimsFlightAirline> list = new ArrayList<>();
        int count = 3;
        long fltId = System.currentTimeMillis();
        for (int i = 0; i < count; i++) {
            FimsFlightAirline entity = new FimsFlightAirline();
            entity.setFlightId(fltId);
            entity.setOrderCode(i);
            entity.setAirportIata("");
            entity.setStd(LocalDateTime.now());
            entity.setEtd(LocalDateTime.now());
            entity.setAtd(LocalDateTime.now());
            entity.setSta(LocalDateTime.now());
            entity.setEta(LocalDateTime.now());
            entity.setAta(LocalDateTime.now());
            entity.setAttribute("3401");
            entity.setRemark("test");
            entity.setDelFlag(0);
            entity.setCreateBy("batchADD");
            entity.setCreateTime(LocalDateTime.now());
            entity.setUpdateBy("");
            entity.setUpdateTime(LocalDateTime.now());

            list.add(entity);
        }
       fimsFlightAirlineMapper.insertBatchSomeColumn(list);
        return Result.ok();
    }
}

