package com.abcode.test.oracletest.fims.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.abcode.test.oracletest.fims.entity.FimsTempPlanFlt;

/**
 * 临时计划航班表(FimsTempPlanFlt)表服务接口
 *
 * @author qinzhitao
 * @since 2023-06-29 16:39:47
 */
public interface FimsTempPlanFltService extends IService<FimsTempPlanFlt> {

}

