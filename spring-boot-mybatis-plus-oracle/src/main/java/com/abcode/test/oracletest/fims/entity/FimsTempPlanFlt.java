package com.abcode.test.oracletest.fims.entity;

import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
/**
 * 临时计划航班表(FimsTempPlanFlt)表实体类
 *
 * @author qinzhitao
 * @since 2023-06-29 16:39:47
 */
@Data
@SuppressWarnings("serial")
@TableName("FIMS_TEMP_PLAN_FLT")
public class FimsTempPlanFlt {
            
    /** 主键 */        
                              
    @TableId(value= "ID")
    private Long id;
                        
    /** 航空公司ID */        
                            
    @TableField("AIRLINES_ID")
    private Long airlinesId;
                        
    /** 航班号 */        
                            
    @TableField("FLIGHT_NO")
    private String flightNo;
                        
    /** 进出港标识：A-进；D-出； */        
                            
    @TableField("OFF_IN_FLAG")
    private String offInFlag;
                        
    /** 任务 */        
                            
    @TableField("TASK")
    private String task;
                        
    /** 属性 */        
                            
    @TableField("ATTRIBUTE")
    private String attribute;
                        
    /** 机型ID */        
                            
    @TableField("CRAFT_TYPE_ID")
    private Long craftTypeId;
                        
    /** 始发站ID */        
                            
    @TableField("START_STATION_ID")
    private Long startStationId;
                        
    /** 计划起飞时间 */        
                            
    @TableField("STD")
    private LocalDateTime std;
                        
    /** 目的站ID */        
                            
    @TableField("TERMINAL_STATION_ID")
    private Long terminalStationId;
                        
    /** 计划降落时间 */        
                            
    @TableField("STA")
    private LocalDateTime sta;
                        
    /** 航站楼 */        
                            
    @TableField("TERMINAL")
    private String terminal;
                        
    /** 是否是虚拟航班：Y-是；N-否； */        
                            
    @TableField("VIRTUAL_FLAG")
    private String virtualFlag;
                        
    /** 主航班ID */        
                            
    @TableField("MAJOR_FLT_ID")
    private Long majorFltId;
                        
    /** 虚拟航班排序 */        
                            
    @TableField("VIRTUAL_ORDER")
    private Integer virtualOrder;
                        
    /** 关联航班ID */        
                            
    @TableField("ASSOCIATED_FLT_ID")
    private Long associatedFltId;
                        
    /** 班期开始时间 */        
                            
    @TableField("START_TIME")
    private LocalDateTime startTime;
                        
    /** 班期结束时间 */        
                            
    @TableField("END_TIME")
    private LocalDateTime endTime;
                        
    /** 星期一 */        
                            
    @TableField("EXEC_MON")
    private String execMon;
                        
    /** 星期二 */        
                            
    @TableField("EXEC_TUE")
    private String execTue;
                        
    /** 星期三 */        
                            
    @TableField("EXEC_WED")
    private String execWed;
                        
    /** 星期四 */        
                            
    @TableField("EXEC_THU")
    private String execThu;
                        
    /** 星期五 */        
                            
    @TableField("EXEC_FRI")
    private String execFri;
                        
    /** 星期六 */        
                            
    @TableField("EXEC_SAT")
    private String execSat;
                        
    /** 星期天 */        
                            
    @TableField("EXEC_SUN")
    private String execSun;
                        
    /** 是否取消：Y-是；N-否； */        
                            
    @TableField("CANCEL_FLAG")
    private String cancelFlag;
                        
    /** 是否删除：0-否；1-是； */        

    @TableField("DEL_FLAG")
    private Integer delFlag;
                        
    /** 创建人 */        
                            
    @TableField("CREATE_BY")
    private String createBy;
                        
    /** 创建时间 */        
                            
    @TableField("CREATE_TIME")
    private LocalDateTime createTime;
                        
    /** 更新人 */        
                            
    @TableField("UPDATE_BY")
    private String updateBy;
                        
    /** 更新时间 */        
                            
    @TableField("UPDATE_TIME")
    private LocalDateTime updateTime;
                        
    /** 备注 */        
                            
    @TableField("REMARK")
    private String remark;
            
}
