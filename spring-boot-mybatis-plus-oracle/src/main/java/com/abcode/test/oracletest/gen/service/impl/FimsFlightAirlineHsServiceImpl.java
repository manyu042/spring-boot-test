package com.abcode.test.oracletest.gen.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.abcode.test.oracletest.gen.mapper.FimsFlightAirlineHsMapper;
import com.abcode.test.oracletest.gen.entity.FimsFlightAirlineHs;
import com.abcode.test.oracletest.gen.service.FimsFlightAirlineHsService;
import org.springframework.stereotype.Service;

/**
 * 动态经停站（航线）表(FimsFlightAirlineHs)表服务实现类
 *
 * @author qinzhitao
 * @since 2023-05-05 11:40:05
 */
@Service
public class FimsFlightAirlineHsServiceImpl extends ServiceImpl<FimsFlightAirlineHsMapper, FimsFlightAirlineHs> implements FimsFlightAirlineHsService {

}

