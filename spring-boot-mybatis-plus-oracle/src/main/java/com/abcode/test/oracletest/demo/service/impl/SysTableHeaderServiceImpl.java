package com.abcode.test.oracletest.demo.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.abcode.test.oracletest.demo.dao.SysTableHeaderDao;
import com.abcode.test.oracletest.demo.entity.SysTableHeader;
import com.abcode.test.oracletest.demo.service.SysTableHeaderService;
import org.springframework.stereotype.Service;

/**
 * 系统默认表的表头信息(SysTableHeader)表服务实现类
 *
 * @author qinzhitao
 * @since 2023-03-14 13:53:54
 */
@Service
public class SysTableHeaderServiceImpl extends ServiceImpl<SysTableHeaderDao, SysTableHeader> implements SysTableHeaderService {

}

