package com.abcode.test.oracletest.gen.entity;

import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 动态共享航班表(FimsFlightShare)表实体类
 *
 * @author qinzhitao
 * @since 2023-08-19 22:02:35
 */
@Data
@SuppressWarnings("serial")
@TableName("FIMS_FLIGHT_SHARE")
public class FimsFlightShare {

    /**
     * ID
     */
    @TableId(value = "ID",type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 航班ID
     */
    @TableField("FLIGHT_ID")
    private Long flightId;

    /**
     * 航空公司二字码
     */
    @TableField("AIRLINES_IATA")
    private String airlinesIata;

    /**
     * 航班号
     */
    @TableField("FLIGHT_NO")
    private String flightNo;

    /**
     * 排序编码
     */
    @TableField("ORDER_CODE")
    private Integer orderCode;

    /**
     * 是否删除
     */
    @TableField("DEL_FLAG")
    private Integer delFlag;

    /**
     * 创建人
     */
    @TableField("CREATE_BY")
    private String createBy;

    /**
     * 创建时间
     */
    @TableField("CREATE_TIME")
    private LocalDateTime createTime;

    /**
     * 更新人
     */
    @TableField("UPDATE_BY")
    private String updateBy;

    /**
     * 更新时间
     */
    @TableField("UPDATE_TIME")
    private LocalDateTime updateTime;

//<editor-fold desc="字段名常量">
    /**
     * ID
     */
    public static final String ID = "ID";
    /**
     * 航班ID
     */
    public static final String FLIGHT_ID = "FLIGHT_ID";
    /**
     * 航空公司二字码
     */
    public static final String AIRLINES_IATA = "AIRLINES_IATA";
    /**
     * 航班号
     */
    public static final String FLIGHT_NO = "FLIGHT_NO";
    /**
     * 排序编码
     */
    public static final String ORDER_CODE = "ORDER_CODE";
    /**
     * 是否删除
     */
    public static final String DEL_FLAG = "DEL_FLAG";
    /**
     * 创建人
     */
    public static final String CREATE_BY = "CREATE_BY";
    /**
     * 创建时间
     */
    public static final String CREATE_TIME = "CREATE_TIME";
    /**
     * 更新人
     */
    public static final String UPDATE_BY = "UPDATE_BY";
    /**
     * 更新时间
     */
    public static final String UPDATE_TIME = "UPDATE_TIME";
//</editor-fold>

}
