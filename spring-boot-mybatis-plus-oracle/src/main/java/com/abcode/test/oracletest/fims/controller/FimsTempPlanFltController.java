package com.abcode.test.oracletest.fims.controller;



import com.abcode.test.oracletest.Result;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.io.Serializable;
import java.util.List;

import com.abcode.test.oracletest.fims.entity.FimsTempPlanFlt;
import com.abcode.test.oracletest.fims.service.FimsTempPlanFltService;


/**
 * 临时计划航班表(FimsTempPlanFlt)表控制层
 *
 * @author qinzhitao
 * @since 2023-06-29 16:39:47
 */
@RestController
@RequestMapping("fimsTempPlanFlt")
public class FimsTempPlanFltController {
    /**
     * 服务对象
     */
    @Resource
    private FimsTempPlanFltService fimsTempPlanFltService;

    /**
     * 分页查询所有数据
     * @param page 分页对象
     * @param fimsTempPlanFlt 查询实体
     * @return 所有数据
     */
    @GetMapping("/selectAll")
    public Result selectAll(Page<FimsTempPlanFlt> page, FimsTempPlanFlt fimsTempPlanFlt) {
        Page<FimsTempPlanFlt> rst = this.fimsTempPlanFltService.page(page, new QueryWrapper<>(fimsTempPlanFlt));
        return Result.ok(rst);
    }

    /**
     * 通过主键查询单条数据
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("/selectOne/{id}")
    public Result selectOne(@PathVariable Serializable id) {
        FimsTempPlanFlt fimsTempPlanFlt = this.fimsTempPlanFltService.getById(id);
        return Result.ok(fimsTempPlanFlt);
    }

    /**
     * 新增数据
     * @param fimsTempPlanFlt 实体对象
     * @return 新增结果
     */
    @PostMapping("/insert")
    public Result insert(@RequestBody FimsTempPlanFlt fimsTempPlanFlt) {
        Boolean rst = this.fimsTempPlanFltService.save(fimsTempPlanFlt);
        return Result.OK(rst);
    }

    /**
     * 修改数据
     * @param fimsTempPlanFlt 实体对象
     * @return 修改结果
     */
    @PutMapping("/update")
    public Result update(@RequestBody FimsTempPlanFlt fimsTempPlanFlt) {
        Boolean rst = this.fimsTempPlanFltService.updateById(fimsTempPlanFlt);
        return Result.OK(rst);
    }

    /**
     * 删除数据
     *
     * @param idList 主键结合
     * @return 删除结果
     */
    @DeleteMapping("/delete")
    public Result delete(@RequestParam("idList") List<Long> idList) {
        boolean rst = this.fimsTempPlanFltService.removeByIds(idList);
        return Result.ok(rst);
    }

    @PostMapping("/testDel")
    public Result testDel(){
        Long id1 = 1674324949200650241L;
        Long id2 = 1674583005998174210L;
        int  delRst = this.fimsTempPlanFltService.getBaseMapper().deleteById(id2);
        return Result.OK(delRst);
    }
}

