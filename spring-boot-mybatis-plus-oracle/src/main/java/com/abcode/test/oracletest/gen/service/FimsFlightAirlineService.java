package com.abcode.test.oracletest.gen.service;

import com.abcode.test.oracletest.Result;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.abcode.test.oracletest.gen.entity.FimsFlightAirline;
import com.abcode.test.oracletest.gen.param.FimsFlightAirlineParam;

import java.util.List;

/**
 * 动态经停站（航线）表(FimsFlightAirline)表服务接口
 *
 * @author qinzhitao
 * @since 2023-08-19 22:02:20
 */
public interface FimsFlightAirlineService extends IService<FimsFlightAirline> {
    /**
     * 分页查找动态经停站（航线）表
     *
     * @param param
     * @return
     */
    IPage pageQuery(FimsFlightAirlineParam param);

    /**
     * 新增动态经停站（航线）表
     *
     * @param param
     * @return
     */
    Result add(FimsFlightAirlineParam param);

    /**
     * 修改动态经停站（航线）表
     *
     * @param param
     * @return
     */
    Result edit(FimsFlightAirlineParam param);

    /**
     * 条件查询动态经停站（航线）表
     *
     * @param param
     * @return
     */
    List<FimsFlightAirline> listQuery(FimsFlightAirlineParam param);

    Result batchAdd();
}

