package test.abcode.poi.test;


import java.util.List;

public class WordParserExample {
    private static final String FILE_PATH = "D:\\tmp\\doc\\危险品航空运输事件信息报告表-导入模板.docx";

    public static void main(String[] args) {

        try {
            // 调用工具类解析 Word 表格中的复选框
            List<List<WordCheckboxParser.CellResult>> results = WordCheckboxParser.parseWordTable(FILE_PATH);

            // 输出解析结果
            for (int rowIndex = 0; rowIndex < results.size(); rowIndex++) {
                System.out.println("Row " + (rowIndex + 1) + ":");
                for (WordCheckboxParser.CellResult cellResult : results.get(rowIndex)) {
                    System.out.println("  Cell Text: " + cellResult.getText());
                    System.out.println("  Checkboxes: " + cellResult.getCheckboxes());
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
