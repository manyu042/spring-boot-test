package test.abcode.poi.test;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 工具类：解析 Word 表格中的复选框及其关联文本
 */
public class WordCheckboxParser {

    /**
     * 解析 Word 文档中的表格，并检查复选框状态
     *
     * @param filePath Word 文件路径
     * @return 表格解析结果，包含每个单元格的内容及复选框状态
     * @throws IOException 如果文件读取失败
     */
    public static List<List<CellResult>> parseWordTable(String filePath) throws IOException {
        // 打开 Word 文档
        try (FileInputStream fis = new FileInputStream(filePath);
             XWPFDocument document = new XWPFDocument(fis)) {

            List<List<CellResult>> tableResults = new ArrayList<>();

            // 遍历文档中的所有表格
            for (XWPFTable table : document.getTables()) {
                List<CellResult> rowResults = new ArrayList<>();

                // 遍历每一行
                for (XWPFTableRow row : table.getRows()) {
                    // 遍历行中的每个单元格
                    for (XWPFTableCell cell : row.getTableCells()) {
                        String cellText = cell.getText().trim(); // 获取单元格内容
                        List<CheckboxResult> checkboxes = detectCheckboxesWithText(cellText); // 检测复选框及其关联文本
                        rowResults.add(new CellResult(cellText, checkboxes));
                    }
                }
                tableResults.add(rowResults);
            }

            return tableResults;
        }
    }

    /**
     * 检测单元格中包含的复选框及其关联的文本
     *
     * @param cellText 单元格内容
     * @return 单元格中所有复选框的状态及关联文本列表
     */
    private static List<CheckboxResult> detectCheckboxesWithText(String cellText) {
        List<CheckboxResult> checkboxResults = new ArrayList<>();

        // 使用正则表达式匹配复选框及其后续文本
        String checkboxPattern = "(☑|☐)\\s*([^☑☐]*)"; // 匹配复选框及其后续文本
        Matcher matcher = Pattern.compile(checkboxPattern).matcher(cellText);

        while (matcher.find()) {
            String checkboxSymbol = matcher.group(1); // 复选框符号 (☑ 或 ☐)
            String associatedText = matcher.group(2).trim(); // 复选框后的文本

            boolean isChecked = "☑".equals(checkboxSymbol);
            checkboxResults.add(new CheckboxResult(isChecked, associatedText));
        }

        return checkboxResults;
    }

    /**
     * 单元格解析结果类
     */
    public static class CellResult {
        private final String text;                       // 单元格文本
        private final List<CheckboxResult> checkboxes;   // 单元格中的复选框列表

        public CellResult(String text, List<CheckboxResult> checkboxes) {
            this.text = text;
            this.checkboxes = checkboxes;
        }

        public String getText() {
            return text;
        }

        public List<CheckboxResult> getCheckboxes() {
            return checkboxes;
        }

        @Override
        public String toString() {
            return "CellResult{" +
                    "text='" + text + '\'' +
                    ", checkboxes=" + checkboxes +
                    '}';
        }
    }

    /**
     * 复选框解析结果类
     */
    public static class CheckboxResult {
        private final boolean isChecked; // 复选框是否被选中
        private final String associatedText; // 复选框关联的文本内容

        public CheckboxResult(boolean isChecked, String associatedText) {
            this.isChecked = isChecked;
            this.associatedText = associatedText;
        }

        public boolean isChecked() {
            return isChecked;
        }

        public String getAssociatedText() {
            return associatedText;
        }

        @Override
        public String toString() {
            return "{isChecked=" + isChecked + ", associatedText='" + associatedText + "'}";
        }
    }
}
