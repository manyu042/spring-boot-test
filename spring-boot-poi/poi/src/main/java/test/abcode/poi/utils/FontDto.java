package test.abcode.poi.utils;

import java.util.Objects;

public class FontDto {

    private String font;

    private String fontSize;

    public String getFont() {
        return font;
    }

    public void setFont(String font) {
        this.font = font;
    }

    public String getFontSize() {
        return fontSize;
    }

    public void setFontSize(String fontSize) {
        this.fontSize = fontSize;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        FontDto fontDto = (FontDto) o;
        return Objects.equals(font, fontDto.font) &&
                Objects.equals(fontSize, fontDto.fontSize);
    }

    @Override
    public int hashCode() {
        return Objects.hash(font, fontSize);
    }
}
