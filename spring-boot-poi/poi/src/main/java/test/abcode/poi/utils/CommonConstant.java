package test.abcode.poi.utils;

/**
 * 公共枚举类
 *
 * @author luojiazhi
 * @date 2020-09-03 16:41
 **/
public interface CommonConstant {
    /**
     * 特殊符号
     */
    String QUESTION_MARK = "?";
    String COLON = ":";
    String COMMA = ",";
    String CHN_COMMA = "，";
    String VERTICAL = "|";
    String HYPHEN = "-";
    String SPACE = " ";
    String SINGLE_QUOTATION_MARK = "'";
    String UNDERSCORE = "_";
    String DIAGONAL = "/";
    String EMPTY = "";
    String POUND = "#";
    String MULTIPLICATION = "*";
    String MULTIPLICATION_REGEXP = "\\*";
    String POINT = ".";
    String SEMICOLON = ";";
    String FULL_STOP = "。";
    String ROOT = "ROOT";
    String PAUSE = "、";
    String SQUARE_BRACKETS = "[]";
    String LEFT_SQUARE_BRACKETS = "[";
    String LEFT_SQUARE_BRACKETS_CHN = "【";
    String RIGHT_SQUARE_BRACKETS = "]";
    String RIGHT_SQUARE_BRACKETS_CHN = "】";
    String LEFT_BRACKET_CHN = "\\（";
    String RIGHT_BRACKET_CHN = "\\）";
    String LEFT_BRACKET = "\\(";
    String RIGHT_BRACKET = "\\)";
    String LINE_BREAKS = "\n";
    String RIGHT_ARROW = "->";
    Integer SC_OK_200 = 200;
    Integer SC_INTERNAL_SERVER_ERROR_500 = 500;
    Integer SC_INTERNAL_SERVER_ERROR_501 = 501;
    Integer SC_JEECG_NO_AUTHZ = 510;
    String DEPT_SECOND = "SECOND";
    String DEPT_THIRD = "THIRD";
    String DEPT_FOURTH = "FOURTH";
    String IMPORT_FLAG_KEY = "1";
    String PREFIX_USER_TOKEN = "PREFIX_USER_TOKEN";
    String DYNABEAN = "dynaBean";
    String REDIS_KEY_VERSION_CODE = "VERSION_CODE_";


    /**
     * 身份验证常量
     */
    String AUTHORIZATION = "Authorization";
    /**
     * 微服务标识
     */
    String PD_KEY = "sms";

    String CONTENT_TYPE_VALUE = "multipart/form-data";

    /**
     * 文件服务器路径
     */
    String FILE_UPLOADS_KEY = "servicecomb.uploads.directory";

    String EXCEL_DOWNLOAD_KEY = "excelDownloadKeys";
    String PREFIX_DEPART_TREE = "depart:tree";
    String PREFIX_NO_EXPAND_TREE = "company:tree";
    String PREFIX_NO_EXPAND_SHORT_TREE = "company:tree:short";
    String COMPANY_MSG_KEY = "company:dist:msg";
    String DEPT_MSG_KEY = "dept:dist:msg";
    String GROUP_EVENT_NUMBER_KEY = "group:event:number";
    String FILLING_COMPANY_MSG_KEY = "filling:company:dist:msg";
    long DEPART_TREE_TIMEOUT = 60 * 60 * 24L;

    /**
     * 12月份字符串数组
     */
    String[] MONTHS = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};

    /**
     * 数据统计维度
     * 检查小组：1/被检查单位：2
     */
    class StatDimension {
        public static final String CHECK_TEAM = "1";
        public static final String BE_CHECKED_DEPART = "2";
    }

    /**
     * 角色类型常量
     */
    class RoleTypeConstant {
        public static final String ADMIN = "admin";
        public static final String OFFICE = "office";
        public static final String LEADER = "leader";
        public static final String SECOND = "second";
        public static final String NORMAL = "normal";
        public static final String CHECK_TEAM = "checkTeam";

    }

    class RoleIdConstant {
        /**
         * 股份二级单位部门安全负责人角色ID
         */
        public static final String SAFETY_MANAGER_ROLE_ID = "15jyUMueA5dz694vkK5";
        /**
         * 集团部门安全负责人角色id
         */
        public static final String GROUP_DEPT_DEPT_SAFE_LEADER_ID = "AMsBHw0pdXcs7FJ2b54";
        /**
         * 集团安全信息联络人角色id
         */
        public static final String GROUP_DEPT_SAFE_SECURITY_CONTACT_ROLE_ID = "DcrREiYhYOKpbbsSNHD";
        /**
         * 安全部门专员
         */
        public static final String SAFETY_SUPERVISION_DEPARTMENT_ROLE_ID = "nQ1vRJZfKFLgfn2PTmd";
        /**
         * 安全部门成员
         */
        public static final String SAFETY_SUPERVISION_DEPARTMENT_MEMBER_ROLE_ID = "RT9Lj5QNeeJFJfy6Hkf";
        /**
         * 安全部门负责人
         */
        public static final String SAFETY_SUPERVISION_DEPARTMENT_PRINCIPAL_ROLE_ID = "nOsTc0nofv2M9oS51an";
        /**
         * 安全分管领导
         */
        public static final String SAFETY_CHARGE_OF_LEADERSHIP_ROLE_ID = "8SjmmsOpF7xkMH8WywV";
        /**
         * 公司领导
         */
        public static final String LEADE_SECONDARY_UNIT_COMPANY_ROLE_ID = "BsuBXaw1pTmVbApkE8Q";
        /**
         * 值班领导
         */
        public static final String DUTY_LEADER_ROLE_ID = "QG4935WfE7Fy647AYBC";
        /**
         * 二级单位运控部门信息员
         */
        public static final String OPERATIONS_CONTROL_ROLE_ID = "JzjlJvlv2cwYdOMSlWx";
        /**
         * 股份运控安质部信息员
         */
        public static final String STOCK_QUALITY_DEPARTMENT_ROLE_ID = "VBEIwofS2WZxmdlJ5uT";
        /**
         * 通用角色-部门负责人
         */
        public static final String DEPT_SAFE_LEADER_ID = "c3G67lYN3L98OzRTilH";
        /**
         * 通用角色-部门员工
         */
        public static final String DEPT_EMPLOYEES_ID = "4mbf5k76mhjIU3arwrY";

        /**
         * 集团安全监管部门负责人
         */
        public static final String GROUP_SAFE_REGULATION_LEADER_ID = "ZCjK4h9ZpnWRkn9prJS";
        /**
         * 集团安全分管领导
         */
        public static final String GROUP_SAFE_LEADER_ID = "MV0yZOlltDzrgHMYTgw";
        /**
         * 集团安全信息联络员
         */
        public static final String GROUP_SAFE_INFO_CONTACT_ID = "DcrREiYhYOKpbbsSNHD";
        /**
         * 股份安全部门管理员
         */
        public static final String BY_SAFE_MANAGER_ID = "AC3NAtOcj6asH5HDUK8";
        /**
         * 股份二级单位分管领导
         */
        public static final String BY_SECOND_SAFE_LEADER_ID = "nWxYdlJZXM70rY7ooIf";
        /**
         * 股份二级单位安全信息员
         */
        public static final String BY_SECOND_SAFE_INFO_CONTACT_ID = "xEpeAzgZIEwl2PPAlFr";
        /**
         * 二级单位部门安全负责人
         */
        public static final String SECOND_LEADER_ID = "qYM48T14lQjytHPqoOv";
        /**
         * 二级单位安全分管领导
         */
        public static final String SECOND_SAFE_LEADER_ID = "nWxYdlJZXM70rY7ooIf";
        /**
         * 二级单位安全信息员
         */
        public static final String SECOND_SAFE_INFO_CONTACT_ID = "xEpeAzgZIEwl2PPAlFr";
        /**
         * 安全分管领导
         */
        public static final String SAFETY_BRANCH_LEADER_SHIP = "8SjmmsOpF7xkMH8WywV";
        /**
         * 信息同步-安保强制报告信息员
         */
        public static final String SECURITY_REPORT_MESSENGER = "9VTvZt0gEr9nh8tBktr";
        /**
         * 信息同步-危险品强制报告信息员
         */
        public static final String DANGER_REPORT_MESSENGER = "M1kexY5YJkem82xwCIy";
        /**
         * 信息同步-民航强制报告信息员
         */
        public static final String MH_REPORT_MESSENGER = "3hjvqPE97doNhJUlJVp";
        /**
         * 信息同步-集团强制报告信息员
         */
        public static final String GROUP_REPORT_MESSENGER = "rybOZ97mLWO9kWbCDHc";
        /**
         * 安全委员会
         */
        public static final String SAFETY_COMMITTEE = "C6oW6MdYKp5urBy4hs0";
        /**
         * 主要负责人
         */
        public static final String SAFETY_FIRST_HEAD_ROLE_ID = "eJWwJQMkbnl6mTyHlXV";
        /**
         * 总经理
         */
        public static final String GENERAL_MANAGER_ROLE_ID = "17QsKZMgYeSj0h6lLIz";
        /**
         * 业务分管领导
         */
        public static final String BUSINESS_LEADER_IN_CHARGE = "XH6SkKfYkFFKdhm2BIG";
        /**
         * 安全总监
         */
        public static final String DIRECTOR_OF_SAFETY = "Y2mrop5YAu3wpCBdr1X";
        /**
         * 风险管理专员
         */
        public static final String RISK_MANAGEMENT_SPECIALIST = "8fs4qOtTMYhlvlxfb6f";
        /**
         * 部门安全员
         */
        public static final String RISK_MANAGEMENT_SPECIALIST_NAME = "风险管理专员";

        /**
         * 部门安全专员
         */
        public static final String DEPT_SAFETY_PERSON = "bLiND4727FN7buzn2U3";
        /**
         * 民航强制报告信息员
         */
        public static final String SAFE_REPORT_MESSENGER = "XVqRigSxD0zwYsTB7nV";
        /**
         * 管理员
         */
        public static final String ADMIN = "cJmtdhte06fVzNbH0Pz";
        /**
         * 区域管理部门成员
         */
        public static final String REGION_ADMIN = "Zd2F4kRTCRsOPoYp5Vi";
        /**
         * 紧急事件初报报送类型-民航以及集团
         */
        public static final String SAFE_URGENT_REPORT_MH_GROUP = "4XVJVIAIcTit1QFisGN";
        /**
         * 紧急事件初报报送类型-安保
         */
        public static final String SAFE_URGENT_REPORT_SECURITY = "L4h9kofBwvntPD2pmpV";
        /**
         * 紧急事件初报报送类型-危险源
         */
        public static final String SAFE_URGENT_REPORT_DANGER = "LrV3hcwLDyFyNK5LvWK";
        /**
         * 危险品安全信息联络员
         */
        public static final String SAFE_DANGER_REPORT_ROLE = "e9foAOdUAMfX54EyUgD";
        /**
         * 安全保卫安全信息联络员
         */
        public static final String SAFE_SECURITY_REPORT_ROLE = "RQQ57mCjBn8tsQYgQzw";
        /**
         * 紧急事件初报-湛江-传阅
         */
        public static final String JIEYANGCHAOSHAN_AIRPORT_COMPANY_URGENT_REPORT_ROLE = "NZ3r1k1cjLnoJP5UzgF";
        /**
         * 短信抄送人
         */
        public static final String MSG_COPY_SEND_USER_ROLE = "bVqgghRCnbXc5upyLy7";

        /**
         * 风险管理业务人员
         */
        public static final String RISK_BUSINESS = "T0qQ8uMqhEtu0CpBGu5";

    }

    class ExcelStr {
        public static final String SUFFIX_XLSX = ".xlsx";
        public static final String SUFFIX_XLX = ".xls";
        public static final String FILE_KEY = "FILE_KEY";
        public static final String FILE_NAME = "FILE_NAME";
        public static final String CONTENT_TYPE = "application/vnd.ms-excel;charset=utf-8";
        public static final String XLS_REGEX = "^.+\\.(?i)(xls)$";
        public static final String XLSX_REGEX = "^.+\\.(?i)(xlsx)$";
        /**
         * excel下载模板中的编号列
         */
        public static final String COLUMN_NUMBER = "COLUMN_NUMBER";
        public static final String SUFFIX_CSV = ".csv";
    }

    class FileType {
        public static final String XLSX = "xlsx";
        public static final String XLS = "xls";
        public static final String PDF = "pdf";
        public static final String DOCX = "docx";
        public static final String DOC = "doc";
        public static final String PPT = "ppt";
        public static final String PPTX = "pptx";
    }

    class SqlStr {
        public static final String LAST_LIMIT_ONE = " LIMIT 1";
        public static final String DEFAULT_EMPTY = "1=1 ";
        public static final String DEFAULT_SQL = "(SY_CREATEUSERID='@USER_ID@' and (AUDFLAG = 'NOSTATUS')) or (SY_COMPANY_ID='@USER_COMPANY_ID@' and (AUDFLAG = 'NOSTATUS'))";
        /**
         * 后台发送时，选择第一个员工
         */
        public static final String ORDER_BY_FIRST_USER = "ORDER BY SY_CREATETIME ";
    }

    class SyCompanyName {

        public static final String ALL_DEPT_NAME = "各部门";
        public static final String ALL_COMPANY_NAME = "各单位";
        public static final String GAA_GROUP_NAME = "广东机场";
        public static final String GAA_COMPANY_NAME = "集团本部";

        public static final String BAIYUN_AIRPORT_NAME = "白云机场";
        public static final String BAIYUN_COMPANY_NAME = "股份本部";
        public static final String STOCK_COMPANY_NAME = "股份公司";
        //集团公司
        public static final String GROUP_COMPANY_LEVEL_CODE = "GROUP_COMPANY";
        //公司
        public static final String COMPANY_LEVEL_CODE = "COMPANY";
    }

    class SyCompanyId {
        /**
         * 广东机场集团
         */
        public static final String GAA_GROUP_ID = "8lP9PMNllXGz5FYj5BT";
        /**
         * 集团本部
         */
        public static final String GAA_COMPANY_ID = "V4iuDozBiH1sqUJortu";
        /**
         * 股份公司
         */
        public static final String BAIYUN_AIRPORT_ID = "4c7cae693f0842b8a72413e8aeb33817";
        /**
         * 股份本部
         */
        public static final String BAIYUN_COMPANY_ID = "rF7tkLhZpqJrpAFy1l1";
        /**
         * 广州白云国际机场股份有限公司
         */
        public static final String BAIYUN_COMPANY_PARENT_ID = "4c7cae693f0842b8a72413e8aeb33817";
        /**
         * 翼通公司
         */
        public static final String PTERYGOTON_ID = "1532280350632439809";
        /**
         * 工程建设指挥部
         */
        public static final String HEADQUARTERS_ID = "1532280350464667649";
        /**
         * 物流公司
         */
        public static final String MATERIAL_FLOW_ID = "1532280350535970818";
        /**
         * 白云信科
         */
        public static final String DOLOMIACEAE_ID = "861b23d5e600428aa804e95f9f739e83";
        /**
         * 置业公司
         */
        public static final String REAL_ESTATE_COMPANY_ID = "1532280350280118274";
        /**
         * 韶关机场
         */
        public static final String SHAOGUAN_AIRPORT_COMPANY_ID = "1532280350372392961";
        /**
         * 惠州机场
         */
        public static final String HUIZHOU_AIRPORT_COMPANY_ID = "1532280350825377794";
        /**
         * 梅州机场
         */
        public static final String MEIZHOU_AIRPORT_COMPANY_ID = "1532280351018315778";
        /**
         * 揭阳潮汕机场
         */
        public static final String JIEYANGCHAOSHAN_AIRPORT_COMPANY_ID = "1532280350921846786";
        /**
         * 湛江机场
         */
        public static final String ZHANJIANG_AIRPORT_COMPANY_ID = "1532280351110590465";

        /**
         * 运控中心
         */
        public static final String OPERATIONS_CONTROL_CENTER_COMPANY_ID = "861b568d14974455a27698685780af31";

        /**
         * 广州新科宇航公司
         */
        public static final String XINKE_COMPANY_ID = "1532280350728908801";
    }

    class SySafeDeptId {
        /**
         * 集团本部 安全质量部
         */
        public static final String GAA_SAFETY_QUALITY_DEPT_ID = "1532280351207059458";
        /**
         * 集团本部 数字科技部
         */
        public static final String GAA_DIGITAL_TECHNOLOGY_DEPT_ID = "1532280352146583554";
        /**
         * 股份本部 安全监察部（保卫部）
         */
        public static final String BAIYUN_AIRPORT_SAFETY_QUALITY_DEPT_ID = "Z93LuL0zJv6pyGBQ6JZ";
    }

    class SySafeCompanyLevelDeptName {
        /**
         * 公司级部门名称-领导班子
         */
        public static final String LEADING_GROUP_DEPARTMENT = "领导班子";
        /**
         * 公司级部门名称-安全质量部
         */
        public static final String SAFETY_AND_QUALITY_DEPARTMENT = "安全质量部";
    }

    class WorkFlowId {
        /**
         * 风险管理任务部门级流程key
         */
        public static final String RISK_MANAGER_DEPT_WORKFLOW_KEY = "3nBqD7AbxZ3cl2qAsgO";

        /**
         * 风险管理任务公司级,股份级 流程key
         */
        public static final String RISK_MANAGER_COMPANY_WORKFLOW_KEY = "EXloSqu0JcR6RV5d5KN";
        /**
         * 隐患整改任务 - 延期流程
         */
        public static final String PITFALL_RECTIFY_DEPT_WORKFLOW_KEY = "RcaY4YSV8LlfbtDIniZ";
        /**
         * 隐患整改任务 公司级及以上
         */
        public static final String PITFALL_RECTIFY_COMPANY_WORKFLOW_KEY = "aJxVO8Kyvn7UfIxYYsU";
        /**
         * 隐患排查任务 部门级
         */
        public static final String PITFALL_TASK_DEPT_WORKFLOW_KEY = "1fP5rKfM3bV7aIcs767";

        /**
         * 事件整改 公司级
         */
        public static final String EVENT_RECTIFY_COMPANY_WORKFLOW_KEY = "HQqVIF1vQgZcH99MmJX";

        /**
         * 事件整改 股份级
         */
        public static final String EVENT_RECTIFY_STOCK_WORKFLOW_KEY = "rkcaZOIxGsR37td17gV";

        /**
         * 工作计划  公司级
         */
        public static final String PLAN_COMPANY_WORKFLOW_KEY = "tWckLtx06PKRvhiWgJv";

        /**
         * 工作计划 部门级
         */
        public static final String PLAN_DEPT_WORKFLOW_KEY = "B9FfwGBGqXvXCdB9LsG";

        /**
         * 变更管理 部门级
         */
        public static final String DEPT_CHANGE_MANAGER_WORKFLOW_KEY = "ou8kweZ1id9BnNMaCIz";

        /**
         * 事件调查 指派二级单位
         */
        public static final String EVENT_TASK_SECOND_WORKFLOW_KEY = "Dj8HDc7Wmhg1L314W7E";

        /**
         * 内审子任务 指派二级单位
         */
        public static final String AUDIT_TASK_SECOND_WORKFLOW_KEY = "fWhb5jgPYtIoU5g6Ar5";
        /**
         * 事件调查任务
         */
        public static final String EVENT_TASK_WORKFLOW_KEY = "PXMrilgT0KFHNVpft20";
        /**
         * 强制报告-危险品事件-转办
         */
        public static final String DANGER_TURN_WORKFLOW_KEY = "SFQd0jINnHHrDmn9oWt";
        /**
         * 强制报告-危险品事件-(股份安监部处理的)转办
         */
        public static final String DANGER_STOCK_TURN_WORKFLOW_KEY = "cuUgstbA6O8XULTTKki";
        /**
         * 强制报告-安全保卫事件-转办
         */
        public static final String SECURITY_TURN_WORKFLOW_KEY = "7a4CGJVw9F3zPwbEKky";
        /**
         * 强制报告-安全保卫事件-(股份安监部处理的)转办
         */
        public static final String SECURITY_STOCK_TURN_WORKFLOW_KEY = "xBhdlHHYDJthRTMNFHn";
        /**
         * 强制报告-集团样例转办
         */
        public static final String JT_TURN_WORKFLOW_KEY = "QGxuNpBp0WcD58iEzia";
        /**
         * 强制报告-集团样例(股份安监部处理的)转办
         */
        public static final String JT_STOCK_TURN_WORKFLOW_KEY = "p7tTz6G239d79gaW77E";
        /**
         * 强制报告-民航用例集团转办
         */
        public static final String MH_TURN_WORKFLOW_KEY = "e3Bdh59qNkj8l7ck0fa";
        /**
         * 强制报告-民航用例(股份安监部处理的)转办
         */
        public static final String MH_STOCK_TURN_WORKFLOW_KEY = "14wbB6TnCxsBSHXIIpK";

        /**
         * 政策与目标-安全政策批量审批流程
         */
        public static final String SAFE_POLICY_WORKFLOW_KEY = "k7c0PNEordV4vAkRiYO";
        /**
         * 运行异常信息流程
         */
        public static final String SAFE_EVENT_INFO_COLLECT_WORKFLOW_KEY = "F4mA2IPpJSDcump9l3G";


    }

    /**
     * 策略ID
     */
    class StrategyId {

        public static final String STRATEGY_ID_CLEAR = "clear";
        /**
         * 紧急事件初报：全部
         */
        public static final String SAFE_EVENT_URGENT_REPORT_COMPANY_QUERY = "4fb9ecbe131a4f0aa48c3add8a073363";
        /**
         * 运行异常信息：全部
         */
        public static final String INFO_COLLECT_COMPANY_QUERY = "a787808753ed49cf93fb39421ff0f0e4";

        /**
         * 信息收集流程: 我发起的
         */
        public static final String INFO_COLLECT_STARTEDUSER = "81409d2355a94973b2fbce7492b1e826";
        /**
         * 信息发起流程: 我经办的
         */
        public static final String INFO_COLLECT_APPROVEDUSERS = "98bccdf4f2cc45ac83ea685ef898b843";
        /**
         * 安全保卫事件流程: 我发起的
         */
        public static final String SECURITY_STARTEDUSER = "1f373c316b4d4a298c1346ab8b7e1e47";
        /**
         * 安全保卫事件流程: 我经办的
         */
        public static final String SECURITY_APPROVEDUSERS = "87e8f3ca0f424a34b93fd0106a7a42da";
        /**
         * 危险品(紧急事件)流程: 我发起的
         */
        public static final String DANGER_STARTEDUSER = "bca8cc79d1c74317b03e14d556e0b564";
        /**
         * 危险品(紧急事件)流程: 我经办的
         */
        public static final String DANGER_APPROVEDUSERS = "966800c4e17f4daf951b62e6a6db5f3d";
        /**
         * 危险品(非紧急事件)流程: 我发起的
         */
        public static final String DANGER_NOT_STARTEDUSER = "2e7d60f22e4b4186acc3922ae2bd5e50";
        /**
         * 危险品(非紧急事件)流程: 我经办的
         */
        public static final String DANGER_NOT_APPROVEDUSERS = "c09359ff84b04c16af30eea8b9d91a81";
    }

    /**
     * PDF导出模板定义信息
     */
    class PdfTemplate {
        //强制报告导出模板名称
        public static final String SAFEEVENTREPORT = "SafeEventReportJustificationLeft.ftl";
        //强制报告导出文件前缀名称
        public static final String SAFEEVENTREPORTPREFIX = "事件报告管理系统";
        //强制报告集团-导出文件前缀名称
        public static final String SAFEEVENTREPORTPREFIX_GROUP = "集团公司安全信息报告表";
        public static final String SAFEEVENTREPORTPREFIX_GROUP_NEW = "安全信息报告表";
        //事件调查报告-导出文件前缀名称
        public static final String EVENTCHECKREPORT = "事件调查报告表";
        //强制报告(集团)导出模板名称
        public static final String SAFEEVENTREPORT_GROUP = "SafeForceInfoReportJustificationLeft.ftl";
        //强制报告(集团)导出模板名称-新版模板
        public static final String SAFEEVENTREPORT_GROUP_FILE = "SafeForceInfo.ftl";
        //强制报告(集团)导出模板名称-无损模板
        public static final String SAFEEVENTREPORT_GROUP_FILE_LOSS = "SafeForceInfoLoss.ftl";
        public static final String SAFE_EVENT_CHECK_REPORT = "SafeEventCheckReport.ftl";

        /**
         * 风险控制记录表模板名称
         */
        public static final String RISK_CONTROL_RECORD = "RiskControlRecord.ftl";
        /**
         * 集团投资-集团公司yyyy年安全保障类项目投资计划
         */
        public static final String SAFE_INVESTMENT = "SafeInvestment.ftl";
        public static final String SAFE_INVESTMENT_PDF_PREFIX = "集团公司安全保障类项目投资计划";
    }

    /**
     * 部分固定短信标题
     */
    class MessageContent {
        /**
         * 信息报告短信标题
         */
        public static final String INFORMATION_REPORT = "紧急事件报告";
        /**
         * 强制报告-集团通知标题
         */
        public static final String SAFE_EVENT_REPORT_GROUP_TITLE = "【集团公司安全信息报告】";
    }

    /**
     * 功能编码
     */
    class FuncCodeContent {
        /**
         * 信息收集-股份
         */
        public static final String SAFE_EVENT_INFO_COLLECT_STOCK = "SAFE_EVENT_INFO_COLLECT_STOCK";
    }

    class UserInfo {
        /**
         * 定时任务账号
         */
        public static final String DSRW_USERCODE = "dsrw2023";

        /**
         * 集团安质部-张翔账号
         */
        public static final String ZHANGXIANG_CODE = "010157";

    }

    class OtherInfo {
        /**
         * 截取通知标题长度
         */
        public static final Integer NOTICE_TITLE_LENGTH = 50;
        /**
         * 年月日时间长度: yyyy-MM-dd
         */
        public static final Integer DATE_LENGTH = 10;
        /**
         * yyyy-MM-dd HH:mm
         */
        public static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm";
        public static final String DATE_FORMAT = "yyyy-M-d";
        public static final String DATE_DD_FORMAT = "yyyy-M-dd";
        public static final String NOTICE_ACTION_TYPE = "insert";
        public static final String NOTICE_ACTION_TYPE_UPDATE = "doUpdate";

        /**
         * 列表保存 key
         */
        public static final String ACTION = "__action__";
        /**
         * 列表保存表示 新增value
         */
        public static final String ACTION_INSERT = "doInsert";
    }

    class CommonHomeSetting {
        /**
         * 不安全事件调查模块ID
         */
        public static final String PARENT_MODULE_ID = "K6weGZdDayn5Nu0FxBY";

    }

    class ExcelImportConstant {
        /**
         * 日期
         */
        public static final String DONT_NAME_FORMAT_PATTERN = "\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2}";
        /**
         * 正整数、小数
         */
        public static final String CLASS_HOUR = "\\d+(\\.\\d+)?";
        /**
         * 空格正则，匹配一个或多个空额
         */
        public static final String BLANK_REGEX = " +";
        public static final String EXAMPLE_TEXT = "示例行";
        public static final Integer ZERO_COLUMN = 0;
        public static final Integer ZERO_ROW = 0;
        public static final Integer FIRST_ROW = 1;
        public static final Integer SECOND_ROW = 2;
        public static final Integer THIRD_ROW = 3;
        public static final Integer FOURTH_ROW = 4;
        public static final Integer FIFTH_ROW = 5;
    }

    /**
     * 强制报告-转办类型
     */
    class TurnFlowTypeConstant {
        /**
         * 民航样例
         */
        public static final String MH_EVENT = "MH";
        /**
         * 危险品事件
         */
        public static final String DANGER = "DANGER";
        /**
         * 安全保卫事件
         */
        public static final String SECURITY = "SECURITY";
        /**
         * 集团样例
         */
        public static final String JT_EVENT = "JT";
    }

    class TableCommonField {
        public static final String SY_COMPANY_ID = "SY_COMPANY_ID";
        public static final String SY_COMPANY_NAME = "SY_COMPANY_NAME";
        public static final String SY_GROUP_COMPANY_ID = "SY_GROUP_COMPANY_ID";
        public static final String SY_GROUP_COMPANY_NAME = "SY_GROUP_COMPANY_NAME";
        public static final String SY_CREATEORGID = "SY_CREATEORGID";
        public static final String SY_CREATEORGNAME = "SY_CREATEORGNAME";
        public static final String SY_CREATEUSERID = "SY_CREATEUSERID";
        public static final String SY_CREATEUSERNAME = "SY_CREATEUSERNAME";
        public static final String SY_CREATETIME = "SY_CREATETIME";
        public static final String SY_AUDFLAG = "SY_AUDFLAG";
    }

    class GeneralStr {
        public static final String VARCHAR = "VARCHAR";
    }

    class PermissionSettingConstant {
        /**
         * 菜单类型
         */
        public static final String MENU_INFO_TYPE = "MENU";
        /**
         * 功能类型
         */
        public static final String MT_INFO_TYPE = "MT";
        /**
         * 子功能类型
         */
        public static final String SUB_FUNC_INFO_TYPE = "commonSubFunc";
        /**
         * 查询功能下所有功能按钮返回key类型-按钮
         */
        public static final String META_FUNC_TYPE_BUTTON = "button";
        /**
         * 查询功能下所有功能按钮返回key类型-本功能
         */
        public static final String META_FUNC_TYPE_MAIN = "main";
        /**
         * 查询功能下所有功能按钮返回key类型-子功能
         */
        public static final String META_FUNC_TYPE_CHILD = "child";

        /**
         * 查询功能下所有功能按钮返回value字段-code字段
         */
        public static final String META_FUNC_FIELD_RESOURCEBUTTON_CODE = "RESOURCEBUTTON_CODE";
        /**
         * 查询功能下所有功能按钮返回value字段-按钮名称字段
         */
        public static final String META_FUNC_FIELD_RESOURCEBUTTON_NAME = "RESOURCEBUTTON_NAME";
        /**
         * 查询功能下所有功能按钮返回value字段-id字段
         */
        public static final String META_FUNC_FIELD_JE_CORE_RESOURCEBUTTON_ID = "JE_CORE_RESOURCEBUTTON_ID";
        /**
         * 查询功能下所有功能按钮返回value字段-FUNCINFO_FUNCCODE字段
         */
        public static final String META_FUNC_FIELD_FUNCINFO_FUNCCODE = "FUNCINFO_FUNCCODE";
        /**
         * 查询功能下所有功能按钮返回value字段-FUNCRELATION_CODE字段
         */
        public static final String META_FUNC_FIELD_FUNCRELATION_CODE = "FUNCRELATION_CODE";
        /**
         * 查询功能下所有功能按钮返回value字段-JE_CORE_FUNCINFO_ID字段
         */
        public static final String META_FUNC_FIELD_JE_CORE_FUNCINFO_ID = "JE_CORE_FUNCINFO_ID";

        /**
         * 查询功能下所有功能按钮返回value字段-JE_CORE_FUNCRELATION_ID字段
         */
        public static final String META_FUNC_FIELD_JE_CORE_FUNCRELATION_ID = "JE_CORE_FUNCRELATION_ID";
        /**
         * 查询功能下所有功能按钮返回value字段-FUNCRELATION_FUNCID字段
         */
        public static final String META_FUNC_FIELD_FUNCRELATION_FUNCID = "FUNCRELATION_FUNCID";
        /**
         * 查询功能下所有功能按钮返回value字段-RESOURCEBUTTON_FUNCINFO_ID字段
         */
        public static final String META_FUNC_FIELD_RESOURCEBUTTON_FUNCINFO_ID = "RESOURCEBUTTON_FUNCINFO_ID";
    }
}
