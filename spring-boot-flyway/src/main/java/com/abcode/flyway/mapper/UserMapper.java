package com.abcode.flyway.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.abcode.flyway.entity.User;

/**
 * 用户表(User)表数据库访问层
 *
 * @author qinzhitao
 * @since 2023-06-01 11:02:07
 */
public interface UserMapper extends BaseMapper<User> {

}

