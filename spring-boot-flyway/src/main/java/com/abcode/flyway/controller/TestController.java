package com.abcode.flyway.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 测试
 *
 * @author qinzhitao
 * @date 2023/6/1 10:24
 */
@RequestMapping("test")
@RestController
public class TestController {

    @RequestMapping("flyTest")
    public String flyTest(){
        return "fly test success";
    }
}
