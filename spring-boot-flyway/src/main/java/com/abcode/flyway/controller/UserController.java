package com.abcode.flyway.controller;



import com.abcode.flyway.common.Result;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.io.Serializable;
import java.util.List;

import com.abcode.flyway.entity.User;
import com.abcode.flyway.service.UserService;


/**
 * 用户表(User)表控制层
 *
 * @author qinzhitao
 * @since 2023-06-01 11:02:07
 */
@RestController
@RequestMapping("user")
public class UserController {
    /**
     * 服务对象
     */
    @Resource
    private UserService userService;

    /**
     * 分页查询所有数据
     * @param page 分页对象
     * @param user 查询实体
     * @return 所有数据
     */
    @GetMapping("/selectAll")
    public Result selectAll(Page<User> page, User user) {
        Page<User> rst = this.userService.page(page, new QueryWrapper<>(user));
        return Result.ok(rst);
    }

    /**
     * 通过主键查询单条数据
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("/selectOne/{id}")
    public Result selectOne(@PathVariable Serializable id) {
        User user = this.userService.getById(id);
        return Result.ok(user);
    }

    /**
     * 新增数据
     * @param user 实体对象
     * @return 新增结果
     */
    @PostMapping("/insert")
    public Result insert(@RequestBody User user) {
        Boolean rst = this.userService.save(user);
        return Result.OK(rst);
    }

    /**
     * 修改数据
     * @param user 实体对象
     * @return 修改结果
     */
    @PutMapping("/update")
    public Result update(@RequestBody User user) {
        Boolean rst = this.userService.updateById(user);
        return Result.OK(rst);
    }

    /**
     * 删除数据
     *
     * @param idList 主键结合
     * @return 删除结果
     */
    @DeleteMapping("/delete")
    public Result delete(@RequestParam("idList") List<Long> idList) {
        boolean rst = this.userService.removeByIds(idList);
        return Result.ok(rst);
    }
}

