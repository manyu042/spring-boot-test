package com.abcode.flyway.service.impl;

import com.abcode.flyway.mapper.UserMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.abcode.flyway.entity.User;
import com.abcode.flyway.service.UserService;
import org.springframework.stereotype.Service;

/**
 * 用户表(User)表服务实现类
 *
 * @author qinzhitao
 * @since 2023-06-01 11:02:07
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

}

