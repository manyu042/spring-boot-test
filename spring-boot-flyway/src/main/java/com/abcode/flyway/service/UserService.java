package com.abcode.flyway.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.abcode.flyway.entity.User;

/**
 * 用户表(User)表服务接口
 *
 * @author qinzhitao
 * @since 2023-06-01 11:02:07
 */
public interface UserService extends IService<User> {

}

