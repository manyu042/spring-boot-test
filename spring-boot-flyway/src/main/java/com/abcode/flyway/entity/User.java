package com.abcode.flyway.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
/**
 * 用户表(User)表实体类
 *
 * @author qinzhitao
 * @since 2023-06-01 11:02:07
 */
@Data
@SuppressWarnings("serial")
@TableName("TEST_USER")
public class User {
            
    /** ID */                                  
    @TableId(value= "ID")
    private Long id;
                        
    /** 账号 */                                
    @TableField("ACCOUNT")
    private String account;
                        
    /** 密码 */                                
    @TableField("PASSWORD")
    private String password;
            
}
