package test.abcode.image.service;

import com.google.cloud.translate.Translate;
import com.google.cloud.translate.TranslateOptions;
import com.google.cloud.translate.Translation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class TranslatorService {

    private final Translate translate;

    @Value("${translation.api-key}")
    private String apiKey;

    public TranslatorService() {
        TranslateOptions options = TranslateOptions.newBuilder().setApiKey(apiKey).build();
        this.translate = options.getService();
    }

    public String translateText(String text, String targetLanguage) {
        Translation translation = translate.translate(
                text,
                Translate.TranslateOption.targetLanguage(targetLanguage)
        );
        return translation.getTranslatedText();
    }
}
