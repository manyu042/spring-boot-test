package test.abcode.image.service;

import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

@Service
public class ImageEditor {

    public BufferedImage replaceTextInImage(BufferedImage image, String translatedText, int x, int y) {
        Graphics2D g2d = image.createGraphics();
        g2d.setFont(new Font("Arial", Font.PLAIN, 20));
        g2d.setColor(Color.BLACK);
        g2d.drawString(translatedText, x, y);  // 设置文本位置
        g2d.dispose();
        return image;
    }

    public void saveImage(BufferedImage image, String outputPath) throws IOException {
        ImageIO.write(image, "png", new File(outputPath));
    }
}
