package test.abcode.image.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.awt.*;

@Data
@AllArgsConstructor
public class TextRegion {
    private String originalText;
    private Rectangle boundingBox;
    private float confidence;
    private String translatedText;
}