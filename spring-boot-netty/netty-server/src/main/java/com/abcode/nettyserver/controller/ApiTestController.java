package com.abcode.nettyserver.controller;

import com.abcode.nettyserver.server.NettyChannelMap;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
@Slf4j
@RestController
@RequestMapping("apiTest")
public class ApiTestController {

    @RequestMapping("test")
    public String test(){
        return "test success";
    }
    @PostMapping("sendToAll")
    public String sendToAll(String msg){
        NettyChannelMap.sendToAll(msg);
        return "sendToAll msg";
    }
}
