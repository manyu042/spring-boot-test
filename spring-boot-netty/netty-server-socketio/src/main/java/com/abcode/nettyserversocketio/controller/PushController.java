package com.abcode.nettyserversocketio.controller;

import cn.hutool.core.util.ObjUtil;
import cn.hutool.core.util.StrUtil;
import com.abcode.nettyserversocketio.server.ClientCache;
import com.abcode.nettyserversocketio.server.MessageInfo;
import com.corundumstudio.socketio.SocketIOClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.UUID;

@RestController
@RequestMapping("/push")
public class PushController {
    @Resource
    private ClientCache clientCache;

    @GetMapping("/user/{userId}")
    public String pushTuUser(@PathVariable("userId") String userId){
        SocketIOClient socketIOClient = clientCache.getUserClient(Long.valueOf(userId));
        if(ObjUtil.isEmpty(socketIOClient)){
            return "NO Client";
        }
        //向客户端推送消息
        socketIOClient.sendEvent("chatevent",new MessageInfo("管理员","向客户段发送的消息"));

        return "success";
    }
}
