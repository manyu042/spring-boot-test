package com.abcode.nettyserversocketio.gen.vo;

import java.util.Date;
import lombok.Data;
/**
 * (UserRelation)前端显示类
 *
 * @author abcode
 * @since 2023-06-17 23:01:19
 */
@Data
@SuppressWarnings("serial")
public class UserRelationVO {
            
    /** 主键 */        
    private Long id;
            
    /** 用户ID */        
    private Long userId;
            
    /** 好友ID */        
    private Long relationId;
            
    /** 昵称 */        
    private String nickName;
            
    /** 创建时间 */        
    private Date createTime;
            
    /** 创建者 */        
    private Long createBy;
            
    /** 更新时间 */        
    private Date updateTime;
            
    /** 更新者 */        
    private Long updateBy;
            
    /** 备注 */        
    private String remark;
            
    /** 是否删除：0-否；1-是； */        
    private Integer delFlag;

}
