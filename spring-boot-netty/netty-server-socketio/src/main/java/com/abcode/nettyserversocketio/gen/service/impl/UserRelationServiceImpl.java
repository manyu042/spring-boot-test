package com.abcode.nettyserversocketio.gen.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.abcode.nettyserversocketio.gen.mapper.UserRelationMapper;
import com.abcode.nettyserversocketio.gen.entity.UserRelation;
import com.abcode.nettyserversocketio.gen.service.UserRelationService;
import org.springframework.stereotype.Service;

/**
 * (UserRelation)表服务实现类
 *
 * @author abcode
 * @since 2023-06-17 22:51:08
 */
@Service
public class UserRelationServiceImpl extends ServiceImpl<UserRelationMapper, UserRelation> implements UserRelationService {

}

