package com.abcode.nettyserversocketio.gen.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.abcode.nettyserversocketio.gen.entity.Article;

/**
 * 文章表(Article)表服务接口
 *
 * @author abcode
 * @since 2023-06-17 22:51:07
 */
public interface ArticleService extends IService<Article> {

}

