package com.abcode.nettyserversocketio.gen.controller;



import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.io.Serializable;
import java.util.List;

import com.abcode.nettyserversocketio.gen.entity.ArticleResource;
import com.abcode.nettyserversocketio.gen.service.ArticleResourceService;
import com.abcode.nettyserversocketio.common.Result;

/**
 * 资源表(ArticleResource)表控制层
 *
 * @author abcode
 * @since 2023-06-17 22:51:07
 */
@RestController
@RequestMapping("articleResource")
public class ArticleResourceController {
    /**
     * 服务对象
     */
    @Resource
    private ArticleResourceService articleResourceService;

    /**
     * 分页查询所有数据
     * @param page 分页对象
     * @param articleResource 查询实体
     * @return 所有数据
     */
    @GetMapping("/selectAll")
    public Result selectAll(Page<ArticleResource> page, ArticleResource articleResource) {
        Page<ArticleResource> rst = this.articleResourceService.page(page, new QueryWrapper<>(articleResource));
        return Result.ok(rst);
    }

    /**
     * 通过主键查询单条数据
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("/selectOne/{id}")
    public Result selectOne(@PathVariable Serializable id) {
        ArticleResource articleResource = this.articleResourceService.getById(id);
        return Result.ok(articleResource);
    }

    /**
     * 新增数据
     * @param articleResource 实体对象
     * @return 新增结果
     */
    @PostMapping("/insert")
    public Result insert(@RequestBody ArticleResource articleResource) {
        Boolean rst = this.articleResourceService.save(articleResource);
        return Result.OK(rst);
    }

    /**
     * 修改数据
     * @param articleResource 实体对象
     * @return 修改结果
     */
    @PutMapping("/update")
    public Result update(@RequestBody ArticleResource articleResource) {
        Boolean rst = this.articleResourceService.updateById(articleResource);
        return Result.OK(rst);
    }

    /**
     * 删除数据
     *
     * @param idList 主键结合
     * @return 删除结果
     */
    @DeleteMapping("/delete")
    public Result delete(@RequestParam("idList") List<Long> idList) {
        boolean rst = this.articleResourceService.removeByIds(idList);
        return Result.ok(rst);
    }
}

