package com.abcode.nettyserversocketio.controller.api;

import cn.hutool.core.util.ObjUtil;
import com.abcode.nettyserversocketio.biz.IUserBizService;
import com.abcode.nettyserversocketio.common.Result;
import com.abcode.nettyserversocketio.gen.vo.UserVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

@Component
public class BaseApiController {

    @Autowired
    IUserBizService userBizService;

    protected UserVO getLoginUser(){
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        String token = attributes.getRequest().getHeader("Authorization");
        UserVO userVO = userBizService.getUserVOByToken(token);

        return userVO;
    }
}
