package com.abcode.nettyserversocketio.gen.param;

import java.util.Date;
import lombok.Data;
/**
 * 用户表(User)参数类
 *
 * @author abcode
 * @since 2023-06-17 23:01:19
 */
@Data
@SuppressWarnings("serial")
public class UserParam {
            
    /** 主键 */        
    private Long id;
            
    /** 用户名 */        
    private String username;
            
    /** 昵称 */        
    private String nickName;
            
    /** 头像 */        
    private String profile;
            
    /** 性别 */        
    private String gender;
            
    /** 个性签名 */        
    private String sign;
            
    /** 区域 */        
    private String area;
            
    /** 账号唯一标识 */        
    private String uuid;
            
    /** 密码 */        
    private String password;
            
    /** 创建时间 */        
    private Date createTime;
            
    /** 创建者 */        
    private Long createBy;
            
    /** 更新时间 */        
    private Date updateTime;
            
    /** 更新者 */        
    private Long updateBy;
            
    /** 备注 */        
    private String remark;
            
    /** 是否删除：0-否；1-是； */        
    private Integer delFlag;

}
