package com.abcode.nettyserversocketio.gen.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
/**
 * 用户表(User)表实体类
 *
 * @author abcode
 * @since 2023-06-17 22:51:08
 */
@Data
@SuppressWarnings("serial")
@TableName("gc_user")
public class User {
            
    /** 主键 */                                  
    @TableId(value= "id")
    private Long id;
                        
    /** 用户名 */                                
    @TableField("username")
    private String username;
                        
    /** 昵称 */                                
    @TableField("nick_name")
    private String nickName;
                        
    /** 头像 */                                
    @TableField("profile")
    private String profile;
                        
    /** 性别 */                                
    @TableField("gender")
    private String gender;
                        
    /** 个性签名 */                                
    @TableField("sign")
    private String sign;
                        
    /** 区域 */                                
    @TableField("area")
    private String area;
                        
    /** 账号唯一标识 */                                
    @TableField("uuid")
    private String uuid;
                        
    /** 密码 */                                
    @TableField("password")
    private String password;
                        
    /** 创建时间 */                                
    @TableField("create_time")
    private Date createTime;
                        
    /** 创建者 */                                
    @TableField("create_by")
    private Long createBy;
                        
    /** 更新时间 */                                
    @TableField("update_time")
    private Date updateTime;
                        
    /** 更新者 */                                
    @TableField("update_by")
    private Long updateBy;
                        
    /** 备注 */                                
    @TableField("remark")
    private String remark;
                        
    /** 是否删除：0-否；1-是； */                                
    @TableField("del_flag")
    private Integer delFlag;
            
}
