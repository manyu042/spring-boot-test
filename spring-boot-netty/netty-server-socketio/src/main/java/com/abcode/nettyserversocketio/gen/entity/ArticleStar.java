package com.abcode.nettyserversocketio.gen.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
/**
 * 文章点赞记录表(ArticleStar)表实体类
 *
 * @author abcode
 * @since 2023-06-17 22:51:07
 */
@Data
@SuppressWarnings("serial")
@TableName("gc_article_star")
public class ArticleStar {
            
    /** 主键 */                                  
    @TableId(value= "id")
    private Long id;
                        
    /** 文章ID */                                
    @TableField("article_id")
    private Long articleId;
                        
    /** 资源类型：0-图片； 1-视频；2-音频； */                                
    @TableField("user_id")
    private Long userId;
                        
    /** 昵称 */                                
    @TableField("nick_name")
    private String nickName;
                        
    /** 创建时间 */                                
    @TableField("create_time")
    private Date createTime;
                        
    /** 创建者 */                                
    @TableField("create_by")
    private Long createBy;
                        
    /** 更新时间 */                                
    @TableField("update_time")
    private Date updateTime;
                        
    /** 更新者 */                                
    @TableField("update_by")
    private Long updateBy;
                        
    /** 备注 */                                
    @TableField("remark")
    private String remark;
                        
    /** 是否删除：0-否；1-是； */                                
    @TableField("del_flag")
    private Integer delFlag;
            
}
