package com.abcode.nettyserversocketio.gen.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.abcode.nettyserversocketio.gen.mapper.ArticleStarMapper;
import com.abcode.nettyserversocketio.gen.entity.ArticleStar;
import com.abcode.nettyserversocketio.gen.service.ArticleStarService;
import org.springframework.stereotype.Service;

/**
 * 文章点赞记录表(ArticleStar)表服务实现类
 *
 * @author abcode
 * @since 2023-06-17 22:51:07
 */
@Service
public class ArticleStarServiceImpl extends ServiceImpl<ArticleStarMapper, ArticleStar> implements ArticleStarService {

}

