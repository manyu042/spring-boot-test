package com.abcode.nettyserversocketio.gen.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.abcode.nettyserversocketio.gen.mapper.UserMapper;
import com.abcode.nettyserversocketio.gen.entity.User;
import com.abcode.nettyserversocketio.gen.service.UserService;
import org.springframework.stereotype.Service;

/**
 * 用户表(User)表服务实现类
 *
 * @author abcode
 * @since 2023-06-17 22:51:08
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

}

