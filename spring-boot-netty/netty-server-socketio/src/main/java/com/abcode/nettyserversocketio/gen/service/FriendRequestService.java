package com.abcode.nettyserversocketio.gen.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.abcode.nettyserversocketio.gen.entity.FriendRequest;

/**
 * 申请好友(FriendRequest)表服务接口
 *
 * @author abcode
 * @since 2023-06-19 22:45:39
 */
public interface FriendRequestService extends IService<FriendRequest> {

}

