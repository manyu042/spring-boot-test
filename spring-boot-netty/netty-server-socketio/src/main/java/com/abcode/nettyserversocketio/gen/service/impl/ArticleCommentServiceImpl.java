package com.abcode.nettyserversocketio.gen.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.abcode.nettyserversocketio.gen.mapper.ArticleCommentMapper;
import com.abcode.nettyserversocketio.gen.entity.ArticleComment;
import com.abcode.nettyserversocketio.gen.service.ArticleCommentService;
import org.springframework.stereotype.Service;

/**
 * 文章评论记录表(ArticleComment)表服务实现类
 *
 * @author abcode
 * @since 2023-06-17 22:51:07
 */
@Service
public class ArticleCommentServiceImpl extends ServiceImpl<ArticleCommentMapper, ArticleComment> implements ArticleCommentService {

}

