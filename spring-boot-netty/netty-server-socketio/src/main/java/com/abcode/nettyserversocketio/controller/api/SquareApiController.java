package com.abcode.nettyserversocketio.controller.api;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.abcode.nettyserversocketio.biz.ISquareBizService;
import com.abcode.nettyserversocketio.common.Result;
import com.abcode.nettyserversocketio.gen.entity.Article;
import com.abcode.nettyserversocketio.gen.vo.ArticleVO;
import com.abcode.nettyserversocketio.gen.vo.UserVO;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.springframework.http.HttpRequest;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.support.StandardMultipartHttpServletRequest;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RequestMapping("/api/square")
@RestController
public class SquareApiController  extends BaseApiController{

    @Resource
    ISquareBizService squareBizService;

    @GetMapping("/getUserPostList/{id}")
    public Result getUserPostList(@PathVariable(name = "id") String id){
        System.out.println("id:----"+ id);
        List<Article> articleList = squareBizService.getUserArticleList(Long.parseLong(id));
        List<ArticleVO> resultList = articleList.stream().map(item->{
            ArticleVO articleVO = new ArticleVO();
            BeanUtil.copyProperties(item, articleVO);

            return articleVO;

        }).collect(Collectors.toList());
        return Result.ok(resultList);

    }

    /**
     * http://localhost:8080/api/square/getPostList?order=time&offset=0&limit=10
     * @param
     * @return
     */
    @GetMapping("/getPostList")
    public Result getPostList(String order, Long offset, Long limit){
        System.out.println("id:----"+ order);
        List<Article> page = squareBizService.pageArticle(order, offset, limit);
        List<ArticleVO> resultList = page.stream().map(item->{
            ArticleVO articleVO = new ArticleVO();
            BeanUtil.copyProperties(item, articleVO);

            return articleVO;

        }).collect(Collectors.toList());
        return Result.ok(resultList);


    }

    @PostMapping(value = "/publishPost")
    public Result publishPost(HttpServletRequest httpRequest, Map<String,Object> paramMap, MultipartFile multipartFile){
        if(!(httpRequest instanceof StandardMultipartHttpServletRequest)){
            return Result.error("请求异常");
        }
        StandardMultipartHttpServletRequest multipartHttp = (StandardMultipartHttpServletRequest) httpRequest;
        String title = multipartHttp.getParameter("title");
        if(StrUtil.isBlank(title)){
            return Result.error("标题不能为空");
        }
        String content = multipartHttp.getParameter("content");
        if(StrUtil.isBlank(content)){
            return Result.error("内容不能为空");
        }
        UserVO userVO = getLoginUser();
        Result rst = squareBizService.publishArticle(userVO, title, content);
        return rst;
    }

    /**
     * 判断是否有点赞
     * @param articleId
     * @return
     */
    @GetMapping("postStarStatus")
    public Result checkStarStatus(Long articleId){
        UserVO userVO = getLoginUser();
        boolean rst = squareBizService.checkStarStatus(userVO, articleId);
        JSONObject jsonObject = JSONUtil.createObj();
        jsonObject.putOnce("starFlag", rst);

        return Result.ok(jsonObject);
    }

    @PostMapping("starPost")
    public Result starPost(@RequestBody Map<String, String> paramMap){
        Long articleId = Long.valueOf(paramMap.get("postId"));
        UserVO userVO = getLoginUser();
        return squareBizService.starArticle(userVO, articleId);
    }

}
