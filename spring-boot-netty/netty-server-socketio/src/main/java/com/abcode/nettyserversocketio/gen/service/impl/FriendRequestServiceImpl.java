package com.abcode.nettyserversocketio.gen.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.abcode.nettyserversocketio.gen.mapper.FriendRequestMapper;
import com.abcode.nettyserversocketio.gen.entity.FriendRequest;
import com.abcode.nettyserversocketio.gen.service.FriendRequestService;
import org.springframework.stereotype.Service;

/**
 * 申请好友(FriendRequest)表服务实现类
 *
 * @author abcode
 * @since 2023-06-19 22:45:39
 */
@Service
public class FriendRequestServiceImpl extends ServiceImpl<FriendRequestMapper, FriendRequest> implements FriendRequestService {

}

