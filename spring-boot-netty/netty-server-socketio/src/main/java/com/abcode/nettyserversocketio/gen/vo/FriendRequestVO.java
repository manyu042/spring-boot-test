package com.abcode.nettyserversocketio.gen.vo;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
/**
 * 申请好友(FriendRequest)前端显示类
 *
 * @author abcode
 * @since 2023-06-19 23:15:06
 */
@Data
@SuppressWarnings("serial")
public class FriendRequestVO {
            
    /** 主键 */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
            
    /** 申请人 */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long fromUserId;
            
    /** 被申请人ID */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long toUserId;
            
    /** 昵称 */        
    private String fromNickName;
            
    /** 申请状态：0-待通过；1-已通过；2-已忽略；3-已阅； */        
    private Integer requestStatus;
            
    /** 处理时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date processTime;
            
    /** 请求信息 */        
    private String requestMsg;
            
    /** 查阅状态：0-未读；1-已阅； */        
    private Integer readStatus;
            
    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;
            
    /** 创建者 */        
    private Long createBy;
            
    /** 更新时间 */        
    private Date updateTime;
            
    /** 更新者 */        
    private Long updateBy;
            
    /** 备注 */        
    private String remark;
            
    /** 是否删除：0-否；1-是； */        
    private Integer delFlag;

    private String type = "addFriend";

}
