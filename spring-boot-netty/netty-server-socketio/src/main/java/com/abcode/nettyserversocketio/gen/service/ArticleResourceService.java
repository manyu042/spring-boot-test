package com.abcode.nettyserversocketio.gen.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.abcode.nettyserversocketio.gen.entity.ArticleResource;

/**
 * 资源表(ArticleResource)表服务接口
 *
 * @author abcode
 * @since 2023-06-17 22:51:07
 */
public interface ArticleResourceService extends IService<ArticleResource> {

}

