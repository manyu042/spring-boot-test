package com.abcode.nettyserversocketio.gen.param;

import java.util.Date;
import lombok.Data;
/**
 * 文章表(Article)参数类
 *
 * @author abcode
 * @since 2023-06-17 23:01:18
 */
@Data
@SuppressWarnings("serial")
public class ArticleParam {
            
    /** 主键 */        
    private Long id;
            
    /** 用户ID */        
    private Long userId;
            
    /** 标题 */        
    private String title;
            
    /** 内容 */        
    private String content;
            
    /** 点赞数 */        
    private Integer star;
            
    /** 创建时间 */        
    private Date createTime;
            
    /** 创建者 */        
    private Long createBy;
            
    /** 更新时间 */        
    private Date updateTime;
            
    /** 更新者 */        
    private Long updateBy;
            
    /** 备注 */        
    private String remark;
            
    /** 是否删除：0-否；1-是； */        
    private Integer delFlag;

}
