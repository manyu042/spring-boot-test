package com.abcode.nettyserversocketio.gen.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.abcode.nettyserversocketio.gen.entity.FriendRequest;

/**
 * 申请好友(FriendRequest)表数据库访问层
 *
 * @author abcode
 * @since 2023-06-19 22:45:39
 */
public interface FriendRequestMapper extends BaseMapper<FriendRequest> {

}

