package com.abcode.nettyserversocketio.biz.impl;

import cn.hutool.core.date.DateUtil;
import com.abcode.nettyserversocketio.biz.ISquareBizService;
import com.abcode.nettyserversocketio.common.Result;
import com.abcode.nettyserversocketio.gen.entity.Article;
import com.abcode.nettyserversocketio.gen.entity.ArticleStar;
import com.abcode.nettyserversocketio.gen.mapper.ArticleMapper;
import com.abcode.nettyserversocketio.gen.service.ArticleService;
import com.abcode.nettyserversocketio.gen.service.ArticleStarService;
import com.abcode.nettyserversocketio.gen.vo.UserVO;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Slf4j
@Service
public class SquareBizServiceImpl implements ISquareBizService {

    @Resource
    ArticleService articleService;
    @Resource
    ArticleStarService articleStarService;
    @Resource
    ArticleMapper articleMapper;


    @Override
    public List<Article> getUserArticleList(long userId) {

        LambdaQueryWrapper<Article> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Article::getUserId, userId);
        queryWrapper.orderByDesc(Article::getCreateTime);

        return articleService.list(queryWrapper);
    }

    @Override
    public List<Article> pageArticle(String order, Long offset, Long limit) {

        LambdaQueryWrapper<Article> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.orderByDesc(Article::getUpdateTime);
        queryWrapper.last("limit "+ offset +","+ limit);

        return articleService.list(queryWrapper);
    }

    @Override
    public Result publishArticle(UserVO userVO, String title, String content) {
        Article article = new Article();
        article.setUserId(userVO.getId());
        article.setTitle(title);
        article.setContent(content);
        article.setCreateTime(DateUtil.date());
        article.setCreateBy(userVO.getId());
        article.setUpdateTime(DateUtil.date());
        boolean rst = articleService.save(article);
        return Result.ok();
    }

    @Override
    public boolean checkStarStatus(UserVO userVO, Long articleId) {
        LambdaQueryWrapper<ArticleStar> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ArticleStar::getUserId, userVO.getId());
        queryWrapper.eq(ArticleStar::getArticleId, articleId);
        long count = articleStarService.count(queryWrapper);
        return count > 0;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public Result starArticle(UserVO userVO, Long articleId) {
        // TODO: 2023/6/24 重复点赞需要处理 
        ArticleStar articleStar = new ArticleStar();
        articleStar.setArticleId(articleId);
        articleStar.setUserId(userVO.getId());
        articleStar.setNickName(userVO.getNickName());
        articleStar.setCreateTime(DateUtil.date());

        articleStarService.save(articleStar);

        LambdaUpdateWrapper<Article> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.eq(Article::getId, articleId);

        updateWrapper.set(Article::getUpdateTime, DateUtil.date());

        updateWrapper.setSql("star=star+1");

        articleService.update(updateWrapper);

        return Result.ok("点赞成功");
    }
}
