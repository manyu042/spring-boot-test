package com.abcode.nettyserversocketio.server.message;

import lombok.Data;

/**
 * 私人消息类
 */
@Data
public class PrivateMsg extends BaseMsg{
    private Long fromUserId;
    private Long toUserId;

}
