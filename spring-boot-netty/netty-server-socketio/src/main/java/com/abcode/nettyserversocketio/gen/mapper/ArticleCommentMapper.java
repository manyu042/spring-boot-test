package com.abcode.nettyserversocketio.gen.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.abcode.nettyserversocketio.gen.entity.ArticleComment;

/**
 * 文章评论记录表(ArticleComment)表数据库访问层
 *
 * @author abcode
 * @since 2023-06-17 22:51:07
 */
public interface ArticleCommentMapper extends BaseMapper<ArticleComment> {

}

