package com.abcode.nettyserversocketio.server.message;

import com.sun.xml.internal.ws.developer.Serialization;
import lombok.Data;

@Data
public class BaseMsg {
    /** 事件名  */
    protected String eventName;
    /** 发送的信息  */
    protected String msgContent;

    protected String msgType;

    protected Long sendTimeStamp;
}
