package com.abcode.nettyserversocketio.common.utils;

import cn.hutool.core.date.DateUtil;
import cn.hutool.crypto.SecureUtil;
import com.abcode.nettyserversocketio.gen.entity.User;
import com.abcode.nettyserversocketio.gen.param.UserParam;

public class BuildUtil {

    /**
     * 构建用户
     * @param userParam
     * @return
     */
    public static User buildUser(UserParam userParam) {
        User user = new User();
        user.setUsername(userParam.getUsername());
        user.setGender("0");
        user.setSign("sdsdfdsfdsf");
        //user.setArea();
        user.setCreateTime(DateUtil.date());
        user.setPassword(SecureUtil.md5(userParam.getPassword()));
        return user;
    }
}
