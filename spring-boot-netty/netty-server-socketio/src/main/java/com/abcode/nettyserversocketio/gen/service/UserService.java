package com.abcode.nettyserversocketio.gen.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.abcode.nettyserversocketio.gen.entity.User;

/**
 * 用户表(User)表服务接口
 *
 * @author abcode
 * @since 2023-06-17 22:51:08
 */
public interface UserService extends IService<User> {

}

