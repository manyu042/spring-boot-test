package com.abcode.nettyserversocketio.gen.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
/**
 * 申请好友(FriendRequest)表实体类
 *
 * @author abcode
 * @since 2023-06-19 23:15:06
 */
@Data
@SuppressWarnings("serial")
@TableName("gc_friend_request")
public class FriendRequest {
            
    /** 主键 */        
                              
    @TableId(value= "id")
    private Long id;
                        
    /** 申请人 */        
                            
    @TableField("from_user_id")
    private Long fromUserId;
                        
    /** 被申请人ID */        
                            
    @TableField("to_user_id")
    private Long toUserId;
                        
    /** 昵称 */        
                            
    @TableField("from_nick_name")
    private String fromNickName;
                        
    /** 申请状态：0-待通过；1-已通过；2-已忽略；3-已阅； */        
                            
    @TableField("request_status")
    private Integer requestStatus;
                        
    /** 处理时间 */        
                            
    @TableField("process_time")
    private Date processTime;
                        
    /** 请求信息 */        
                            
    @TableField("request_msg")
    private String requestMsg;
                        
    /** 查阅状态：0-未读；1-已阅； */        
                            
    @TableField("read_status")
    private Integer readStatus;
                        
    /** 创建时间 */        
                            
    @TableField("create_time")
    private Date createTime;
                        
    /** 创建者 */        
                            
    @TableField("create_by")
    private Long createBy;
                        
    /** 更新时间 */        
                            
    @TableField("update_time")
    private Date updateTime;
                        
    /** 更新者 */        
                            
    @TableField("update_by")
    private Long updateBy;
                        
    /** 备注 */        
                            
    @TableField("remark")
    private String remark;
                        
    /** 是否删除：0-否；1-是； */        
                            
    @TableField("del_flag")
    private Integer delFlag;
            
}
