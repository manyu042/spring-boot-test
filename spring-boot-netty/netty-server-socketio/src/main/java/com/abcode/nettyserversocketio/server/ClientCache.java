package com.abcode.nettyserversocketio.server;

import cn.hutool.core.util.ObjUtil;
import cn.hutool.core.util.StrUtil;
import com.corundumstudio.socketio.SocketIOClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @ClassNameClientCache
 * @Description TODO 缓存用户 - 页面sessionId - 通道连接
 * @Author DELL
 * @Date 2022/1/2113:55
 * @Version 1.0
 **/
@Component
public class ClientCache {
    /**
     * @Author 二哈老头子
     * @Description //TODO 用户信息缓存
     * @Date 14:00 2022/1/21
     * @Param
     * @return
     **/
    private static Map<String, HashMap<UUID, SocketIOClient>> concurrentHashMap = new ConcurrentHashMap<>();
    /**
     * socket缓存
     */
    private static Map<String, SocketIOClient>  socketIdMap = new ConcurrentHashMap<>();

    private static Map<Long, String>  userIdMap = new ConcurrentHashMap<>();
    private static Map<Long, Boolean>  userOnlineMap = new ConcurrentHashMap<>();

    /**
     * 缓存用户与socket的绑定关系
     * @param userId
     * @param sessionId
     * @param socketIOClient
     */
    public void saveClient(String userId,String sessionId,SocketIOClient socketIOClient){
        if(!socketIdMap.containsKey(sessionId)){
            socketIdMap.put(sessionId, socketIOClient);
        }
        if(ObjUtil.isNotEmpty(userId)){
            userIdMap.put(Long.valueOf(userId), sessionId);
            userOnlineMap.put(Long.valueOf(userId), true);
        }
    }

    /**
     * 获取用户的socket
     * @param userId
     * @return
     */
    public SocketIOClient getUserClient(Long userId){
        if(userIdMap.containsKey(userId)){
            String sessionId = userIdMap.get(userId);

            return socketIdMap.get(sessionId);
        }
        return null;
    }


    public void deleteSessionClientByUserId(Long userId){
        String sessionId = userIdMap.get(userId);
        userIdMap.remove(userId);
        socketIdMap.remove(sessionId);
        userOnlineMap.remove(userId);
    }


    public void deleteUserCacheBySessionId(String sessionId){
        Long userId = getUserIdBySessionId(sessionId);
        socketIdMap.remove(sessionId);
        if(userId != null){
            userIdMap.remove(userId);
            userOnlineMap.remove(userId);
        }
    }

    public Long getUserIdBySessionId(String sessionId) {
        Long resultId = null;
        for (Long userId : userIdMap.keySet()) {
            String tempSessionId = userIdMap.get(userId);
            if(sessionId.equals(tempSessionId)){
                resultId = userId;
                break;
            }
        }
        return resultId;
    }

    public void bindUserId(String userId, SocketIOClient client) {
        String sessionId = client.getSessionId().toString();
        if(!socketIdMap.containsKey(sessionId)){
            socketIdMap.put(sessionId, client);
        }
        userIdMap.put(Long.valueOf(userId), sessionId);
        userOnlineMap.put(Long.valueOf(userId), true);

    }

    /**
     * 判断用户是否在线
     * @param userId
     * @return
     */
    public boolean checkOnlineStatus(Long userId) {
        if(userOnlineMap.containsKey(userId)){

            return userOnlineMap.get(userId);

        }else{
            return false;
        }
    }

}
