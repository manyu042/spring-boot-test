package com.abcode.nettyserversocketio.gen.vo;

import java.util.Date;
import lombok.Data;
/**
 * 资源表(ArticleResource)前端显示类
 *
 * @author abcode
 * @since 2023-06-17 23:01:18
 */
@Data
@SuppressWarnings("serial")
public class ArticleResourceVO {
            
    /** 主键 */        
    private Long id;
            
    /** 资源文件名称 */        
    private String resName;
            
    /** 资源大小（单位KB） */        
    private Integer resSize;
            
    /** 资源类型：0-图片； 1-视频；2-音频； */        
    private Integer resType;
            
    /** 文章ID */        
    private Long articleId;
            
    /** 创建时间 */        
    private Date createTime;
            
    /** 创建者 */        
    private Long createBy;
            
    /** 更新时间 */        
    private Date updateTime;
            
    /** 更新者 */        
    private Long updateBy;
            
    /** 备注 */        
    private String remark;
            
    /** 是否删除：0-否；1-是； */        
    private Integer delFlag;

}
