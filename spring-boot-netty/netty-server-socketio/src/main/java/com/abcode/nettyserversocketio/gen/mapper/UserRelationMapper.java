package com.abcode.nettyserversocketio.gen.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.abcode.nettyserversocketio.gen.entity.UserRelation;

/**
 * (UserRelation)表数据库访问层
 *
 * @author abcode
 * @since 2023-06-17 22:51:08
 */
public interface UserRelationMapper extends BaseMapper<UserRelation> {

}

