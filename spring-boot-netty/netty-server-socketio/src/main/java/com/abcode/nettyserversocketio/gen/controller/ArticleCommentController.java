package com.abcode.nettyserversocketio.gen.controller;



import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.io.Serializable;
import java.util.List;

import com.abcode.nettyserversocketio.gen.entity.ArticleComment;
import com.abcode.nettyserversocketio.gen.service.ArticleCommentService;
import com.abcode.nettyserversocketio.common.Result;

/**
 * 文章评论记录表(ArticleComment)表控制层
 *
 * @author abcode
 * @since 2023-06-17 22:51:07
 */
@RestController
@RequestMapping("articleComment")
public class ArticleCommentController {
    /**
     * 服务对象
     */
    @Resource
    private ArticleCommentService articleCommentService;

    /**
     * 分页查询所有数据
     * @param page 分页对象
     * @param articleComment 查询实体
     * @return 所有数据
     */
    @GetMapping("/selectAll")
    public Result selectAll(Page<ArticleComment> page, ArticleComment articleComment) {
        Page<ArticleComment> rst = this.articleCommentService.page(page, new QueryWrapper<>(articleComment));
        return Result.ok(rst);
    }

    /**
     * 通过主键查询单条数据
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("/selectOne/{id}")
    public Result selectOne(@PathVariable Serializable id) {
        ArticleComment articleComment = this.articleCommentService.getById(id);
        return Result.ok(articleComment);
    }

    /**
     * 新增数据
     * @param articleComment 实体对象
     * @return 新增结果
     */
    @PostMapping("/insert")
    public Result insert(@RequestBody ArticleComment articleComment) {
        Boolean rst = this.articleCommentService.save(articleComment);
        return Result.OK(rst);
    }

    /**
     * 修改数据
     * @param articleComment 实体对象
     * @return 修改结果
     */
    @PutMapping("/update")
    public Result update(@RequestBody ArticleComment articleComment) {
        Boolean rst = this.articleCommentService.updateById(articleComment);
        return Result.OK(rst);
    }

    /**
     * 删除数据
     *
     * @param idList 主键结合
     * @return 删除结果
     */
    @DeleteMapping("/delete")
    public Result delete(@RequestParam("idList") List<Long> idList) {
        boolean rst = this.articleCommentService.removeByIds(idList);
        return Result.ok(rst);
    }
}

