package com.abcode.nettyserversocketio.biz;

import com.abcode.nettyserversocketio.common.Result;
import com.abcode.nettyserversocketio.gen.entity.Article;
import com.abcode.nettyserversocketio.gen.vo.UserVO;
import com.baomidou.mybatisplus.core.metadata.IPage;

import java.util.List;

/**
 * 广场接口类
 */
public interface ISquareBizService {
    /**
     * 取用户的帖子列表
     * @param userId
     * @return
     */
    List<Article> getUserArticleList(long userId);

    List<Article> pageArticle(String order, Long offset, Long limit);

    /**
     * 发帖子
     * @param userVO
     * @param title
     * @param content
     * @return
     */
    Result publishArticle(UserVO userVO, String title, String content);

    /**
     * 判断用户是否对指定帖子点赞
     * @param userVO
     * @param articleId
     * @return
     */
    boolean checkStarStatus(UserVO userVO, Long articleId);

    /**
     * 点赞
     * @param userVO
     * @param articleId
     * @return
     */
    Result starArticle(UserVO userVO, Long articleId);
}
