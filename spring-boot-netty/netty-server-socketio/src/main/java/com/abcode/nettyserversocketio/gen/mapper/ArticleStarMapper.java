package com.abcode.nettyserversocketio.gen.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.abcode.nettyserversocketio.gen.entity.ArticleStar;

/**
 * 文章点赞记录表(ArticleStar)表数据库访问层
 *
 * @author abcode
 * @since 2023-06-17 22:51:07
 */
public interface ArticleStarMapper extends BaseMapper<ArticleStar> {

}

