package com.abcode.nettyserversocketio.gen.vo;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
/**
 * 文章表(Article)前端显示类
 *
 * @author abcode
 * @since 2023-06-17 23:01:18
 */
@Data
@SuppressWarnings("serial")
public class ArticleVO {
            
    /** 主键 */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
            
    /** 用户ID */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long userId;
            
    /** 标题 */        
    private String title;
            
    /** 内容 */        
    private String content;
            
    /** 点赞数 */        
    private Integer star;
            
    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;
            
    /** 创建者 */        
    private Long createBy;
            
    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;
            
    /** 更新者 */        
    private Long updateBy;
            
    /** 备注 */        
    private String remark;
            
    /** 是否删除：0-否；1-是； */        
    private Integer delFlag;

}
