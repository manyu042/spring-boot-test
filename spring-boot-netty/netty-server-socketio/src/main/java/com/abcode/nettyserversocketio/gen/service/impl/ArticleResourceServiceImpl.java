package com.abcode.nettyserversocketio.gen.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.abcode.nettyserversocketio.gen.mapper.ArticleResourceMapper;
import com.abcode.nettyserversocketio.gen.entity.ArticleResource;
import com.abcode.nettyserversocketio.gen.service.ArticleResourceService;
import org.springframework.stereotype.Service;

/**
 * 资源表(ArticleResource)表服务实现类
 *
 * @author abcode
 * @since 2023-06-17 22:51:07
 */
@Service
public class ArticleResourceServiceImpl extends ServiceImpl<ArticleResourceMapper, ArticleResource> implements ArticleResourceService {

}

