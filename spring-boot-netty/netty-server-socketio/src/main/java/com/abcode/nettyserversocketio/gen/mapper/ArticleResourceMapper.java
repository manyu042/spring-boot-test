package com.abcode.nettyserversocketio.gen.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.abcode.nettyserversocketio.gen.entity.ArticleResource;

/**
 * 资源表(ArticleResource)表数据库访问层
 *
 * @author abcode
 * @since 2023-06-17 22:51:07
 */
public interface ArticleResourceMapper extends BaseMapper<ArticleResource> {

}

