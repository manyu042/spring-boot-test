package com.abcode.nettyserversocketio.gen.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.abcode.nettyserversocketio.gen.entity.ArticleStar;

/**
 * 文章点赞记录表(ArticleStar)表服务接口
 *
 * @author abcode
 * @since 2023-06-17 22:51:07
 */
public interface ArticleStarService extends IService<ArticleStar> {

}

