package com.abcode.nettyserversocketio.biz;

import com.abcode.nettyserversocketio.common.Result;
import com.abcode.nettyserversocketio.gen.entity.FriendRequest;
import com.abcode.nettyserversocketio.gen.entity.User;
import com.abcode.nettyserversocketio.gen.param.UserParam;
import com.abcode.nettyserversocketio.gen.vo.UserVO;

import java.util.List;

/**
 * 用户相关接口类
 */
public interface IUserBizService {
    /**
     * 注册账号
     * @param userParam
     * @return
     */
    Result reg(UserParam userParam);

    Result login(UserParam userParam);

    Result verifyToken(String token);

    /**
     * 解析token
     * @param token
     * @return
     */
    UserVO getUserVOByToken(String token);

    /**
     * 查询用户的所有好友列表
     * @param id
     * @return
     */
    List<User> getAllFriends(Long id);

    /**
     * 根据uid查找用户信息
     *
     * @param uuid 用户标识
     * @return
     */
    List<User> selectUser(String uuid);

    /**
     * 判断muid对应的用户是否是id用户的好友
     * @param id
     * @param relationId
     * @return
     */
    boolean isOwnFriend(Long id, Long relationId);

    /**
     * 添加为朋友
     * @param id
     * @param uuid
     * @return
     */
    boolean addFriend(Long id, String uuid);

    /**
     * 申请好友
     * @param fromUser 申请人
     * @param toUserId 被申请人
     * @param message 验证信息
     * @return
     */
    boolean friendRequest(UserVO fromUser, Long toUserId, String message);

    /**
     * 查询未查阅的好友请求数量
     * @param toUserId
     * @return
     */
    int getAllNoticesNumber(Long toUserId);

    /**
     * 查询未查阅的好友请求列表
     * @param toUserId
     * @return
     */
    List<FriendRequest> getAllNotices(Long toUserId);

    /**
     * 通过好友申请
     * @param userVO
     * @param friendRequestId
     * @return
     */
    Result agreeFriendRequest(UserVO userVO, Long friendRequestId);

    /**
     * 根据唯一标识获取用户信息
     * @param uuid
     * @return
     */
    User getUserByUuid(String uuid);

}
