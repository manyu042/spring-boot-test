package com.abcode.nettyserversocketio.gen.controller;



import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.io.Serializable;
import java.util.List;

import com.abcode.nettyserversocketio.gen.entity.UserRelation;
import com.abcode.nettyserversocketio.gen.service.UserRelationService;
import com.abcode.nettyserversocketio.common.Result;

/**
 * (UserRelation)表控制层
 *
 * @author abcode
 * @since 2023-06-17 22:51:08
 */
@RestController
@RequestMapping("userRelation")
public class UserRelationController {
    /**
     * 服务对象
     */
    @Resource
    private UserRelationService userRelationService;

    /**
     * 分页查询所有数据
     * @param page 分页对象
     * @param userRelation 查询实体
     * @return 所有数据
     */
    @GetMapping("/selectAll")
    public Result selectAll(Page<UserRelation> page, UserRelation userRelation) {
        Page<UserRelation> rst = this.userRelationService.page(page, new QueryWrapper<>(userRelation));
        return Result.ok(rst);
    }

    /**
     * 通过主键查询单条数据
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("/selectOne/{id}")
    public Result selectOne(@PathVariable Serializable id) {
        UserRelation userRelation = this.userRelationService.getById(id);
        return Result.ok(userRelation);
    }

    /**
     * 新增数据
     * @param userRelation 实体对象
     * @return 新增结果
     */
    @PostMapping("/insert")
    public Result insert(@RequestBody UserRelation userRelation) {
        Boolean rst = this.userRelationService.save(userRelation);
        return Result.OK(rst);
    }

    /**
     * 修改数据
     * @param userRelation 实体对象
     * @return 修改结果
     */
    @PutMapping("/update")
    public Result update(@RequestBody UserRelation userRelation) {
        Boolean rst = this.userRelationService.updateById(userRelation);
        return Result.OK(rst);
    }

    /**
     * 删除数据
     *
     * @param idList 主键结合
     * @return 删除结果
     */
    @DeleteMapping("/delete")
    public Result delete(@RequestParam("idList") List<Long> idList) {
        boolean rst = this.userRelationService.removeByIds(idList);
        return Result.ok(rst);
    }
}

