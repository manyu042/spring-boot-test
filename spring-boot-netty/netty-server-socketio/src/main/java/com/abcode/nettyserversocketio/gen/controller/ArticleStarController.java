package com.abcode.nettyserversocketio.gen.controller;



import com.abcode.nettyserversocketio.common.Result;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.io.Serializable;
import java.util.List;

import com.abcode.nettyserversocketio.gen.entity.ArticleStar;
import com.abcode.nettyserversocketio.gen.service.ArticleStarService;


/**
 * 文章点赞记录表(ArticleStar)表控制层
 *
 * @author abcode
 * @since 2023-06-17 22:51:07
 */
@RestController
@RequestMapping("articleStar")
public class ArticleStarController {
    /**
     * 服务对象
     */
    @Resource
    private ArticleStarService articleStarService;

    /**
     * 分页查询所有数据
     * @param page 分页对象
     * @param articleStar 查询实体
     * @return 所有数据
     */
    @GetMapping("/selectAll")
    public Result selectAll(Page<ArticleStar> page, ArticleStar articleStar) {
        Page<ArticleStar> rst = this.articleStarService.page(page, new QueryWrapper<>(articleStar));
        return Result.ok(rst);
    }

    /**
     * 通过主键查询单条数据
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("/selectOne/{id}")
    public Result selectOne(@PathVariable Serializable id) {
        ArticleStar articleStar = this.articleStarService.getById(id);
        return Result.ok(articleStar);
    }

    /**
     * 新增数据
     * @param articleStar 实体对象
     * @return 新增结果
     */
    @PostMapping("/insert")
    public Result insert(@RequestBody ArticleStar articleStar) {
        Boolean rst = this.articleStarService.save(articleStar);
        return Result.OK(rst);
    }

    /**
     * 修改数据
     * @param articleStar 实体对象
     * @return 修改结果
     */
    @PutMapping("/update")
    public Result update(@RequestBody ArticleStar articleStar) {
        Boolean rst = this.articleStarService.updateById(articleStar);
        return Result.OK(rst);
    }

    /**
     * 删除数据
     *
     * @param idList 主键结合
     * @return 删除结果
     */
    @DeleteMapping("/delete")
    public Result delete(@RequestParam("idList") List<Long> idList) {
        boolean rst = this.articleStarService.removeByIds(idList);
        return Result.ok(rst);
    }
}

