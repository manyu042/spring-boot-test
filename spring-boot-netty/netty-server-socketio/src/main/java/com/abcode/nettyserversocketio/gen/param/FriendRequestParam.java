package com.abcode.nettyserversocketio.gen.param;

import java.util.Date;
import lombok.Data;
/**
 * 申请好友(FriendRequest)参数类
 *
 * @author abcode
 * @since 2023-06-19 23:15:06
 */
@Data
@SuppressWarnings("serial")
public class FriendRequestParam {
            
    /** 主键 */        
    private Long id;
            
    /** 申请人 */        
    private Long fromUserId;
            
    /** 被申请人ID */        
    private Long toUserId;
            
    /** 昵称 */        
    private String fromNickName;
            
    /** 申请状态：0-待通过；1-已通过；2-已忽略；3-已阅； */        
    private Integer requestStatus;
            
    /** 处理时间 */        
    private Date processTime;
            
    /** 请求信息 */        
    private String requestMsg;
            
    /** 查阅状态：0-未读；1-已阅； */        
    private Integer readStatus;
            
    /** 创建时间 */        
    private Date createTime;
            
    /** 创建者 */        
    private Long createBy;
            
    /** 更新时间 */        
    private Date updateTime;
            
    /** 更新者 */        
    private Long updateBy;
            
    /** 备注 */        
    private String remark;
            
    /** 是否删除：0-否；1-是； */        
    private Integer delFlag;

}
