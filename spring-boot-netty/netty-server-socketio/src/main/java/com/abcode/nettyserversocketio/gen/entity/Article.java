package com.abcode.nettyserversocketio.gen.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
/**
 * 文章表(Article)表实体类
 *
 * @author abcode
 * @since 2023-06-17 22:51:07
 */
@Data
@SuppressWarnings("serial")
@TableName("gc_article")
public class Article {
            
    /** 主键 */                                  
    @TableId(value= "id")
    private Long id;
                        
    /** 用户ID */                                
    @TableField("user_id")
    private Long userId;
                        
    /** 标题 */                                
    @TableField("title")
    private String title;
                        
    /** 内容 */                                
    @TableField("content")
    private String content;
                        
    /** 点赞数 */                                
    @TableField("star")
    private Integer star;
                        
    /** 创建时间 */                                
    @TableField("create_time")
    private Date createTime;
                        
    /** 创建者 */                                
    @TableField("create_by")
    private Long createBy;
                        
    /** 更新时间 */                                
    @TableField("update_time")
    private Date updateTime;
                        
    /** 更新者 */                                
    @TableField("update_by")
    private Long updateBy;
                        
    /** 备注 */                                
    @TableField("remark")
    private String remark;
                        
    /** 是否删除：0-否；1-是； */                                
    @TableField("del_flag")
    private Integer delFlag;
            
}
