package com.abcode.nettyserversocketio.gen.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
/**
 * 文章评论记录表(ArticleComment)表实体类
 *
 * @author abcode
 * @since 2023-06-17 22:51:07
 */
@Data
@SuppressWarnings("serial")
@TableName("gc_article_comment")
public class ArticleComment {
            
    /** 主键 */                    
    /** 资源类型：0-图片； 1-视频；2-音频； */                    
    /** 文章ID */                    
    /** 评论内容 */                    
    /** 昵称 */                    
    /** 根评论ID */                    
    /** 目标评论ID */                    
    /** 创建时间 */                    
    /** 创建者 */                    
    /** 更新时间 */                    
    /** 更新者 */                    
    /** 备注 */                    
    /** 是否删除：0-否；1-是； */        
}
