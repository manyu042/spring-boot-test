package com.abcode.nettyserversocketio.gen.param;

import java.util.Date;
import lombok.Data;
/**
 * 文章评论记录表(ArticleComment)参数类
 *
 * @author abcode
 * @since 2023-06-17 23:01:18
 */
@Data
@SuppressWarnings("serial")
public class ArticleCommentParam {
            
    /** 主键 */        
    private Long id;
            
    /** 资源类型：0-图片； 1-视频；2-音频； */        
    private Long userId;
            
    /** 文章ID */        
    private Long articleId;
            
    /** 评论内容 */        
    private String content;
            
    /** 昵称 */        
    private String nickName;
            
    /** 根评论ID */        
    private Long rootCommentId;
            
    /** 目标评论ID */        
    private Long toCommentId;
            
    /** 创建时间 */        
    private Date createTime;
            
    /** 创建者 */        
    private Long createBy;
            
    /** 更新时间 */        
    private Date updateTime;
            
    /** 更新者 */        
    private Long updateBy;
            
    /** 备注 */        
    private String remark;
            
    /** 是否删除：0-否；1-是； */        
    private Integer delFlag;

}
