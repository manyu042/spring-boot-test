package com.abcode.nettyserversocketio.gen.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.abcode.nettyserversocketio.gen.entity.Article;

/**
 * 文章表(Article)表数据库访问层
 *
 * @author abcode
 * @since 2023-06-17 22:51:07
 */
public interface ArticleMapper extends BaseMapper<Article> {

}

