-- MySQL dump 10.13  Distrib 8.0.31, for macos12 (x86_64)
--
-- Host: 127.0.0.1    Database: abcode_go_chat
-- ------------------------------------------------------
-- Server version	8.0.33

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `gc_user`
--

DROP TABLE IF EXISTS `gc_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `gc_user` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `username` varchar(24) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户名',
  `nick_name` varchar(16) DEFAULT NULL COMMENT '昵称',
  `profile` varchar(45) DEFAULT NULL COMMENT '头像',
  `gender` varchar(45) DEFAULT NULL COMMENT '性别',
  `sign` varchar(45) DEFAULT NULL COMMENT '个性签名',
  `area` varchar(45) DEFAULT NULL COMMENT '区域',
  `uuid` varchar(32) NOT NULL COMMENT '账号唯一标识',
  `password` varchar(45) NOT NULL COMMENT '密码',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `create_by` bigint DEFAULT NULL COMMENT '创建者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `update_by` bigint DEFAULT NULL COMMENT '更新者',
  `remark` varchar(45) DEFAULT NULL COMMENT '备注',
  `del_flag` int NOT NULL DEFAULT '0' COMMENT '是否删除：0-否；1-是；',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uuid_UNIQUE` (`uuid`),
  UNIQUE KEY `username_UNIQUE` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=1670345028333117442 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='用户表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gc_user`
--

LOCK TABLES `gc_user` WRITE;
/*!40000 ALTER TABLE `gc_user` DISABLE KEYS */;
INSERT INTO `gc_user` VALUES (1670323409216757762,'guo1234','guo1234','default.png','0','这个人不懒，但什么都没有写。',NULL,'63950389','d0dcbf0d12a6b1e7fbfa2ce5848f3eff','2023-06-18 06:51:48',NULL,NULL,NULL,NULL,0),(1670343433398689794,'guo200_0','guo200_0','default.png','0','这个人不懒，但什么都没有写。',NULL,'61926694','d0dcbf0d12a6b1e7fbfa2ce5848f3eff','2023-06-18 08:11:23',NULL,NULL,NULL,NULL,0),(1670343434237550593,'guo200_0_1','guo200_0_1','default.png','0','这个人不懒，但什么都没有写。',NULL,'24164883','d0dcbf0d12a6b1e7fbfa2ce5848f3eff','2023-06-18 08:11:23',NULL,NULL,NULL,NULL,0),(1670343434329825281,'guo200_0_1_2','guo200_0_1_2','default.png','0','这个人不懒，但什么都没有写。',NULL,'58638710','d0dcbf0d12a6b1e7fbfa2ce5848f3eff','2023-06-18 08:11:23',NULL,NULL,NULL,NULL,0),(1670343434413711362,'guo200_0_1_2_3','guo200_0_1_2_3','default.png','0','这个人不懒，但什么都没有写。',NULL,'14145975','d0dcbf0d12a6b1e7fbfa2ce5848f3eff','2023-06-18 08:11:23',NULL,NULL,NULL,NULL,0),(1670343434480820225,'guo200_0_1_2_3_4','guo200_0_1_2_3_4','default.png','0','这个人不懒，但什么都没有写。',NULL,'08573891','d0dcbf0d12a6b1e7fbfa2ce5848f3eff','2023-06-18 08:11:23',NULL,NULL,NULL,NULL,0),(1670345025875255298,'guo300_0','guo300_0','default.png','0','这个人不懒，但什么都没有写。',NULL,'75621813','d0dcbf0d12a6b1e7fbfa2ce5848f3eff','2023-06-18 08:17:42',NULL,NULL,NULL,NULL,0),(1670345026030444546,'guo300_1','guo300_1','default.png','0','这个人不懒，但什么都没有写。',NULL,'57795661','d0dcbf0d12a6b1e7fbfa2ce5848f3eff','2023-06-18 08:17:42',NULL,NULL,NULL,NULL,0),(1670345026219188226,'guo300_2','guo300_2','default.png','0','这个人不懒，但什么都没有写。',NULL,'88476928','d0dcbf0d12a6b1e7fbfa2ce5848f3eff','2023-06-18 08:17:42',NULL,NULL,NULL,NULL,0),(1670345026437292034,'guo300_3','guo300_3','default.png','0','这个人不懒，但什么都没有写。',NULL,'63303569','d0dcbf0d12a6b1e7fbfa2ce5848f3eff','2023-06-18 08:17:42',NULL,NULL,NULL,NULL,0),(1670345026575704065,'guo300_4','guo300_4','default.png','0','这个人不懒，但什么都没有写。',NULL,'29681064','d0dcbf0d12a6b1e7fbfa2ce5848f3eff','2023-06-18 08:17:42',NULL,NULL,NULL,NULL,0),(1670345026697338882,'guo300_5','guo300_5','default.png','0','这个人不懒，但什么都没有写。',NULL,'92433323','d0dcbf0d12a6b1e7fbfa2ce5848f3eff','2023-06-18 08:17:43',NULL,NULL,NULL,NULL,0),(1670345026777030658,'guo300_6','guo300_6','default.png','0','这个人不懒，但什么都没有写。',NULL,'12706370','d0dcbf0d12a6b1e7fbfa2ce5848f3eff','2023-06-18 08:17:43',NULL,NULL,NULL,NULL,0),(1670345026881888258,'guo300_7','guo300_7','default.png','0','这个人不懒，但什么都没有写。',NULL,'25420396','d0dcbf0d12a6b1e7fbfa2ce5848f3eff','2023-06-18 08:17:43',NULL,NULL,NULL,NULL,0),(1670345027037077505,'guo300_8','guo300_8','default.png','0','这个人不懒，但什么都没有写。',NULL,'88723398','d0dcbf0d12a6b1e7fbfa2ce5848f3eff','2023-06-18 08:17:43',NULL,NULL,NULL,NULL,0),(1670345027238404098,'guo300_9','guo300_9','default.png','0','这个人不懒，但什么都没有写。',NULL,'59816677','d0dcbf0d12a6b1e7fbfa2ce5848f3eff','2023-06-18 08:17:43',NULL,NULL,NULL,NULL,0),(1670345027414564865,'guo300_10','guo300_10','default.png','0','这个人不懒，但什么都没有写。',NULL,'44579478','d0dcbf0d12a6b1e7fbfa2ce5848f3eff','2023-06-18 08:17:43',NULL,NULL,NULL,NULL,0),(1670345027481673730,'guo300_11','guo300_11','default.png','0','这个人不懒，但什么都没有写。',NULL,'96249684','d0dcbf0d12a6b1e7fbfa2ce5848f3eff','2023-06-18 08:17:43',NULL,NULL,NULL,NULL,0),(1670345027548782594,'guo300_12','guo300_12','default.png','0','这个人不懒，但什么都没有写。',NULL,'84106719','d0dcbf0d12a6b1e7fbfa2ce5848f3eff','2023-06-18 08:17:43',NULL,NULL,NULL,NULL,0),(1670345027611697154,'guo300_13','guo300_13','default.png','0','这个人不懒，但什么都没有写。',NULL,'90715292','d0dcbf0d12a6b1e7fbfa2ce5848f3eff','2023-06-18 08:17:43',NULL,NULL,NULL,NULL,0),(1670345027695583233,'guo300_14','guo300_14','default.png','0','这个人不懒，但什么都没有写。',NULL,'30199019','d0dcbf0d12a6b1e7fbfa2ce5848f3eff','2023-06-18 08:17:43',NULL,NULL,NULL,NULL,0),(1670345027771080705,'guo300_15','guo300_15','default.png','0','这个人不懒，但什么都没有写。',NULL,'85603985','d0dcbf0d12a6b1e7fbfa2ce5848f3eff','2023-06-18 08:17:43',NULL,NULL,NULL,NULL,0),(1670345027871744002,'guo300_16','guo300_16','default.png','0','这个人不懒，但什么都没有写。',NULL,'08428285','d0dcbf0d12a6b1e7fbfa2ce5848f3eff','2023-06-18 08:17:43',NULL,NULL,NULL,NULL,0),(1670345028064681986,'guo300_17','guo300_17','default.png','0','这个人不懒，但什么都没有写。',NULL,'80030033','d0dcbf0d12a6b1e7fbfa2ce5848f3eff','2023-06-18 08:17:43',NULL,NULL,NULL,NULL,0),(1670345028261814274,'guo300_18','guo300_18','default.png','0','这个人不懒，但什么都没有写。',NULL,'33459188','d0dcbf0d12a6b1e7fbfa2ce5848f3eff','2023-06-18 08:17:43',NULL,NULL,NULL,NULL,0),(1670345028333117441,'guo300_19','guo300_19','default.png','0','这个人不懒，但什么都没有写。',NULL,'70895080','d0dcbf0d12a6b1e7fbfa2ce5848f3eff','2023-06-18 08:17:43',NULL,NULL,NULL,NULL,0);
/*!40000 ALTER TABLE `gc_user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-06-19 23:30:27
