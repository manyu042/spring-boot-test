package com.abcode.nettyserverwebsocket;

import com.abcode.nettyserverwebsocket.server.WebSocketServer;
import io.netty.channel.ChannelFuture;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
 @SpringBootApplication
public class NettyServerWebsocketApplication  implements CommandLineRunner{
 public static void main(String[] args) {
        SpringApplication.run(NettyServerWebsocketApplication.class, args);
    }

     @Override
     public void run(String... args) throws Exception {
         WebSocketServer server = new WebSocketServer(9999);
         server.run();
     }
 }