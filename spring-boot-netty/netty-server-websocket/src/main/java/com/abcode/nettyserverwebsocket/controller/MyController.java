package com.abcode.nettyserverwebsocket.controller;

import com.abcode.nettyserverwebsocket.server.WebSocketHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
 @RestController
public class MyController {

     @GetMapping("/send")
    public String sendMessage(String message) {
         WebSocketHandler.sendToAll(message);
        return "Message sent to connection ";
    }
}