package com.abcode.nettyclient;

import com.abcode.nettyclient.client.NettyClientConfig;
import com.abcode.nettyclient.client.pojo.ChatDto;
import com.alibaba.fastjson2.JSON;
import io.netty.channel.ChannelFuture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;


@Component
public class NettyClientRunner implements CommandLineRunner {

    @Override
    public void run(String... args) throws Exception {
        System.out.println("---run---");
    }
}