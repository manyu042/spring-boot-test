package com.abcode.nettyclient;

import com.abcode.nettyclient.client.NettyClientConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class NettyClientApplication {

    public static void main(String[] args) throws InterruptedException{
        ConfigurableApplicationContext context = SpringApplication.run(NettyClientApplication.class, args);
        NettyClientConfig config = context.getBean(NettyClientConfig.class);
        config.start();
        System.out.println("----重连监听-----");
    }

}
