package com.abcode.nettyclient.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("test")
@RestController
public class TestApiController {

    @RequestMapping("index")
    public String test(){
        return "test index";
    }
}
