package com.abcode.nettyclient.controller;

import com.abcode.nettyclient.client.NettyClient;
import com.abcode.nettyclient.client.NettyClientConfig;
import com.abcode.nettyclient.client.pojo.ChatDto;
import com.alibaba.fastjson2.JSON;
import io.netty.channel.ChannelFuture;
import io.netty.util.internal.ObjectUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RequestMapping("my")
@RestController
public class MyController {

    @Autowired
    ChannelFuture channelFuture;

     @GetMapping("/sendMsg")
    public String sendMessage(String msg) {
         if(ObjectUtils.isEmpty(msg)){
             msg = "send msg";
         }
         ChatDto dto=new ChatDto();
         dto.setClientId(NettyClientConfig.clientId).setMsg(msg);
         channelFuture.channel().writeAndFlush(JSON.toJSONString(dto));
        return "Message sent to server.";
    }
}