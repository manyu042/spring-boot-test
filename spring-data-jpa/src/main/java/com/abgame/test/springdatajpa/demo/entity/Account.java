package com.abgame.test.springdatajpa.demo.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * 用户表(Account)实体类
 *
 * @author makejava
 * @since 2021-01-16 18:00:36
 */

@Data
@Entity
@JsonIgnoreProperties(value = {"hibernateLazyInitializer"})
@Table(name = "tb_account", schema = "spring_boot_test")
public class Account implements Serializable {

    private static final long serialVersionUID = 292362584311439267L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "account_id")
    private Integer accountId;

    /**
     * 账号
     */
    @Basic
    @Column(name = "account")
    private String account;

    /**
     * 昵称
     */
    @Basic
    @Column(name = "account_name")
    private String accountName;

    /**
     * 代理商ID
     */
    @Basic
    @Column(name = "agent_id")
    private Integer agentId;

    /**
     * 创建时间
     */
    @Basic
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 更新时间
     */
    @Basic
    @Column(name = "update_time")
    private Date updateTime;

    /**
     * 状态：正常，正常；删除，删除；
     */
    @Basic
    @Column(name = "tb_status")
    private String tbStatus;

}