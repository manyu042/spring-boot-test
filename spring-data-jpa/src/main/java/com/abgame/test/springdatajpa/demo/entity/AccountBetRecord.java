package com.abgame.test.springdatajpa.demo.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * 用户投注记录表(AccountBetRecord)实体类
 *
 * @author makejava
 * @since 2021-01-16 18:00:37
 */

@Data
@Entity
@JsonIgnoreProperties(value = {"hibernateLazyInitializer"})
@Table(name = "tb_account_bet_record", schema = "spring_boot_test")
public class AccountBetRecord implements Serializable {

    private static final long serialVersionUID = -85158345172069328L;

    /**
     * 用户投注记录id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_bet_record_id")
    private Integer userBetRecordId;

    /**
     * 渠道id
     */
    @Basic
    @Column(name = "channel_id")
    private Integer channelId;

    /**
     * 游戏各自的投注id
     */
    @Basic
    @Column(name = "bet_id")
    private String betId;

    /**
     * 用户id
     */
    @Basic
    @Column(name = "account_id")
    private Integer accountId;

    /**
     * 游戏类型 百家乐、龙虎
     */
    @Basic
    @Column(name = "game_name")
    private String gameName;

    /**
     * 用户登录账号
     */
    @Basic
    @Column(name = "username")
    private String username;

    /**
     * 用户昵称
     */
    @Basic
    @Column(name = "user_nickname")
    private String userNickname;

    /**
     * 代理商ID
     */
    @Basic
    @Column(name = "agent_id")
    private Integer agentId;

    /**
     * 房间ID
     */
    @Basic
    @Column(name = "room_id")
    private Integer roomId;

    /**
     * 局数
     */
    @Basic
    @Column(name = "round")
    private String round;

    /**
     * 投注金额
     */
    @Basic
    @Column(name = "bet_amount")
    private Double betAmount;

    /**
     * 投注名称内容
     */
    @Basic
    @Column(name = "bet_info")
    private String betInfo;

    /**
     * 中奖结果 中、没中、和局、流局
     */
    @Basic
    @Column(name = "win_result")
    private String winResult;

    /**
     * 开奖详细结果 龙虎：龙\\\\虎\\\\和 百家乐:庄\\\\闲\\\\庄对\\\\闲对\\\\和
     */
    @Basic
    @Column(name = "lottery_result")
    private String lotteryResult;

    /**
     * 中奖金额
     */
    @Basic
    @Column(name = "won_amount")
    private Double wonAmount;

    /**
     * 投注订单号
     */
    @Basic
    @Column(name = "bet_order_id")
    private String betOrderId;

    /**
     * eos区块链的局数
     */
    @Basic
    @Column(name = "eos_round")
    private String eosRound;

    /**
     * 备注
     */
    @Basic
    @Column(name = "remark")
    private String remark;

    /**
     * 货币类型 EOS,jf_EOS，jf_GA，jf_SVT
     */
    @Basic
    @Column(name = "currency_type")
    private String currencyType;

    /**
     * 投注时间
     */
    @Basic
    @Column(name = "bet_time")
    private String betTime;

    @Basic
    @Column(name = "account_type")
    private String accountType;

    /**
     * 有效投注
     */
    @Basic
    @Column(name = "true_bet")
    private String trueBet;

    /**
     * 游戏id
     */
    @Basic
    @Column(name = "game_id")
    private Integer gameId;

    /**
     * 投注前金额
     */
    @Basic
    @Column(name = "amount_before_bet")
    private Double amountBeforeBet;

    /**
     * ip
     */
    @Basic
    @Column(name = "ip")
    private String ip;

    @Basic
    @Column(name = "app_key")
    private String appKey;

    @Basic
    @Column(name = "app_name")
    private String appName;

    /**
     * 平台类型
     */
    @Basic
    @Column(name = "app_type")
    private String appType;

    /**
     * 渠道
     */
    @Basic
    @Column(name = "channel")
    private String channel;

    /**
     * 房间类型
     */
    @Basic
    @Column(name = "room_type")
    private String roomType;

    /**
     * 创建时间
     */
    @Basic
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 更新时间
     */
    @Basic
    @Column(name = "update_time")
    private Date updateTime;

    /**
     * 状态：正常，正常；删除，删除；
     */
    @Basic
    @Column(name = "tb_status")
    private String tbStatus;

}