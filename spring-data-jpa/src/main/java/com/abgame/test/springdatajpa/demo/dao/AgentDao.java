package com.abgame.test.springdatajpa.demo.dao;

import com.abgame.test.springdatajpa.demo.entity.Agent;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * 代理商(Agent)表数据库访问层
 *
 * @author zry
 * @since 2021-01-19 16:45:56
 */
public interface AgentDao extends JpaRepository<Agent, Integer> {


}