package com.abgame.test.springdatajpa.demo.controller;

import com.abgame.test.springdatajpa.demo.entity.Account;
import com.abgame.test.springdatajpa.demo.param.CreateAccountParam;
import com.abgame.test.springdatajpa.demo.param.UpdateAccountParam;
import com.abgame.test.springdatajpa.demo.service.AccountService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.sql.Timestamp;
import java.util.Random;

/**
 * 用户表(Account)表控制层
 *
 * @author makejava
 * @since 2021-01-16 16:35:53
 */
@Slf4j
@RestController
@RequestMapping("account")
public class AccountController {
    /**
     * 服务对象
     */
    @Resource
    private AccountService accountService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne/{accountId}")
    public Account selectOne(@PathVariable("accountId") Integer id) {
        return this.accountService.queryById(id);
    }


    @GetMapping("/selectById")
    public Account selectByAccount(@RequestParam("accountId") Integer id) {
        return this.accountService.queryById(id);
    }


    /*@PostMapping("/create")
    public Account create(@RequestParam("account") String account, @RequestParam("agentId") Integer agentId){
        Account accountObj = new Account();
        accountObj.setAccountName(account+"_"+ new Random().nextInt(1000000));
        accountObj.setAgentId(agentId);
        accountObj.setTbStatus("正常");

        Account newObj = accountService.createAccount(accountObj);

        return newObj;
    }*/

    @PostMapping("/create")
    public Account create(@RequestBody CreateAccountParam accountParam){
        Account accountObj = new Account();

        accountObj.setAccountName(accountParam.getAccountName()+"_"+ new Random().nextInt(1000000));
        accountObj.setAccount(accountObj.getAccountName());
        accountObj.setAgentId(accountParam.getAgentId());
        accountObj.setTbStatus("正常");

        Account newObj = accountService.insert(accountObj);

        return newObj;
    }

    @PostMapping("/update")
    public Account update(@RequestBody UpdateAccountParam accountParam){

        Account oldAccount = accountService.queryById(accountParam.getAccountId());

        oldAccount.setAccountName(accountParam.getAccountName()+"_"+ new Random().nextInt(1000000));
        oldAccount.setAgentId(accountParam.getAgentId());
        oldAccount.setUpdateTime(new Timestamp(System.currentTimeMillis()));

        //oldAccount =accountService.update(oldAccount);
        int rst = accountService.modify(oldAccount);

        return oldAccount;
    }

    @RequestMapping("/delete/{accountId}")
    public String deleteAccount(@PathVariable("accountId") Integer accountId){
        int updateRst = -1;
        Account account =  accountService.queryById(accountId);
        if(account != null){
            updateRst = accountService.deleteAccount(account);
        }

        return updateRst+"";
    }

    @RequestMapping("/queryByName/{name}")
    public Account getAccountByName(@PathVariable("name") String name){
        Account account =  accountService.getAccountByName(name);
        return account;
    }

    @RequestMapping("/list/{pageNum}")
    public Page<Account> pageAccount(@PathVariable("pageNum") Integer pageNum){
        if(pageNum== null){
            pageNum=1;
        }
        Page<Account> page =  accountService.pageAccount(pageNum);
        log.info("select * ", page.getContent());
        return page;
    }
}