package com.abgame.test.springdatajpa.demo.service;

import com.abgame.test.springdatajpa.demo.entity.Account;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * 用户表(Account)表服务接口
 *
 * @author zry
 * @since 2021-01-16 16:35:51
 */
public interface AccountService {
    Account queryById(Integer accountId);

    Page<Account> queryAllByLimit(int offset, int limit);

    Account insert(Account account);

    Account update(Account account);

    boolean deleteById(Integer accountId);

    List<Account> getall();

    int modify(Account account);

    int deleteAccount(Account account);

    Account getAccountByName(String name);

    Page<Account> pageAccount(Integer pageNum);
}