package com.abgame.test.springdatajpa.demo.service.impl;

import com.abgame.test.springdatajpa.demo.dao.AgentDao;
import com.abgame.test.springdatajpa.demo.entity.Agent;
import com.abgame.test.springdatajpa.demo.service.AgentService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 代理商(Agent)表服务实现类
 *
 * @author makejava
 * @since 2021-01-19 16:45:57
 */
@Service("agentService")
public class AgentServiceImpl implements AgentService {
    @Resource
    private AgentDao agentDao;

    @Override
    public Agent queryById(Integer agentId) {
        return this.agentDao.findById(agentId).orElse(null);
    }

    @Override
    public List<Agent> getall() {
        return this.agentDao.findAll();

    }

    @Override
    public Page<Agent> queryAllByLimit(int offset, int limit) {
        return this.agentDao.findAll(PageRequest.of((offset - 1)
                * limit, limit));
    }

    @Override
    public Agent insert(Agent agent) {

        return this.agentDao.save(agent);
    }


    @Override
    public Agent update(Agent agent) {

        return this.agentDao.save(agent);
    }


    @Override
    public boolean deleteById(Integer agentId) {

        try {
            this.agentDao.deleteById(agentId);
        } catch (Exception ex) {
            return false;
        }
        return true;

    }
}