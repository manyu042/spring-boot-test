package com.abgame.test.springdatajpa.demo.controller;

import com.abgame.test.springdatajpa.demo.entity.Agent;
import com.abgame.test.springdatajpa.demo.service.AgentService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 代理商(Agent)表控制层
 *
 * @author makejava
 * @since 2021-01-19 16:45:57
 */
@RestController
@RequestMapping("agent")
public class AgentController {
    /**
     * 服务对象
     */
    @Resource
    private AgentService agentService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public Agent selectOne(Integer id) {
        return this.agentService.queryById(id);
    }

}