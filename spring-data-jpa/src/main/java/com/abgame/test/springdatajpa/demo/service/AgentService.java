package com.abgame.test.springdatajpa.demo.service;

import com.abgame.test.springdatajpa.demo.entity.Agent;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * 代理商(Agent)表服务接口
 *
 * @author zry
 * @since 2021-01-19 16:45:57
 */
public interface AgentService {
    Agent queryById(Integer agentId);

    Page<Agent> queryAllByLimit(int offset, int limit);

    Agent insert(Agent agent);

    Agent update(Agent agent);

    boolean deleteById(Integer agentId);

    List<Agent> getall();
}