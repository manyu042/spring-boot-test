package com.abgame.test.springdatajpa.demo.service;

import com.abgame.test.springdatajpa.demo.entity.AccountBetRecord;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * 用户投注记录表(AccountBetRecord)表服务接口
 *
 * @author zry
 * @since 2021-01-16 16:35:44
 */
public interface AccountBetRecordService {
    AccountBetRecord queryById(Integer userBetRecordId);

    Page<AccountBetRecord> queryAllByLimit(int offset, int limit);

    AccountBetRecord insert(AccountBetRecord accountBetRecord);

    AccountBetRecord update(AccountBetRecord accountBetRecord);

    boolean deleteById(Integer userBetRecordId);

    List<AccountBetRecord> getall();
}