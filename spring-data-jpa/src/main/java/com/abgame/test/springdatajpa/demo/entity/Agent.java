package com.abgame.test.springdatajpa.demo.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * 代理商(Agent)实体类
 *
 * @author makejava
 * @since 2021-01-19 16:45:54
 */

@Data
@Entity
@JsonIgnoreProperties(value = {"hibernateLazyInitializer"})
@Table(name = "tb_agent", schema = "spring_boot_test")
public class Agent implements Serializable {

    private static final long serialVersionUID = 905656081143141226L;

    /**
     * 代理商ID
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "agent_id")
    private Integer agentId;

    /**
     * 代理商账号
     */
    @Basic
    @Column(name = "agent_account")
    private String agentAccount;

    /**
     * 代理商昵称
     */
    @Basic
    @Column(name = "agent_name")
    private String agentName;

    /**
     * 代理层级
     */
    @Basic
    @Column(name = "agent_level")
    private Integer agentLevel;

    /**
     * 上级代理ID
     */
    @Basic
    @Column(name = "parent_agent_id")
    private Integer parentAgentId;

    /**
     * 所有上代理的ID
     */
    @Basic
    @Column(name = "parent_agent_id_group")
    private String parentAgentIdGroup;

    /**
     * 创建时间
     */
    @Basic
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 更新时间
     */
    @Basic
    @Column(name = "update_time")
    private Date updateTime;

    /**
     * 状态：正常，正常；删除，删除；
     */
    @Basic
    @Column(name = "tb_status")
    private String tbStatus;

}