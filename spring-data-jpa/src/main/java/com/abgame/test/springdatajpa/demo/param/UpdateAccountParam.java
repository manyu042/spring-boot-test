package com.abgame.test.springdatajpa.demo.param;

import lombok.Data;

import java.io.Serializable;

@Data
public class UpdateAccountParam implements Serializable {

    private Integer accountId;

    private String accountName;

    private Integer agentId;
}
