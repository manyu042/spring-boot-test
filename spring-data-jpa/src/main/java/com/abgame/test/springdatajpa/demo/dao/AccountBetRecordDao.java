package com.abgame.test.springdatajpa.demo.dao;

import com.abgame.test.springdatajpa.demo.entity.AccountBetRecord;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * 用户投注记录表(AccountBetRecord)表数据库访问层
 *
 * @author zry
 * @since 2021-01-16 16:35:43
 */
public interface AccountBetRecordDao extends JpaRepository<AccountBetRecord, Integer> {


}