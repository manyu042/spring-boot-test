package com.abgame.test.springdatajpa.demo.controller;

import com.abgame.test.springdatajpa.demo.entity.AccountBetRecord;
import com.abgame.test.springdatajpa.demo.service.AccountBetRecordService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 用户投注记录表(AccountBetRecord)表控制层
 *
 * @author makejava
 * @since 2021-01-16 16:35:45
 */
@RestController
@RequestMapping("accountBetRecord")
public class AccountBetRecordController {
    /**
     * 服务对象
     */
    @Resource
    private AccountBetRecordService accountBetRecordService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public AccountBetRecord selectOne(Integer id) {
        return this.accountBetRecordService.queryById(id);
    }

}