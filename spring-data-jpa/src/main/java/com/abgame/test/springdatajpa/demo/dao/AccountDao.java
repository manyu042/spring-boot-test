package com.abgame.test.springdatajpa.demo.dao;

import com.abgame.test.springdatajpa.demo.entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 * 用户表(Account)表数据库访问层
 *
 * @author zry
 * @since 2021-01-16 16:35:51
 */
public interface AccountDao extends JpaRepository<Account, Integer> {

    @Modifying
    @Transactional
    @Query(" update Account  a set a.accountName= :#{#account.accountName}, a.agentId=:#{#account.agentId} where a.accountId=:#{#account.accountId}")
    int modifyWithHql(@Param("account") Account account);


    @Modifying
    @Transactional
    @Query( value = " update tb_account  a set a.account_name= :#{#account.accountName}, a.agent_id=:#{#account.agentId} where a.account_id=:#{#account.accountId}" ,nativeQuery = true)
    int modifyWithSQL(@Param("account") Account account);
}