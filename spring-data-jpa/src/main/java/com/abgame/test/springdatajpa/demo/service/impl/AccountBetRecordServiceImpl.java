package com.abgame.test.springdatajpa.demo.service.impl;

import com.abgame.test.springdatajpa.demo.dao.AccountBetRecordDao;
import com.abgame.test.springdatajpa.demo.entity.AccountBetRecord;
import com.abgame.test.springdatajpa.demo.service.AccountBetRecordService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 用户投注记录表(AccountBetRecord)表服务实现类
 *
 * @author makejava
 * @since 2021-01-16 16:35:44
 */
@Service("accountBetRecordService")
public class AccountBetRecordServiceImpl implements AccountBetRecordService {
    @Resource
    private AccountBetRecordDao accountBetRecordDao;

    @Override
    public AccountBetRecord queryById(Integer userBetRecordId) {
        return this.accountBetRecordDao.getOne(userBetRecordId);
    }

    @Override
    public List<AccountBetRecord> getall() {
        return this.accountBetRecordDao.findAll();

    }

    @Override
    public Page<AccountBetRecord> queryAllByLimit(int offset, int limit) {
        return this.accountBetRecordDao.findAll(PageRequest.of((offset - 1)
                * limit, limit));
    }

    @Override
    public AccountBetRecord insert(AccountBetRecord accountBetRecord) {

        return this.accountBetRecordDao.save(accountBetRecord);
    }


    @Override
    public AccountBetRecord update(AccountBetRecord accountBetRecord) {

        return this.accountBetRecordDao.save(accountBetRecord);
    }


    @Override
    public boolean deleteById(Integer userBetRecordId) {

        try {
            this.accountBetRecordDao.deleteById(userBetRecordId);
        } catch (Exception ex) {
            return false;
        }
        return true;

    }
}