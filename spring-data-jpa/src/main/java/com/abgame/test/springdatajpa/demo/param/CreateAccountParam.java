package com.abgame.test.springdatajpa.demo.param;

import lombok.Data;

import java.io.Serializable;

@Data
public class CreateAccountParam implements Serializable {

    private String accountName;

    private Integer agentId;
}
