# spring-boot-test-jpa

#### 介绍
使用插件Easy Code生成代码

#### Entity 模板

```
##引入宏定义
$!define

##使用宏定义设置回调（保存位置与文件后缀）
#save("/entity", ".java")

##使用宏定义设置包后缀
#setPackageSuffix("entity")

##使用全局变量实现默认包导入
$!autoImport
import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
 
##使用宏定义实现类注释信息
#tableComment("实体类")
 
@Data
@Entity
@JsonIgnoreProperties(value={"hibernateLazyInitializer"})
@Table(name = "$tableInfo.obj.name",schema="$tableInfo.obj.dasParent.name")
public class $!{tableInfo.name} implements Serializable {
 
    private static final long serialVersionUID = $!tool.serial();
##生成所有字段
#foreach($column in $tableInfo.fullColumn)
    ##如果表字段存在标注，即生成注释
    #if(${column.comment})
    
    /**
    * ${column.comment}
    */#end
    ##遍历包含的主键
    #foreach($pkColumn in $tableInfo.pkColumn)
        ##如果列名是主键那么生成主键id，否则生成basic字段注解
        #if($pkColumn.equals($column))
      
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name= "${column.obj.name}")
    private $!{tool.getClsNameByFullName($column.type)} $!{column.name};
        #else
        
    @Basic
    @Column(name= "${column.obj.name}")
    private $!{tool.getClsNameByFullName($column.type)} $!{column.name};
        #end
    #end
#end
 
}
```

#### Dao 模板

```
##定义初始变量
#set($tableName = $tool.append($tableInfo.name, "Dao"))
##设置回调
$!callback.setFileName($tool.append($tableName, ".java"))
$!callback.setSavePath($tool.append($tableInfo.savePath, "/dao"))

##拿到主键
#if(!$tableInfo.pkColumn.isEmpty())
    #set($pk = $tableInfo.pkColumn.get(0))
#end

#if($tableInfo.savePackageName)package $!{tableInfo.savePackageName}.#{end}dao;

import $!{tableInfo.savePackageName}.entity.$!{tableInfo.name};

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
/**
 * $!{tableInfo.comment}($!{tableInfo.name})表数据库访问层
 *
 * @author zry
 * @since $!time.currTime()
 */
public interface $!{tableName} extends JpaRepository<$!{tableInfo.name} ,$!pk.shortType>{

  
}


```

#### Service 模板

```
##定义初始变量
#set($tableName = $tool.append($tableInfo.name, "Service"))
##设置回调
$!callback.setFileName($tool.append($tableName, ".java"))
$!callback.setSavePath($tool.append($tableInfo.savePath, "/service"))

##拿到主键
#if(!$tableInfo.pkColumn.isEmpty())
    #set($pk = $tableInfo.pkColumn.get(0))
#end

#if($tableInfo.savePackageName)package $!{tableInfo.savePackageName}.#{end}service;

import $!{tableInfo.savePackageName}.entity.$!{tableInfo.name};
import java.util.List;
import org.springframework.data.domain.Page;
/**
 * $!{tableInfo.comment}($!{tableInfo.name})表服务接口
 *
 * @author zry
 * @since $!time.currTime()
 */
public interface $!{tableName} {
    $!{tableInfo.name} queryById($!pk.shortType $!pk.name);
    Page<$!{tableInfo.name}> queryAllByLimit(int offset, int limit);
    $!{tableInfo.name} insert($!{tableInfo.name} $!tool.firstLowerCase($!{tableInfo.name}));
    $!{tableInfo.name} update($!{tableInfo.name} $!tool.firstLowerCase($!{tableInfo.name}));
    boolean deleteById($!pk.shortType $!pk.name);
     List<$!{tableInfo.name}> getall();
}
```

#### ServiceImpl 模板

```
##定义初始变量
#set($tableName = $tool.append($tableInfo.name, "ServiceImpl"))
##设置回调
$!callback.setFileName($tool.append($tableName, ".java"))
$!callback.setSavePath($tool.append($tableInfo.savePath, "/service/impl"))

##拿到主键
#if(!$tableInfo.pkColumn.isEmpty())
    #set($pk = $tableInfo.pkColumn.get(0))
#end

#if($tableInfo.savePackageName)package $!{tableInfo.savePackageName}.#{end}service.impl;

import $!{tableInfo.savePackageName}.entity.$!{tableInfo.name};
import $!{tableInfo.savePackageName}.dao.$!{tableInfo.name}Dao;
import $!{tableInfo.savePackageName}.service.$!{tableInfo.name}Service;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import javax.annotation.Resource;
import java.util.List;
import org.springframework.data.domain.Page;
/**
 * $!{tableInfo.comment}($!{tableInfo.name})表服务实现类
 *
 * @author $!author
 * @since $!time.currTime()
 */
@Service("$!tool.firstLowerCase($!{tableInfo.name})Service")
public class $!{tableName} implements $!{tableInfo.name}Service {
    @Resource
    private $!{tableInfo.name}Dao $!tool.firstLowerCase($!{tableInfo.name})Dao;

    @Override
    public $!{tableInfo.name} queryById($!pk.shortType $!pk.name) {
        return this.$!{tool.firstLowerCase($!{tableInfo.name})}Dao.findById($!pk.name).orElse(null);
    }

    @Override
    public List<$!{tableInfo.name}> getall() {
        return this.$!{tool.firstLowerCase($!{tableInfo.name})}Dao.findAll();

    }
    
     @Override
    public Page<$!{tableInfo.name}> queryAllByLimit(int offset, int limit) {
        return this.$!{tool.firstLowerCase($!{tableInfo.name})}Dao.findAll(PageRequest.of((offset-1)
*limit,limit));
    }

    @Override
    public $!{tableInfo.name} insert($!{tableInfo.name} $!tool.firstLowerCase($!{tableInfo.name})) {
       
        return this.$!{tool.firstLowerCase($!{tableInfo.name})}Dao.save($!tool.firstLowerCase($!{tableInfo.name}));
    }


    @Override
    public $!{tableInfo.name} update($!{tableInfo.name} $!tool.firstLowerCase($!{tableInfo.name})) {
       
        return this.$!{tool.firstLowerCase($!{tableInfo.name})}Dao.save($!tool.firstLowerCase($!{tableInfo.name}));
    }

  
    @Override
    public boolean deleteById($!pk.shortType $!pk.name) {
    
     try{
             this.$!{tool.firstLowerCase($!{tableInfo.name})}Dao.deleteById($!pk.name) ;
        }catch (Exception ex){
            return false;
        }
        return true;
      
    }
}


```

#### Controller 模板

```
##定义初始变量
#set($tableName = $tool.append($tableInfo.name, "Controller"))
##设置回调
$!callback.setFileName($tool.append($tableName, ".java"))
$!callback.setSavePath($tool.append($tableInfo.savePath, "/controller"))
##拿到主键
#if(!$tableInfo.pkColumn.isEmpty())
    #set($pk = $tableInfo.pkColumn.get(0))
#end

#if($tableInfo.savePackageName)package $!{tableInfo.savePackageName}.#{end}controller;

import $!{tableInfo.savePackageName}.entity.$!{tableInfo.name};
import $!{tableInfo.savePackageName}.service.$!{tableInfo.name}Service;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * $!{tableInfo.comment}($!{tableInfo.name})表控制层
 *
 * @author $!author
 * @since $!time.currTime()
 */
@RestController
@RequestMapping("$!tool.firstLowerCase($tableInfo.name)")
public class $!{tableName} {
    /**
     * 服务对象
     */
    @Resource
    private $!{tableInfo.name}Service $!tool.firstLowerCase($tableInfo.name)Service;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public $!{tableInfo.name} selectOne($!pk.shortType id) {
        return this.$!{tool.firstLowerCase($tableInfo.name)}Service.queryById(id);
    }

}

```


#### 去掉表格前缀 模板
###### Global Config 的define文件中，在前面加上
```
##去掉表的t_前缀
#if($tableInfo.obj.name.startsWith("tb_"))
    $!tableInfo.setName($tool.getClassName($tableInfo.obj.name.substring(3)))
#end
```