package com.abcode.test.bilidanmu.websocket.entity.room_data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author BanqiJane
 * @ClassName CheckTx
 * @Description TODO
 * @date 2020年8月10日 下午12:23:03
 * @Copyright:2020 blogs.acproject.xyz Inc. All rights reserved.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CheckTx implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 6849509148214688485L;
    private Long room_id;
    private String gift_name;
    private Short time;


}
