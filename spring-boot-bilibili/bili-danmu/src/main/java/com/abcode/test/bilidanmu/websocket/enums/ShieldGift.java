package com.abcode.test.bilidanmu.websocket.enums;

/**
 * @author BanqiJane
 * @ClassName ShieldGift
 * @Description TODO
 * @date 2020年8月10日 下午12:28:11
 * @Copyright:2020 blogs.acproject.xyz Inc. All rights reserved.
 */
public enum ShieldGift {
    SILVER, OPTIONAL, HIGH_PRICE, CUSTOM_RULE
}
