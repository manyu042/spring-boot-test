package com.abcode.test.bilidanmu.websocket.conf.base;

import com.abcode.test.bilidanmu.websocket.component.ThreadComponent;

/**
 * @author Admin
 * @ClassName StartThreadInterface
 * @Description TODO
 * @date 2023/1/13 9:18
 * @Copyright:2023
 */
public interface StartThreadInterface {
    void start(ThreadComponent threadComponent);
}
