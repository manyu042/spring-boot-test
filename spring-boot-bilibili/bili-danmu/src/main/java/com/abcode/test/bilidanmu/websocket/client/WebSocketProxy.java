package com.abcode.test.bilidanmu.websocket.client;

import com.abcode.test.bilidanmu.websocket.entity.room_data.Room;
import lombok.extern.slf4j.Slf4j;

import java.net.URISyntaxException;

/**
 * websocket代理
 *
 * @author BanqiJane
 * @ClassName WebSocketProxy
 * @date 2020年8月10日 下午12:20:31
 * @Copyright:2020 blogs.acproject.xyz Inc. All rights reserved.
 */
@Slf4j
public class WebSocketProxy extends Websocket {
    public WebSocketProxy(String url, Room room) throws URISyntaxException, InterruptedException {
        super(url, room);
        log.info("Connectin(连接中)...........................................");
        super.connectBlocking();
        log.info("Connecting Success(连接成功)");
    }

    @Override
    public void onClose(int code, String reason, boolean remote) {

        super.onClose(code, reason, remote);

    }
}
