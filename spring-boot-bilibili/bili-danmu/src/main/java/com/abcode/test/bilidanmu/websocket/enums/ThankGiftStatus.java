package com.abcode.test.bilidanmu.websocket.enums;

/**
 * @author BanqiJane
 * @ClassName ThankGiftStatus
 * @Description TODO
 * @date 2020年8月10日 下午12:28:20
 * @Copyright:2020 blogs.acproject.xyz Inc. All rights reserved.
 */
public enum ThankGiftStatus {
    one_people, some_people, some_peoples
}
