package com.abcode.test.bilidanmu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class BiliDanmuApplication {

    public static void main(String[] args) {
        SpringApplication.run(BiliDanmuApplication.class, args);
    }

}
