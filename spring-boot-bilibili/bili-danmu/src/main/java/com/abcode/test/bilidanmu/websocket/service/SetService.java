package com.abcode.test.bilidanmu.websocket.service;

import com.abcode.test.bilidanmu.websocket.conf.CenterSetConf;

/**
 * @author BanqiJane
 * @ClassName SetService
 * @Description TODO
 * @date 2020年8月10日 下午12:29:33
 * @Copyright:2020 blogs.acproject.xyz Inc. All rights reserved.
 */
public interface SetService {
    void init();

    void changeSet(CenterSetConf centerSetConf);

    void changeSet(CenterSetConf centerSetConf, boolean check);

    void connectSet(CenterSetConf centerSetConf);

    void holdSet(CenterSetConf centerSetConf);

    void quit();
}
