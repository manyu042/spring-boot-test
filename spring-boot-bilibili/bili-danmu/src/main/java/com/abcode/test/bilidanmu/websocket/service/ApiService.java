package com.abcode.test.bilidanmu.websocket.service;


import com.abcode.test.bilidanmu.websocket.entity.Weather.Weather;
import com.abcode.test.bilidanmu.websocket.entity.Weather.WeatherV2;
import com.abcode.test.bilidanmu.websocket.entity.apex.ApexMessage;
import com.abcode.test.bilidanmu.websocket.entity.apex.PredatorResult;

public interface ApiService {

    Weather getWeather(String city, Short day);

    WeatherV2 getWeatherV2(String city, Short day);

    PredatorResult getApexPredator(String key, String type);

    ApexMessage getApexMessage();
}
