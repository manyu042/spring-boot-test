package com.abcode.test.bilidanmu.websocket.client;

import com.abcode.test.bilidanmu.websocket.conf.PublicDataConf;
import com.abcode.test.bilidanmu.websocket.entity.room_data.Room;
import com.abcode.test.bilidanmu.websocket.thread.core.ReConnThread;
import com.abcode.test.bilidanmu.websocket.ws.HandleWebsocketPackage;
import lombok.extern.slf4j.Slf4j;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.ByteBuffer;

/**
 * @author BanqiJane
 * @ClassName Websocket
 * @Description TODO
 * @date 2020年8月10日 下午12:20:25
 * @Copyright:2020 blogs.acproject.xyz Inc. All rights reserved.
 */
@Slf4j
public class Websocket extends WebSocketClient {

    public Websocket(String url, Room room) throws URISyntaxException {
        super(new URI(url));
        log.info("已尝试连接至服务器地址:" + url + ";真实房间号为:" + room.getRoomid() + ";主播名字为:" + room.getUname());
    }

    @Override
    public void onOpen(ServerHandshake handshakedata) {
        log.info("websocket connect open(连接窗口打开)");
    }

    @Override
    public void onMessage(ByteBuffer message) {

        if (PublicDataConf.parseMessageThread != null && !PublicDataConf.parseMessageThread.FLAG) {
            try {
                HandleWebsocketPackage.handleMessage(message);
            } catch (Exception e) {
                log.info("解析错误日志生成，请将log底下文件发给管理员,或github开issue发送错误" + e);
            }
//			synchronized (PublicDataConf.parseMessageThread) {
//				PublicDataConf.parseMessageThread.notify();
//			}
        }
    }

    @Override
    public void onClose(int code, String reason, boolean remote) {
        log.info("websocket connect close(连接已经断开)，纠错码:" + code);
        PublicDataConf.heartByteThread.HFLAG = true;
        PublicDataConf.parseMessageThread.FLAG = true;
        if (code != 1000) {
            log.info("websocket connect close(连接意外断开，正在尝试重连)，错误码:" + code);
            if (!PublicDataConf.webSocketProxy.isOpen()) {
                if (PublicDataConf.reConnThread != null) {
                    if (PublicDataConf.reConnThread.getState().toString().equals("TERMINATED")) {
                        PublicDataConf.reConnThread = new ReConnThread();
                        PublicDataConf.reConnThread.start();
                    } else {

                    }
                } else {
                    PublicDataConf.reConnThread = new ReConnThread();
                    PublicDataConf.reConnThread.start();
                }
            } else {
                PublicDataConf.reConnThread.RFLAG = true;
            }
        }
    }

    @Override
    public void onError(Exception ex) {

        log.error("[错误信息，请将log文件下的日志发送给管理员]websocket connect error,message:" + ex.getMessage());
        log.info("尝试重新链接");
        synchronized (PublicDataConf.webSocketProxy) {
            PublicDataConf.webSocketProxy.close(1006);
            if (!PublicDataConf.webSocketProxy.isOpen()) {
                if (PublicDataConf.reConnThread != null) {
                    if (PublicDataConf.reConnThread.getState().toString().equals("TERMINATED")) {
                        PublicDataConf.reConnThread = new ReConnThread();
                        PublicDataConf.reConnThread.start();
                    } else {

                    }
                } else {
                    PublicDataConf.reConnThread = new ReConnThread();
                    PublicDataConf.reConnThread.start();
                }
            } else {
                PublicDataConf.reConnThread.RFLAG = true;
            }
        }
    }

    @Override
    public void onMessage(String message) {


    }

}
