package com.abcode.rabbitmq.consumer;

import com.abcode.rabbitmq.config.RabbitConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

@Slf4j
@Component
@RabbitListener(queues = RabbitConfig.QUEUE_DIRECT_TEST)
public class DirectReceiver4 {

    @RabbitHandler
    public void process(String msg) {
        String message = new String(msg);
        log.info("|direct3|消息接收|成功|msg:{}", message);
        try {
            TimeUnit.SECONDS.sleep(4);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

}