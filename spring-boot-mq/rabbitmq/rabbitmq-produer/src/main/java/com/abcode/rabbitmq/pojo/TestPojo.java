package com.abcode.rabbitmq.pojo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * TODO
 *
 * @author qinzhitao
 * @date 2023/4/11 11:13
 */
@Data
public class TestPojo {

    private String name;

    private Integer age;

    private BigDecimal balance;
}
