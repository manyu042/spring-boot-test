package com.abcode.rabbitmq.service;

import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

@Component
public class ConfirmCallbackService implements RabbitTemplate.ConfirmCallback {
    /***
     * @param correlationData 相关配置信息
     *  @param ack exchange交换机 是否成功收到了消息。true 成功，false代表失败
     *  @param cause 失败原因
     * */
    @Override
    public void confirm(CorrelationData correlationData , boolean ack , String cause) {

        if (ack){
            //消息发送成功
            System.out.println ("消息发送成功到交换机");
        }else{
            System.out.println ("发送失败"+cause);
        }
    }
}