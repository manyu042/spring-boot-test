package com.abcode.rabbitmq.callback;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.ReturnedMessage;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class ConfirmMessageReturnCallback implements RabbitTemplate.ReturnsCallback {
    @Override
    public void returnedMessage(@NonNull ReturnedMessage message) {
        log.info("消息 {} 退回，退回原因{},交换机{},路由键{}", new String(message.getMessage().getBody()),
                 message.getReplyText(), message.getExchange(), message.getRoutingKey());
    }
}
