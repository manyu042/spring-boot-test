package com.abcode.rabbitmq.consumer;

import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;

/**
 * 消费者监听的队列不存在，则创建一个队列
 *
 * @author qinzhitao
 * @date 2023/5/18 13:55
 */
//@Component
//@RabbitListener(queuesToDeclare = @Queue("test_auto_queue"))
public class TestAuToConsumer{

    //@RabbitHandler
    public void receive(String msg){
        System.out.println(msg);
    }

    /**
     * 队列不存在时，需要创建一个队列，并且与exchange绑定
     */
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = "topic.n1", durable = "false", autoDelete = "true"),
            exchange = @Exchange(value = "topic.e", type = ExchangeTypes.TOPIC),
            key = "r"))
    public void consumerNoQueue(String data) {
        System.out.println("consumerNoQueue: " + data);
    }
}
