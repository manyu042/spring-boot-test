package com.abcode.rabbitmq.callback;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

/**
 * 消息发送到MQ确认
 *
 * @author qinzhitao
 * @date 2023/5/17 16:46
 */
@Slf4j
@Component
public class ConfirmMessageCallback implements RabbitTemplate.ConfirmCallback {
    /**
     * 消息发送调
     * @param correlationData 相关配置信息
     * @param result 发送结果
     * @param cause 失败原因
     */
    @Override
    public void confirm(CorrelationData correlationData, boolean result, String cause) {
        String id = correlationData != null ? correlationData.getId() : "";
        if(result){
            log.info("|confirm|消息发送|成功|id:{}", id);
        }else{
            log.info("|confirm|消息发送|失败|id:{}", id);
        }
    }
}
