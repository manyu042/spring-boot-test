package com.abcode.rabbitmq.controller;

import com.abcode.rabbitmq.pojo.TestPojo;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * demo
 *
 * @author qinzhitao
 * @date 2023/4/11 9:33
 */
@RestController
@RequestMapping("/demo")
public class DemoController {

    /**
     * 发送消息
     * @param msg
     * @return
     */
    @RequestMapping("/send")
    public String send(String msg){
        return "send msg success.";
    }

    @PostMapping("/add")
    public String add(@RequestBody TestPojo testPojo){
        return "add name:" + testPojo.getName();
    }
}
