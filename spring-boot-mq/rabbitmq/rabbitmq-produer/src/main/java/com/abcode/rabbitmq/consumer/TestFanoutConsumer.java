package com.abcode.rabbitmq.consumer;

import com.abcode.rabbitmq.config.RabbitConfig;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;

/**
 * 消费广播消息
 * @author qinzhitao
 * @date 2023/5/19 16:45
 */
//@Component
public class TestFanoutConsumer {

    //@RabbitListener(queues = {RabbitConfig.QUEUE_FANOUT_TEST_1})
    public void process1(byte[] msg) {
        String message = new String(msg);
        System.out.println("|QUEUE_FANOUT_TEST_1|收到消息  : " + message);
    }

    @RabbitListener(queues = {RabbitConfig.QUEUE_FANOUT_TEST_2})
    public void process2(byte[] msg) {
        String message = new String(msg);
        System.out.println("|QUEUE_FANOUT_TEST_2|收到消息  : " + message);
    }

    @RabbitListener(queues = {RabbitConfig.QUEUE_FANOUT_TEST_3})
    public void process3(byte[] msg) {
        String message = new String(msg);
        System.out.println("|QUEUE_FANOUT_TEST_3|收到消息  : " + message);
    }

    /**
     * 队列不存在时，需要创建一个队列，并且与exchange绑定
     */
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = "RabbitConfig.QUEUE_FANOUT_TEST_4", durable = "true", autoDelete = "false"),
            exchange = @Exchange(value = RabbitConfig.EXCHANGE_FANOUT_TEST, type = ExchangeTypes.FANOUT)
            ))
    public void process401(byte[] msg) {
        String message = new String(msg);
        System.out.println("|QUEUE_FANOUT_TEST_4_01|收到消息  : " + message);
    }

    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = "RabbitConfig.QUEUE_FANOUT_TEST_4", durable = "true", autoDelete = "false"),
            exchange = @Exchange(value = RabbitConfig.EXCHANGE_FANOUT_TEST, type = ExchangeTypes.FANOUT)
    ))
    public void process402(byte[] msg) {
        String message = new String(msg);
        System.out.println("|QUEUE_FANOUT_TEST_4_02|收到消息  : " + message);
    }

    /**
     * 如果需要一个消息被所有的实例消费，则配置自动创建队列
     * @param msg
     */
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(),
            exchange = @Exchange(value = RabbitConfig.EXCHANGE_FANOUT_TEST, type = ExchangeTypes.FANOUT)
    ))
    public void process111(byte[] msg) {
        String message = new String(msg);
        System.out.println("|process111|收到消息  : " + message);
    }

    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(),
            exchange = @Exchange(value = RabbitConfig.EXCHANGE_FANOUT_TEST, type = ExchangeTypes.FANOUT)
    ))
    public void process222(byte[] msg) {
        String message = new String(msg);
        System.out.println("|process222|收到消息  : " + message);
    }
}
