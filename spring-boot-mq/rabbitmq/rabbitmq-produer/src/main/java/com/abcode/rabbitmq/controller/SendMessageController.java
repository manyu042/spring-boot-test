package com.abcode.rabbitmq.controller;

import com.abcode.rabbitmq.config.RabbitConfig;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * @Author : JCccc
 * @CreateTime : 2019/9/3
 * @Description :
 **/
@RestController
@RequestMapping("/rabbit")
public class SendMessageController {
 
    @Autowired
    RabbitTemplate rabbitTemplate;  //使用RabbitTemplate,这提供了接收/发送等等方法
 
    @GetMapping("/sendDirectMessage")
    public String sendDirectMessage(String msg) {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            String messageId = String.valueOf(UUID.randomUUID());
            msg = i + 1 + "";
            String createTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
            Map<String, Object> map = new HashMap<>();
            map.put("messageId", messageId);
            map.put("messageData", msg);
            map.put("createTime", createTime);
            //将消息携带绑定键值：TestDirectRouting 发送到交换机TestDirectExchange
            rabbitTemplate.convertAndSend(RabbitConfig.EXCHANGE_DIRECT_TEST, RabbitConfig.ROUTING_DIRECT_TEST, msg);
            // 该对象将在回调时携带到回调方法中
            //CorrelationData correlationData = new CorrelationData(msg);
            //rabbitTemplate.send(RabbitConfig.EXCHANGE_DIRECT_TEST, RabbitConfig.ROUTING_DIRECT_TEST, new Message(map.toString().getBytes()), correlationData);
            list.add(messageId);
        }

        return list.toString();
    }

    /**
     * 测试消息回退接口
     * @param msg
     * @return
     */
    @GetMapping("/testReturnCallback")
    public String testReturnCallback(String msg) {
        String messageId = String.valueOf(UUID.randomUUID());
        if(!StringUtils.hasLength(msg)){
            msg = "test message, hello!";
        }
        String createTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        Map<String,Object> map=new HashMap<>();
        map.put("messageId",messageId);
        map.put("messageData",msg);
        map.put("createTime",createTime);

        CorrelationData correlationData = new CorrelationData(messageId);
        //发送到不存在的队列
        rabbitTemplate.send("TestDirectExchange", "[xxxxxx]", new Message(map.toString().getBytes()), correlationData);
        return "ok";
    }


    @GetMapping("/testAutoCreateQueue")
    public String testAutoCreateQueue(String msg) {
        String messageId = String.valueOf(UUID.randomUUID());
        if(!StringUtils.hasLength(msg)){
            msg = "test testAutoCreateQueue, hello!";
        }
        String createTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        Map<String,Object> map=new HashMap<>();
        map.put("messageId",messageId);
        map.put("messageData",msg);
        map.put("createTime",createTime);

        CorrelationData correlationData = new CorrelationData(messageId);

        rabbitTemplate.send("topic.n1", new Message(map.toString().getBytes()), correlationData);
        return "ok";
    }

    @GetMapping("/testFanoutMessage")
    public String testFanoutMessage(String msg) {
        String messageId = String.valueOf(UUID.randomUUID());
        if(!StringUtils.hasLength(msg)){
            msg = "test testFanoutMessage, hello!";
        }
        String createTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        Map<String,Object> map=new HashMap<>();
        map.put("messageId",messageId);
        map.put("messageData",msg);
        map.put("createTime",createTime);

        CorrelationData correlationData = new CorrelationData(messageId);

/*        MessagePostProcessor messagePostProcessor = new MessagePostProcessor() {
            @Override
            public Message postProcessMessage(Message message) throws AmqpException {
                message.getMessageProperties().setExpiration("3000");//消息过期时间
                return message;
            }
        };
        rabbitTemplate.convertAndSend(Exchange_Name,"boot.haha","boot mq hello",messagePostProcessor);*/

        //rabbitTemplate.convertAndSend(RabbitConfig.EXCHANGE_FANOUT_TEST, "", map);
        rabbitTemplate.send(RabbitConfig.EXCHANGE_FANOUT_TEST,"", new Message(map.toString().getBytes()), correlationData);
        return "ok";
    }

    @GetMapping("/testFanout")
    public String testFanout(String msg) {
        String messageId = String.valueOf(UUID.randomUUID());
        if(!StringUtils.hasLength(msg)){
            msg = "test testFanoutMessage, hello!";
        }
        String createTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        Map<String,Object> map=new HashMap<>();
        map.put("messageId",messageId);
        map.put("messageData",msg);
        map.put("createTime",createTime);

        rabbitTemplate.convertAndSend(RabbitConfig.EXCHANGE_FANOUT_TEST, "", new Message(map.toString().getBytes()));
        return "ok";
    }
}