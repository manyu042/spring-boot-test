package com.abcode.rabbitmq.consumer;

import com.abcode.rabbitmq.config.RabbitConfig;
import org.springframework.amqp.rabbit.annotation.RabbitListener;

//Component
public class RabbitMQConsumer {
    @RabbitListener(queues = RabbitConfig.QUEUE_FANOUT_COMMON)
    public void processMessage(String message) {
        System.out.println("Consumer1 received message: " + message);
    }
}