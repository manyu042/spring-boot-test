package com.abcode.rabbitmq.consumer.constant;

/**
 * TODO
 *
 * @author qinzhitao
 * @date 2023/5/19 16:30
 */
public class RabbitConstant {
    public static final String EXCHANGE_DIRECT_TEST = "TestDirectExchange";
    public static final String QUEUE_DIRECT_TEST = "TestDirectQueue";
    public static final String ROUTING_DIRECT_TEST = "TestDirectRouting";

    public static final String EXCHANGE_FANOUT_TEST = "TestFanoutExchange";
    public static final String QUEUE_FANOUT_TEST_1 = "TestFanoutQueue1";
    public static final String QUEUE_FANOUT_TEST_2 = "TestFanoutQueue2";
    public static final String QUEUE_FANOUT_TEST_3 = "TestFanoutQueue3";
    public static final String QUEUE_FANOUT_TEST_4 = "TestFanoutQueue4";
}
