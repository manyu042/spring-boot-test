package com.abcode.rabbitmq.consumer.listener;

import com.abcode.rabbitmq.consumer.constant.RabbitConstant;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 消费者测试
 *
 * @author qinzhitao
 * @date 2023/5/19 10:30
 */
@Component
@RabbitListener(queues = RabbitConstant.QUEUE_DIRECT_TEST)
public class TestConsumer {

    @RabbitHandler
    public void process(byte[] msg) {
        String message = new String(msg);
        System.out.println("|TestConsumer|消费者收到消息  : " + message);
    }
}
