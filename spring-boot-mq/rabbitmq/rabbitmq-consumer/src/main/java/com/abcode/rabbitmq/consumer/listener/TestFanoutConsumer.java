package com.abcode.rabbitmq.consumer.listener;

import com.abcode.rabbitmq.consumer.constant.RabbitConstant;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * 消费广播消息
 * @author qinzhitao
 * @date 2023/5/19 16:45
 */
@Component

public class TestFanoutConsumer {

    @RabbitListener(queues = {RabbitConstant.QUEUE_FANOUT_TEST_1})
    public void process1(byte[] msg) {
        String message = new String(msg);
        System.out.println("|QUEUE_FANOUT_TEST_1|收到消息  : " + message);
    }

    @RabbitListener(queues = {RabbitConstant.QUEUE_FANOUT_TEST_2})
    public void process2(byte[] msg) {
        String message = new String(msg);
        System.out.println("|QUEUE_FANOUT_TEST_2|收到消息  : " + message);
    }

    @RabbitListener(queues = {RabbitConstant.QUEUE_FANOUT_TEST_3})
    public void process3(byte[] msg) {
        String message = new String(msg);
        System.out.println("|QUEUE_FANOUT_TEST_3|收到消息  : " + message);
    }

    /**
     * 队列不存在时，需要创建一个队列，并且与exchange绑定
     */
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = RabbitConstant.QUEUE_FANOUT_TEST_4, durable = "true", autoDelete = "false"),
            exchange = @Exchange(value = RabbitConstant.EXCHANGE_FANOUT_TEST, type = ExchangeTypes.FANOUT)
    ))
    public void process4(byte[] msg) {
        String message = new String(msg);
        System.out.println("|QUEUE_FANOUT_TEST_4|收到消息  : " + message);
    }
}
