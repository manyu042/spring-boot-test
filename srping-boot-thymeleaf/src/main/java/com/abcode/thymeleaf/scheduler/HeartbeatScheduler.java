package com.abcode.thymeleaf.scheduler;

import com.abcode.thymeleaf.websocket.WebSocketHandler;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;


@Component
public class HeartbeatScheduler {

    private final WebSocketHandler myWebSocketHandler;

    public HeartbeatScheduler(WebSocketHandler myWebSocketHandler) {
        this.myWebSocketHandler = myWebSocketHandler;
    }

    @Scheduled(fixedRate = 10000)  // 每隔30秒发送一次心跳
    public void sendHeartbeat() {
        myWebSocketHandler.sendHeartbeat();
    }
}