package com.abcode.thymeleaf.controller;

import com.alibaba.fastjson.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class IndexController {
    // 直接用域名/IP或者后缀index.html都可以访问
    @GetMapping({"/", "index.html"})
    public String getIndexPage(Model model) {
        model.addAttribute("name", "abcode");
        JSONObject jo = new JSONObject();
        jo.put("name", "abcode-2");
        model.addAttribute("json", jo);
        // 数据可以放到Model中，不过这里的作用只是返回页面，内部数据由Vue处理
        // 返回字符串为页面文件名，Thymeleaf会自动去 classpath:/templates/中找
        return "index";
    }

    @GetMapping({"/websocket"})
    public String websocket(Model model) {
        return "websocket";
    }

    @PostMapping("/tree")
    // 这里ResponseBody一定要加上，否则返回到前端是没有值的
    @ResponseBody
    public JSONObject getTree() {
        JSONObject jo = new JSONObject();
        jo.put("code", 0);
        return jo;
    }

    @GetMapping({"/gameYg"})
    public String gameYg(Model model) {
        return "game-yg";
    }
}
