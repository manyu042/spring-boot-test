const config = {
    type: Phaser.AUTO,
    width: 800,
    height: 600,
    parent: 'game-container',
    scene: {
        preload: preload,
        create: create,
        update: update
    }
};

const game = new Phaser.Game(config);

let players = [];
let dayPhase = true;
let dayCount = 1;
let playerCount = 8;
let ghostCount = 2;
let selectedPlayer = null;
let phaseText;
let dayCountText;
let actionButton;
let resultText;

function preload() {
    this.load.image('background', 'https://labs.phaser.io/assets/skies/space3.png');
    this.load.image('player', 'https://labs.phaser.io/assets/sprites/phaser-dude.png');
}

function create() {
    this.add.image(400, 300, 'background');

    // 初始化玩家
    for (let i = 0; i < playerCount; i++) {
        const x = 100 + (i % 4) * 200;
        const y = 150 + Math.floor(i / 4) * 150;
        const player = this.add.sprite(x, y, 'player').setInteractive();
        player.isGhost = i < ghostCount;
        player.isAlive = true;
        player.on('pointerdown', () => selectPlayer(player));
        players.push(player);
    }

    phaseText = this.add.text(400, 50, '', { fontSize: '24px', fill: '#fff' }).setOrigin(0.5);
    dayCountText = this.add.text(400, 80, '', { fontSize: '18px', fill: '#fff' }).setOrigin(0.5);
    actionButton = this.add.text(400, 550, '', { fontSize: '18px', fill: '#fff', backgroundColor: '#000', padding: 10 })
        .setOrigin(0.5)
        .setInteractive()
        .on('pointerdown', performAction);

    resultText = this.add.text(400, 500, '', { fontSize: '18px', fill: '#fff' }).setOrigin(0.5);

    updatePhaseDisplay();
}

function update() {
    // 游戏循环逻辑（如果需要）
}

function selectPlayer(player) {
    if (!player.isAlive) return;
    players.forEach(p => p.setTint(0xffffff));
    player.setTint(0xff0000);
    selectedPlayer = player;
}

function performAction() {
    if (!selectedPlayer) {
        resultText.setText("请先选择一个玩家！");
        return;
    }

    if (dayPhase) {
        // 投票阶段
        if (selectedPlayer.isGhost) {
            resultText.setText("正确找出鬼！");
        } else {
            resultText.setText("投票错误，淘汰了粉丝！");
        }
        selectedPlayer.isAlive = false;
        selectedPlayer.setVisible(false);
    } else {
        // 夜晚阶段（鬼的行动）
        if (!selectedPlayer.isGhost) {
            resultText.setText("鬼袭击了一名粉丝！");
            selectedPlayer.isAlive = false;
            selectedPlayer.setVisible(false);
        } else {
            resultText.setText("鬼不能攻击其他鬼！");
            return;
        }
    }

    if (checkGameEnd()) return;

    dayPhase = !dayPhase;
    if (dayPhase) dayCount++;
    updatePhaseDisplay();
    selectedPlayer = null;
    players.forEach(p => p.setTint(0xffffff));
}

function updatePhaseDisplay() {
    phaseText.setText(dayPhase ? "白天阶段" : "夜晚阶段");
    dayCountText.setText(`第 ${dayCount} 天`);
    actionButton.setText(dayPhase ? "投票" : "夜间行动");
}

function checkGameEnd() {
    const aliveFans = players.filter(p => p.isAlive && !p.isGhost).length;
    const aliveGhosts = players.filter(p => p.isAlive && p.isGhost).length;

    if (aliveGhosts === 0) {
        endGame("粉丝胜利！");
        return true;
    } else if (aliveGhosts >= aliveFans) {
        endGame("鬼胜利！");
        return true;
    }
    return false;
}

function endGame(message) {
    resultText.setText(message);
    actionButton.setText("重新开始");
    actionButton.removeListener('pointerdown');
    actionButton.on('pointerdown', () => location.reload());
}