package cn.abcode.test.springbootsecurity.demo.auth;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.web.authentication.WebAuthenticationDetails;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class CustomWebAuthenticationDetails extends WebAuthenticationDetails {

    private boolean isPassed;

    public CustomWebAuthenticationDetails(HttpServletRequest request) {

        super(request);
        String contentType = request.getContentType();
        String parameterCode = null;
        String sessionCode = (String) request.getSession().getAttribute("verify_code");

       /* if(MediaType.APPLICATION_JSON_VALUE.equals(contentType)
                || MediaType.APPLICATION_PROBLEM_JSON_UTF8_VALUE.equals(contentType)
                || "text/plain;charset=UTF-8".equals(contentType)
        ){
            Map<String ,String> paramMap = new HashMap<>();
            try {
                paramMap = new ObjectMapper().readValue(request.getInputStream(),Map.class);
            } catch (IOException e) {
                e.printStackTrace();
            }finally {
                parameterCode = paramMap.get("verifyCode");
            }

        }else{
            parameterCode = request.getParameter("verifyCode");

        }*/

        if(parameterCode !=null && parameterCode.equals(sessionCode)){
            isPassed = true;
        }
        isPassed = true;
    }

    public boolean isPassed(){
        return isPassed;
    }
}
