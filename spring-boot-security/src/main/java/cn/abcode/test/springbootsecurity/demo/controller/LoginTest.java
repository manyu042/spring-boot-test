package cn.abcode.test.springbootsecurity.demo.controller;

import cn.abcode.test.springbootsecurity.demo.jpa.Role;
import cn.abcode.test.springbootsecurity.demo.jpa.User;
import cn.abcode.test.springbootsecurity.demo.jpa.UserDao;
import cn.abcode.test.springbootsecurity.demo.param.CreateUserParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/test")
public class LoginTest {

    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    UserDao userDao;

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public String create(@RequestBody CreateUserParam userParam){

        String password = userParam.getPassword();
        String encodePwd = passwordEncoder.encode(password);
        User user = new User();
        user.setUsername(userParam.getUsername());
        user.setPassword(encodePwd);
        user.setAccountNonExpired(true);
        user.setAccountNonLocked(true);
        user.setCredentialsNonExpired(true);
        user.setEnabled(true);
        List<Role> rs2 = new ArrayList<>();
        Role r2 = new Role();
        r2.setName("ROLE_user");
        r2.setNameZh("普通用户");
        rs2.add(r2);
        user.setRoles(rs2);
        userDao.save(user);
        System.out.println(userParam.toString());
        return user.toString();
    }

    @GetMapping("/hello")
    public String test(){
        return "hello security";
    }

    @RequestMapping("/index")
    public String index(){
        return "login success";
    }

    @RequestMapping("/fail")
    public String fail(){
        return "login fail";
    }

    @RequestMapping("/logout")
    public String logout(){
        return "logout";
    }

    @RequestMapping("/outSuccess")
    public String outSuccess(){
        return "logout outSuccess";
    }
}
