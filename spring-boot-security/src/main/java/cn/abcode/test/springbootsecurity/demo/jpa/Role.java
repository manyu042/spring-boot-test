package cn.abcode.test.springbootsecurity.demo.jpa;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "security_role")
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String nameZh;
}
