package cn.abcode.test.springbootsecurity.demo.auth;

import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * 实现除了判断用户名和密码外，还可以添加判断条件；如验证码
 */
public class AuthenticationCustomProvider extends DaoAuthenticationProvider {

    /**
     * 添加的判断条件
     * @param userDetails
     * @param authentication
     * @throws AuthenticationException
     */
    @Override
    protected void additionalAuthenticationChecks(UserDetails userDetails, UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
        CustomWebAuthenticationDetails webAuthenticationDetails = (CustomWebAuthenticationDetails) authentication.getDetails();
        if(!webAuthenticationDetails.isPassed()){
            throw new AuthenticationServiceException("验证码错误");
        }
        super.additionalAuthenticationChecks(userDetails, authentication);
    }
}
