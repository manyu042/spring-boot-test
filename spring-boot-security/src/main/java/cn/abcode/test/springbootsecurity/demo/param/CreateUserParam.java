package cn.abcode.test.springbootsecurity.demo.param;

import lombok.Data;

@Data
public class CreateUserParam {

    private String username;

    private String password;
}
