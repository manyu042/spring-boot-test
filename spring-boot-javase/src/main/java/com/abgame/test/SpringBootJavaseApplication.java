package com.abgame.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class SpringBootJavaseApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootJavaseApplication.class, args);
    }
}
