package com.abgame.test.websocket;

import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class WebSocketClientInitializer {

    private final WebsocketListener webSocketClient;

    public WebSocketClientInitializer(WebsocketListener webSocketClient) {
        this.webSocketClient = webSocketClient;
    }

    @PostConstruct
    public void init() {
        webSocketClient.connect();
    }
}
