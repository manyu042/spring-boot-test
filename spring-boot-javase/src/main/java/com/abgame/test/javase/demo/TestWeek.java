package com.abgame.test.javase.demo;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.time.temporal.IsoFields;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import static java.time.temporal.ChronoUnit.WEEKS;

public class TestWeek {
    public static void main(String[] args) {
        LocalDate startDate = LocalDate.of(2023, 7, 1);
        LocalDate endDate = LocalDate.of(2023, 7, 31);
         Map<String, List<LocalDate>> weekGroups = groupByWeek(startDate, endDate);
         for (Map.Entry<String, List<LocalDate>> entry : weekGroups.entrySet()) {
            String weekGroup = entry.getKey();
            List<LocalDate> dates = entry.getValue();
             System.out.println("Week Group: " + weekGroup);
            System.out.println("Start Date: " + dates.get(0));
            System.out.println("End Date: " + dates.get(dates.size() - 1));
            System.out.println();
        }
         List<LocalDate> randomDates = getRandomDates(startDate, endDate, 10);
         for (LocalDate date : randomDates) {
            String weekGroup = getWeekGroup(date, weekGroups);
             System.out.println("Date: " + date);
            System.out.println("Week Group: " + weekGroup);
            System.out.println();
        }
    }
     public static Map<String, List<LocalDate>> groupByWeek(LocalDate startDate, LocalDate endDate) {
        Map<String, List<LocalDate>> weekGroups = new HashMap<>();
        LocalDate currentDate = startDate;
         while (currentDate.isBefore(endDate) || currentDate.isEqual(endDate)) {
            String weekGroup = getWeekGroup(currentDate);
             if (!weekGroups.containsKey(weekGroup)) {
                weekGroups.put(weekGroup, new ArrayList<>());
            }
             weekGroups.get(weekGroup).add(currentDate);
             currentDate = currentDate.plus(1, ChronoUnit.DAYS);
        }
         return weekGroups;
    }
     public static String getWeekGroup(LocalDate date) {
         int weekNumber = date.get(IsoFields.WEEK_OF_WEEK_BASED_YEAR);

         return "Week " + weekNumber;
    }
     public static List<LocalDate> getRandomDates(LocalDate startDate, LocalDate endDate, int count) {
        List<LocalDate> randomDates = new ArrayList<>();
        Random random = new Random();
         while (randomDates.size() < count) {
            LocalDate randomDate = startDate.plusDays(random.nextInt((int) ChronoUnit.DAYS.between(startDate, endDate) + 1));
             if (!randomDates.contains(randomDate)) {
                randomDates.add(randomDate);
            }
        }
         return randomDates;
    }
     public static String getWeekGroup(LocalDate date, Map<String, List<LocalDate>> weekGroups) {
        for (Map.Entry<String, List<LocalDate>> entry : weekGroups.entrySet()) {
            List<LocalDate> dates = entry.getValue();
             if (dates.contains(date)) {
                return entry.getKey();
            }
        }
         return null;
    }
}