package com.abgame.test.javase.delayed;

import lombok.Data;

import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

/**
 * 任务
 * @author
 * @since 2021/3/18
 */
@Data
public class DelayTask implements Delayed {

    private Long orderId;
    /** 过期时间 */
    private long expireTime;

    /**
     *
     * @param orderId
     * @param  time 有效时间
     */
    public DelayTask(Long orderId, long time) {
        this.orderId = orderId;
        this.expireTime = System.currentTimeMillis() + time;
    }

    @Override
    public long getDelay(TimeUnit unit) {
        return expireTime - System.currentTimeMillis();
    }

    @Override
    public int compareTo(Delayed o) {
        return (int) (getDelay(TimeUnit.MILLISECONDS) - o.getDelay(TimeUnit.MILLISECONDS));
    }

}