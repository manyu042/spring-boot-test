package com.abgame.test.javase.tool.zip;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.zip.Deflater;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.Inflater;
import java.util.zip.InflaterInputStream;
 
/**
 * zlib 压缩算法
 * java 就是牛，原生支持
 * @author jx
 *
 */
public class ZlibUtil {
 
	
	/**  
     * 压缩  
     *   
     * @param data  
     *            待压缩数据  
     * @return byte[] 压缩后的数据  
     */  
    public static byte[] compress(byte[] data) {   
        byte[] output = new byte[0];   
  
        Deflater compresser = new Deflater();   
  
        compresser.reset();   
        compresser.setInput(data);   
        compresser.finish();   
        ByteArrayOutputStream bos = new ByteArrayOutputStream(data.length);   
        try {   
            byte[] buf = new byte[1024];   
            while (!compresser.finished()) {   
                int i = compresser.deflate(buf);   
                bos.write(buf, 0, i);   
            }   
            output = bos.toByteArray();   
        } catch (Exception e) {   
            output = data;   
            e.printStackTrace();   
        } finally {   
            try {   
                bos.close();   
            } catch (IOException e) {   
                e.printStackTrace();   
            }   
        }   
        compresser.end();   
        return output;   
    }   
  
    /**  
     * 压缩  
     *   
     * @param data  
     *            待压缩数据  
     *   
     * @param os  
     *            输出流  
     */  
    public static void compress(byte[] data, OutputStream os) {   
        DeflaterOutputStream dos = new DeflaterOutputStream(os);   
  
        try {   
            dos.write(data, 0, data.length);   
  
            dos.finish();   
  
            dos.flush();   
        } catch (IOException e) {   
            e.printStackTrace();   
        }   
    }   
  
    /**  
     * 解压缩  
     *   
     * @param data  
     *            待压缩的数据  
     * @return byte[] 解压缩后的数据  
     */  
    public static byte[] decompress(byte[] data) {   
        byte[] output = new byte[0];   
  
        Inflater decompresser = new Inflater();   
        decompresser.reset();   
        decompresser.setInput(data);   
  
        ByteArrayOutputStream o = new ByteArrayOutputStream(data.length);   
        try {   
            byte[] buf = new byte[1024];   
            while (!decompresser.finished()) {   
                int i = decompresser.inflate(buf);   
                o.write(buf, 0, i);   
            }   
            output = o.toByteArray();   
        } catch (Exception e) {   
            output = data;   
            e.printStackTrace();   
        } finally {   
            try {   
                o.close();   
            } catch (IOException e) {   
                e.printStackTrace();   
            }   
        }   
  
        decompresser.end();   
        return output;   
    }   
  
    /**  
     * 解压缩  
     *   
     * @param is  
     *            输入流  
     * @return byte[] 解压缩后的数据  
     */  
    public static byte[] decompress(InputStream is) {   
        InflaterInputStream iis = new InflaterInputStream(is);   
        ByteArrayOutputStream o = new ByteArrayOutputStream(1024);   
        try {   
            int i = 1024;   
            byte[] buf = new byte[i];   
  
            while ((i = iis.read(buf, 0, i)) > 0) {   
                o.write(buf, 0, i);   
            }   
  
        } catch (IOException e) {   
            e.printStackTrace();   
        }   
        return o.toByteArray();   
    }   
    
    public static void main(String[] args) throws UnsupportedEncodingException {
    	String data = "aadfklafdafla我是中国人的啦啦啦";
    	System.out.println("原文："+data);
    	
    	// 报错
//    	String a = Base64Util.encode(ZlibUtil.compress(data.getBytes()));
//    	
//    	String b = new String(ZlibUtil.decompress(Base64Util.decode(a)));
    	
    	byte[] a = ZlibUtil.compress(data.getBytes("UTF-8"));
    	System.out.println(">>"+Arrays.toString(a));
    	
    	byte[] b = ZlibUtil.decompress(a);
    	
    	
    	
//    	System.out.println("压缩后："+a);
    	System.out.println("jieya后："+new String(b));
	}
    
    void a(){
//    	 try { 
//    		 // Encode a String into bytes 
//    		 String inputString = "Pehla nasha Pehla khumaar Naya pyaar hai naya intezaar Kar loon main kya apna haal Aye dil-e-bekaraar Mere dil-e-bekaraar Tu hi bata Pehla nasha Pehla khumaar Udta hi firoon in hawaon mein kahin Ya main jhool jaoon in ghataon mein kahin Udta hi firoon in hawaon mein kahin Ya main jhool jaoon in ghataon mein kahin Ek kar doon aasmaan zameen Kaho yaaron kya karoon kya nahin Pehla nasha Pehla khumaar Naya pyaar hai naya intezaar Kar loon main kya apna haal Aye dil-e-bekaraar Mere dil-e-bekaraar Tu hi bata Pehla nasha Pehla khumaar Usne baat ki kuchh aise dhang se Sapne de gaya vo hazaaron range ke Usne baat ki kuchh aise dhang se Sapne de gaya vo hazaaron range ke Reh jaoon jaise main haar ke Aur choome vo mujhe pyaar se Pehla nasha Pehla khumaar Naya pyaar hai naya intezaar Kar loon main kya apna haal Aye dil-e-bekaraar Mere dil-e-bekaraar"; 
//    	 byte[] input = inputString.getBytes("UTF-8"); 
//    	 // Compress the bytes 
//    	 byte[] output1 = new byte[input.length]; 
//    	 Deflater compresser = new Deflater();
//    	 compresser.setInput(input); 
//    	 compresser.finish();
//    	 int compressedDataLength = compresser.deflate(output1); 
//    	 compresser.end();
//    	 String str = Base64.encode(output1); 
//    	 System.out.println("Deflated String:" + str); 
//    	 byte[] output2 = Base64.decode(str);
//    	 // Decompress the bytes 
//    	 Inflater decompresser = new Inflater(); 
//    	 decompresser.setInput(output2); 
//    	 byte[] result = str.getBytes(); 
//    	 int resultLength = decompresser.inflate(result); 
//    	 decompresser.end(); 
//    	 // Decode the bytes into a String 
//    	 String outputString = new String(result, 0, resultLength, "UTF-8"); 
//    	 System.out.println("Deflated String:" + outputString); 
//    	 } catch (UnsupportedEncodingException e) { 
//    		 // TODO Auto-generated catch block 
//    		 e.printStackTrace(); 
//    	} catch (DataFormatException e) {
//    	 // TODO Auto-generated catch block
//    			e.printStackTrace();
//    	} 
//    		
    }
}