package com.abgame.test.javase.demo;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.time.format.DateTimeFormatter;
import java.util.*;
 public class WeekGrouping {
    public static void main(String[] args) {
        LocalDate startDate = LocalDate.of(2022, 1, 1);
        LocalDate endDate = LocalDate.of(2022, 1, 31);
         Map<Integer, List<LocalDate>> weekGroups = groupDatesByWeek(startDate, endDate);
        for (Map.Entry<Integer, List<LocalDate>> entry : weekGroups.entrySet()) {
            int weekNumber = entry.getKey();
            List<LocalDate> weekDates = entry.getValue();
            LocalDate weekStartDate = weekDates.get(0);
            LocalDate weekEndDate = weekDates.get(weekDates.size() - 1);
             System.out.println("Week " + weekNumber + " (" + weekStartDate + " - " + weekEndDate + "):");
            System.out.println("Grouping Information: " + weekDates);
            System.out.println();
        }
         List<LocalDate> randomDates = getRandomDates(startDate, endDate, 10);
        System.out.println("Random Dates: " + randomDates);
        System.out.println();
         Map<Integer, List<LocalDate>> groupedRandomDates = groupDatesByWeek(startDate, endDate, randomDates);
        for (Map.Entry<Integer, List<LocalDate>> entry : groupedRandomDates.entrySet()) {
            int weekNumber = entry.getKey();
            List<LocalDate> weekDates = entry.getValue();
            LocalDate weekStartDate = weekDates.get(0);
            LocalDate weekEndDate = weekDates.get(weekDates.size() - 1);
             System.out.println("Week " + weekNumber + " (" + weekStartDate + " - " + weekEndDate + "):");
            System.out.println("Grouped Random Dates: " + weekDates);
            System.out.println();
        }
    }
     public static Map<Integer, List<LocalDate>> groupDatesByWeek(LocalDate startDate, LocalDate endDate) {
        Map<Integer, List<LocalDate>> weekGroups = new HashMap<>();
        LocalDate currentDate = startDate;
         int weekNumber = 1;
        while (!currentDate.isAfter(endDate)) {
            LocalDate weekStartDate = currentDate.with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY));
            LocalDate weekEndDate = currentDate.with(TemporalAdjusters.nextOrSame(DayOfWeek.SUNDAY));
             weekGroups.computeIfAbsent(weekNumber, k -> new ArrayList<>()).add(weekStartDate);
            weekGroups.computeIfAbsent(weekNumber, k -> new ArrayList<>()).add(weekEndDate);
             currentDate = currentDate.plusDays(7);
            weekNumber++;
        }
         return weekGroups;
    }
     public static List<LocalDate> getRandomDates(LocalDate startDate, LocalDate endDate, int count) {
        List<LocalDate> dates = new ArrayList<>();
        LocalDate currentDate = startDate;
         while (!currentDate.isAfter(endDate)) {
            dates.add(currentDate);
            currentDate = currentDate.plusDays(1);
        }
         Collections.shuffle(dates);
        return dates.subList(0, count);
    }
     public static Map<Integer, List<LocalDate>> groupDatesByWeek(LocalDate startDate, LocalDate endDate, List<LocalDate> dates) {
        Map<Integer, List<LocalDate>> weekGroups = groupDatesByWeek(startDate, endDate);
         for (LocalDate date : dates) {
            int weekNumber = getWeekNumber(startDate, date);
            weekGroups.computeIfAbsent(weekNumber, k -> new ArrayList<>()).add(date);
        }
         return weekGroups;
    }
     public static int getWeekNumber(LocalDate startDate, LocalDate date) {
        return (int) ((date.toEpochDay() - startDate.toEpochDay()) / 7) + 1;
    }
}