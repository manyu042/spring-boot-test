package com.abgame.test.javase.dependency.pojo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author abgame
 * @date 2020/8/25 10:42
 */
@Component
public class DependencyA {
    @Autowired
    private DependencyB dependencyB;


}
