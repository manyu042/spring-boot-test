package com.abgame.test.javase.demo;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
 public class DayOfWeekCalculator {
    public static void main(String[] args) {
        LocalDate startDate = LocalDate.of(2022, 1, 1);
        LocalDate endDate = LocalDate.of(2022, 1, 10);
         calculateDayOfWeek(startDate, endDate);
    }
     public static void calculateDayOfWeek(LocalDate startDate, LocalDate endDate) {
        LocalDate currentDate = startDate;
         while (!currentDate.isAfter(endDate)) {
            DayOfWeek dayOfWeek = currentDate.getDayOfWeek();
            String formattedDate = currentDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
            String dayOfWeekString = dayOfWeek.toString();
             System.out.println(formattedDate + " is " + dayOfWeekString);
             currentDate = currentDate.plusDays(1);
        }
    }
}