package com.abgame.test.javase.delayed;

import org.springframework.stereotype.Component;

/**
 * @author qzt
 * @Description TODO
 * @createTime 2022/3/1 11:11
 */
@Component
public class ParkOrderQueryHandler {
    public void handle(DelayTask task) {
        System.out.println("---业务处理---"  + task.getOrderId() );
    }
}
