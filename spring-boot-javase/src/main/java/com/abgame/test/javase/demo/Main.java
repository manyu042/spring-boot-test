package com.abgame.test.javase.demo;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.time.temporal.IsoFields;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
 public class Main {
    public static void main(String[] args) {
        LocalDate startDate = LocalDate.of(2023, 6, 8);
        LocalDate endDate = LocalDate.of(2023, 8, 11);
         Map<Integer, List<LocalDate>> weekGroups = groupByNaturalWeek(startDate, endDate);
         for (Map.Entry<Integer, List<LocalDate>> entry : weekGroups.entrySet()) {
            int weekNumber = entry.getKey();
            List<LocalDate> dates = entry.getValue();
             LocalDate weekStartDate = dates.get(0);
            LocalDate weekEndDate = dates.get(dates.size() - 1);
             System.out.println("Week Group: " + weekNumber);
            System.out.println("Start Date: " + weekStartDate);
            System.out.println("End Date: " + weekEndDate);
            System.out.println();
        }
    }
     public static Map<Integer, List<LocalDate>> groupByNaturalWeek(LocalDate startDate, LocalDate endDate) {
        Map<Integer, List<LocalDate>> weekGroups = new HashMap<>();
        LocalDate currentDate = startDate;
         while (currentDate.isBefore(endDate) || currentDate.isEqual(endDate)) {
            int weekNumber = currentDate.get(IsoFields.WEEK_OF_WEEK_BASED_YEAR);
             if (!weekGroups.containsKey(weekNumber)) {
                weekGroups.put(weekNumber, new ArrayList<>());
            }
             weekGroups.get(weekNumber).add(currentDate);
            currentDate = currentDate.plus(1, ChronoUnit.DAYS);
        }
         return weekGroups;
    }
}