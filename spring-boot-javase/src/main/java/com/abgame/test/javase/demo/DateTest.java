package com.abgame.test.javase.demo;

import lombok.Data;
import lombok.ToString;
import org.springframework.format.datetime.joda.LocalDateTimeParser;

import java.time.*;
import java.time.temporal.WeekFields;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

public class DateTest {
    public static void main(String[] args) {
        LocalDate startDate = LocalDate.of(2023, 6, 1);
        LocalDate endDate = LocalDate.of(2023, 12, 31);
        LocalDate tempDate = LocalDate.of(2023, 7, 8);
        System.out.println(getSundayOfGivenWeek(tempDate));
        LocalDate tempDate1 = LocalDate.of(2023, 7, 2);
        System.out.println(getSundayOfGivenWeek(tempDate1));
        LocalDate tempDate2 = LocalDate.of(2023, 7, 3);
        System.out.println(getSundayOfGivenWeek(tempDate2));
        LocalDate tempDate3 = LocalDate.of(2023, 7, 9);
        System.out.println(getSundayOfGivenWeek(tempDate3));
//        int numberOfDates = 5;
//         List<LocalDate> randomDates = getRandomDates(startDate, endDate, numberOfDates);
//        randomDates = randomDates.stream().sorted().collect(Collectors.toList());
//        System.out.println("Random Dates:");
//        for (LocalDate date : randomDates) {
//            LocalDate mondayDate = getMondayDate(date);
//            System.out.println("日期："+date);
//            System.out.println("周一："+mondayDate);
//            printNaturalMonthAndWeek(date);
//        }
//        System.out.println("-----------------");
//        getCurrentWeekTimeFrame();
    }
     public static List<LocalDate> getRandomDates(LocalDate startDate, LocalDate endDate, int numberOfDates) {
        List<LocalDate> randomDates = new ArrayList<>();
        long startEpochDay = startDate.toEpochDay();
        long endEpochDay = endDate.toEpochDay();
         for (int i = 0; i < numberOfDates; i++) {
            long randomEpochDay = ThreadLocalRandom.current().nextLong(startEpochDay, endEpochDay + 1);
            randomDates.add(LocalDate.ofEpochDay(randomEpochDay));
        }
         return randomDates;
    }

    private List<WeekInfo> divideSchedulePlanFltBySelectDate(List<LocalDate> selectDateList) {
        // 去重并排序
        selectDateList = selectDateList.stream().distinct().sorted().collect(Collectors.toList());
        int selectSize = 0;
        // 最早的日期
        LocalDate firstDate = selectDateList.get(0);
        // 最晚的日期
        LocalDate lastDate = selectDateList.get(selectSize-1);
        // 第一周的星期一日期
        LocalDate firstWeekMonday = getMondayOfGivenWeek(firstDate);
        // 最后一周的星期天日期
        LocalDate lastWeekSunday = getSundayOfGivenWeek(lastDate);

        List<WeekInfo> naturalWeekList = new ArrayList<>();
        LocalDate compareDate = firstWeekMonday;
        // 拿到每一个自然周的基本信息
        WeekInfo weekInfo = null;
        int weekDayCnt =  7;
        int dayCount = 0;
        while (!compareDate.isAfter(lastWeekSunday)){
            dayCount ++;
            // 7天为一周
            if(dayCount == weekDayCnt){
                weekInfo = new WeekInfo();
                weekInfo.setWeekNum(naturalWeekList.size() +1);
                weekInfo.setMondayDate(getMondayOfGivenWeek(compareDate));
                weekInfo.setSundayDate(getSundayOfGivenWeek(compareDate));

                // 选出属于本周期的日期
                WeekInfo finalWeekInfo = weekInfo;
                List<LocalDate> includeDateList = selectDateList.stream().filter(item-> finalWeekInfo.getMondayDate().equals(getMondayOfGivenWeek(item))).collect(Collectors.toList());
                weekInfo.setSelectDateList(includeDateList);
                // 格式化字符串赋值
                weekInfo.formatWeek();

                //删除已划分的日期
                selectDateList.remove(includeDateList);

                // 重置天数
                dayCount = 0;
            }
            // 递增1天
            compareDate.plusDays(1);
        }

        return null;
    }



    public static void printNaturalMonthAndWeek(LocalDate date) {
        YearMonth yearMonth = YearMonth.from(date);
        System.out.println("Natural Year: " + yearMonth.getYear());
        System.out.println("Natural Month: " + yearMonth.getMonth().getValue());
        WeekFields weekFields = WeekFields.of(Locale.getDefault());
        int weekNumber = date.get(weekFields.weekOfWeekBasedYear());
        int weekDay = date.get(weekFields.dayOfWeek());
        System.out.println("Natural Week: " + weekNumber);
        System.out.println("Natural Week dy: " + weekDay);
    }

    /**
     * 周一做为每周的第一天
     * @return
     */
    public static long[] getCurrentWeekTimeFrame() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(TimeZone.getTimeZone("GMT+8"));
        //start of the week
        if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
            calendar.add(Calendar.DAY_OF_YEAR,-1);
        }
        calendar.add(Calendar.DAY_OF_WEEK, -(calendar.get(Calendar.DAY_OF_WEEK) - 2));
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        long startTime = calendar.getTimeInMillis();
        System.out.println("周一日期：" + new Date(startTime));
        //end of the week
        calendar.add(Calendar.DAY_OF_WEEK, 6);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
        long endTime = calendar.getTimeInMillis();
        System.out.println("周日日期：" + new Date(endTime));
        return new long[]{startTime, endTime};
    }

    /**
     * 求日期所属周的周一的日期
     * @param date
     */
    public static LocalDate getMondayDate(LocalDate date) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(date.getYear(), date.getMonthValue(), date.getDayOfMonth());
        calendar.setTimeZone(TimeZone.getTimeZone("GMT+8"));
        //start of the week
        if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
            calendar.add(Calendar.DAY_OF_YEAR,-1);
        }
        calendar.add(Calendar.DAY_OF_WEEK, -(calendar.get(Calendar.DAY_OF_WEEK) - 2));
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        LocalDate localDate = calendar.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        return localDate;
    }

    /**
     * 获得指定日期所在周的星期一的日期
     * @param date 指定的日期
     * @return
     */
    public static LocalDate getMondayOfGivenWeek(LocalDate date) {
        DayOfWeek currentDayOfWeek = date.getDayOfWeek();
        int daysToAdd = currentDayOfWeek.getValue() - DayOfWeek.MONDAY.getValue();
        return date.minusDays(daysToAdd);
    }

    /**
     * 获得指定日期所在周的星期天的日期
     * @param date 指定的日期
     * @return
     */
    public static LocalDate getSundayOfGivenWeek(LocalDate date) {
        DayOfWeek currentDayOfWeek = date.getDayOfWeek();
        int daysToAdd = currentDayOfWeek.getValue() - DayOfWeek.SUNDAY.getValue();
        return date.minusDays(daysToAdd);
    }


    public void processDate(List<LocalDate> selectDateList){

        //排序
        selectDateList = selectDateList.stream().sorted().collect(Collectors.toList());

        LocalDate firstDate = selectDateList.get(0);
        // 求第一个日期所在周的基本信息
        WeekInfo weekInfo = getWeekInfo(firstDate);

    }

    /**
     * 求日期所在周的基本信息
     * @param localDate
     * @return
     */
    public WeekInfo getWeekInfo(LocalDate localDate){

        YearMonth yearMonth = YearMonth.from(localDate);
        int yearNum = yearMonth.getYear();
        int monthNum = yearMonth.getMonth().getValue();

        WeekFields weekFields = WeekFields.of(Locale.getDefault());
        int weekNum = localDate.get(weekFields.weekOfWeekBasedYear());
        int weekDay = localDate.get(weekFields.dayOfWeek());

        WeekInfo weekInfo = new WeekInfo();

        weekInfo.setYearNum(yearNum);
        weekInfo.setMonthNum(monthNum);
        weekInfo.setWeekNum(weekNum);
     ///   weekInfo.setMondayDate();
        int disDay = weekDay - (weekDay-2);
       // weekInfo.setSundayDate(weekDay-);

        return null;

    }

    /**
     * 日期在年份中的基本信息
     */

    @ToString
    @Data
    class DateInfo {
        /** 日期 */
        private LocalDate localDate;

        /** 年 */
        private int yearNum;

        /** 第几月 */
        private int monthNum;

        /** 第几周 */
        private int weekNum;

        /** 周几 */
        private int weekDay;

        /** 周一的日期 */
        private LocalDate mondayDate;

        /** 周日的日期 */
        private LocalDate sundayDate;

    }

    /**
     * 自然周信息
     */
    @ToString
    @Data
    class WeekInfo {

        /** 年 */
        private int yearNum;

        /** 第几月 */
        private int monthNum;

        /** 第几周 */
        private int weekNum;

        /** 周一的日期 */
        private LocalDate mondayDate;

        /** 周日的日期 */
        private LocalDate sundayDate;

        /** 选中的日期 */
        private List<LocalDate> selectDateList;

        /** 选中周期显示格式(12,,5,7)  */
        private String formatStr;

        private boolean [] weekDaySelectFlagArr = new boolean[7];

        public void formatWeek() {
        }
    }

}