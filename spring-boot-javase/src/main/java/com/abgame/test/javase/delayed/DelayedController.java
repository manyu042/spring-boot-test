package com.abgame.test.javase.delayed;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author qzt
 * @Description TODO
 * @createTime 2022/3/1 11:14
 */
@RestController
@RequestMapping("/delayed")
public class DelayedController {
    @Autowired
    DelayQueueManager delayQueueManager;

    private volatile long count = 0l;

    @RequestMapping("/test")
    public String test(){
        count++;
        long now = System.currentTimeMillis() + 5000;
        delayQueueManager.put(new DelayTask(count,now));
        return "run success";
    }
}
