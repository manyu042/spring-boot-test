package com.abgame.test.javase.dependency;

import com.abgame.test.javase.dependency.pojo.DependencyA;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * @author abgame
 * @date 2020/8/25 10:46
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class PojoTest {

    @Autowired
    private DependencyA dependencyA;

    @Test
    public void createTest(){
        System.out.println(dependencyA);
    }


    @Test
    public void dateTest() {
        LocalDate startDate = LocalDate.of(2022, 1, 1);
        LocalDate endDate = LocalDate.of(2022, 1, 10);
        calculateDayOfWeek(startDate, endDate);
    }
    public  void calculateDayOfWeek(LocalDate startDate, LocalDate endDate) {
        LocalDate currentDate = startDate;
        while (!currentDate.isAfter(endDate)) {
            DayOfWeek dayOfWeek = currentDate.getDayOfWeek();
            String formattedDate = currentDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
            String dayOfWeekString = dayOfWeek.toString();
            System.out.println(formattedDate + " is " + dayOfWeekString);
            currentDate = currentDate.plusDays(1);
        }
    }
}
