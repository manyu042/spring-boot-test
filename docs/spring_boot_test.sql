/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50725
Source Host           : localhost:3306
Source Database       : spring_boot_test

Target Server Type    : MYSQL
Target Server Version : 50725
File Encoding         : 65001

Date: 2021-02-07 15:05:55
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for security_role
-- ----------------------------
DROP TABLE IF EXISTS `security_role`;
CREATE TABLE `security_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `name_zh` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of security_role
-- ----------------------------
INSERT INTO `security_role` VALUES ('1', 'ROLE_admin', '管理员');
INSERT INTO `security_role` VALUES ('2', 'ROLE_user', '普通用户');
INSERT INTO `security_role` VALUES ('3', 'ROLE_user', '普通用户');
INSERT INTO `security_role` VALUES ('4', 'ROLE_user', '普通用户');

-- ----------------------------
-- Table structure for security_user
-- ----------------------------
DROP TABLE IF EXISTS `security_user`;
CREATE TABLE `security_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `account_non_expired` bit(1) NOT NULL,
  `account_non_locked` bit(1) NOT NULL,
  `credentials_non_expired` bit(1) NOT NULL,
  `enabled` bit(1) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of security_user
-- ----------------------------
INSERT INTO `security_user` VALUES ('1', '', '', '', '', '123', 'guo');
INSERT INTO `security_user` VALUES ('2', '', '', '', '', '123', 'zhou');
INSERT INTO `security_user` VALUES ('3', '', '', '', '', '$2a$10$W59MWCrpvY/Og4uQzrkC2ee4d3H7IwpkQOPtH8KgnA9y2ED8Nz1BK', 'qin');
INSERT INTO `security_user` VALUES ('5', '', '', '', '', '$2a$10$LuEcb0gs3a3qrmm1MxdTAe7b31B.5EjZy9Apq5fLmkaZXBv3I3py2', 'qin1');

-- ----------------------------
-- Table structure for security_user_roles
-- ----------------------------
DROP TABLE IF EXISTS `security_user_roles`;
CREATE TABLE `security_user_roles` (
  `security_user_id` bigint(20) NOT NULL,
  `roles_id` bigint(20) NOT NULL,
  KEY `FKpeipgir7x1pwerspyimiq6pef` (`roles_id`),
  KEY `FKf70bjlely9vr1dfklgisjm6jj` (`security_user_id`),
  CONSTRAINT `FKf70bjlely9vr1dfklgisjm6jj` FOREIGN KEY (`security_user_id`) REFERENCES `security_user` (`id`),
  CONSTRAINT `FKpeipgir7x1pwerspyimiq6pef` FOREIGN KEY (`roles_id`) REFERENCES `security_role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of security_user_roles
-- ----------------------------
INSERT INTO `security_user_roles` VALUES ('1', '1');
INSERT INTO `security_user_roles` VALUES ('2', '2');
INSERT INTO `security_user_roles` VALUES ('3', '3');
INSERT INTO `security_user_roles` VALUES ('5', '4');

-- ----------------------------
-- Table structure for tb_account
-- ----------------------------
DROP TABLE IF EXISTS `tb_account`;
CREATE TABLE `tb_account` (
  `account_id` int(11) NOT NULL AUTO_INCREMENT,
  `account` varchar(32) NOT NULL COMMENT '账号',
  `account_name` varchar(32) NOT NULL COMMENT '昵称',
  `agent_id` int(11) NOT NULL DEFAULT '0' COMMENT '代理商ID',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `tb_status` char(20) NOT NULL DEFAULT '正常' COMMENT '状态：正常，正常；删除，删除；',
  PRIMARY KEY (`account_id`),
  UNIQUE KEY `account` (`account`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=utf8mb4 COMMENT='用户表';

-- ----------------------------
-- Records of tb_account
-- ----------------------------
INSERT INTO `tb_account` VALUES ('5', 'jack999', 'jack999', '4', '2021-01-14 15:38:11', '2021-01-16 15:25:01', '正常');
INSERT INTO `tb_account` VALUES ('6', 'guo123', 'guo123', '1', '2021-01-14 18:14:47', '2021-01-16 15:25:08', '正常');
INSERT INTO `tb_account` VALUES ('9', 'guo123_59156', 'guo123_59156', '2', '2021-01-14 18:21:00', '2021-01-18 14:57:20', '正常');
INSERT INTO `tb_account` VALUES ('12', 'guoguo321_83461', 'guoguo321_83461', '1', '2021-01-16 18:15:08', '2021-01-16 18:15:08', '正常');
INSERT INTO `tb_account` VALUES ('13', 'guoguo321_552164', 'guoguo321_552164', '1', '2021-01-16 18:15:53', '2021-01-16 18:15:53', '正常');
INSERT INTO `tb_account` VALUES ('14', 'ddddd555', 'sdfdsfsd34342', '6', '2021-01-22 16:39:03', '2021-01-22 16:39:03', '正常');
INSERT INTO `tb_account` VALUES ('16', 'account999', 'guoguo321', '1', '2021-01-23 17:18:37', '2021-01-23 17:18:37', '正常');
INSERT INTO `tb_account` VALUES ('17', 'account_0', 'accountName_0', '0', '2021-01-26 17:06:17', '2021-01-26 17:06:17', '??');
INSERT INTO `tb_account` VALUES ('18', 'account_1', 'accountName_1', '1', '2021-01-26 17:06:17', '2021-01-26 17:06:17', '??');
INSERT INTO `tb_account` VALUES ('19', 'account_2', 'accountName_2', '2', '2021-01-26 17:06:17', '2021-01-26 17:06:17', '??');
INSERT INTO `tb_account` VALUES ('20', 'account_3', 'accountName_3', '3', '2021-01-26 17:06:17', '2021-01-26 17:06:17', '??');
INSERT INTO `tb_account` VALUES ('21', 'account_4', 'accountName_4', '4', '2021-01-26 17:06:17', '2021-01-26 17:06:17', '??');
INSERT INTO `tb_account` VALUES ('22', 'account_5', 'accountName_5', '5', '2021-01-26 17:06:17', '2021-01-26 17:06:17', '??');
INSERT INTO `tb_account` VALUES ('23', 'account_6', 'accountName_6', '6', '2021-01-26 17:06:17', '2021-01-26 17:06:17', '??');
INSERT INTO `tb_account` VALUES ('24', 'account_7', 'accountName_7', '7', '2021-01-26 17:06:17', '2021-01-26 17:06:17', '??');
INSERT INTO `tb_account` VALUES ('25', 'account_8', 'accountName_8', '8', '2021-01-26 17:06:17', '2021-01-26 17:06:17', '??');
INSERT INTO `tb_account` VALUES ('26', 'account_9', 'accountName_9', '9', '2021-01-26 17:06:17', '2021-01-26 17:06:17', '??');
INSERT INTO `tb_account` VALUES ('37', 'account1_0', 'accountName1_0', '0', '2021-01-26 17:13:24', '2021-01-26 17:13:24', '正常');
INSERT INTO `tb_account` VALUES ('38', 'account1_1', 'accountName1_1', '1', '2021-01-26 17:13:24', '2021-01-26 17:13:24', '正常');
INSERT INTO `tb_account` VALUES ('39', 'account1_2', 'accountName1_2', '2', '2021-01-26 17:13:24', '2021-01-26 17:13:24', '正常');
INSERT INTO `tb_account` VALUES ('40', 'account1_3', 'accountName1_3', '3', '2021-01-26 17:13:24', '2021-01-26 17:13:24', '正常');
INSERT INTO `tb_account` VALUES ('41', 'account1_4', 'accountName1_4', '4', '2021-01-26 17:13:24', '2021-01-26 17:13:24', '正常');
INSERT INTO `tb_account` VALUES ('42', 'account1_5', 'accountName1_5', '5', '2021-01-26 17:13:24', '2021-01-26 17:13:24', '正常');
INSERT INTO `tb_account` VALUES ('43', 'account1_6', 'accountName1_6', '6', '2021-01-26 17:13:24', '2021-01-26 17:13:24', '正常');
INSERT INTO `tb_account` VALUES ('44', 'account1_7', 'accountName1_7', '7', '2021-01-26 17:13:24', '2021-01-26 17:13:24', '正常');
INSERT INTO `tb_account` VALUES ('45', 'account1_8', 'accountName1_8', '8', '2021-01-26 17:13:24', '2021-01-26 17:13:24', '正常');
INSERT INTO `tb_account` VALUES ('46', 'account1_9', 'accountName1_9', '9', '2021-01-26 17:13:24', '2021-01-26 17:13:24', '正常');
INSERT INTO `tb_account` VALUES ('67', 'account3_0', 'accountName3_0', '0', '2021-01-29 18:26:11', '2021-01-29 18:26:11', '正常');
INSERT INTO `tb_account` VALUES ('68', 'account3_1', 'accountName3_1', '1', '2021-01-29 18:26:11', '2021-01-29 18:26:11', '正常');
INSERT INTO `tb_account` VALUES ('69', 'account3_2', 'accountName3_2', '2', '2021-01-29 18:26:11', '2021-01-29 18:26:11', '正常');
INSERT INTO `tb_account` VALUES ('70', 'account3_3', 'accountName3_3', '3', '2021-01-29 18:26:11', '2021-01-29 18:26:11', '正常');
INSERT INTO `tb_account` VALUES ('71', 'account3_4', 'accountName3_4', '4', '2021-01-29 18:26:11', '2021-01-29 18:26:11', '正常');
INSERT INTO `tb_account` VALUES ('72', 'account3_5', 'accountName3_5', '5', '2021-01-29 18:26:11', '2021-01-29 18:26:11', '正常');
INSERT INTO `tb_account` VALUES ('73', 'account3_6', 'accountName3_6', '6', '2021-01-29 18:26:11', '2021-01-29 18:26:11', '正常');
INSERT INTO `tb_account` VALUES ('74', 'account3_7', 'accountName3_7', '7', '2021-01-29 18:26:11', '2021-01-29 18:26:11', '正常');
INSERT INTO `tb_account` VALUES ('75', 'account3_8', 'accountName3_8', '8', '2021-01-29 18:26:11', '2021-01-29 18:26:11', '正常');
INSERT INTO `tb_account` VALUES ('76', 'account3_9', 'accountName3_9', '9', '2021-01-29 18:26:11', '2021-01-29 18:26:11', '正常');

-- ----------------------------
-- Table structure for tb_account_bet_record
-- ----------------------------
DROP TABLE IF EXISTS `tb_account_bet_record`;
CREATE TABLE `tb_account_bet_record` (
  `user_bet_record_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户投注记录id',
  `channel_id` int(11) DEFAULT NULL COMMENT '渠道id',
  `bet_id` char(64) DEFAULT NULL COMMENT '游戏各自的投注id',
  `account_id` int(11) DEFAULT NULL COMMENT '用户id',
  `game_name` char(50) NOT NULL COMMENT '游戏类型 百家乐、龙虎',
  `username` char(50) NOT NULL COMMENT '用户登录账号',
  `user_nickname` char(50) DEFAULT NULL COMMENT '用户昵称',
  `agent_id` int(11) NOT NULL COMMENT '代理商ID',
  `room_id` int(11) DEFAULT NULL COMMENT '房间ID',
  `round` char(50) DEFAULT NULL COMMENT '局数',
  `bet_amount` decimal(40,20) DEFAULT NULL COMMENT '投注金额',
  `bet_info` varchar(1000) DEFAULT NULL COMMENT '投注名称内容',
  `win_result` char(50) DEFAULT NULL COMMENT '中奖结果 中、没中、和局、流局',
  `lottery_result` char(50) DEFAULT NULL COMMENT '开奖详细结果 龙虎：龙\\\\\\\\虎\\\\\\\\和 百家乐:庄\\\\\\\\闲\\\\\\\\庄对\\\\\\\\闲对\\\\\\\\和',
  `won_amount` decimal(40,20) DEFAULT NULL COMMENT '中奖金额',
  `bet_order_id` varchar(1000) DEFAULT NULL COMMENT '投注订单号',
  `eos_round` char(64) DEFAULT NULL COMMENT 'eos区块链的局数',
  `remark` char(64) DEFAULT NULL COMMENT '备注',
  `currency_type` char(64) DEFAULT 'EOS' COMMENT '货币类型 EOS,jf_EOS，jf_GA，jf_SVT',
  `bet_time` char(255) DEFAULT NULL COMMENT '投注时间',
  `account_type` char(50) DEFAULT NULL,
  `true_bet` char(50) DEFAULT NULL COMMENT '有效投注',
  `game_id` int(11) DEFAULT NULL COMMENT '游戏id',
  `amount_before_bet` decimal(40,0) DEFAULT NULL COMMENT '投注前金额',
  `ip` char(50) DEFAULT NULL COMMENT 'ip',
  `app_key` char(64) DEFAULT NULL,
  `app_name` char(64) DEFAULT NULL,
  `app_type` char(64) DEFAULT NULL COMMENT '平台类型',
  `channel` char(64) DEFAULT NULL COMMENT '渠道',
  `room_type` char(64) DEFAULT NULL COMMENT '房间类型',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `tb_status` char(20) NOT NULL DEFAULT '正常' COMMENT '状态：正常，正常；删除，删除；',
  PRIMARY KEY (`user_bet_record_id`),
  KEY `bet_time` (`bet_time`) USING BTREE,
  KEY `username` (`username`) USING BTREE,
  KEY `app_key` (`app_key`) USING BTREE,
  KEY `app_type` (`app_type`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户投注记录表';

-- ----------------------------
-- Records of tb_account_bet_record
-- ----------------------------

-- ----------------------------
-- Table structure for tb_agent
-- ----------------------------
DROP TABLE IF EXISTS `tb_agent`;
CREATE TABLE `tb_agent` (
  `agent_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '代理商ID',
  `agent_account` varchar(32) NOT NULL COMMENT '代理商账号',
  `agent_name` varchar(32) NOT NULL COMMENT '代理商昵称',
  `agent_level` int(11) NOT NULL DEFAULT '0' COMMENT '代理层级',
  `parent_agent_id` int(11) NOT NULL COMMENT '上级代理ID',
  `parent_agent_id_group` text NOT NULL COMMENT '所有上代理的ID',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `tb_status` char(20) NOT NULL DEFAULT '正常' COMMENT '状态：正常，正常；删除，删除；',
  PRIMARY KEY (`agent_id`),
  UNIQUE KEY `agent_account` (`agent_account`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COMMENT='代理商';

-- ----------------------------
-- Records of tb_agent
-- ----------------------------
INSERT INTO `tb_agent` VALUES ('1', 'jack', 'jack', '0', '0', '[0],', '2021-01-14 15:39:21', '2021-01-16 18:01:19', '正常');
INSERT INTO `tb_agent` VALUES ('2', 'jack2', 'jack2', '1', '1', '[0],[1],', '2021-01-14 15:39:21', '2021-01-16 18:01:57', '正常');
INSERT INTO `tb_agent` VALUES ('3', 'jack3', 'jack3', '1', '1', '[0],[1],', '2021-01-14 15:39:21', '2021-01-16 18:01:57', '正常');
INSERT INTO `tb_agent` VALUES ('4', 'jack999', 'jack999', '1', '1', '[0],[1],', '2021-01-14 15:39:21', '2021-01-16 18:02:00', '正常');

-- ----------------------------
-- Procedure structure for add_account
-- ----------------------------
DROP PROCEDURE IF EXISTS `add_account`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `add_account`(IN `p_account_name` varchar(50))
BEGIN

DECLARE auto_agent_id INT;

START TRANSACTION;

INSERT INTO tb_agent(agent_name) VALUES(p_account_name);

SELECT agent_id into auto_agent_id from tb_agent where agent_name = p_account_name;

insert into tb_account(account_name, agent_id) VALUES(p_account_name,auto_agent_id);

COMMIT;

END
;;
DELIMITER ;
