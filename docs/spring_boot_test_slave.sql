/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50725
Source Host           : localhost:3306
Source Database       : spring_boot_test_slave

Target Server Type    : MYSQL
Target Server Version : 50725
File Encoding         : 65001

Date: 2021-02-07 15:06:05
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tb_game_kind
-- ----------------------------
DROP TABLE IF EXISTS `tb_game_kind`;
CREATE TABLE `tb_game_kind` (
  `game_kind_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '类型ID',
  `kind_name` varchar(64) NOT NULL COMMENT '类型',
  `sort_id` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
  `enable` int(11) NOT NULL DEFAULT '1' COMMENT '是否可用',
  `remark` varchar(80) DEFAULT '' COMMENT '备注',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`game_kind_id`),
  UNIQUE KEY `kind_name` (`kind_name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8mb4 COMMENT='游戏类型';

-- ----------------------------
-- Records of tb_game_kind
-- ----------------------------
INSERT INTO `tb_game_kind` VALUES ('1', '视频', '0', '1', '', '2020-04-26 20:50:26', '2020-04-26 20:50:25');
INSERT INTO `tb_game_kind` VALUES ('2', '街机', '10', '1', '', '2020-04-21 11:09:03', '2020-04-21 11:09:03');
INSERT INTO `tb_game_kind` VALUES ('3', '捕鱼', '20', '1', '', '2020-04-21 11:09:03', '2020-10-15 17:04:41');
INSERT INTO `tb_game_kind` VALUES ('4', '对战', '30', '1', '', '2020-04-21 11:09:03', '2020-10-15 17:10:43');
INSERT INTO `tb_game_kind` VALUES ('5', '多人', '40', '1', '', '2020-04-21 11:09:03', '2020-10-15 17:08:52');
INSERT INTO `tb_game_kind` VALUES ('6', '休闲', '50', '1', '', '2020-04-21 11:09:03', '2020-10-15 17:08:41');
INSERT INTO `tb_game_kind` VALUES ('7', '彩票', '5', '1', '', '2020-12-08 11:28:40', '2020-12-08 11:28:56');
INSERT INTO `tb_game_kind` VALUES ('28', '??——0', '0', '0', '', '2021-01-26 17:05:15', '2021-01-26 17:05:15');
INSERT INTO `tb_game_kind` VALUES ('29', '??——1', '1', '0', '', '2021-01-26 17:05:15', '2021-01-26 17:05:15');
INSERT INTO `tb_game_kind` VALUES ('30', '??——2', '2', '0', '', '2021-01-26 17:05:15', '2021-01-26 17:05:15');
INSERT INTO `tb_game_kind` VALUES ('31', '??——3', '3', '0', '', '2021-01-26 17:05:15', '2021-01-26 17:05:15');
INSERT INTO `tb_game_kind` VALUES ('32', '??——4', '4', '0', '', '2021-01-26 17:05:15', '2021-01-26 17:05:15');
INSERT INTO `tb_game_kind` VALUES ('33', '??——5', '5', '0', '', '2021-01-26 17:05:15', '2021-01-26 17:05:15');
INSERT INTO `tb_game_kind` VALUES ('34', '??——6', '6', '0', '', '2021-01-26 17:05:15', '2021-01-26 17:05:15');
INSERT INTO `tb_game_kind` VALUES ('35', '??——7', '7', '0', '', '2021-01-26 17:05:15', '2021-01-26 17:05:15');
INSERT INTO `tb_game_kind` VALUES ('36', '??——8', '8', '0', '', '2021-01-26 17:05:15', '2021-01-26 17:05:15');
INSERT INTO `tb_game_kind` VALUES ('37', '??——9', '9', '0', '', '2021-01-26 17:05:15', '2021-01-26 17:05:15');
INSERT INTO `tb_game_kind` VALUES ('38', '测试——10', '0', '0', '', '2021-01-26 17:14:00', '2021-01-26 17:14:00');
INSERT INTO `tb_game_kind` VALUES ('39', '测试——11', '1', '0', '', '2021-01-26 17:14:00', '2021-01-26 17:14:00');
INSERT INTO `tb_game_kind` VALUES ('40', '测试——12', '2', '0', '', '2021-01-26 17:14:00', '2021-01-26 17:14:00');
INSERT INTO `tb_game_kind` VALUES ('41', '测试——13', '3', '0', '', '2021-01-26 17:14:00', '2021-01-26 17:14:00');
INSERT INTO `tb_game_kind` VALUES ('42', '测试——14', '4', '0', '', '2021-01-26 17:14:00', '2021-01-26 17:14:00');
INSERT INTO `tb_game_kind` VALUES ('43', '测试——15', '5', '0', '', '2021-01-26 17:14:00', '2021-01-26 17:14:00');
INSERT INTO `tb_game_kind` VALUES ('44', '测试——16', '6', '0', '', '2021-01-26 17:14:00', '2021-01-26 17:14:00');
INSERT INTO `tb_game_kind` VALUES ('45', '测试——17', '7', '0', '', '2021-01-26 17:14:00', '2021-01-26 17:14:00');
INSERT INTO `tb_game_kind` VALUES ('46', '测试——18', '8', '0', '', '2021-01-26 17:14:00', '2021-01-26 17:14:00');
INSERT INTO `tb_game_kind` VALUES ('47', '测试——19', '9', '0', '', '2021-01-26 17:14:00', '2021-01-26 17:14:00');
