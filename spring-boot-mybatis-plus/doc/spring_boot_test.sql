/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50725
Source Host           : localhost:3306
Source Database       : spring_boot_test

Target Server Type    : MYSQL
Target Server Version : 50725
File Encoding         : 65001

Date: 2021-01-16 18:35:22
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tb_account
-- ----------------------------
DROP TABLE IF EXISTS `tb_account`;
CREATE TABLE `tb_account` (
  `account_id` int(11) NOT NULL AUTO_INCREMENT,
  `account` varchar(32) NOT NULL COMMENT '账号',
  `account_name` varchar(32) NOT NULL COMMENT '昵称',
  `agent_id` int(11) NOT NULL DEFAULT '0' COMMENT '代理商ID',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `tb_status` char(20) NOT NULL DEFAULT '正常' COMMENT '状态：正常，正常；删除，删除；',
  PRIMARY KEY (`account_id`),
  UNIQUE KEY `account` (`account`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COMMENT='用户表';

-- ----------------------------
-- Records of tb_account
-- ----------------------------
INSERT INTO `tb_account` VALUES ('2', 'jack9999', 'guoguo7778_707023', '3', '2021-01-14 15:38:11', '2021-01-16 18:27:13', '正常');
INSERT INTO `tb_account` VALUES ('5', 'jack999', 'jack999', '4', '2021-01-14 15:38:11', '2021-01-16 15:25:01', '正常');
INSERT INTO `tb_account` VALUES ('6', 'guo123', 'guo123', '1', '2021-01-14 18:14:47', '2021-01-16 15:25:08', '正常');
INSERT INTO `tb_account` VALUES ('9', 'guo123_59156', 'guo123_59156', '1', '2021-01-14 18:21:00', '2021-01-16 15:25:14', '正常');
INSERT INTO `tb_account` VALUES ('12', 'guoguo321_83461', 'guoguo321_83461', '1', '2021-01-16 18:15:08', '2021-01-16 18:15:08', '正常');
INSERT INTO `tb_account` VALUES ('13', 'guoguo321_552164', 'guoguo321_552164', '1', '2021-01-16 18:15:53', '2021-01-16 18:15:53', '正常');

-- ----------------------------
-- Table structure for tb_account_bet_record
-- ----------------------------
DROP TABLE IF EXISTS `tb_account_bet_record`;
CREATE TABLE `tb_account_bet_record` (
  `user_bet_record_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户投注记录id',
  `channel_id` int(11) DEFAULT NULL COMMENT '渠道id',
  `bet_id` char(64) DEFAULT NULL COMMENT '游戏各自的投注id',
  `account_id` int(11) DEFAULT NULL COMMENT '用户id',
  `game_name` char(50) NOT NULL COMMENT '游戏类型 百家乐、龙虎',
  `username` char(50) NOT NULL COMMENT '用户登录账号',
  `user_nickname` char(50) DEFAULT NULL COMMENT '用户昵称',
  `agent_id` int(11) NOT NULL COMMENT '代理商ID',
  `room_id` int(11) DEFAULT NULL COMMENT '房间ID',
  `round` char(50) DEFAULT NULL COMMENT '局数',
  `bet_amount` decimal(40,20) DEFAULT NULL COMMENT '投注金额',
  `bet_info` varchar(1000) DEFAULT NULL COMMENT '投注名称内容',
  `win_result` char(50) DEFAULT NULL COMMENT '中奖结果 中、没中、和局、流局',
  `lottery_result` char(50) DEFAULT NULL COMMENT '开奖详细结果 龙虎：龙\\\\\\\\虎\\\\\\\\和 百家乐:庄\\\\\\\\闲\\\\\\\\庄对\\\\\\\\闲对\\\\\\\\和',
  `won_amount` decimal(40,20) DEFAULT NULL COMMENT '中奖金额',
  `bet_order_id` varchar(1000) DEFAULT NULL COMMENT '投注订单号',
  `eos_round` char(64) DEFAULT NULL COMMENT 'eos区块链的局数',
  `remark` char(64) DEFAULT NULL COMMENT '备注',
  `currency_type` char(64) DEFAULT 'EOS' COMMENT '货币类型 EOS,jf_EOS，jf_GA，jf_SVT',
  `bet_time` char(255) DEFAULT NULL COMMENT '投注时间',
  `account_type` char(50) DEFAULT NULL,
  `true_bet` char(50) DEFAULT NULL COMMENT '有效投注',
  `game_id` int(11) DEFAULT NULL COMMENT '游戏id',
  `amount_before_bet` decimal(40,0) DEFAULT NULL COMMENT '投注前金额',
  `ip` char(50) DEFAULT NULL COMMENT 'ip',
  `app_key` char(64) DEFAULT NULL,
  `app_name` char(64) DEFAULT NULL,
  `app_type` char(64) DEFAULT NULL COMMENT '平台类型',
  `channel` char(64) DEFAULT NULL COMMENT '渠道',
  `room_type` char(64) DEFAULT NULL COMMENT '房间类型',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `tb_status` char(20) NOT NULL DEFAULT '正常' COMMENT '状态：正常，正常；删除，删除；',
  PRIMARY KEY (`user_bet_record_id`),
  KEY `bet_time` (`bet_time`) USING BTREE,
  KEY `username` (`username`) USING BTREE,
  KEY `app_key` (`app_key`) USING BTREE,
  KEY `app_type` (`app_type`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户投注记录表';

-- ----------------------------
-- Records of tb_account_bet_record
-- ----------------------------

-- ----------------------------
-- Table structure for tb_agent
-- ----------------------------
DROP TABLE IF EXISTS `tb_agent`;
CREATE TABLE `tb_agent` (
  `agent_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '代理商ID',
  `agent_account` varchar(32) NOT NULL COMMENT '代理商账号',
  `agent_name` varchar(32) NOT NULL COMMENT '代理商昵称',
  `agent_level` int(11) NOT NULL DEFAULT '0' COMMENT '代理层级',
  `parent_agent_id` int(11) NOT NULL COMMENT '上级代理ID',
  `parent_agent_id_group` text NOT NULL COMMENT '所有上代理的ID',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `tb_status` char(20) NOT NULL DEFAULT '正常' COMMENT '状态：正常，正常；删除，删除；',
  PRIMARY KEY (`agent_id`),
  UNIQUE KEY `agent_account` (`agent_account`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COMMENT='代理商';

-- ----------------------------
-- Records of tb_agent
-- ----------------------------
INSERT INTO `tb_agent` VALUES ('1', 'jack', 'jack', '0', '0', '[0],', '2021-01-14 15:39:21', '2021-01-16 18:01:19', '正常');
INSERT INTO `tb_agent` VALUES ('2', 'jack2', 'jack2', '1', '1', '[0],[1],', '2021-01-14 15:39:21', '2021-01-16 18:01:57', '正常');
INSERT INTO `tb_agent` VALUES ('3', 'jack3', 'jack3', '1', '1', '[0],[1],', '2021-01-14 15:39:21', '2021-01-16 18:01:57', '正常');
INSERT INTO `tb_agent` VALUES ('4', 'jack999', 'jack999', '1', '1', '[0],[1],', '2021-01-14 15:39:21', '2021-01-16 18:02:00', '正常');

-- ----------------------------
-- Procedure structure for add_account
-- ----------------------------
DROP PROCEDURE IF EXISTS `add_account`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `add_account`(IN `p_account_name` varchar(50))
BEGIN

DECLARE auto_agent_id INT;

START TRANSACTION;

INSERT INTO tb_agent(agent_name) VALUES(p_account_name);

SELECT agent_id into auto_agent_id from tb_agent where agent_name = p_account_name;

insert into tb_account(account_name, agent_id) VALUES(p_account_name,auto_agent_id);

COMMIT;

END
;;
DELIMITER ;
