# spring-boot-test-mybatis-plus

#### 介绍
使用插件Easy Code生成代码

#### Entity 模板

```
##导入宏定义
$!define

##保存文件（宏定义）
#save("/entity", ".java")

##包路径（宏定义）
#setPackageSuffix("entity")

##自动导入包（全局变量）
$!autoImport
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import lombok.Data;

##表注释（宏定义）
#tableComment("表实体类")
@Data
@SuppressWarnings("serial")
@TableName("$tableInfo.obj.name")
public class $!{tableInfo.name} extends Model<$!{tableInfo.name}> {

##生成所有字段
#foreach($column in $tableInfo.fullColumn)
    ##如果表字段存在标注，即生成注释
    #if(${column.comment})
    
    /**
    * ${column.comment}
    */#end
    ##遍历包含的主键
    #foreach($pkColumn in $tableInfo.pkColumn)
        ##如果列名是主键那么生成主键id，否则生成basic字段注解
        #if($pkColumn.equals($column))
    
    @TableId(value= "${column.obj.name}", type = IdType.AUTO)
    private $!{tool.getClsNameByFullName($column.type)} $!{column.name};
        #else
        
   
    @TableField("${column.obj.name}")
    private $!{tool.getClsNameByFullName($column.type)} $!{column.name};
        #end
    #end
#end

###foreach($column in $tableInfo.fullColumn)
##    #getSetMethod($column)
###end

#foreach($column in $tableInfo.pkColumn)
    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.$!column.name;
    }
    #break
#end
}

```

#### Dao 模板

```
##导入宏定义
$!define

##设置表后缀（宏定义）
#setTableSuffix("Dao")

##保存文件（宏定义）
#save("/dao", "Dao.java")

##包路径（宏定义）
#setPackageSuffix("dao")

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import $!{tableInfo.savePackageName}.entity.$!tableInfo.name;

##表注释（宏定义）
#tableComment("表数据库访问层")
public interface $!{tableName} extends BaseMapper<$!tableInfo.name> {

}

```

#### Service 模板

```
##导入宏定义
$!define

##设置表后缀（宏定义）
#setTableSuffix("Service")

##保存文件（宏定义）
#save("/service", "Service.java")

##包路径（宏定义）
#setPackageSuffix("service")

import com.baomidou.mybatisplus.extension.service.IService;
import $!{tableInfo.savePackageName}.entity.$!tableInfo.name;

##表注释（宏定义）
#tableComment("表服务接口")
public interface $!{tableName} extends IService<$!tableInfo.name> {

}

```

#### ServiceImpl 模板

```
##导入宏定义
$!define

##设置表后缀（宏定义）
#setTableSuffix("ServiceImpl")

##保存文件（宏定义）
#save("/service/impl", "ServiceImpl.java")

##包路径（宏定义）
#setPackageSuffix("service.impl")

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import $!{tableInfo.savePackageName}.dao.$!{tableInfo.name}Dao;
import $!{tableInfo.savePackageName}.entity.$!{tableInfo.name};
import $!{tableInfo.savePackageName}.service.$!{tableInfo.name}Service;
import org.springframework.stereotype.Service;

##表注释（宏定义）
#tableComment("表服务实现类")
@Service("$!tool.firstLowerCase($tableInfo.name)Service")
public class $!{tableName} extends ServiceImpl<$!{tableInfo.name}Dao, $!{tableInfo.name}> implements $!{tableInfo.name}Service {

}

```

#### Controller 模板

```
##导入宏定义
$!define

##设置表后缀（宏定义）
#setTableSuffix("Controller")

##保存文件（宏定义）
#save("/controller", "Controller.java")

##包路径（宏定义）
#setPackageSuffix("controller")

##定义服务名
#set($serviceName = $!tool.append($!tool.firstLowerCase($!tableInfo.name), "Service"))

##定义实体对象名
#set($entityName = $!tool.firstLowerCase($!tableInfo.name))

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.api.ApiController;
import com.baomidou.mybatisplus.extension.api.R;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import $!{tableInfo.savePackageName}.entity.$!tableInfo.name;
import $!{tableInfo.savePackageName}.service.$!{tableInfo.name}Service;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.List;

##表注释（宏定义）
#tableComment("表控制层")
@RestController
@RequestMapping("/$!tool.firstLowerCase($!tableInfo.name)")
public class $!{tableName} extends ApiController {
    /**
     * 服务对象
     */
    @Resource
    private $!{tableInfo.name}Service $!{serviceName};

    /**
     * 分页查询所有数据
     *
     * @param page 分页对象
     * @param $!entityName 查询实体
     * @return 所有数据
     */
    @GetMapping("/selectAll")
    public R selectAll(Page<$!tableInfo.name> page, $!tableInfo.name $!entityName) {
        return success(this.$!{serviceName}.page(page, new QueryWrapper<>($!entityName)));
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("/selectOne/{id}")
    public R selectOne(@PathVariable Serializable id) {
        return success(this.$!{serviceName}.getById(id));
    }

    /**
     * 新增数据
     *
     * @param $!entityName 实体对象
     * @return 新增结果
     */
    @PostMapping("/insert")
    public R insert(@RequestBody $!tableInfo.name $!entityName) {
        return success(this.$!{serviceName}.save($!entityName));
    }

    /**
     * 修改数据
     *
     * @param $!entityName 实体对象
     * @return 修改结果
     */
    @PutMapping("/update")
    public R update(@RequestBody $!tableInfo.name $!entityName) {
        return success(this.$!{serviceName}.updateById($!entityName));
    }

    /**
     * 删除数据
     *
     * @param idList 主键结合
     * @return 删除结果
     */
    @DeleteMapping("/delete")
    public R delete(@RequestParam("idList") List<Long> idList) {
        return success(this.$!{serviceName}.removeByIds(idList));
    }
}

```

#### dto 模板

```
##导入宏定义
$!define

##保存文件（宏定义）
#save("/dto", "DTO.java")

##包路径（宏定义）
#setPackageSuffix("dto")


##自动导入包（全局变量）
$!autoImport
##import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import lombok.Data;
##import com.baomidou.mybatisplus.annotation.IdType;
##import com.baomidou.mybatisplus.annotation.TableId;

##表注释（宏定义）
#tableComment("表实体类")
@Data
@SuppressWarnings("serial")
public class $!{tableInfo.name}DTO implements Serializable {
  
#foreach($column in $tableInfo.fullColumn)
    #if(${column.comment})/**${column.comment}*/#end

    private $!{tool.getClsNameByFullName($column.type)} $!{column.name};
#end

###foreach($column in $tableInfo.fullColumn)
##    #getSetMethod($column)
###end

###foreach($column in $tableInfo.pkColumn)
##    /**
##     * 获取主键值
##     *
##     * @return 主键值
##     */
##    @Override
##    protected Serializable pkVal() {
##        return this.$!column.name;
##    }
##    #break
###end
}


```


#### 去掉表格前缀 模板
###### Global Config 的define文件中，在前面加上
```
##去掉表的t_前缀
#if($tableInfo.obj.name.startsWith("tb_"))
    $!tableInfo.setName($tool.getClassName($tableInfo.obj.name.substring(3)))
#end
```