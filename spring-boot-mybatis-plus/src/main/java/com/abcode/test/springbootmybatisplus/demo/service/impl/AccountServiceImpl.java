package com.abcode.test.oracletest.demo.service.impl;

import com.abcode.test.oracletest.demo.dao.AccountDao;
import com.abcode.test.oracletest.demo.entity.Account;
import com.abcode.test.oracletest.demo.service.AccountService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 用户表(Account)表服务实现类
 *
 * @author abcode
 * @since 2021-01-22 20:37:45
 */
@Service("accountService")
public class AccountServiceImpl extends ServiceImpl<AccountDao, Account> implements AccountService {

}