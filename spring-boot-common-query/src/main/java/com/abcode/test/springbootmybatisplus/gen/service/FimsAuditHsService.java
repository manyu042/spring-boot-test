package com.abcode.test.oracletest.gen.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.abcode.test.oracletest.gen.entity.FimsAuditHs;

/**
 * 审计信息(FimsAuditHs)表服务接口
 *
 * @author qinzhitao
 * @since 2023-05-05 10:59:09
 */
public interface FimsAuditHsService extends IService<FimsAuditHs> {

}

