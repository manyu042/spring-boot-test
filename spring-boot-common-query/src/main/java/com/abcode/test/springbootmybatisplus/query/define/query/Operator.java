package com.abcode.test.oracletest.query.define.query;

/**
 * TODO
 *
 * @author qinzhitao
 * @date 2023/6/7 11:37
 */
public enum Operator {
    EQ, // 等于
    NE, // 不等于
    GT, // 大于
    GE, // 大于等于
    LT, // 小于
    LE, // 小于等于
    LIKE, // 模糊查询
    NOT_LIKE, // 不包含
    IN, // 包含于
    NOT_IN, // 不包含于
    // 其他比较符号
}
