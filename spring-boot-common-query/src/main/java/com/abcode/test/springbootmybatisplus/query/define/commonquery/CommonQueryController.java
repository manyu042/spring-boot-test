package com.abcode.test.oracletest.query.define.commonquery;

import com.abcode.test.oracletest.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1")
public class CommonQueryController {
     @Autowired
    private CommonQueryService commonQueryService;
     @GetMapping("/query")
    public Result<List<Map<String, Object>>> query(@RequestParam String tableName,
                                                   @RequestParam(required = false) List<String> queryFields,
                                                   @RequestParam(required = false) List<QueryCondition> queryConditions,
                                                   @RequestParam(defaultValue = "1") int pageNum,
                                                   @RequestParam(defaultValue = "10") int pageSize,
                                                   @RequestParam(required = false) String orderByField,
                                                   @RequestParam(defaultValue = "false") boolean isAsc) {
        if (CollectionUtils.isEmpty(queryFields)) {
            queryFields = Collections.singletonList("*"); // 默认查询所有字段
        }
        List<Map<String, Object>> data = commonQueryService.query(tableName, queryFields, queryConditions, pageNum, pageSize, orderByField, isAsc);
        return Result.ok(data);
    }
}