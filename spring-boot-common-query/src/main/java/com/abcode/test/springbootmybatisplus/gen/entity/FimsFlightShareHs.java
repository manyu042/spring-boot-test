package com.abcode.test.oracletest.gen.entity;

import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
/**
 * 动态共享航班表(FimsFlightShareHs)表实体类
 *
 * @author qinzhitao
 * @since 2023-05-05 11:40:05
 */
@Data
@SuppressWarnings("serial")
@TableName("FIMS_FLIGHT_SHARE_HS")
public class FimsFlightShareHs {
            
    /** ID */                                  
    @TableId(value= "ID")
    private Long id;
                        
    /** 航班ID */                                
    @TableField("FLIGHT_ID")
    private Long flightId;
                        
    /** 航空公司二字码 */                                
    @TableField("AIRLINES_IATA")
    private String airlinesIata;
                        
    /** 航班号 */                                
    @TableField("FLIGHT_NO")
    private String flightNo;
                        
    /** 排序编码 */                                
    @TableField("ORDER_CODE")
    private Integer orderCode;
                        
    /** 是否删除 */                                
    @TableField("DEL_FLAG")
    private Integer delFlag;
                        
    /** 创建人 */                                
    @TableField("CREATE_BY")
    private String createBy;
                        
    /** 创建时间 */                                
    @TableField("CREATE_TIME")
    private LocalDateTime createTime;
                        
    /** 更新人 */                                
    @TableField("UPDATE_BY")
    private String updateBy;
                        
    /** 更新时间 */                                
    @TableField("UPDATE_TIME")
    private LocalDateTime updateTime;
            
}
