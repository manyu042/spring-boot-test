package com.abcode.test.oracletest.demo.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.abcode.test.oracletest.demo.dao.TbAuditDao;
import com.abcode.test.oracletest.demo.entity.TbAudit;
import com.abcode.test.oracletest.demo.service.TbAuditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 审计信息(TbAudit)表服务实现类
 *
 * @author qinzhitao
 * @since 2023-03-14 14:46:56
 */
@Service("tbAuditService")
public class TbAuditServiceImpl extends ServiceImpl<TbAuditDao, TbAudit> implements TbAuditService {
    @Autowired
    TbAuditDao tbAuditDao;
    @Override
    public boolean insertOne(TbAudit tbAudit) {
        int rst = tbAuditDao.insertOne(tbAudit);
        return rst>0;
    }
}

