package com.abcode.test.oracletest.gen.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.abcode.test.oracletest.gen.entity.FimsFlightShareHs;

/**
 * 动态共享航班表(FimsFlightShareHs)表数据库访问层
 *
 * @author qinzhitao
 * @since 2023-05-05 11:40:05
 */
public interface FimsFlightShareHsMapper extends BaseMapper<FimsFlightShareHs> {

}

