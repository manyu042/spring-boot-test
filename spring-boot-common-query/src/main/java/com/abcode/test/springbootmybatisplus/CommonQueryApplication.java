package com.abcode.test.oracletest;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@MapperScan({"com.abcode.test.springbootmybatisplus.demo.dao", "com.abcode.test.springbootmybatisplus.gen.mapper","com.abcode.test.springbootmybatisplus.query.define.query"})
@SpringBootApplication
public class CommonQueryApplication {

    public static void main(String[] args) {
        SpringApplication.run(CommonQueryApplication.class, args);
    }

}
