package com.abcode.test.oracletest.demo.dao;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import com.abcode.test.oracletest.demo.entity.TbAudit;

/**
 * 审计信息(TbAudit)表数据库访问层
 *
 * @author qinzhitao
 * @since 2023-03-14 14:46:55
 */
public interface TbAuditDao extends BaseMapper<TbAudit> {

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<TbAudit> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<TbAudit> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<TbAudit> 实例对象列表
     * @return 影响行数
     * @throws org.springframework.jdbc.BadSqlGrammarException 入参是空List的时候会抛SQL语句错误的异常，请自行校验入参
     */
    int insertOrUpdateBatch(@Param("entities") List<TbAudit> entities);


    int insertOne(@Param("entity") TbAudit entity);
}

