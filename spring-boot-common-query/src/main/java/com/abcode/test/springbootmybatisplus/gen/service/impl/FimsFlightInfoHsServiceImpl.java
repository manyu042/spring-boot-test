package com.abcode.test.oracletest.gen.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.abcode.test.oracletest.gen.mapper.FimsFlightInfoHsMapper;
import com.abcode.test.oracletest.gen.entity.FimsFlightInfoHs;
import com.abcode.test.oracletest.gen.service.FimsFlightInfoHsService;
import org.springframework.stereotype.Service;

/**
 * 航班动态表(FimsFlightInfoHs)表服务实现类
 *
 * @author qinzhitao
 * @since 2023-05-04 17:01:59
 */
@Service
public class FimsFlightInfoHsServiceImpl extends ServiceImpl<FimsFlightInfoHsMapper, FimsFlightInfoHs> implements FimsFlightInfoHsService {

}

