package com.abcode.test.oracletest.demo.dao;

import com.abcode.test.oracletest.demo.entity.Account;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 用户表(Account)表数据库访问层
 *
 * @author abcode
 * @since 2021-01-22 20:37:45
 */
public interface AccountDao extends BaseMapper<Account> {

}