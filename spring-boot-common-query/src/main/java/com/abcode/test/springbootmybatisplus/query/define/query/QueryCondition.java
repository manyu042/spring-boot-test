package com.abcode.test.oracletest.query.define.query;

import lombok.Data;

@Data
public class QueryCondition {
    private String fieldName;
    private Object fieldValue;
    private Operator operator;
     public String getSql() {
        switch (operator) {
            case EQ:
                return fieldName + " = #{fieldValue}";
            case NE:
                return fieldName + " != #{fieldValue}";
            case GT:
                return fieldName + " > #{fieldValue}";
            case GE:
                return fieldName + " >= #{fieldValue}";
            case LT:
                return fieldName + " < #{fieldValue}";
            case LE:
                return fieldName + " <= #{fieldValue}";
            case LIKE:
                return fieldName + " LIKE CONCAT('%', #{fieldValue}, '%')";
            case NOT_LIKE:
                return fieldName + " NOT LIKE CONCAT('%', #{fieldValue}, '%')";
            case IN:
                return fieldName + " IN (#{fieldValue})";
            case NOT_IN:
                return fieldName + " NOT IN (#{fieldValue})";
            default:
                throw new IllegalArgumentException("Unsupported operator: " + operator);
        }
    }
}