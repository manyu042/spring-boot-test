package com.abcode.test.oracletest.gen.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.abcode.test.oracletest.gen.entity.FimsFlightAirlineHs;

/**
 * 动态经停站（航线）表(FimsFlightAirlineHs)表服务接口
 *
 * @author qinzhitao
 * @since 2023-05-05 11:40:05
 */
public interface FimsFlightAirlineHsService extends IService<FimsFlightAirlineHs> {

}

