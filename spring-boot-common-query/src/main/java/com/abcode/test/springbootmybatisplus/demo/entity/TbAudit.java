package com.abcode.test.oracletest.demo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;


import java.time.LocalDateTime;
import java.util.Date;

/**
 * <p>审计信息</p>
 *
 * @author zhangwencheng
 * @date 2023-02-22 14:19
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class TbAudit {

    private static final long serialVersionUID = 1L;
    /**
     * 主键ID
     */
    @TableId(type = IdType.ASSIGN_ID)
    private Long id;
    /**
     * 操作表名
     */

    private String actionTableName;
    /**
     * 操作时间
     */

    private Date actionTime;
    /**
     * 操作类型
     */

    private String actionType;
    /**
     * 航空公司二字码
     */

    private String airlinesIata;
    /**
     * 创建人
     */
    private String createBy;
    /**
     * 创建时间
     */

    private Date createTime;
    /**
     * 是否删除
     */

    private Boolean delFlag;
    /**
     * 操作描述
     */

    private String description;
    /**
     * 航班ID
     */

    private Long flightId;
    /**
     * 航班号
     */

    private String flightNo;
    /**
     * 工号
     */

    private String jobNo;
    /**
     * 消息类型
     */

    private String msgType;
    /**
     * 进出标志（A进港D出港）
     */

    private String offInFlag;
    /**
     * 角色名称
     */

    private String roleName;
    /**
     * 操作表子类型
     */

    private String subtableType;
    /**
     * 更新人
     */

    private String updateBy;
    /**
     * 更新时间
     */

    private LocalDateTime updateTime;
    /**
     * 操作人员姓名
     */

    private String username;
}