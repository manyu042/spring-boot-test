package com.abcode.test.oracletest.gen.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.abcode.test.oracletest.gen.entity.FimsFlightAirlineHs;

/**
 * 动态经停站（航线）表(FimsFlightAirlineHs)表数据库访问层
 *
 * @author qinzhitao
 * @since 2023-05-05 11:40:05
 */
public interface FimsFlightAirlineHsMapper extends BaseMapper<FimsFlightAirlineHs> {

}

