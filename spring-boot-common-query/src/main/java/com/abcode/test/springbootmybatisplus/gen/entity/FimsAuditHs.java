package com.abcode.test.oracletest.gen.entity;

import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
/**
 * 审计信息(FimsAuditHs)表实体类
 *
 * @author qinzhitao
 * @since 2023-05-05 11:03:57
 */
@Data
@SuppressWarnings("serial")
@TableName("FIMS_AUDIT_HS")
public class FimsAuditHs {
            
    /** 主键ID */                                  
    @TableId(value= "ID")
    private Long id;
                        
    /** 工号 */                                
    @TableField("JOB_NO")
    private String jobNo;
                        
    /** 操作人员姓名 */                                
    @TableField("USERNAME")
    private String username;
                        
    /** 角色名称 */                                
    @TableField("ROLE_NAME")
    private String roleName;
                        
    /** 操作时间 */                                
    @TableField("ACTION_TIME")
    private LocalDateTime actionTime;
                        
    /** 操作表名 */                                
    @TableField("ACTION_TABLE_NAME")
    private String actionTableName;
                        
    /** 操作类型 */                                
    @TableField("ACTION_TYPE")
    private String actionType;
                        
    /** 航班ID */                                
    @TableField("FLIGHT_ID")
    private Long flightId;
                        
    /** 航班号 */                                
    @TableField("FLIGHT_NO")
    private String flightNo;
                        
    /** 进出标志（A进港D出港） */                                
    @TableField("OFF_IN_FLAG")
    private String offInFlag;
                        
    /** 操作描述 */                                
    @TableField("DESCRIPTION")
    private String description;
                        
    /** 消息类型 */                                
    @TableField("MSG_TYPE")
    private String msgType;
                        
    /** 航空公司二字码 */                                
    @TableField("AIRLINES_IATA")
    private String airlinesIata;
                        
    /** 操作表子类型 */                                
    @TableField("SUBTABLE_TYPE")
    private String subtableType;
                        
    /** 是否删除 */                                
    @TableField("DEL_FLAG")
    private Integer delFlag;
                        
    /** 创建人 */                                
    @TableField("CREATE_BY")
    private String createBy;
                        
    /** 创建时间 */                                
    @TableField("CREATE_TIME")
    private LocalDateTime createTime;
                        
    /** 更新人 */                                
    @TableField("UPDATE_BY")
    private String updateBy;
                        
    /** 更新时间 */                                
    @TableField("UPDATE_TIME")
    private LocalDateTime updateTime;
                        
    /** 航班动态编辑历史记录对象（只存当前业务相关内容） */                                
    @TableField("SNAPSHOT_RECORD")
    private String snapshotRecord;
                        
    /** 是否混合，默认为0（不混合） */                                
    @TableField("FLIGHT_ATTRIBUTE")
    private String flightAttribute;
            
}
