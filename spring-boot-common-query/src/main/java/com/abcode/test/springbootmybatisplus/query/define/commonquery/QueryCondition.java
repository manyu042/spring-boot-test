package com.abcode.test.oracletest.query.define.commonquery;

import lombok.Data;

@Data
public class QueryCondition {
    private String fieldName; // 字段名
    private Object fieldValue; // 字段值
    private String operator; // 比较符
     // 省略getter和setter方法
}