package com.abcode.test.oracletest.gen.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.abcode.test.oracletest.gen.mapper.FimsFlightShareHsMapper;
import com.abcode.test.oracletest.gen.entity.FimsFlightShareHs;
import com.abcode.test.oracletest.gen.service.FimsFlightShareHsService;
import org.springframework.stereotype.Service;

/**
 * 动态共享航班表(FimsFlightShareHs)表服务实现类
 *
 * @author qinzhitao
 * @since 2023-05-05 11:40:05
 */
@Service
public class FimsFlightShareHsServiceImpl extends ServiceImpl<FimsFlightShareHsMapper, FimsFlightShareHs> implements FimsFlightShareHsService {

}

