package com.abcode.test.oracletest.gen.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.abcode.test.oracletest.gen.entity.FimsFlightInfoHs;

/**
 * 航班动态表(FimsFlightInfoHs)表数据库访问层
 *
 * @author qinzhitao
 * @since 2023-05-04 17:01:59
 */
public interface FimsFlightInfoHsMapper extends BaseMapper<FimsFlightInfoHs> {

}

