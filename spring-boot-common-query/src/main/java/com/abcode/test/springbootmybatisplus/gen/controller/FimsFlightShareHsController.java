package com.abcode.test.oracletest.gen.controller;



import com.abcode.test.oracletest.Result;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.io.Serializable;
import java.util.List;

import com.abcode.test.oracletest.gen.entity.FimsFlightShareHs;
import com.abcode.test.oracletest.gen.service.FimsFlightShareHsService;


/**
 * 动态共享航班表(FimsFlightShareHs)表控制层
 *
 * @author qinzhitao
 * @since 2023-05-05 11:40:05
 */
@RestController
@RequestMapping("fimsFlightShareHs")
public class FimsFlightShareHsController {
    /**
     * 服务对象
     */
    @Resource
    private FimsFlightShareHsService fimsFlightShareHsService;

    /**
     * 分页查询所有数据
     * @param page 分页对象
     * @param fimsFlightShareHs 查询实体
     * @return 所有数据
     */
    @GetMapping("/selectAll")
    public Result selectAll(Page<FimsFlightShareHs> page, FimsFlightShareHs fimsFlightShareHs) {
        Page<FimsFlightShareHs> rst = this.fimsFlightShareHsService.page(page, new QueryWrapper<>(fimsFlightShareHs));
        return Result.ok(rst);
    }

    /**
     * 通过主键查询单条数据
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("/selectOne/{id}")
    public Result selectOne(@PathVariable Serializable id) {
        FimsFlightShareHs fimsFlightShareHs = this.fimsFlightShareHsService.getById(id);
        return Result.ok(fimsFlightShareHs);
    }

    /**
     * 新增数据
     * @param fimsFlightShareHs 实体对象
     * @return 新增结果
     */
    @PostMapping("/insert")
    public Result insert(@RequestBody FimsFlightShareHs fimsFlightShareHs) {
        for (int i = 0; i < 100; i++) {
            fimsFlightShareHs.setId(null);
            fimsFlightShareHs.setFlightId((long) (10000+i));
            Boolean rst = this.fimsFlightShareHsService.save(fimsFlightShareHs);
        }

        return Result.OK(true);
    }

    /**
     * 修改数据
     * @param fimsFlightShareHs 实体对象
     * @return 修改结果
     */
    @PutMapping("/update")
    public Result update(@RequestBody FimsFlightShareHs fimsFlightShareHs) {
        Boolean rst = this.fimsFlightShareHsService.updateById(fimsFlightShareHs);
        return Result.OK(rst);
    }

    /**
     * 删除数据
     *
     * @param idList 主键结合
     * @return 删除结果
     */
    @DeleteMapping("/delete")
    public Result delete(@RequestParam("idList") List<Long> idList) {
        boolean rst = this.fimsFlightShareHsService.removeByIds(idList);
        return Result.ok(rst);
    }
}

