package com.abcode.test.oracletest.gen.controller;



import com.abcode.test.oracletest.Result;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.io.Serializable;
import java.util.List;

import com.abcode.test.oracletest.gen.entity.FimsFlightAirlineHs;
import com.abcode.test.oracletest.gen.service.FimsFlightAirlineHsService;


/**
 * 动态经停站（航线）表(FimsFlightAirlineHs)表控制层
 *
 * @author qinzhitao
 * @since 2023-05-05 11:40:05
 */
@RestController
@RequestMapping("fimsFlightAirlineHs")
public class FimsFlightAirlineHsController {
    /**
     * 服务对象
     */
    @Resource
    private FimsFlightAirlineHsService fimsFlightAirlineHsService;

    /**
     * 分页查询所有数据
     * @param page 分页对象
     * @param fimsFlightAirlineHs 查询实体
     * @return 所有数据
     */
    @GetMapping("/selectAll")
    public Result selectAll(Page<FimsFlightAirlineHs> page, FimsFlightAirlineHs fimsFlightAirlineHs) {
        Page<FimsFlightAirlineHs> rst = this.fimsFlightAirlineHsService.page(page, new QueryWrapper<>(fimsFlightAirlineHs));
        return Result.ok(rst);
    }

    /**
     * 通过主键查询单条数据
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("/selectOne/{id}")
    public Result selectOne(@PathVariable Serializable id) {
        FimsFlightAirlineHs fimsFlightAirlineHs = this.fimsFlightAirlineHsService.getById(id);
        return Result.ok(fimsFlightAirlineHs);
    }

    /**
     * 新增数据
     * @param fimsFlightAirlineHs 实体对象
     * @return 新增结果
     */
    @PostMapping("/insert")
    public Result insert(@RequestBody FimsFlightAirlineHs fimsFlightAirlineHs) {
        for (int i = 0; i < 100; i++) {
            fimsFlightAirlineHs.setFlightId((long) (10000+i));
            fimsFlightAirlineHs.setId(null);
            Boolean rst = this.fimsFlightAirlineHsService.save(fimsFlightAirlineHs);
        }

        return Result.OK(true);
    }

    /**
     * 修改数据
     * @param fimsFlightAirlineHs 实体对象
     * @return 修改结果
     */
    @PutMapping("/update")
    public Result update(@RequestBody FimsFlightAirlineHs fimsFlightAirlineHs) {
        Boolean rst = this.fimsFlightAirlineHsService.updateById(fimsFlightAirlineHs);
        return Result.OK(rst);
    }

    /**
     * 删除数据
     *
     * @param idList 主键结合
     * @return 删除结果
     */
    @DeleteMapping("/delete")
    public Result delete(@RequestParam("idList") List<Long> idList) {
        boolean rst = this.fimsFlightAirlineHsService.removeByIds(idList);
        return Result.ok(rst);
    }
}

