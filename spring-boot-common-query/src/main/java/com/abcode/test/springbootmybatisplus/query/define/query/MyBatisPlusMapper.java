package com.abcode.test.oracletest.query.define.query;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface MyBatisPlusMapper extends BaseMapper<Object> {
    @Select("<script>"
            + "SELECT * FROM ${tableName}"
            + "<where>"
            + "<foreach collection='conditions' item='condition' separator='AND'>"
            + "${condition.sql}"
            + "</foreach>"
            + "</where>"
            + "</script>")
    List<Object> selectByTableNameAndConditions(@Param("tableName") String tableName,
                                                @Param("conditions") List<QueryCondition> conditions);
}