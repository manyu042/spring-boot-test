package com.abcode.test.oracletest.query.define.commonquery;

import java.util.List;
import java.util.Map;

public interface CommonQueryService {
    /**
     * 查询数据
     *
     * @param tableName          表名
     * @param queryFields        查询字段
     * @param queryConditions    查询条件
     * @param pageNum            当前页码
     * @param pageSize           每页显示的记录数
     * @param orderByField       排序字段
     * @param isAsc              是否升序
     * @return 查询结果
     */
    List<Map<String, Object>> query(String tableName, List<String> queryFields, List<QueryCondition> queryConditions,
                                    int pageNum, int pageSize, String orderByField, boolean isAsc);
}