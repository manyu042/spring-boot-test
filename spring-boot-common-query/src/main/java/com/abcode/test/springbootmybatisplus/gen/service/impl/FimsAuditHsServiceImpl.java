package com.abcode.test.oracletest.gen.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.abcode.test.oracletest.gen.mapper.FimsAuditHsMapper;
import com.abcode.test.oracletest.gen.entity.FimsAuditHs;
import com.abcode.test.oracletest.gen.service.FimsAuditHsService;
import org.springframework.stereotype.Service;

/**
 * 审计信息(FimsAuditHs)表服务实现类
 *
 * @author qinzhitao
 * @since 2023-05-05 10:59:09
 */
@Service
public class FimsAuditHsServiceImpl extends ServiceImpl<FimsAuditHsMapper, FimsAuditHs> implements FimsAuditHsService {

}

